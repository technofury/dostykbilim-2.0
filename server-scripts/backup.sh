#!/usr/bin/env bash

DATE=`date +%Y-%m-%d:%H:%M:%S`
APP_NAME=dostykbilim2
DB=dostykbilim2
echo dumping database...
mkdir -p /home/rsa/backup
pg_dump -i -h localhost -p 5432 -U technovision -F c -b -v -f "/home/rsa/backup/$APP_NAME $DATE.backup" $DB
