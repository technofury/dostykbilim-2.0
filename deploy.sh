#!/usr/bin/env bash

REMOTE=rsa@195.93.152.92
REMOTE_APP=/home/rsa/dostykbilim2/
DATE=`date +%Y-%m-%d:%H:%M:%S`
echo Building project...;
sbt stage || exit 1;
echo Dumping database...;
ssh $REMOTE "cd $REMOTE_APP; ./stop.sh; ./backup.sh;";
echo Syncronising project with server...;
rsync -va target/universal/stage/bin/ $REMOTE:$REMOTE_APP/bin/;
rsync -va target/universal/stage/lib/ $REMOTE:$REMOTE_APP/lib/;
rsync -va target/universal/stage/conf/ $REMOTE:$REMOTE_APP/conf/;
echo Starting server...;
#ssh $REMOTE "cd $REMOTE_APP; ./start.sh";