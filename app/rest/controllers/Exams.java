package rest.controllers;

import be.objectify.deadbolt.java.actions.Group;
import be.objectify.deadbolt.java.actions.Restrict;
import com.google.gson.Gson;
import models.entities.examination.Answer;
import models.entities.examination.Exam;
import models.entities.examination.LessonAnswer;
import models.entities.user.AuthorisedUser;
import models.logic.education.RegisterLogic;
import models.logic.examination.*;
import models.rest.User;
import play.data.DynamicForm;
import play.mvc.Controller;
import play.mvc.Result;
import rest.gson_classes.*;

import java.util.ArrayList;
import java.util.List;

import static play.data.Form.form;

/**
 * Written by RSA on 21-May-15.
 */
public class Exams extends Controller {
    @Restrict({ @Group("admin"), @Group("moderator"), @Group("student") })
    public static Result check(long uid){
        try{
            return ok(new Gson().toJson(getPage(AuthorisedUser.getUserById(uid))));
        }catch (Exception e){
            e.printStackTrace();
            return ok("exception");
        }
    }
    public static Result results(){
        try {
            DynamicForm df = form().bindFromRequest();
            long uid = Long.parseLong(df.get("uid"));
            String token = df.get("token");
            User current = Users.checkUser(uid, token);
            if (current != null) {
                ExamResultPage page = getPage(current.user);
                Gson gson = new Gson();
                return ok(gson.toJson(page));
            } else
                return ok("relogin");
        }catch (Exception e){
            e.printStackTrace();
            return ok("exception");
        }
    }
    public static ExamResultPage getPage(AuthorisedUser user) throws Exception{
        List<Exam> exams = ExamLogic.getByGradeSorted(user.profile.course.classNumber);
        List<models.entities.examination.ExamResult> exam_results = ExamResultLogic.getByUser(user);
        int k = 0;
        for (int i = 1; i < exams.size(); i++)
            if (!exams.get(i).examType.equals(exams.get(i - 1).examType)) {
                k = i;
                break;
            }
        ExamResultPage page = new ExamResultPage();
        page.userId = user.id;
        page.missed = RegisterLogic.getCount(user, "notPresent");
        page.divider = k;
        if(exams.size()>0)
            page.grade = exams.get(0).grade;
        page.examResults = new ArrayList<>();
        LessonAnswer lessonAnswer; Answer answer;
        ExamResult eTemp; LessonResult lTemp; QuestionResult qTemp;

        for(models.entities.examination.ExamResult eResult: exam_results){

            eTemp = new ExamResult();
            eTemp.examId = eResult.id;
            eTemp.examName = eResult.exam.name;
            eTemp.kzRating = eResult.rankKZ;
            eTemp.dostykRating = eResult.rankDostyk;
            eTemp.groupRating = eResult.rankGroup;
            eTemp.variant = eResult.variant;
            eTemp.lessonResults = new ArrayList<>();
            for(models.entities.examination.DetailedResult dResult: DetailedResultLogic.getByExamResult(eResult)){
                lessonAnswer = LessonAnswerLogic.getBy(eTemp.variant, dResult.lesson);

                lTemp = new LessonResult();
                lTemp.examId = dResult.examResult.id;
                lTemp.lessonId = dResult.lesson.id;
                lTemp.lessonCode = dResult.lesson.code;
                lTemp.lessonName = dResult.lesson.name;
                lTemp.totalResult = dResult.correct;
                lTemp.questionResults = new ArrayList<>();
                for(int i=1;i<=dResult.studentAnswers.length();i++){
                    answer = Answer.getBy(lessonAnswer, i);
                    try{
                    qTemp = new QuestionResult();                    
                    qTemp.sequenceNumber = i;
                    qTemp.lessonId = lessonAnswer.lesson.id;
                    qTemp.questionId = answer.id;
                    qTemp.videoLink = answer.pathToVideo;
                    qTemp.topicName = answer.topic;
                    qTemp.studentAnswer = dResult.studentAnswers.charAt(i-1)+"";
                    qTemp.correctAnswer = answer.answerCode;
                    
                    lTemp.questionResults.add(qTemp);
                    }catch(Exception e){}
                }
                eTemp.lessonResults.add(lTemp);
            }
            page.examResults.add(eTemp);
        }
        return page;
    }
    
    @Restrict({ @Group("admin"), @Group("moderator"), @Group("student") })
    public static Result getAllVideos(){
        try{
            List<VideoSolution> results = fetchAllVideos();
            return ok(new Gson().toJson(results));
        }catch(Exception e){
            e.printStackTrace();
            return ok("exception");
        }
    }
    public static Result postAllVideos(){
        try {
            DynamicForm df = form().bindFromRequest();
            long uid = Long.parseLong(df.get("uid"));
            String token = df.get("token");
            User current = Users.checkUser(uid, token);
            if (current != null) {
                List<VideoSolution> results = fetchAllVideos();
                return ok(new Gson().toJson(results));
            } else
                return ok("relogin");
        }catch (Exception e){
            e.printStackTrace();
            return ok("exception");
        }
    }
    public static List<VideoSolution> fetchAllVideos(){
        List<Answer> answers = Answer.getAllWithVideo();
        List<VideoSolution> results = new ArrayList<>();
        VideoSolution vs;
        for(Answer answer: answers){
            vs = new VideoSolution();
            vs.lessonId = answer.lessonAnswer.lesson.id;
            vs.lessonName = answer.lessonAnswer.lesson.name;
            vs.topicName = answer.topic;
            vs.questionId = answer.id;
            vs.questionNumber = answer.questionNumber;
            vs.correctAnswer = answer.answerCode;
            vs.videoLink = answer.pathToVideo;
            results.add(vs);
        }
        return results;
    }
}