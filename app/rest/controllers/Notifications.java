package rest.controllers;

import be.objectify.deadbolt.java.actions.Group;
import be.objectify.deadbolt.java.actions.Restrict;
import com.avaje.ebean.Ebean;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.Gson;
import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsService;
import controllers.Application;
import controllers.Assets;
import models.entities.user.AuthorisedUser;
import models.entities.user.SecurityRole;
import models.rest.Notification;
import models.rest.User;
import play.data.DynamicForm;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import rest.gson_classes.NotificationJSON;
import scala.util.parsing.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static play.data.Form.form;

/**
 * Created by arsen on 17/01/2016.
 */
public class Notifications extends Controller {
    //gcm
    private static final String googleApiKey = "AIzaSyBkfu1t7CJqeI-xB36W8D1_S623yEOAMIA";
    private static final String gcm_url = "https://android.googleapis.com/gcm/send";
    //ios
    private static final String passphrase = "";

    public static Result getNotification(){

        try{
            DynamicForm df = form().bindFromRequest();
            long uid = Long.parseLong(df.get("uid"));
            String token = df.get("token");

            User current = Users.checkUser(uid, token);
            if(current!=null){

                AuthorisedUser user = AuthorisedUser.getUserById(uid);
                return ok(new Gson().toJson(getMessage(user)));

            }else
                return ok("relogin");
        }catch(Exception e){
            e.printStackTrace();
            return ok("exception");
        }
    }

    public static Result saveNotification(){

        try{
            DynamicForm df = form().bindFromRequest();
            long uid = Long.parseLong(df.get("uid"));
            String token = df.get("token");
            String message = df.get("message");
            String users = df.get("users");
            String date = "";
            if(df.get("date")!=null)
                date = df.get("date");
            DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            User current = Users.checkUser(uid, token);
            if(current!=null){
                AuthorisedUser user = AuthorisedUser.getUserById(uid);
                Notification n; List<Notification> temp;
                String result; ObjectNode gen = Json.newObject(); ObjectNode nodes;
                boolean already;
                if(user.roles.contains(SecurityRole.getByName("teacher"))) {
                    for(String sid : users.split("_")) {
                        already=false;
                        AuthorisedUser u = AuthorisedUser.find.byId(Long.parseLong(sid));
                        temp = Notification.getByInfo(format.parse(date),user,u,message);
                        if(temp.size()>=1) {
                            n = Notification.getByInfo(format.parse(date), user, u, message).get(0);
                            already=true;
                        }else
                            n = new Notification();
                        result = saveNewNotification(n,user,u,message,date,true,already);
                        nodes = Json.newObject();
                        nodes.put("sending-result",result);
                        gen.put("for-user-with-id_"+u.id,nodes);
                    }
                    already = false;
                    temp = Notification.getByInfo(format.parse(date),user,user,message);
                    if(temp.size()>=1) {
                        n = Notification.getByInfo(format.parse(date), user, user, message).get(0);
                        already=true;
                    }else
                        n = new Notification();
                    //send message
                    result = saveNewNotification(n,user,user,message,date,false,already);
                    nodes = Json.newObject();
                    nodes.put("sending-result",result);
                    gen.put("for-user-with-id_"+user.id,nodes);
                    return ok(gen);
                }else{
                    return ok("access denied");
                }
            }else
                return ok("relogin");
        }catch(Exception e){
            e.printStackTrace();
            return ok("exception");
        }
    }

    private static String saveNewNotification(Notification toSave, AuthorisedUser object, AuthorisedUser subject,
                                            String message, String date, boolean notify,boolean alreadySaved) throws Exception{
        //sending push notification
        String result;
        if(User.getByUser(object)!=null) {
            User user = User.getByUser(subject);
            String response="failure in sending";
            if(user.osType!=null&&user.deviceId!=null){
                if (user.osType.equals("android") && notify == true) {
                    response = sendMessageByGCM(message, user.deviceId);
                    if (response.contains("\"success\":1"))
                        response = "success in sending";
                    else
                        response = "failure in sending";
                } else if (user.osType.equals("ios") && notify == true) {
                    response = sendMessageByAPNS(message, user.deviceId)+" in sending";
                }
            }else
                response = "don't have mobile app";
            if(notify==false)
                response = "no need to send";
                DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm");
                toSave.teacher = object;
                toSave.user = subject;
                toSave.message = message;
                if (!date.isEmpty())
                    toSave.date = format.parse(date);
                toSave.save();
            if(alreadySaved==false)
                result=response+", saved to database";
            else
                result=response+", already saved to database";
        }else
            result = "subject not found";
        return result;
    }
    private static String sendMessageByGCM (String message, String regId){

        String response = post(googleApiKey,message,regId);

        return response;
    }
    public static String post(String apiKey, String message,String regId){
        try{
            URL url = new URL(gcm_url);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Authorization", "key="+apiKey);
            conn.setDoOutput(true);
            conn.setDoInput(true);
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(conn.getOutputStream()));
            bw.write(
              "{"+
                "\"to\": \""+regId+"\","+
                "\"data\": {"+
                  "\"message\": \""+message+"\","+
                "}"+
              "}"
            );
            bw.flush();
            bw.close();


            if(conn.getInputStream()!=null) {
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(conn.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                return response.toString();
            }
            return "zero";
        } catch (Exception e) {
            e.printStackTrace();
            return "exception";
        }
    }
    private static String sendMessageByAPNS(String message, String deviceToken){

        ApnsService service =
                APNS.newService()
                        .withCert(Application.getRootPath()+"/public/certificates/ck.p12", passphrase)
                        .withProductionDestination()
                        //.withSandboxDestination() //comment this when ios app ready for distribution
                        .build();
        String payload = APNS.newPayload()
                .alertBody(message)
                .sound("default")
                .actionKey("Play").build();
        service.push(deviceToken, payload);
        Map<String, Date> inactiveDevices = service.getInactiveDevices();
        for (String token : inactiveDevices.keySet()) {
            //Date inactiveAsOf = inactiveDevices.get(token);
            if(token.equals(deviceToken))
                return "failure";
        }
        return "success";
    }

    public static List<NotificationJSON> getMessage(AuthorisedUser user) throws Exception{

        List <NotificationJSON> temp = new ArrayList<>();
        List<Notification> t;
        List<Notification> t2 = new ArrayList<>();
        if(user.roles.contains(SecurityRole.getByName("teacher"))) {
            t = Notification.find.where().eq("teacher", user).order("id").findList();
        }else
            t = Notification.find.where().eq("user", user).order("id").findList();
        if(user.roles.contains(SecurityRole.getByName("student"))) {
            for (Notification n : t) {

                NotificationJSON nj = new NotificationJSON();
                nj.tid = n.teacher.id;
                nj.uid = n.user.id;
                nj.message = n.message;
                nj.date = n.date;
                nj.id = n.id;
                temp.add(nj);
            }
        }else{
            String message="";
            Date date = new Date();
            for(Notification k: t){
                if(!message.equals(k.message)&&!date.equals(k.date)) {
                    message = k.message;
                    date = k.date;
                    t2.add(k);
                }
            }
            for (Notification n : t2) {

                NotificationJSON nj = new NotificationJSON();
                nj.tid = n.teacher.id;
                nj.uid = n.user.id;
                nj.message = n.message;
                nj.date = n.date;
                nj.id = n.id;
                temp.add(nj);
            }
        }
        return temp;
    }

    public static void clearRepeatedNotifications(){
        List<Notification> temp1 = Notification.find.all();
        List<Notification> temp2;
        int count=0;
        for(Notification temp: temp1){
            temp2 = Notification.getByInfo(temp.date,temp.teacher,temp.user,temp.message);
            if(temp2.size()>1)
                for(int i=0;i<temp2.size()-1;i++) {
                    temp2.get(i).delete();
                    count++;
                }
        }

    }
}
