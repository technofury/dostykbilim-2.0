package rest.controllers;

import be.objectify.deadbolt.java.actions.Group;
import be.objectify.deadbolt.java.actions.Restrict;
import com.google.gson.Gson;
import models.entities.social.DostykGroup;
import models.entities.user.AuthorisedUser;
import models.logic.education.RegisterLogic;
import models.logic.social.DostykGroupLogic;
import models.logic.user.AuthorisedUserLogic;
import models.rest.User;
import play.data.DynamicForm;
import play.mvc.Controller;
import play.mvc.Result;
import rest.gson_classes.*;
import models.timetable.Timetable;
import java.util.ArrayList;
import java.util.List;

import static play.data.Form.form;

/**
 * Written by Arsen on 03/08/2015.
 */
public class Groups extends Controller {

    @Restrict({ @Group("admin"), @Group("moderator"), @Group("teacher"), @Group("student") })
    public static Result checkGroup(){
        try{
            AuthorisedUser user = AuthorisedUser.getUserByEmail(session("connected"));
            List<GroupInfo> groups = getGroupInfo(user);
            if(groups!=null)
                return ok(new Gson().toJson(groups));
            else
                return ok("zero");
        }catch (Exception e){
            e.printStackTrace();
            return ok("exception");
        }
    }
    public static Result group() {
        try{
            DynamicForm df = form().bindFromRequest();
            long uid = Long.parseLong(df.get("uid"));
            String token = df.get("token");
            User current = Users.checkUser(uid, token);
            if(current!=null){
                AuthorisedUser user = AuthorisedUser.getUserById(uid);
                List<GroupInfo> groups = getGroupInfo(user);
                if(groups!=null)
                    return ok(new Gson().toJson(groups));
                else
                    return ok("zero");
            }else
                return ok("relogin");
        }catch(Exception e){
            e.printStackTrace();
            return ok("exception");
        }
    }
    protected static List<GroupInfo> getGroupInfo(AuthorisedUser user) throws Exception{
        List<DostykGroup> groups = DostykGroupLogic.getAllWithUser(user);
        return getGroups(groups, user);
    }
    protected static List<GroupInfo> getGroups(List<DostykGroup> groups, AuthorisedUser user) throws Exception{
        List<GroupInfo> result = new ArrayList<>();
        for(DostykGroup group: groups)
            result.add(getGroup(group, user));
        return result;
    }
    protected static GroupInfo getGroup(DostykGroup group, AuthorisedUser user) throws Exception{
        GroupInfo temp = new GroupInfo();
        temp.id = group.id;
        temp.name = group.name;
        temp.curator = Users.getUser(group.curator);
        List<AuthorisedUser> students = AuthorisedUserLogic.getByGroup(group);
        temp.students = Users.getUsers(students);
        /*if(user.roles.get(0).name.equals("teacher")&&Users.isCurator(user)==false)
            temp.schedules = Schedules.getSchedules(Timetable.byGrade(user));
        else
            temp.schedules = Schedules.getSchedules(Timetable.byGrade(group));*/
        if(user.roles.get(0).name.equals("teacher")&&Users.isCurator(user)==false)
            temp.attendances = Schedules.getAttendances(RegisterLogic.find.where().in("teacher", user).findList());
        else if(user.roles.get(0).name.equals("teacher")&&Users.isCurator(user)==true)
            temp.attendances = Schedules.getAttendances(RegisterLogic.find.where().in("student", students).findList());
        else
            temp.attendances = Schedules.getAttendances(RegisterLogic.find.where()
                    .eq("timetable.dostykGroup", group).findList());
        return temp;
    }
    @Restrict({ @Group("admin"), @Group("moderator"), @Group("teacher"), @Group("student") })
    public static Result checkSingle(long groupId){
        try{
            AuthorisedUser user = AuthorisedUser.getUserByEmail(session("connected"));
            DostykGroup group = DostykGroupLogic.getById(groupId);
            boolean tf = validateSingle(group, user);
            if(tf==true)
                return ok(new Gson().toJson(getGroup(group,user)));
            else
                return ok("permission denied");
        }catch (Exception e){
            e.printStackTrace();
            return ok("exception");
        }
    }
    public static Result single(){
        try{
            DynamicForm df = form().bindFromRequest();
            long uid = Long.parseLong(df.get("uid"));
            long gid = Long.parseLong(df.get("gid"));
            String token = df.get("token");
            User current = Users.checkUser(uid, token);
            if(current!=null){
                AuthorisedUser user = AuthorisedUser.getUserById(uid);
                DostykGroup group;
                if(DostykGroupLogic.getById(gid)!=null){
                    group = DostykGroupLogic.getById(gid);
                    boolean tf = validateSingle(group, user);
                    if(tf==true)
                        return ok(new Gson().toJson(getGroup(group,user)));
                    else
                        return ok("permission denied");
                }else
                    return ok("no such group");
            }else
                return ok("relogin");
        }catch(Exception e){
            e.printStackTrace();
            return ok("exception");
        }
    }
    protected static boolean validateSingle(DostykGroup group, AuthorisedUser user){
        boolean result = true;
        if(user.roles.get(0).name.equals("student"))
            if(user.profile.dostykGroup.id!=group.id)
                result = false;
        else if(user.roles.get(0).name.equals("teacher")){
            if(group.curator.id==user.id) {
            }/*else  if(Timetable.find.where().eq("dostykGroup",group).eq("teacher", user).findRowCount()>0) {
            }else
                result = false;*/
        }else if(user.roles.get(0).name.equals("moderator")){
            if(group.dostyk.id!=user.dostyks.get(0).id)
                result = false;
        }else if(user.roles.get(0).name.equals("admin")){

        }else{
                result = false;
        }
        return result;
    }

    public static Result getCuratorGroups(){

        try{
            DynamicForm df = form().bindFromRequest();
            long uid = Long.parseLong(df.get("uid"));
            String token = df.get("token");
            User current = Users.checkUser(uid, token);
            if(current!=null){

                AuthorisedUser user = AuthorisedUser.getUserById(uid);
                List<DostykGroup> groups = DostykGroupLogic.find.where().eq("curator",user).findList();

                return ok(new Gson().toJson(getCuratorGroup(groups)));

            }else
                return ok("relogin");
        }catch(Exception e){
            e.printStackTrace();
            return ok("exception");
        }
    }

    protected static List<CuratorGroup> getCuratorGroup(List<DostykGroup> groups) throws Exception{
        List <CuratorGroup> temp = new ArrayList<>();
        List<AuthorisedUser> t;
        CuratorGroup c;
        for(DostykGroup d : groups) {
            t = AuthorisedUser.getByDostykGroup(d);
            c = new CuratorGroup();
            c.students = getUsers(t);
            c.name = d.name;
            c.id = d.id;
            temp.add(c);
        }
        return temp;
    }

    protected static List<UserInfo> getUsers(List<AuthorisedUser> users) throws Exception{
        List<UserInfo> result = new ArrayList<>();
        for(AuthorisedUser user: users)
            result.add(getUser(user));
        return result;
    }
    protected static UserInfo getUser(AuthorisedUser user) throws Exception{
        UserInfo userInfo = new UserInfo();
        userInfo.uid = user.id;
        userInfo.firstName = user.profile.firstName;
        userInfo.lastName = user.profile.lastName;
        userInfo.role = user.roles.get(0).name;
        userInfo.telephone = user.profile.phoneNumber;
        if(User.getByUser(user)!=null){
            userInfo.token = User.getByUser(user).token;
            userInfo.deviceId = User.getByUser(user).deviceId;
        }
        return userInfo;
    }
}