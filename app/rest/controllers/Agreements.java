package rest.controllers;

import be.objectify.deadbolt.java.actions.Group;
import be.objectify.deadbolt.java.actions.Restrict;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import models.entities.accounting.Payment;
import models.entities.accounting.PaymentPeriod;
import models.entities.user.AuthorisedUser;
import models.logic.accounting.PaymentLogic;
import models.logic.accounting.PaymentPeriodLogic;
import models.rest.User;
import play.data.DynamicForm;
import play.mvc.Result;
import play.mvc.Controller;
import rest.gson_classes.Agreement;
import rest.gson_classes.Period;

import java.util.ArrayList;

import static play.data.Form.form;

/**
 * Written by RSA on 12-May-15.
 */
public class Agreements extends Controller {
    @Restrict({ @Group("admin"), @Group("moderator"), @Group("student") })
    public static Result check(long uid){
        try{
            Agreement agreement = getAgreement(AuthorisedUser.getUserById(uid));
            if(agreement==null)
                return ok("zero");
            Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy").create();
            return ok(gson.toJson(agreement));
        }catch (Exception e){
            e.printStackTrace();
            return ok("exception");
        }
    }
    public static Result get(){
        try {
            DynamicForm df = form().bindFromRequest();
            long uid = Long.parseLong(df.get("uid"));
            String token = df.get("token");
            User current = Users.checkUser(uid, token);
            if (current != null) {
                AuthorisedUser user = AuthorisedUser.getUserById(uid);
                if(PaymentLogic.getActualPayment(user)!=null){
                    Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy").create();
                    return ok(gson.toJson(getAgreement(user)));
                }else
                    return ok("zero");
            } else {
                return ok("relogin");
            }
        }catch(Exception e){
            e.printStackTrace();
            return ok("exception");
        }
    }
    public static Agreement getAgreement(AuthorisedUser user) throws Exception{
        Payment payment = PaymentLogic.getActualPayment(user);
        if(payment==null)
            return null;
        payment.periods = PaymentPeriodLogic.getPeriods(payment);
        Agreement agreement = new Agreement();
        agreement.id = payment.id;
        agreement.uid = user.id;
        agreement.startDate = payment.startDate;
        agreement.endDate = payment.endDate;
        agreement.discount = payment.discount;
        agreement.plannedPayment = (int) payment.plannedPayment;
        agreement.payedPayment = (int) payment.payedPayment;
        agreement.debtPayment = (int) payment.debtPayment;
        agreement.periods = new ArrayList<>();
        agreement.comment = payment.comment;
        Period temp;
        for (PaymentPeriod period : payment.periods) {
            temp = new Period();
            temp.id = period.id;
            temp.agreementId = agreement.id;
            temp.plannedPaymentDate = period.plannedPaymentDate;
            temp.plannedPayment = (int) period.plannedPayment;
            temp.payedPayment = (int) period.payedPayment;
            temp.debtPayment = (int) period.debtPayment;
            temp.payedPaymentDate = period.payedPaymentDate;
            agreement.periods.add(temp);
            temp.comment = period.comment;
            temp.status = period.status;
        }
        return agreement;
    }
}
