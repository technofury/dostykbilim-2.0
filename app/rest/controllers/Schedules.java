package rest.controllers;

import be.objectify.deadbolt.java.actions.Group;
import be.objectify.deadbolt.java.actions.Restrict;
import com.google.gson.Gson;
import models.entities.accounting.Payment;
import models.entities.education.Register;
import models.entities.social.DostykGroup;
import models.entities.user.SecurityRole;
import models.logic.accounting.PaymentLogic;
import models.logic.education.LessonLogic;
import models.logic.education.RegisterLogic;
import rest.gson_classes.Attendance;
import rest.gson_classes.Schedule;
import models.entities.user.AuthorisedUser;
import models.rest.User;
import play.data.DynamicForm;
import play.mvc.Controller;
import play.mvc.Result;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import models.timetable.Timetable;
import static play.data.Form.form;

/**
 * Written by RSA on 23-Apr-15.
 */
public class Schedules extends Controller {
    private static DateFormat format = new SimpleDateFormat("HH:mm");
    //@Restrict({ @Group("admin"), @Group("moderator"), @Group("teacher"), @Group("student") })
    public static Result checkSchedule(long uid){
        try{
            List<Schedule> list = getScheduleByUser(AuthorisedUser.getUserById(uid));
            if(list.size()>0)
                return ok(new Gson().toJson(list));
            else
                return ok("zero");
        }catch (Exception e){
            e.printStackTrace();
            return ok("exception");
        }
    }
    public static Result schedule() {
        try{
            DynamicForm df = form().bindFromRequest();
            long uid = Long.parseLong(df.get("uid"));
            String token = df.get("token");
            User current = Users.checkUser(uid, token);
            if(current!=null){
                AuthorisedUser user = AuthorisedUser.getUserById(uid);
                List<Schedule> journals = getScheduleByUser(user);
                if(journals.size()>0)
                    return ok(new Gson().toJson(journals));
                else
                    return ok("zero");
            }else {
                   return ok("relogin");
            }
        }catch(Exception e){
            e.printStackTrace();
            return ok("exception");
        }
    }
    protected static List<Schedule> getScheduleByUser(AuthorisedUser user) throws Exception{
        List<Timetable> list;
        List<Schedule> journals = new ArrayList<>();
        /*if (user.roles.get(0).name.equals("student"))
            list = TimetableLogic.getByGroup(user.profile.dostykGroup);
        else
            list = TimetableLogic.getByTeacher(user);
        String teacherName="", lessonName="", lessonId, teacherId="";// electiveId="";
        DateFormat format = new SimpleDateFormat("HH:mm");
        Schedule journal;
        int count=0;
        for(Timetable timetable: list){
            if(timetable.teacher==null) {
                if (user.profile.electiveGroup != null) {
                    if (timetable.electiveGroup.id == user.profile.electiveGroup.id) {
                        teacherName = timetable.electiveGroup.curator.profile.lastName + "." +
                                timetable.electiveGroup.curator.profile.firstName.toUpperCase().charAt(0);
                        lessonName = timetable.electiveGroup.name;
                        teacherId = "-1";
                        lessonId = "-1";
                    } else
                        lessonId = "ket";
                }else
                    lessonId = "ket";
            }else{
                teacherName = timetable.teacher.profile.lastName+"."+
                        timetable.teacher.profile.firstName.toUpperCase().charAt(0)+".";
                lessonName = timetable.lesson.name;
                teacherId = timetable.teacher.id+"";
                lessonId = timetable.lesson.id+"";
                //electiveId = "-1";
            }
            if(lessonId != "ket"){
                count++;
                journal = new Schedule();
                journal.id = timetable.id;
                journal.uid = user.id;
                journal.row = timetable.row+"";
                journal.day = timetable.day;
                journal.teacherId = Long.parseLong(teacherId);
                journal.teacherName = teacherName;
                journal.lessonId = Long.parseLong(lessonId);
                journal.lessonName = lessonName;
                journal.cabinet = timetable.cabinet;
                journal.startTime = format.format(timetable.startTime);
                journal.endTime = format.format(timetable.endTime);
                journal.groupId = timetable.dostykGroup.id;
                journal.groupName = timetable.dostykGroup.name;
                journals.add(journal);
            }
        }*/
        return journals;
    }
    protected static List<Schedule> getScheduleByGroup(DostykGroup group, AuthorisedUser user) throws Exception{
        List<Timetable> list;
        List<Schedule> journals = new ArrayList<>();
        /*list = TimetableLogic.getByGroup(group);
        String teacherName="", lessonName="", lessonId, teacherId="";// electiveId="";
        Schedule journal;
        int count=0;
        for(Timetable timetable: list){
            if(timetable.teacher==null) {
                if (group != null) {
                    if (timetable.electiveGroup.id == user.profile.electiveGroup.id) {
                        teacherName = timetable.electiveGroup.curator.profile.lastName + "." +
                                timetable.electiveGroup.curator.profile.firstName.toUpperCase().charAt(0);
                        lessonName = timetable.electiveGroup.name;
                        teacherId = "-1";
                        lessonId = "-1";
                    } else
                        lessonId = "ket";
                }else
                    lessonId = "ket";
            }else{
                teacherName = timetable.teacher.profile.lastName+"."+
                        timetable.teacher.profile.firstName.toUpperCase().charAt(0)+".";
                lessonName = timetable.lesson.name;
                teacherId = timetable.teacher.id+"";
                lessonId = timetable.lesson.id+"";
                //electiveId = "-1";
            }
            if(lessonId != "ket"){
                count++;
                journal = new Schedule();
                journal.id = timetable.id;
                journal.uid = user.id;
                journal.row = timetable.row+"";
                journal.day = timetable.day;
                journal.teacherId = Long.parseLong(teacherId);
                journal.teacherName = teacherName;
                journal.lessonId = Long.parseLong(lessonId);
                journal.lessonName = lessonName;
                journal.cabinet = timetable.cabinet;
                journal.startTime = format.format(timetable.startTime);
                journal.endTime = format.format(timetable.endTime);
                journals.add(journal);
            }
        } */

        return journals;
    }
    protected static List<Schedule> getSchedules(List<Timetable> timetables){
        List<Schedule> result = new ArrayList<>();
        for(Timetable timetable: timetables)
            result.add(getSchedule(timetable));
        return result;
    }
    protected static Schedule getSchedule(Timetable timetable){
        Schedule temp = new Schedule();
        temp = new Schedule();
        /*temp.id = timetable.id;
        //temp.uid = user.id;
        temp.row = timetable.row+"";
        temp.day = timetable.day;
        if(timetable.teacher!=null) {
            temp.teacherId = timetable.teacher.id;
            temp.teacherName = timetable.teacher.profile.lastName + "." +
                    timetable.teacher.profile.firstName.toUpperCase().charAt(0) + ".";
        }
        if(timetable.lesson!=null){
            temp.lessonId = timetable.lesson.id;
            temp.lessonName = timetable.lesson.name;
        }
        temp.cabinet = timetable.cabinet;
        temp.startTime = format.format(timetable.startTime);
        temp.endTime = format.format(timetable.endTime);*/
        return temp;
    }
    @Restrict({ @Group("admin"), @Group("moderator"), @Group("teacher"), @Group("student") })
    public static Result checkAttendance(long uid){
        try{
            List<Attendance> list = getAttendance(AuthorisedUser.getUserById(uid));
            if(list.size()>0)
                return ok(new Gson().toJson(list));
            else
                return ok("zero");
        }catch (Exception e){
            e.printStackTrace();
            return ok("exception");
        }
    }
    public static Result attendance(){
        try {
            DynamicForm df = form().bindFromRequest();
            long uid = Long.parseLong(df.get("uid"));
            String token = df.get("token");
            User current = Users.checkUser(uid, token);
            if(current!=null) {
                AuthorisedUser user = AuthorisedUser.getUserById(uid);
                List<Attendance> list = getAttendance(user);
                if(list.size()>0)
                    return ok(new Gson().toJson(list));
                else
                    return ok("zero");
            }else{
                return ok("relogin");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return ok("exception");
        }
    }
    protected static List<Attendance> getAttendance(AuthorisedUser user) throws Exception{
        List<Register> list;
        DateFormat format = new SimpleDateFormat("dd MM yyyy HH:mm");
        DateFormat yy = new SimpleDateFormat("yyyy");
        Payment actual = PaymentLogic.getActualPayment(user);
        Date first, today = new Date(), endDate, last;
        if(actual!=null){
            first = actual.startDate;
            endDate = actual.endDate;
        }else{
            first = format.parse("01 01 "+yy.format(today)+" 00:00");
            endDate = format.parse("30 12 "+yy.format(today)+" 00:00");
        }
        if(today.before(endDate))
            last = today;
        else
            last = endDate;
        list = RegisterLogic.getByDates(user, first, last);
        List<Attendance> attendances = new ArrayList<>();
        Attendance attendance;
        int count=0;
        for(Register register: list){
            count++;
            attendance = new Attendance();
            if(register.timetable!=null)
                attendance.timetableId = register.timetable.id;
            if(user.roles.get(0).name.equals("student"))
                attendance.uid = user.id;
            else
                attendance.uid = register.student.id;
            attendance.status = register.attendance;
            attendance.date = register.date;
            attendances.add(attendance);
        }

        return attendances;
    }
    protected static List<Attendance> getAttendances(List<Register> inputs) throws Exception{
        List<Attendance> result = new ArrayList<>();
        for(Register input: inputs)
            result.add(getAttendance(input));
        return result;
    }
    protected static Attendance getAttendance(Register input) throws Exception{
        Attendance temp = new Attendance();
        if(input.timetable!=null)
            temp.timetableId = input.timetable.id;
        temp.uid = input.student.id;
        temp.status = input.attendance;
        temp.date = input.date;
        return temp;
    }
    public static Result saveAttendance(){
        try{
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            DynamicForm df = form().bindFromRequest();

            long uid = Long.parseLong(df.get("uid"));
            String token = df.get("token");
            User current = Users.checkUser(uid, token);
            if(current!=null){
                long teacherId = Long.parseLong(df.get("teacher-id"));
                //long studentId = Long.parseLong(df.get("student-id"));
                long lessonId = Long.parseLong(df.get("lesson-id"));
                long timetableId = Long.parseLong(df.get("timetable-id"));
                String kun = df.get("date");
                String list = df.get("list");
              //  String validate = validateAttendance(teacherId, studentId, lessonId, timetableId, kun, status);

                Date date = format.parse(kun);


                AuthorisedUser teacher = AuthorisedUser.getUserById(teacherId);
                Timetable timetable = Timetable.byId(timetableId);


                for (String s : list.split("_")) {

                    if(s.length()>2) {
                        String user_id = s.split(":")[0];
                        String status = s.split(":")[1];
                        AuthorisedUser student = AuthorisedUser.getUserById(Long.parseLong(user_id));
                        String validation = validateAttendance(teacherId,student.id,lessonId,timetableId,kun,status);
                        if(validation.equals("ok"))
                            registerAttendance(lessonId, student, teacher, timetable, date, status);
                        else
                            return ok(validation);
                    }
                }

                return ok("success");

            }else
                return ok("relogin");
        }catch (Exception e){
            e.printStackTrace();
            return ok("exception");
        }
    }
    private static String validateAttendance(long teacherId, long studentId, long lessonId, long timetableId, 
                                             String kun, String status){
        List<SecurityRole> teachers = new ArrayList<>();
        teachers.add(SecurityRole.getByName("teacher"));
        if(AuthorisedUser.find.where().eq("id", teacherId).in("roles", teachers).findRowCount()!=1)
            return "no such teacher";
        List<SecurityRole> students = new ArrayList<>();
        students.add(SecurityRole.getByName("student"));
        if(AuthorisedUser.find.where().eq("id", studentId).in("roles", students).findRowCount()!=1)
            return "no such student";
        if(LessonLogic.find.byId(lessonId)==null)
            return "no such lesson";
        if(Timetable.byId(timetableId)==null)
            return "no such timetable";
        if(!kun.contains("-"))
            return "date does not match pattern";
        if(!status.equals("present")&&!status.equals("notPresent")&&!status.equals("late"))
            return "status does not match standard";
        return "ok";
    }
    private static void registerAttendance (long lessonId, AuthorisedUser student, AuthorisedUser teacher,
                                           Timetable timetable, Date date, String status) throws Exception{
        Register register;
        if(RegisterLogic.getByFields(student,timetable,date)!=null)
            register = RegisterLogic.getByFields(student,timetable,date);
        else
            register = new Register();
        register.lessonid = lessonId;
        register.student = student;
        register.teacher = teacher;
        register.timetable = timetable;
        register.date = date;
        register.attendance = status;
        register.save();
    }
}