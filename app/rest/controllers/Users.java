package rest.controllers;

import com.avaje.ebean.Ebean;
import com.google.gson.Gson;
import controllers.utils.Utils;
import models.entities.social.DostykGroup;
import models.entities.user.AuthorisedUser;
import models.logic.social.DostykGroupLogic;
import models.rest.User;
import play.data.DynamicForm;
import play.libs.Crypto;
import play.mvc.Controller;
import play.mvc.Result;
import rest.gson_classes.AllInOne;
import rest.gson_classes.UserInfo;

import java.util.ArrayList;
import java.util.List;

import static play.data.Form.form;

/**
 * Written by Arsen on 15.04.2015.
 */
public class Users extends Controller {
    public static Result authUser(){
        try{

            DynamicForm df = form().bindFromRequest();
            String login = df.get("email"), password = df.get("password"), deviceId = df.get("device_id");
            String osType = df.get("os_type");
            boolean isId = false, isEmail = false;
            isId = Utils.checkWhetherId(login);
            isEmail = Utils.checkWhetherEmail(login);
            String result = "";
            AuthorisedUser user = null;

            if (isId == true||isEmail == true) {
                password = Crypto.encryptAES(password);
                if(isId==true)
                    user = AuthorisedUser.getUserById(login, password);
                else
                    user = AuthorisedUser.getUser(login, password);
                if (user != null) {
                    if (!user.status.equals("active"))
                        result="inactive";
                    else
                        result="success";
                } else
                    result="incorrect";
            } else
                result="validation_error";
            if(!osType.equals("android")&&!osType.equals("ios")&&!osType.equals("windows"))
                result = "unsupported os";

            if(result.equals("success")){
                User current = User.getByUser(user);
                if(current!=null){
                    if(current.token.equals("-1"))
                        current.token = Crypto.generateSignedToken();
                    current.deviceId = deviceId;
                    current.osType = osType;
                    current.update();
                }else{

                    User toCreate = new User();
                    toCreate.user = user;
                    toCreate.token = Crypto.generateSignedToken();
                    toCreate.deviceId = deviceId;
                    toCreate.osType = osType;
                    toCreate.save();
                }
                Gson gson = new Gson();
                return ok(gson.toJson(getUser(user)));
            }else
                return ok(result);
        }catch (Exception e){
            e.printStackTrace();
            return ok("exception");
        }
    }

    protected static List<UserInfo> getUsers(List<AuthorisedUser> users) throws Exception{
        List<UserInfo> result = new ArrayList<>();
        for(AuthorisedUser user: users)
            result.add(getUser(user));
        return result;
    }
    protected static UserInfo getUser(AuthorisedUser user) throws Exception{
        UserInfo userInfo = new UserInfo();
        userInfo.uid = user.id;
        userInfo.firstName = user.profile.firstName;
        userInfo.lastName = user.profile.lastName;
        userInfo.role = user.roles.get(0).name;
        userInfo.telephone = user.profile.phoneNumber;
        if(User.getByUser(user)!=null){
            userInfo.token = User.getByUser(user).token;
            userInfo.deviceId = User.getByUser(user).deviceId;
        }
        return userInfo;
    }
    public static User checkUser(long uid, String token){
        AuthorisedUser user;

        if(AuthorisedUser.getUserById(uid)!=null){

            user = AuthorisedUser.getUserById(uid);
            User current;
            if(User.getByUser(user)!=null){

                current = User.getByUser(user);
                if(!current.token.equals("-1")&&Crypto.compareSignedTokens(current.token, token)==true) {

                    return current;
                }

            }
        }

        return null;
    }
    public static Result today(){
        try {
            DynamicForm df = form().bindFromRequest();
            long uid = Long.parseLong(df.get("uid"));
            String token = df.get("token");
            User user = checkUser(uid, token);

            if (user != null&&user.deviceId!=null) {
                return ok("today");
            }else
                return ok("relogin");
        }catch(Exception e){
            e.printStackTrace();
            return ok("exception");
        }
    }
    public static Result allInOne(){
        Gson gson = new Gson();
        DynamicForm df = form().bindFromRequest();
        long uid = Long.parseLong(df.get("uid"));
        String token = df.get("token");
        String role = df.get("role");
        boolean includeSchedules = Boolean.parseBoolean(df.get("schedule"));
        boolean includeAttendances = Boolean.parseBoolean(df.get("attendance"));
        boolean includePayments = Boolean.parseBoolean(df.get("payment"));
        boolean includeExamResults = Boolean.parseBoolean(df.get("exam"));
        boolean includeVideos = Boolean.parseBoolean(df.get("video"));
        boolean includeGroups = Boolean.parseBoolean(df.get("groups"));
        boolean includeNotifications = Boolean.parseBoolean(df.get("notifications"));
        AllInOne allInOne = new AllInOne();
        allInOne.userId = uid;
        User restUser = checkUser(uid,token);
        AuthorisedUser user = AuthorisedUser.getUserById(restUser.user.id);
        List<DostykGroup> groups = DostykGroupLogic.find.where().eq("curator",user).findList();
        if(restUser==null||restUser.deviceId==null){
            allInOne.comment = "relogin";
        }else
            allInOne.comment = "ok";
        try{
            if(allInOne.comment.equals("ok")) {
                if (role.equals("student")) {
                    if (includeSchedules == true && Schedules.getScheduleByUser(user) != null)
                        allInOne.schedules = Schedules.getScheduleByUser(user);
                    if (includeAttendances == true && Schedules.getAttendance(user) != null)
                        allInOne.attendances = Schedules.getAttendance(user);
                    if (includePayments == true && Agreements.getAgreement(user) != null)
                        allInOne.agreement = Agreements.getAgreement(user);
                    if (includeExamResults == true && Exams.getPage(user) != null)
                        allInOne.examResultPage = Exams.getPage(user);
                    if (includeVideos == true && Exams.fetchAllVideos() != null)
                        allInOne.videos = Exams.fetchAllVideos();
                    if (includeNotifications == true && Notifications.getMessage(user) != null)
                        allInOne.notifications = Notifications.getMessage(user);
                }else if(role.equals("teacher")){
                    if (includeSchedules == true && Schedules.getScheduleByUser(user) != null)
                        allInOne.schedules = Schedules.getScheduleByUser(user);
                    if (includeAttendances == true && Schedules.getAttendance(user) != null)
                        allInOne.attendances = Schedules.getAttendance(user);
                    if (includeNotifications == true && Notifications.getMessage(user) != null)
                        allInOne.notifications = Notifications.getMessage(user);
                    if (includeGroups == true && groups!=null)
                        allInOne.groups = Groups.getCuratorGroup(groups);
                }
            }
        }catch(Exception e){
            e.printStackTrace();
            allInOne.comment="exception";
        }finally {
            return ok(gson.toJson(allInOne));
        }
    }
    public static Result saveEmail(){
        try {
            DynamicForm df = form().bindFromRequest();
            long uid = Long.parseLong(df.get("uid"));
            String token = df.get("token");
            User user = checkUser(uid, token);
            if (user != null) {
                AuthorisedUser auser = AuthorisedUser.getUserById(uid);
                String email = df.get("email");
                if(isValid(email,"email")) {
                    auser.email = email;
                    auser.save();
                    return ok("success");
                }else
                    return ok("validation-error");
            }else
                return ok("relogin");
        }catch(Exception e){
            e.printStackTrace();
            return ok("exception");
        }
    }
    public static Result savePassword(){
        try {
            DynamicForm df = form().bindFromRequest();
            long uid = Long.parseLong(df.get("uid"));
            String token = df.get("token");
            User user = checkUser(uid, token);
            if (user != null) {
                AuthorisedUser auser = AuthorisedUser.getUserById(uid);
                String password = df.get("password");
                if(isValid(password,"other")) {
                    auser.password = Crypto.encryptAES(password);
                    auser.save();
                    return ok("success");
                }else
                    return ok("validation-error");
            }else
                return ok("relogin");
        }catch(Exception e){
            e.printStackTrace();
            return ok("exception");
        }
    }
    public static Result saveTelephone(){
        try {
            DynamicForm df = form().bindFromRequest();
            long uid = Long.parseLong(df.get("uid"));
            String token = df.get("token");
            User user = checkUser(uid, token);
            if (user != null) {
                AuthorisedUser auser = AuthorisedUser.getUserById(uid);
                String telephone = df.get("telephone");
                if(isValid(telephone,"other")==true){
                    auser.profile.phoneNumber = telephone;
                    auser.profile.save();
                    auser.save();
                    return ok("success");
                }else
                    return ok("validation-error");
            }else
                return ok("relogin");
        }catch(Exception e){
            e.printStackTrace();
            return ok("exception");
        }
    }
    private static boolean isValid(String value, String special){
        if(value==null)
            return false;
        if(value.isEmpty()==true)
            return false;
        if(value.equals(""))
            return false;
        if(special.equals("email")&&!value.contains("@"))
            return false;
        return true;
    }
    protected static boolean isCurator(AuthorisedUser user){
        if(DostykGroupLogic.getByCurator(user).size()>0)
            return true;
        else
            return false;
    }
    public static void clearAllUsers(){
        int count=0;
        for(User toDelete: User.find.all()) {
            Ebean.delete(toDelete);
            count++;
        }

    }
}