package rest.gson_classes;

import com.google.gson.annotations.SerializedName;
import java.util.List;

/**
 * Written by Arsen on 6/11/2015.
 */
public class ExamResult {
    public ExamResult(){}
    @SerializedName("exam_id")
    public long examId;
    @SerializedName("exam_name")
    public String examName;
    @SerializedName("variant")
    public String variant;
    @SerializedName("kz_rating")
    public int kzRating;
    @SerializedName("dostyk_rating")
    public int dostykRating;
    @SerializedName("group_rating")
    public int groupRating;
    @SerializedName("lesson_results")
    public List<LessonResult> lessonResults;
}