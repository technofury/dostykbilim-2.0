package rest.gson_classes;

import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

public class NotificationJSON {
    public NotificationJSON(){}
    @SerializedName("id")
    public long id;
    @SerializedName("uid")
    public long uid;
    @SerializedName("tid")
    public long tid;
    @SerializedName("message")
    public String message;
    @SerializedName("date")
    public Date date;
}