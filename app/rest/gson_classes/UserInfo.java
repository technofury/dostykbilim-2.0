package rest.gson_classes;

import com.google.gson.annotations.SerializedName;

/**
 * Written by rsa on 01/08/2015.
 */
public class UserInfo {
    public UserInfo(){}
    @SerializedName("uid")
    public long uid;
    @SerializedName("token")
    public String token;
    @SerializedName("first_name")
    public String firstName;
    @SerializedName("last_name")
    public String lastName;
    @SerializedName("telephone")
    public String telephone;
    @SerializedName("role")
    public String role;
    @SerializedName("device_id")
    public String deviceId;
}
