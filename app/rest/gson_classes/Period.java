package rest.gson_classes;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Written by RSA on 12-May-15.
 */
public class Period {
    public Period(){}
    @SerializedName("id")
    public long id;
    @SerializedName("agreement_id")
    public long agreementId;
    @SerializedName("planned_payment")
    public int plannedPayment = 0;
    @SerializedName("planned_payment_date")
    public Date plannedPaymentDate = new Date();
    @SerializedName("payed_payment")
    public int payedPayment = 0;
    @SerializedName("payed_payment_date")
    public Date payedPaymentDate = new Date();
    @SerializedName("debt_payment")
    public int debtPayment = 0;
    @SerializedName("status")
    public String status;
    @SerializedName("comment")
    public String comment;
}
