package rest.gson_classes;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by arsenanai on 09/11/15.
 */
public class AllInOne {
    public AllInOne(){}
    @SerializedName("user_id")
    public long userId;
    @SerializedName("comment")
    public String comment;
    @SerializedName("schedules")
    public List<Schedule> schedules;
    @SerializedName("attendances")
    public List<Attendance> attendances;
    @SerializedName("agreement")
    public Agreement agreement;
    @SerializedName("exam")
    public ExamResultPage examResultPage;
    @SerializedName("videos")
    public List<VideoSolution> videos;
    @SerializedName("groups")
    public List<CuratorGroup> groups;
    @SerializedName("notifications")
    public List<NotificationJSON> notifications;
}
