package rest.gson_classes;

import com.google.gson.annotations.SerializedName;

/**
 * Created by arsenanai on 10/20/15.
 */
public class VideoSolution {
    public VideoSolution(){}
    @SerializedName("question_id")
    public long questionId;
    @SerializedName("question_number")
    public int questionNumber;
    @SerializedName("lesson_id")
    public long lessonId;
    @SerializedName("lesson_name")
    public String lessonName;
    @SerializedName("topic_name")
    public String topicName;
    @SerializedName("video_link")
    public String videoLink;
    @SerializedName("correct_answer")
    public String correctAnswer;
}
