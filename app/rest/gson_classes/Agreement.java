package rest.gson_classes;

import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

/**
 * Written by RSA on 12-May-15.
 */
public class Agreement {
    public Agreement(){}
    @SerializedName("id")
    public long id;
    @SerializedName("uid")
    public long uid;
    @SerializedName("start_date")
    public Date startDate;
    @SerializedName("end_date")
    public Date endDate;
    @SerializedName("discount")
    public int discount;
    @SerializedName("planned_payment")
    public int plannedPayment;
    @SerializedName("payed_payment")
    public int payedPayment;
    @SerializedName("debt_payment")
    public int debtPayment;
    @SerializedName("periods")
    public List<Period> periods;
    @SerializedName("comment")
    public String comment;
}
