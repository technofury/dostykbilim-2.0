package rest.gson_classes;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Written by rsa on 31-Jul-15.
 */
public class CuratorGroup {
    public CuratorGroup(){}
    @SerializedName("id")
    public long id;
    @SerializedName("name")
    public String name;
    @SerializedName("students")
    public List<UserInfo> students;
}