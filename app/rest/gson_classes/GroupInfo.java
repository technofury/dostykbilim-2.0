package rest.gson_classes;

import com.google.gson.annotations.SerializedName;
import models.rest.User;

import java.util.List;

/**
 * Written by rsa on 31-Jul-15.
 */
public class GroupInfo {
    public GroupInfo(){}
    @SerializedName("id")
    public long id;
    @SerializedName("name")
    public String name;
    @SerializedName("curator")
    public UserInfo curator;
    @SerializedName("students")
    public List<UserInfo> students;
    @SerializedName("schedules")
    public List<Schedule> schedules;
    @SerializedName("attendances")
    public List<Attendance> attendances;
}