package rest.gson_classes;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Written by Arsen on 6/11/2015.
 */
public class LessonResult {
    public LessonResult(){}
    @SerializedName("lesson_id")
    public long lessonId;
    @SerializedName("exam_id")
    public long examId;
    @SerializedName("lesson_code")
    public String lessonCode;
    @SerializedName("lesson_name")
    public String lessonName;
    @SerializedName("total_result")
    public int totalResult;
    @SerializedName("question_results")
    public List<QuestionResult> questionResults;
}
