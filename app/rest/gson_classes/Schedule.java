package rest.gson_classes;

/**
 * Written by RSA on 30-Apr-15.
 */
import com.google.gson.annotations.SerializedName;

public class Schedule {
    public Schedule(){}
    @SerializedName("id")
    public long id;
    @SerializedName("uid")
    public long uid;
    @SerializedName("row")
    public String row;
    @SerializedName("day")
    public int day;
    @SerializedName("lesson_id")
    public long lessonId;
    @SerializedName("lesson_name")
    public String lessonName;
    @SerializedName("teacher_id")
    public long teacherId;
    @SerializedName("teacher_name")
    public String teacherName;
    @SerializedName("cabinet")
    public String cabinet;
    @SerializedName("start_time")
    public String startTime;
    @SerializedName("end_time")
    public String endTime;
    @SerializedName("group_id")
    public long groupId;
    @SerializedName("group_name")
    public String groupName;
}