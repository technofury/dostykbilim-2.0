package rest.gson_classes;

import com.google.gson.annotations.SerializedName;

/**
 * Written by Arsen on 6/11/2015.
 */
public class QuestionResult {
    public QuestionResult(){}
    @SerializedName("question_id")
    public long questionId;
    @SerializedName("sequence_number")
    public int sequenceNumber;
    @SerializedName("lesson_id")
    public long lessonId;
    @SerializedName("topic_name")
    public String topicName;
    @SerializedName("video_link")
    public String videoLink;
    @SerializedName("student_answer")
    public String studentAnswer;
    @SerializedName("correct_answer")
    public String correctAnswer;
}
