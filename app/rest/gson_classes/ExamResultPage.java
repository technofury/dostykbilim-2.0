package rest.gson_classes;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Written by Arsen on 6/11/2015.
 */
public class ExamResultPage {
    public ExamResultPage(){}
    @SerializedName("user_id")
    public long userId;
    @SerializedName("divider")
    public int divider;
    @SerializedName("missed")
    public int missed;
    @SerializedName("grade")
    public int grade;
    @SerializedName("exam_results")
    public List<ExamResult> examResults;
}
