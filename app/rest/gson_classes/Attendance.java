package rest.gson_classes;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Written by RSA on 8-May-15.
 */
public class Attendance {
    @SerializedName("uid")
    public long uid;
    @SerializedName("timetable_id")
    public long timetableId;
    @SerializedName("status")
    public String status;
    @SerializedName("date")
    public Date date;
}