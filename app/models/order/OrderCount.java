package models.order;

import models.entities.social.Dostyk;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.List;

@Entity
public class OrderCount extends Model {

    private static final long serialVersionUID = 1L;
    public static Finder<Long,OrderCount> find = new Finder<Long, OrderCount>(Long.class, OrderCount.class);

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @ManyToOne
    public Orders orders;

    @ManyToOne
    public Dostyk dostyk;

    public int count;

    public static OrderCount getByOrderAndDostyk(Orders order, Dostyk dostyk){
                return find.where().eq("dostyk", dostyk).eq("orders",order).findUnique();
            }

    public static List<OrderCount> getByDostyk(Dostyk dostyk){
                return find.where().eq("dostyk", dostyk).findList();
            }


}
