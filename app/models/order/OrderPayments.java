package models.order;

import models.entities.social.Dostyk;
import play.data.format.Formats;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class OrderPayments extends Model {

    private static final long serialVersionUID = 1L;
    public static Finder<Long,OrderPayments> find = new Finder<Long, OrderPayments>(Long.class, OrderPayments.class);

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @Formats.DateTime(pattern = "yyyy-MM-dd")
    public Date date;

    public int amount;

    @ManyToOne
    public Dostyk dostyk;

    public static List<OrderPayments> getByDostyk(Dostyk dostyk) {
                return find.where().eq("dostyk_id", dostyk.id).findList();
            }

}
