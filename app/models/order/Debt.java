package models.order;

import models.entities.social.Dostyk;
import play.db.ebean.Model;

import javax.persistence.*;

@Entity
public class Debt extends Model {

    public static Finder<Long,Debt> find = new Finder<Long, Debt>(Long.class, Debt.class);

    @Id
    public Long id;

    @OneToOne
    public Dostyk dostyk;

    public int debt;


}
