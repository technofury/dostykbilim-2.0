package models.order;

import models.entities.social.Dostyk;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Suplier extends Model {

    private static final long serialVersionUID = 1L;
    public static Finder<Long, Suplier> find = new Finder<Long, Suplier>(Long.class, Suplier.class);

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;
    public String name;
    public String city;
    public String address;
    public String phone;
    public String director;
    public String bik;
    public String iik;
    public String bin;
    public String bank;
    public String kbe;


    public String getCity(){
        return this.city==null?"":this.city;
    }
    public String getAddress(){
        return this.address==null?"":this.address;
    }
    public String getName(){ return this.name==null?"":this.name;}
    public String getPhone(){
        return this.phone==null?"":this.phone;
    }
    public String getDirector(){
        return this.director==null?"":this.director;
    }
    public String getBik(){
        return this.bik==null?"":this.bik;
    }
    public String getIik(){
        return this.iik==null?"":this.iik;
    }
    public String getBin(){
        return this.bin==null?"":this.bin;
    }
    public String getBank(){
        return this.bank==null?"":this.bank;
    }
    public String getKbe(){
        return this.kbe==null?"":this.kbe;
    }
}
