package models.order;

import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Category extends Model {

    private static final long serialVersionUID = 1L;
    public static Finder<Long,Category> find = new Finder<Long, Category>(Long.class, Category.class);

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    public String name;


}
