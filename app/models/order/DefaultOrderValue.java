package models.order;

import models.entities.social.Dostyk;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;

import javax.persistence.*;
import models.entities.user.AuthorisedUser;
import models.logic.social.DostykLogic;
import static play.mvc.Controller.session;

@Entity
public class DefaultOrderValue extends Model {

    private static final long serialVersionUID = 1L;
    public static Finder<Long,DefaultOrderValue> find = new Finder<Long, DefaultOrderValue>(Long.class, DefaultOrderValue.class);

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    public int value;

    @OneToOne
    public Dostyk dostyk;


}
