package models.order;

import models.entities.social.Dostyk;
import play.data.format.Formats;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class Orders extends Model {

    private static final long serialVersionUID = 1L;
    public static Finder<Long,Orders> find = new Finder<Long, Orders>(Long.class, Orders.class);

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @Required
    public String name;

    @ManyToOne
    public Category category;

    public int price;

    @ManyToOne
    public OrderStatus orderStatus;

    public String description;

    @Formats.DateTime(pattern = "yyyy-MM-dd")
    public Date date;

    public String image;

    public int getOrderAmount(){
        List<OrderCount> orderCountList = OrderCount.find.where().eq("orders_id",id).findList();
        if(orderCountList.size() > 0){
            int totalAmount = 0;
            for(int i = 0; i<orderCountList.size(); i++){
                totalAmount+= orderCountList.get(i).count;
            }
            return  totalAmount;
        }else{
            return 0;
        }
    }

}
