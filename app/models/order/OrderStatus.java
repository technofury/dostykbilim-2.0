package models.order;

import models.entities.social.Dostyk;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;

import javax.persistence.*;

@Entity
public class OrderStatus extends Model {

    private static final long serialVersionUID = 1L;
    public static Finder<Long,OrderStatus> find = new Finder<Long, OrderStatus>(Long.class, OrderStatus.class);

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    public String status;


}
