package models.timetable;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import models.entities.social.Dostyk;

import models.entities.user.AuthorisedUser;
import models.logic.social.DostykLogic;
import play.db.ebean.Model.Finder;
import play.db.ebean.Model;

import controllers.user.AuthorisedUsers;

import static play.mvc.Controller.session;

@Entity
@Table(name = "lesson_periods")
public class LessonPeriod extends Model {
	
	private static Finder<Long, LessonPeriod> find = new Finder<>(Long.class, LessonPeriod.class);

	@Id
	public Long id;
	
	public int number;
	
	public Date startTime;
	
	public Date endTime;
	
	@ManyToOne
	public Settings settings;
	
	@ManyToOne
	public Dostyk organization;
	
	public static List<LessonPeriod> bySettings(Settings settings) {
		AuthorisedUser user = AuthorisedUser.getUserByEmail(session("connected"));
		Dostyk dostyk = DostykLogic.getByUser(user);
		return find.where()
				.eq("organization", dostyk)
				.eq("settings", settings)
				.orderBy("number")
				.findList();
	}
	
	public static LessonPeriod byNumber(int number, Settings settings) {
		AuthorisedUser user = AuthorisedUser.getUserByEmail(session("connected"));
		Dostyk dostyk = DostykLogic.getByUser(user);
		return find.where()
				.eq("organization", dostyk)
				.eq("settings", settings)
				.eq("number", number)				
				.findUnique();
	}


	public String getStartTime() {
        return (this.startTime == null) ? "" : new SimpleDateFormat("HH:mm").format(this.startTime);
    }
	
	public String getEndTime() {
        return (this.endTime == null) ? "" : new SimpleDateFormat("HH:mm").format(this.endTime);
    }
	
	public int getStartInMin() {
		String[] s = getStartTime().split(":");
		return Integer.parseInt(s[0])*60 + Integer.parseInt(s[1]);
	}
	
	public int getEndInMin() {
		String[] s = getEndTime().split(":");
		return Integer.parseInt(s[0])*60 + Integer.parseInt(s[1]);
	}
	
	public String getPeriod() {
		return getStartTime() + "-" + getEndTime();
	}
}
