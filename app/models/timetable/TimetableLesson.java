package models.timetable;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import models.entities.education.Lesson;
import models.entities.social.Dostyk;
import models.entities.user.AuthorisedUser;
import models.entities.user.Profile;

import models.logic.social.DostykLogic;
import play.db.ebean.Model.Finder;
import play.db.ebean.Model;

import controllers.user.AuthorisedUsers;

import static play.mvc.Controller.session;

@Entity
@Table(name = "timetable_lessons")
public class TimetableLesson extends Model {
	
	private static Finder<Long, TimetableLesson> find = new Finder<>(Long.class, TimetableLesson.class);
	
	@Id
	public Long id;

	public int day;
	
	@ManyToOne
	public LessonPeriod period;
	
	@ManyToOne
	public AuthorisedUser subjectTeacher;
	
	@ManyToOne
	public Lesson subject;
	
	@ManyToOne
	public Timetable timetable;
	
	@ManyToOne
	public Dostyk organization;
	
	public static TimetableLesson byId(Long id) {
		AuthorisedUser user = AuthorisedUser.getUserByEmail(session("connected"));
		Dostyk dostyk = DostykLogic.getByUser(user);
		return find.where().eq("id", id).eq("organization", dostyk).findUnique();
	}
	
	public static List<TimetableLesson> byTimetable(Timetable timetable) {
		AuthorisedUser user = AuthorisedUser.getUserByEmail(session("connected"));
		Dostyk dostyk = DostykLogic.getByUser(user);
		return find.where()
				.eq("organization", dostyk)
				.eq("timetable", timetable)				
				.findList();
	}
	
	public static List<TimetableLesson> all() {
		AuthorisedUser user = AuthorisedUser.getUserByEmail(session("connected"));
		Dostyk dostyk = DostykLogic.getByUser(user);
		return find.where().eq("organization", dostyk).findList();
	}

	public static List<TimetableLesson> byUser(AuthorisedUser user) {
		return find.where().eq("subjectTeacher", user).findList();
	}

}
