package models.timetable;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.avaje.ebean.Expr;
import com.avaje.ebean.ExpressionList;
import models.entities.user.AuthorisedUser;
import models.logic.social.DostykGroupLogic;
import models.logic.social.DostykLogic;
import play.db.ebean.Model.Finder;
import play.db.ebean.Model;
import controllers.user.AuthorisedUsers;
import models.entities.social.DostykGroup;
import models.entities.social.Dostyk;

import static play.mvc.Controller.session;

@Entity
@Table(name = "timetables")
public class Timetable extends Model {
	
	private static Finder<Long, Timetable> find = new Finder<>(Long.class, Timetable.class);
	
	@Id
	public Long id;
	
	public boolean isActive;

	@ManyToOne
	public DostykGroup grade;
	
	@ManyToOne
	public Settings settings;
	
	@ManyToOne
	public Dostyk organization;
	
	public static Timetable byId(Long id) {
		AuthorisedUser user = AuthorisedUser.getUserByEmail(session("connected"));
		Dostyk dostyk = DostykLogic.getByUser(user);
		return find.where().eq("organization", dostyk).eq("id", id).findUnique();
	}
	
	public static Timetable findActive(DostykGroup grade) {
		AuthorisedUser user = AuthorisedUser.getUserByEmail(session("connected"));
		Dostyk dostyk = DostykLogic.getByUser(user);
		return find.where()
				.eq("organization", dostyk)
				.eq("grade", grade)
				.eq("settings", Settings.findActive())
				.eq("isActive", true)				
				.findUnique();
	}   
	
	public static List<Timetable> all() {
		AuthorisedUser user = AuthorisedUser.getUserByEmail(session("connected"));
		Dostyk dostyk = DostykLogic.getByUser(user);
		return find.where()
				.eq("organization", dostyk)
				.findList();
	}

	public static List<Timetable> byGrade(DostykGroup grade){
		AuthorisedUser user = AuthorisedUser.getUserByEmail(session("connected"));
		Dostyk dostyk = DostykLogic.getByUser(user);
		return find.where()
				.eq("organization", dostyk)
				.eq("grade", grade)
				.findList();
	}
	public static List<Timetable> byParams(Long gid, String sid) {
		AuthorisedUser user = AuthorisedUser.getUserByEmail(session("connected"));
		Dostyk dostyk = DostykLogic.getByUser(user);
		ExpressionList<Timetable> query = find.where()
				.eq("organization", dostyk);

		if (gid != 0) {
			DostykGroup grade = DostykGroupLogic.getById(gid);
			query.add(Expr.eq("grade", grade));
		}
		if (!sid.equals("all")) {
			boolean isActive = false;
			if (sid.equals("active")) isActive = true;
			query.add(Expr.eq("isActive", isActive));
		}
		return query.findList();
	}
	
	public static List<Timetable> bySettings(Settings settings) {
		AuthorisedUser user = AuthorisedUser.getUserByEmail(session("connected"));
		Dostyk dostyk = DostykLogic.getByUser(user);
		return find.where()
				.eq("organization", dostyk)
				.eq("settings", settings)
				.findList();
	}
}
