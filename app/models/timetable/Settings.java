package models.timetable;

import java.util.List;

import javax.persistence.*;

import models.entities.social.Dostyk;
import models.entities.user.AuthorisedUser;
import models.logic.social.DostykLogic;
import play.db.ebean.Model.Finder;
import play.db.ebean.Model;
import static play.mvc.Controller.session;

@Entity
@Table(name = "timetable_settings")
public class Settings extends Model {
	
	private static Finder<Long, Settings> find = new Finder<>(Long.class, Settings.class);
	
	@Id
	public Long id;
	
	public String name;

	public int lessonTime = 45;
	
	public int lessonDays = 5;
	
	public int maxLessons = 12;
	
	public boolean isActive;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "settings")
	public List<LessonPeriod> lessonPeriods;
	
	@ManyToOne
	public Dostyk organization;
	
	public static Settings byId(Long id) {
		AuthorisedUser user = AuthorisedUser.getUserByEmail(session("connected"));
		Dostyk dostyk = DostykLogic.getByUser(user);
		return find.where().eq("organization", dostyk).eq("id", id).findUnique();
	}
	
	public static Settings findActive() {
		AuthorisedUser user = AuthorisedUser.getUserByEmail(session("connected"));
		Dostyk dostyk = DostykLogic.getByUser(user);
		return find.where().eq("organization", dostyk).eq("isActive", true).findUnique();
	}

	public static List<Settings> all() {
		AuthorisedUser user = AuthorisedUser.getUserByEmail(session("connected"));
		Dostyk dostyk = DostykLogic.getByUser(user);
		return find.where().eq("organization", dostyk).findList();
	}
}
