package models.entities.education;

/**
 * @author balancy
 */

import javax.persistence.*;

import play.db.ebean.Model;
import java.util.List;

@Entity
@Table(name = "lessons")
public class Lesson extends Model {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Id
	public Long id;

	public String name;

	public String language;

	public String code;

	@ManyToMany(cascade = CascadeType.ALL)
	public List<Course> course;

}
