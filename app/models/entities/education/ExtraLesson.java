package models.entities.education;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import models.entities.social.Dostyk;
import models.entities.social.DostykGroup;
import models.entities.user.AuthorisedUser;
import play.data.format.Formats;
import play.db.ebean.Model;

@Entity
@Table(name = "extra_lessons")
public class ExtraLesson extends Model {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Id
	public Long id;

	@ManyToOne
	public AuthorisedUser teacher;

	@ManyToOne
	public DostykGroup dostykGroup;

	@Formats.DateTime(pattern = "EE dd MM yyyy")
	public Date date;

	@ManyToOne
	public Lesson lesson;

	@ManyToOne
	public Topic topic;

	public static Finder<Long, ExtraLesson> find = new Finder<Long, ExtraLesson>(
			Long.class, ExtraLesson.class);

	public static ExtraLesson by(AuthorisedUser teacher, DostykGroup group,
			Lesson lesson, Date date) {
		return find.where().eq("teacher", teacher).eq("lesson", lesson)
				.eq("dostykGroup", group).eq("date", date).findUnique();
	}

	public static Integer countByDostyk(Dostyk dostyk) {
		return find.where().eq("dostykGroup.dostyk", dostyk).findRowCount();
	}

	public static Integer countByTeacher(AuthorisedUser teacher) {
		return find.where().eq("teacher", teacher).findRowCount();
	}

	public static Integer countByGroup(DostykGroup dostykGroup) {
		return find.where().eq("dostykGroup", dostykGroup).findRowCount();
	}

	public static List<ExtraLesson> by(long teacherId) {
		return find.where().eq("teacher.id", teacherId).orderBy("date")
				.findList();
	}
}
