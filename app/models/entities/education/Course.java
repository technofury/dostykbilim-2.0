package models.entities.education;

import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by User on 15-Feb-17.
 */
@Entity
@Table(name = "courses")
public class Course extends Model {
    public static Finder<Long, Course> find = new Finder<>(Long.class, Course.class);
    @Id
    public Long id;

    public int title;

    public static Course byTitle(int title){
        return find.where().eq("title", title).findUnique();
    }
}
