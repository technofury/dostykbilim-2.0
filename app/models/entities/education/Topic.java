package models.entities.education;

import java.util.List;

import javax.persistence.*;

import models.logic.education.CourseLogic;
import models.logic.education.CourseTypeLogic;
import models.logic.education.LessonLogic;
import play.data.validation.Constraints;
import play.db.ebean.Model;

@Entity
@Table(name = "topics")
public class Topic extends Model {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Id
	public Long id;

	@Constraints.Required
	public String name;

	@ManyToOne
	public Lesson lesson;

	public String fileName;

	public String pathToFile;

	@ManyToOne
	public Course courses;

	public static Finder<Long, Topic> find = new Finder<Long, Topic>(
			Long.class, Topic.class);

	public static List<Topic> findAll() {
		return find.all();
	}

	public static Topic findByInfo(Lesson lesson, String name) {
		return find.where().eq("lesson", lesson).eq("name", name)
				.orderBy("name").findUnique();
	}

	public static int countByInfo(Lesson lesson, String name) {
		return find.where().eq("lesson", lesson).eq("name", name)
				.findRowCount();
	}

	public static List<Topic> findByLesson(Lesson lesson) {
		return find.where().eq("lesson", lesson).orderBy("name").findList();
	}

	public static List<Topic> getTopicList(long lid, long cid) {
			Lesson lesson = LessonLogic.getById(lid);
			Course courses = CourseLogic.getById(cid);
			return find.where().eq("lesson", lesson).eq("courses", courses).orderBy("id").findList();
	}
}