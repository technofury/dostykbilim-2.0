package models.entities.education;

/**
 * @author balancy
 */

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import models.entities.user.AuthorisedUser;
import models.timetable.Timetable;
import play.data.format.Formats;
import play.db.ebean.Model;

@Entity
@Table(name = "registers")
public class Register extends Model {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Id
	public Long id;

	@ManyToOne
	public Timetable timetable;

	@ManyToOne
	public AuthorisedUser student;

	@ManyToOne
	public AuthorisedUser teacher;

	@Formats.DateTime(pattern = "yyyy-MM-dd")
	public Date date;

	public String attendance;

	@Column(name = "lessonid")
	public Long lessonid;


}
