package models.entities.education;

import static play.mvc.Controller.session;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.avaje.ebean.Expr;

import models.entities.social.Dostyk;
import models.entities.user.AuthorisedUser;
import models.logic.social.DostykLogic;
import models.timetable.TimetableLesson;
import play.data.format.Formats;
import play.db.ebean.Model;

@Entity
@Table(name = "attendances")
public class Attendance extends Model {
	
	private static Finder<Long, Attendance> find = new Finder<>(Long.class, Attendance.class);
	
	@Id
	public Long id;
	
	public int status;
	
	@Formats.DateTime(pattern = "yyyy-MM-dd")
	public Date mydate;
	
	@ManyToOne
	public TimetableLesson lesson;
	
	@ManyToOne
	public AuthorisedUser student;		
	
	@ManyToOne
	public Dostyk organization;
	
	public static Attendance byId(Long id) {
		AuthorisedUser user = AuthorisedUser.getUserByEmail(session("connected"));
		Dostyk dostyk = DostykLogic.getByUser(user);
		return find.where()
				.eq("id", id)
				.eq("organization", dostyk)
				.findUnique();				
	}
	
	public static List<Attendance> allByStudent(AuthorisedUser student) {
		AuthorisedUser user = AuthorisedUser.getUserByEmail(session("connected"));
		Dostyk dostyk = DostykLogic.getByUser(user);
		return find.where()
				.eq("student", student)
				.eq("organization", dostyk)
				.findList();				
	}
	
	public static List<Attendance> byStudentDates(AuthorisedUser student, Date start, Date end) {
		AuthorisedUser user = AuthorisedUser.getUserByEmail(session("connected"));
		Dostyk dostyk = DostykLogic.getByUser(user);
		return find.where()
				.eq("student", student)
				.between("mydate", start, end)
				.eq("organization", dostyk)
				.findList();				
	}
	
	public static List<Attendance> absentOrLate(AuthorisedUser student, Date start, Date end) {
		AuthorisedUser user = AuthorisedUser.getUserByEmail(session("connected"));
		Dostyk dostyk = DostykLogic.getByUser(user);
		return find.where()
				.eq("student", student)
				.between("mydate", start, end)
				.not(Expr.eq("status", 0))
				.eq("organization", dostyk)
				.orderBy("mydate desc")				
				.findList();				
	}
		
	public static List<Attendance> byDateLesson(Date date, TimetableLesson lesson) {
		AuthorisedUser user = AuthorisedUser.getUserByEmail(session("connected"));
		Dostyk dostyk = DostykLogic.getByUser(user);
		return find.where()
				.eq("mydate", date)
				.eq("lesson", lesson)
				.eq("organization", dostyk)
				.findList();
	}
	
	public String getDateValue() {
        return (this.mydate == null) ? "" : change(new SimpleDateFormat("yyyy-MM-dd").format(this.mydate));
    }
	
	private String change(String date) {
		String s[] = date.split("-");
		return s[2] + "." + s[1] + "." + s[0];
	}
}
