package models.entities.education;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import models.entities.social.DostykGroup;
import play.db.ebean.Model;

@Entity
@Table(name = "topic_registers")
public class TopicRegister extends Model {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Id
	public Long id;

	@Column(name = "topic_id")
	@ManyToOne
	public Topic topic;

	@Column(name = "dostyk_group_id")
	@ManyToOne
	public DostykGroup dostykGroup;

	public Date date;

	public static Finder<Long, TopicRegister> find = new Finder<Long, TopicRegister>(
			Long.class, TopicRegister.class);

	public static TopicRegister findByFields(DostykGroup group, Date date,
			Lesson lesson) {
		return find.where().eq("dostykGroup", group).eq("date", date)
				.eq("topic.lesson", lesson).findUnique();
	}

}