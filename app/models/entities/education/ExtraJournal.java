package models.entities.education;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import models.entities.user.AuthorisedUser;
import play.db.ebean.Model;

@Entity
@Table(name = "extra_journals")
public class ExtraJournal extends Model {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Id
	public Long id;

	public Date date;

	@ManyToOne
	public AuthorisedUser user;

	public int kaz;

	public int rus;

	public int math;

	public int history;

	public int selected;

	public static Finder<Long, ExtraJournal> find = new Finder<Long, ExtraJournal>(
			Long.class, ExtraJournal.class);

	public static List<ExtraJournal> findAll() {
		return find.all();
	}

	public static ExtraJournal getBy(AuthorisedUser u, Date date) {
		return find.where().eq("date", date).eq("user", u).findUnique();
	}

}