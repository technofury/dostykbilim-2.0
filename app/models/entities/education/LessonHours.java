package models.entities.education;

/**
 * @author balancy
 */

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import models.entities.social.Dostyk;
import play.db.ebean.Model;

@Entity
@Table(name = "lesson_hours")
public class LessonHours extends Model {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Id
	public Long id;

	@ManyToOne
	public Lesson lesson;

	@ManyToOne
	public Dostyk dostyk;

	public int hours;

}
