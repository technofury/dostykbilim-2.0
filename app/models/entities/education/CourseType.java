package models.entities.education;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import models.entities.user.Profile;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;

@Entity
@Table(name = "course_types")
public class CourseType extends Model {
	private static final long serialVersionUID = 1L;

	@Id
	public Long id;

	@Column(unique = true)
	public String code;

	@Required
	@Column(unique = true)
	public String name;

	public int classNumber;

	@OneToMany
	public List<Profile> profiles;
}
