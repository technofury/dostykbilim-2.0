package models.entities.examination;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import models.entities.education.Lesson;
import models.entities.user.AuthorisedUser;
import play.db.ebean.Model;

@Entity
@Table(name = "ent")
public class ENT extends Model {
	private static final long serialVersionUID = 1L;

	@SequenceGenerator(name = "seq_gen_name", sequenceName = "task_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_gen_name")
	@Column(name = "id")
	@Id
	public Long id;

	@Column(name = "points")
	public int points;
	
	@Column(name = "kaz")
	public int kaz;
	
	public int rus;
	public int math;
	public int history;
	public int selected;

	@Column(name = "user_id")
	@OneToOne
	public AuthorisedUser user;
	
	@ManyToOne
	public Lesson lesson;
	
}
