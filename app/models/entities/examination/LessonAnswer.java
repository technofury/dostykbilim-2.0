package models.entities.examination;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import models.entities.education.Lesson;
import play.db.ebean.Model;

@Entity
@Table(name = "lesson_answer")
public class LessonAnswer extends Model {
	private static final long serialVersionUID = 1L;

	@Id
	public Long id;

	@ManyToOne
	public Lesson lesson;

	@ManyToOne
	public AnswerKey answerKey;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "lessonAnswer")
	public List<Answer> answers;

	public String code;

	@Column(name = "path_to_video")
	public String pathToVideo;
}
