package models.entities.examination;

/**
 * @author balancy
 */

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import models.entities.education.Lesson;
import play.db.ebean.Model;

@Entity
@Table(name = "detailed_results")
public class DetailedResult extends Model {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Id
	public Long id;

	@ManyToOne
	public ExamResult examResult;

	@ManyToOne
	public Lesson lesson;

	public int correct;
	public int incorrect;
	public int blank;
	public int mark;

	public String studentAnswers;

	public int rankGroup, rankDostyk, rankKZ;

}
