package models.entities.examination;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import models.entities.education.Lesson;
import models.entities.social.DostykGroup;
import models.entities.user.AuthorisedUser;
import play.db.ebean.Model;

@Entity
@Table(name = "attutt")
public class ATTUTT extends Model {
	private static final long serialVersionUID = 1L;

	@Id
	public Long id;

	@Column(name = "user_id")
	@ManyToOne
	public AuthorisedUser user;

	public String type;

	@Column(name = "lesson_id")
	@ManyToOne
	public Lesson lesson;

	public int number;

	public int mark;

	public static Finder<Long, ATTUTT> find = new Finder<Long, ATTUTT>(
			Long.class, ATTUTT.class);

	public static ATTUTT getByFields(long l_id, long u_id, int number,
			String type) {
		return find.where().eq("user.id", u_id).eq("lesson.id", l_id)
				.eq("number", number).eq("type", type).findUnique();
	}

	public static List<ATTUTT> getByFields(DostykGroup dg, int number,
			String type, long l_id) {
		return find.where().eq("number", number).eq("type", type)
				.eq("lesson.id", l_id).eq("user.profile.dostykGroup", dg)
				.findList();
	}
}
