package models.entities.examination;

/**
 * @author balancy
 */

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import play.data.format.Formats;
import play.db.ebean.Model;

@Entity
@Table(name = "exams")
public class Exam extends Model {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Id
	public Long id;

	public String name;

	@ManyToOne
	public ExamType examType;

	@Formats.DateTime(pattern = "dd MM yyyy HH:mm")
	public Date startDate = new Date();

	@Formats.DateTime(pattern = "dd MM yyyy HH:mm")
	public Date endDate = new Date();

	public String status;

	public int grade;

	@OneToMany
	public List<AnswerKey> answerKeys;

}
