package models.entities.examination;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import play.db.ebean.Model;

@Entity
@Table(name = "jauaptar")
public class Answer extends Model implements Comparable<Answer> {
	private static final long serialVersionUID = 1L;

	@Id
	public Long id;

	public int questionNumber;

	public String answerCode;

	public String topic;

	@ManyToOne
	public LessonAnswer lessonAnswer;

	@Column(name = "path_to_video")
	public String pathToVideo;

	@Override
	public int compareTo(Answer o) {
		return questionNumber - o.questionNumber;
	}

	public static Finder<Long, Answer> find = new Finder<Long, Answer>(
			Long.class, Answer.class);

	public static List<Answer> getByLessonAnswer(LessonAnswer la) {
		return find.where().eq("lessonAnswer", la).orderBy("questionNumber")
				.findList();
	}

	public static Answer getBy(LessonAnswer l, int q) {
		return find.where().eq("lessonAnswer", l).eq("questionNumber", q)
				.findUnique();
	}

	public static List<Answer> getAllWithVideo(){
		return find.where().ne("pathToVideo", null)
				.orderBy("lessonAnswer.answerKey.exam.endDate").orderBy("lessonAnswer.lesson.code")
				.orderBy("questionNumber").findList();

	}
}