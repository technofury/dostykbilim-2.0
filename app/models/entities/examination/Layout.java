package models.entities.examination;

/**
 * @author balancy
 */

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import play.db.ebean.Model;

@Entity
@Table(name = "layouts")
public class Layout extends Model {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Id
	public Long id;

	public String name;

	@OneToMany(cascade = CascadeType.ALL)
	public List<Block> blocks;
}
