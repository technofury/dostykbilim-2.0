package models.entities.examination;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import play.db.ebean.Model;

@Entity
@Table(name = "jauapkilti")
public class AnswerKey extends Model {
	private static final long serialVersionUID = 1L;

	@Id
	public Long id;

	@Column(unique = true)
	public String variant;

	public int grade;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "answerKey")
	public List<LessonAnswer> lessonAnswers;

	@ManyToOne
	public Exam exam;

}
