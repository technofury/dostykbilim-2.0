package models.entities.examination;

/**
 * @author balancy
 */

import java.util.List;

import javax.persistence.*;

import models.entities.user.AuthorisedUser;
import play.db.ebean.Model;

@Entity
@Table(name = "exam_results")
public class ExamResult extends Model {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Id
	public Long id;

	@ManyToOne
	public AuthorisedUser student;

	@ManyToOne
	public Exam exam;

	public String variant;

	@OneToMany
	public List<DetailedResult> detailedResults;

	public int rankGroup, rankDostyk, rankKZ, total;


	@Transient
	public int getTotalMark(){
		int result = 0;
		for(DetailedResult dr: detailedResults){
			result+=dr.correct-(dr.incorrect/4);
		}

		return result;
	}
}
