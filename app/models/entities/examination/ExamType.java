package models.entities.examination;

/**
 * @author balancy
 */

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import play.db.ebean.Model;

@Entity
@Table(name = "exam_types")
public class ExamType extends Model {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Id
	public Long id;

	public String name;

	@ManyToOne
	public Layout layout;
}
