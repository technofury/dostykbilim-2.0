package models.entities.social;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Column;

import play.db.ebean.Model;

@Entity
@Table(name = "schools")
public class School extends Model {

	private static final long serialVersionUID = 1L;

	@Id
	public Long id;

	public String code;

	public String name_kz;
	
	@Column(name="name_rus")
	public String name_ru;
	
	@ManyToOne
	public Region region;

	@ManyToOne
	public District district;

}
