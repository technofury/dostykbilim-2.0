package models.entities.social;

import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by rsa on 26/03/16.
 */
@Entity
@Table(name="firms")
public class Firm extends Model {
    private static final long serialVersionUID = 1L;

    @Id
    public Long id;
    public String name;
    public String city;
    public String address;
    public String phone;
    public String director;
    public String bik;
    public String iik;
    public String bin;
    public String bank;
    public String kbe;
    @ManyToOne
    public Dostyk dostyk;

    public static Finder<Long, Firm> find = new Finder<Long, Firm>(
            Long.class, Firm.class);

    public static List<Firm> getAll(Dostyk dostyk){
        return find.where().eq("dostyk",dostyk).orderBy("id").findList();
    }

    public static List<String> list(Dostyk dostyk){
        List<String> result = new ArrayList<>();
        for(Firm firm: getAll(dostyk))
            result.add(firm.name);
        return result;
    }
    public static int countByName(Dostyk dostyk, String name){
        return find.where().eq("dostyk", dostyk).eq("name", name).findRowCount();
    }
    public static Firm getByName(Dostyk dostyk, String name){
        return find.where().eq("dostyk", dostyk).eq("name", name).findUnique();
    }
    public static Firm getById(Long id){
        return find.byId(id);
    }

    public String getCity(){
        return this.city==null?"":this.city;
    }
    public String getAddress(){
        return this.address==null?"":this.address;
    }
    public String getName(){ return this.name==null?"":this.name;}
    public String getPhone(){
        return this.phone==null?"":this.phone;
    }
    public String getDirector(){
        return this.director==null?"":this.director;
    }
    public String getBik(){
        return this.bik==null?"":this.bik;
    }
    public String getIik(){
        return this.iik==null?"":this.iik;
    }
    public String getBin(){
        return this.bin==null?"":this.bin;
    }
    public String getBank(){
        return this.bank==null?"":this.bank;
    }
    public String getKbe(){
        return this.kbe==null?"":this.kbe;
    }
}
