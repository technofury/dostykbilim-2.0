package models.entities.social;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import models.entities.accounting.DiscountValue;
import models.entities.accounting.PaymentReceipt;
import models.entities.education.CourseType;
import models.entities.education.LessonHours;
import models.entities.user.AuthorisedUser;
import models.entities.user.SecurityRole;
import models.logic.user.ProfileLogic;
import play.data.validation.Constraints.MaxLength;
import play.data.validation.Constraints.MinLength;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;

@Entity
@Table(name = "dostyks")
public class Dostyk extends Model {

	private static final long serialVersionUID = 1L;

	public static final Finder<Long, Dostyk> find = new Finder<Long, Dostyk>(
			Long.class, Dostyk.class);

	@Id
	public Long id;

	@Required
	public String name;

	public int smsLeft = 0;

	public int status;

	@Required
	@Column(unique = true)
	public int code;

	public String organization;

	public String mainFirmId;

	@OneToMany(cascade = CascadeType.PERSIST, mappedBy = "dostyk", targetEntity = DiscountValue.class)
	public List<DiscountValue> discountValues;

	@OneToMany(cascade = CascadeType.PERSIST, mappedBy = "dostyk", targetEntity = PaymentReceipt.class)
	public List<PaymentReceipt> receipts;

	@ManyToMany
	public List<AuthorisedUser> users;

	@OneToMany
	public List<Firm> firms;

	@OneToMany(mappedBy = "dostyk", targetEntity = DostykGroup.class)
	public List<DostykGroup> dostykGroups;

	@OneToMany(mappedBy = "dostyk", targetEntity = LessonHours.class)
	public List<LessonHours> lessonHours;

	@Transient
	public Map<String, Integer> ulgerimi = new HashMap<>();
	
	@Transient
	public Map<Date, Integer> movement = new HashMap<>();
	
	@Transient
	public Map<Date, Integer> revenue = new HashMap<>();

	@Transient
	public Map<CourseType, Integer> courseTypeCount = new HashMap<>();

	@Transient
	public int getUlgerimiCount(String type) {
		return ProfileLogic.find.where()
				.eq("user.roles", SecurityRole.getByName("student"))
				.eq("user.lessons", this).eq("user.status", "active")
				.eq("ulgerimi", type).findRowCount();
	}
}
