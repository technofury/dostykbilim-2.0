package models.entities.social;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import models.entities.user.AuthorisedUser;
import play.data.format.Formats;
import play.db.ebean.Model;

@Entity
@Table(name = "contents")
public class Content extends Model {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Id
	public Long id;

	@ManyToOne
	public Dostyk dostyk;

	@ManyToOne
	public DostykGroup dostykGroup;

	public String title;

	@Column(columnDefinition = "TEXT")
	public String body;

	@ManyToOne
	public AuthorisedUser author;

	@Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
	public Date createdDate = new Date();

	@Formats.DateTime(pattern = "dd/MM/yyyy HH:mm")
	public Date changedDate = new Date();

}
