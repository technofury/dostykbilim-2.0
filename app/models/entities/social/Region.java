package models.entities.social;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Column;

import play.db.ebean.Model;

@Entity
@Table(name = "regions")
public class Region extends Model {

	private static final long serialVersionUID = 1L;

	@Id
	public Long id;

	public String code;
	
	public String name_kz;
	
	@Column(name="name_rus")
	public String name_ru;

	@OneToMany
	public List<District> districts;
}
