package models.entities.social;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import models.entities.education.Course;
import models.entities.user.AuthorisedUser;
import models.entities.user.Profile;
import play.db.ebean.Model;

@Entity
@Table(name = "dostyk_groups")
public class DostykGroup extends Model {
	private static final long serialVersionUID = 1L;

	@Id
	public Long id;

	public String name;

	public String language;

	@OneToMany(cascade = CascadeType.ALL, targetEntity = Profile.class, mappedBy = "dostykGroup")
	public List<Profile> students;

	@OneToMany(cascade = CascadeType.ALL, targetEntity = Profile.class, mappedBy = "electiveGroup")
	public List<Profile> electiveStudents;

	@ManyToOne
	public AuthorisedUser curator;

	@ManyToOne
	public Course courses;

	//@OneToOne
	//public Timetable timeTable;

	@ManyToOne
	public Dostyk dostyk;

}
