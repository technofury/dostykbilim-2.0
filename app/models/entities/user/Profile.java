package models.entities.user;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import models.entities.education.CourseType;
import models.entities.education.Lesson;
import models.entities.social.District;
import models.entities.social.DostykGroup;
import models.entities.social.Region;
import models.entities.social.School;
import models.entities.ten.ExamSession;
import play.data.format.Formats;
import play.data.validation.Constraints;
import play.data.validation.Constraints.MinLength;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;

@Entity
@Table(name = "profiles")
public class Profile extends Model {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    public Long id;

    @OneToOne(mappedBy = "profile")
    public AuthorisedUser user;

    @Column(unique = true)
    public String idNumber;

    @Required
    @MinLength(1)
    public String firstName;

    @Constraints.Required
    public String lastName;

    public String secondName = "";

    @Formats.DateTime(pattern = "yyyy-mm-dd")
    public Date birthDate = new Date();

    @Required
    public String gender;

    @Required
    public String phoneNumber;

    public String parentNumber;

    public String inn;

    public String ulgerimi = "jaksy";

    public String lang;

    // @ManyToMany(cascade = CascadeType.ALL, targetEntity = Dostyk.class,
    // mappedBy = "users")
    // public List<Dostyk> lessons;

    @ManyToOne(targetEntity = ExamSession.class)
    public ExamSession examSession;

    @ManyToOne(cascade = CascadeType.ALL, targetEntity = DostykGroup.class)
    public DostykGroup dostykGroup;

    @ManyToOne(cascade = CascadeType.ALL, targetEntity = DostykGroup.class)
    public DostykGroup electiveGroup;

    @ManyToOne(cascade = CascadeType.ALL, targetEntity = CourseType.class)
    public CourseType course;

    @ManyToOne(cascade = CascadeType.ALL, targetEntity = Region.class)
    public Region region;

    @ManyToOne(cascade = CascadeType.ALL, targetEntity = District.class)
    public District district;

    @ManyToOne(cascade = CascadeType.ALL, targetEntity = School.class)
    public School school;

    @ManyToMany(cascade = CascadeType.ALL)
    public List<Lesson> lessons;

    //public String ulgerimi = "jaksy";
}
