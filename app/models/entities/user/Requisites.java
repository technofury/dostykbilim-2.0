package models.entities.user;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import models.entities.education.CourseType;
import models.entities.education.Lesson;
import models.entities.social.District;
import models.entities.social.DostykGroup;
import models.entities.social.Region;
import models.entities.social.School;
import play.data.format.Formats;
import play.data.validation.Constraints;
import play.data.validation.Constraints.MinLength;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;

@Entity
@Table(name = "requisites")
public class Requisites extends Model {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Id
	public Long id;

	@OneToOne(mappedBy = "requisite")
	public AuthorisedUser user;

	public String dostykOfficial= "____________________";
	public String dostykCity= "____________________";
	public String dostykAddress= "____________________";
	public String dostykTel= "____________________";
	public String dostykDirector = "____________________";
	public String dostykBIK = "____________________";
	public String dostykIIK = "____________________";
	public String dostykBIN = "____________________";
	public String dostykBANK = "____________________";
	public String dostykKBE = "____________________";

}

