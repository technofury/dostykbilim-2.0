package models.entities.user;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import play.db.ebean.Model;
import be.objectify.deadbolt.core.models.Role;

@Entity
@Table(name = "roles")
public class SecurityRole extends Model implements Role {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Id
	public Long id;

	public String name;

	public static final Finder<Long, SecurityRole> find = new Finder<Long, SecurityRole>(
			Long.class, SecurityRole.class);

	@Override
	public String getName() {
		return name;
	}

	public static SecurityRole getRoleByName(String name) {
		return find.where().ilike("name", name).findUnique();
	}

	public static SecurityRole getByName(String name) {
		return find.where().eq("name", name).findUnique();
	}

	public static List<String> list() {
		List<SecurityRole> SecurityRoles = find.all();
		List<String> names = new ArrayList<String>();
		for (int i = 0; i < SecurityRoles.size(); i++) {
			names.add(SecurityRoles.get(i).name);
		}
		return names;
	}

}
