package models.entities.user;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import play.db.ebean.Model;

@Entity
@Table(name = "parents")
public class Parent extends Model {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public static final Finder<Long, Parent> find = new Finder<Long, Parent>(
			Long.class, Parent.class);

	@Id
	public Long id;

	@ManyToOne
	public AuthorisedUser child;

	public String firstName;

	public String lastName;

	public String parentType;

	public String phoneNumber1;

	public String phoneNumber2;

	public String work;
	
	public String inn;

	public Boolean responsible = false;

	public String getFirstName(){return firstName==null?"":firstName;}
	public String getLastName(){return lastName==null?"":lastName;}
	public String getParentType(){return parentType==null?"":parentType;}
	public String getPhoneNumber1(){return phoneNumber1==null?"":phoneNumber1;}
	public String getPhoneNumber2(){return phoneNumber2==null?"":phoneNumber2;}
	public String getWork(){return work==null?"":work;}
	public String getInn(){return inn==null?"":inn;}
}
