package models.entities.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import play.db.ebean.Model;
import be.objectify.deadbolt.core.models.Permission;

@Entity
@Table(name = "permissions")
public class UserPermission extends Model implements Permission {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Id
	public Long id;

	@Column(name = "permission_value")
	public String value;

	public static final Model.Finder<Long, UserPermission> find = new Model.Finder<Long, UserPermission>(
			Long.class, UserPermission.class);

	@Override
	public String getValue() {
		return value;
	}

	public static UserPermission getByValue(String value) {
		return find.where().eq("value", value).findUnique();
	}
}
