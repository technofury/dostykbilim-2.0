package models.entities.user;

import java.util.*;

import javax.persistence.*;

import models.entities.accounting.Payment;
import models.entities.accounting.PaymentReceipt;
import models.entities.education.Course;
import models.entities.social.*;
import models.logic.social.DistrictLogic;
import models.logic.social.DostykLogic;
import models.logic.social.RegionLogic;
import models.logic.social.SchoolLogic;
import play.data.format.Formats;
import play.data.validation.Constraints.Email;
import play.data.validation.Constraints.MinLength;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import be.objectify.deadbolt.core.models.Permission;
import be.objectify.deadbolt.core.models.Role;
import be.objectify.deadbolt.core.models.Subject;

import com.avaje.ebean.Expr;
import com.avaje.ebean.Page;

import static play.mvc.Controller.session;

@Entity
@Table(name = "users")
public class AuthorisedUser extends Model implements Subject {
	private static final long serialVersionUID = 1L;

	@Id
	public Long id;

	@Required
	public String status = new String("waiting");

	public Date createdDate = new Date();
	
	public Date changedDate = new Date();

	@ManyToMany
	public List<SecurityRole> roles;

	@ManyToMany
	public List<UserPermission> permissions;

	@Email
	/* @Required */
	public String email;

	@MinLength(6)
	@Required
	public String password;

	@Transient
	public String repeatPassword;

	@OneToOne
	public Profile profile;
	
	@OneToOne
	public Requisites requisite = new Requisites();

	@OneToMany(mappedBy = "child", cascade = CascadeType.ALL)
	public List<Parent> parents;

	@ManyToMany(cascade = CascadeType.ALL, targetEntity = Dostyk.class, mappedBy = "users")
	public List<Dostyk> dostyks;

	@ManyToOne
	public Firm firm;

	@OneToMany(cascade = CascadeType.PERSIST, mappedBy = "user", targetEntity = Payment.class)
	public List<Payment> payments;

	@OneToMany(cascade = CascadeType.PERSIST, mappedBy = "user", targetEntity = PaymentReceipt.class)
	public List<PaymentReceipt> receipts;

	public static final Model.Finder<Long, AuthorisedUser> find = new Model.Finder<Long, AuthorisedUser>(
			Long.class, AuthorisedUser.class);

	public AuthorisedUser() {
	}

	public AuthorisedUser(String email, String password) {
		this.email = email;
		this.password = password;
	}

	public static AuthorisedUser getUser(String email, String password) {
		return find.where().eq("email", email).eq("password", password)
				.findUnique();
	}

	public static List<AuthorisedUser> getByFirm(Firm firm){
		return find.where().eq("firm", firm).findList();
	}

	public static AuthorisedUser getUserById(String id, String password) {
		return find.where().eq("profile.idNumber", id).eq("password", password)
				.findUnique();
	}

	public static AuthorisedUser getByIdNumber(String ss) {
		return find.where().eq("profile.idNumber", ss).findUnique();
	}

	public static AuthorisedUser getUserByEmail(String email) {
		return find.where().eq("email", email).findUnique();
	}

	public static AuthorisedUser findByUserName(String userName) {
		return find.where().eq("userName", userName).findUnique();
	}

	// Deadbolt methods
	@Override
	public List<? extends Role> getRoles() {
		return roles;
	}

	@Override
	public List<? extends Permission> getPermissions() {
		return permissions;
	}

	@Override
	public String getIdentifier() {
		return email;
	}

	public static AuthorisedUser getUserById(long id) {
		return find.byId(id);
	}

	public static String paymentStatus(long id) {
		AuthorisedUser user = getUserById(id);

		if (user.payments == null) {
			return "none";
		} else {
		}
		return "none";
	}

	public static List<AuthorisedUser> getBySchool(String region, String district, String school){
		AuthorisedUser user = AuthorisedUser.getUserByEmail(session().get("connected"));
		List<Dostyk> dostyks = null;
		if (user.roles.contains(SecurityRole.getByName("admin"))) {
			dostyks = DostykLogic.getAll();
		}
		if (user.roles.contains(SecurityRole.getByName("moderator")) || user.roles.contains(SecurityRole.getByName("director"))) {
			dostyks = user.dostyks;
		}

		Region r = RegionLogic.find.where().eq("code",region).findUnique();
		District d = DistrictLogic.find.where().eq("code",district).eq("region",r).findUnique();
		School s = SchoolLogic.find.where().eq("code",school).eq("region",r).eq("district",d).findUnique();

		List<AuthorisedUser> studList = AuthorisedUser.find.where()
						.in("dostyks", dostyks)
						.eq("status", "active")
						.eq("profile.region", r)
						.eq("profile.district", d)
						.eq("profile.school", s)
						//.orderBy("profile.lastName")
						.findList();
		Collections.sort(studList, new Comparator<AuthorisedUser>() {
			public int compare(AuthorisedUser u1, AuthorisedUser u2) {
				return u1.profile.lastName.compareTo(u2.profile.lastName);
			}
		});


		return studList;
	}

	public static Page<AuthorisedUser> getJaiPage(List<AuthorisedUser> list,
											   String filter, int page, String sortBy, String order, String grade, String role) {
		List<Long> ids = new ArrayList<Long>();
		for (AuthorisedUser a : list)
			ids.add(a.id);
		if (sortBy.equals("name"))
			sortBy = "profile.firstName";
		if (sortBy.equals("surname"))
			sortBy = "profile.lastName";

		if(!role.equals("all") && grade.equals("all")){
			return find
					.where()
					.in("id", ids)
					.ne("roles.name", "moderator")
					.ne("roles.name", "ten")
					.eq("roles.name",role)
					.or(Expr.or(Expr.ilike("profile.lastName", "%" + filter + "%"),
							Expr.ilike("profile.firstName", "%" + filter + "%")),
							Expr.or(Expr.ilike("profile.lastName", "%"
											+ fromLatin(filter) + "%"),
									Expr.ilike("profile.firstName", "%"
											+ fromLatin(filter) + "%")))
					.orderBy(sortBy + " " + order).orderBy("id desc")
					.findPagingList(30).getPage(page);
		}else if(role.equals("all") && !grade.equals("all")) {
			return find
					.where()
					.in("id", ids)
					.ne("roles.name", "moderator")
					.ne("roles.name", "ten")
					.eq("profile.course.classNumber",Integer.parseInt(grade))
					.or(Expr.or(Expr.ilike("profile.lastName", "%" + filter + "%"),
							Expr.ilike("profile.firstName", "%" + filter + "%")),
							Expr.or(Expr.ilike("profile.lastName", "%"
											+ fromLatin(filter) + "%"),
									Expr.ilike("profile.firstName", "%"
											+ fromLatin(filter) + "%")))
					.orderBy(sortBy + " " + order).orderBy("id desc")
					.findPagingList(30).getPage(page);

		}else if(!role.equals("all") && !grade.equals("all")) {

			return find
					.where()
					.in("id", ids)
					.ne("roles.name", "moderator")
					.ne("roles.name", "ten")
					.eq("roles.name",role)
					.eq("profile.course.classNumber",Integer.parseInt(grade))
					.or(Expr.or(Expr.ilike("profile.lastName", "%" + filter + "%"),
							Expr.ilike("profile.firstName", "%" + filter + "%")),
							Expr.or(Expr.ilike("profile.lastName", "%"
											+ fromLatin(filter) + "%"),
									Expr.ilike("profile.firstName", "%"
											+ fromLatin(filter) + "%")))
					.orderBy(sortBy + " " + order).orderBy("id desc")
					.findPagingList(30).getPage(page);
		}else{
				return find
						.where()
						.in("id", ids)
						.ne("roles.name", "moderator")
						.ne("roles.name", "ten")
						.or(Expr.or(Expr.ilike("profile.lastName", "%" + filter + "%"),
								Expr.ilike("profile.firstName", "%" + filter + "%")),
								Expr.or(Expr.ilike("profile.lastName", "%"
												+ fromLatin(filter) + "%"),
										Expr.ilike("profile.firstName", "%"
												+ fromLatin(filter) + "%")))
						.orderBy(sortBy + " " + order).orderBy("id desc")
						.findPagingList(30).getPage(page);
			}


	}

	public static Page<AuthorisedUser> getTenPage(List<AuthorisedUser> list,
												  String filter, int page, String sortBy, String order) {
		List<Long> ids = new ArrayList<Long>();
		for (AuthorisedUser a : list)
			ids.add(a.id);
		if (sortBy.equals("name"))
			sortBy = "profile.firstName";
		if (sortBy.equals("surname"))
			sortBy = "profile.lastName";
		// System.err.println(filter);
		return find
				.where()
				.in("id", ids)
				.eq("roles.name", "ten")
				.or(Expr.or(Expr.ilike("profile.lastName", "%" + filter + "%"),
						Expr.ilike("profile.firstName", "%" + filter + "%")),
						Expr.or(Expr.ilike("profile.lastName", "%"
										+ fromLatin(filter) + "%"),
								Expr.ilike("profile.firstName", "%"
										+ fromLatin(filter) + "%")))
				.orderBy(sortBy + " " + order).orderBy("id desc")
				.findPagingList(30).getPage(page);
	}

	public static Page<AuthorisedUser> getPage(List<AuthorisedUser> list,
			String filter, int page, String sortBy, String order) {
		List<Long> ids = new ArrayList<Long>();
		for (AuthorisedUser a : list)
			ids.add(a.id);
		if (sortBy.equals("name"))
			sortBy = "profile.firstName";
		if (sortBy.equals("surname"))
			sortBy = "profile.lastName";
		// System.err.println(filter);
		return find
				.where()
				.in("id", ids)
				.or(Expr.or(Expr.ilike("profile.lastName", "%" + filter + "%"),
						Expr.ilike("profile.firstName", "%" + filter + "%")),
						Expr.or(Expr.ilike("profile.lastName", "%"
								+ fromLatin(filter) + "%"),
								Expr.ilike("profile.firstName", "%"
										+ fromLatin(filter) + "%")))
										.orderBy(sortBy + " " + order).orderBy("id desc")
										.findPagingList(30).getPage(page);
	}

	private static String fromLatin(String filter) {
		String alp = "abcdefghijklmnopqrstuvwxyz";
		String kaz = "абцдефгхижклмнопқрстувшхыз";
		filter = filter.toLowerCase();
		String ret = "";
		for (int i = 0; i < filter.length(); i++) {
			int j = alp.indexOf(filter.charAt(i));
			if (j < 0)
				return "$$";
			ret = ret + kaz.charAt(j);
		}
		return ret;
	}

	public static List<AuthorisedUser> getTeachers(long id) {
		/*
		 * List<AuthorisedUser> list = new ArrayList<AuthorisedUser>(); for
		 * (AuthorisedUser user : find.all()) { boolean ok = false; for
		 * (SecurityRole role : user.roles) { if (role.name.equals("teacher"))
		 * ok = true; } if (!ok) continue; for (Dostyk dostyk : user.lessons) if
		 * (dostyk.id==id) ok = true; if (ok) list.add(user); } return list;
		 */
		return find.where().eq("roles.name", "teacher").eq("lessons.id", id)
				.findList();
	}

	public static List<AuthorisedUser> getByElectiveGroup(
			DostykGroup electiveGroup) {
		return find.where().eq("profile.electiveGroup", electiveGroup)
				.findList();
	}

	public static List<AuthorisedUser> getByDostykGroup(
			DostykGroup dostykGroup, String paymentStatus, List<String> statuses) {
		if (!paymentStatus.equals("all"))
			return find.where().eq("profile.dostykGroup", dostykGroup)
					.eq("payments.status", paymentStatus)
					.in("status", statuses).findList();
		else
			return find.where().eq("profile.dostykGroup", dostykGroup)
					.in("status", statuses).findList();
	}

	public static List<AuthorisedUser> getByDostykGroup(DostykGroup dostykGroup) {
		return find.where().eq("profile.dostykGroup", dostykGroup).findList();
	}

	public static List<AuthorisedUser> getModers() {
		return find.where().eq("roles.name", "moderator")
				.eq("status", "active").findList();
	}

	public static List<AuthorisedUser> getDirectors() {
		return find.where().eq("roles.name", "director").eq("status", "active")
				.findList();
	}

	public static List<AuthorisedUser> getDirectorsByDostyk(Dostyk dostyk){
		return find.where().eq("roles.name", "director").eq("status", "active").eq("dostyks", dostyk).findList();
	}

	public String getModerByDostyk(Long id){
		Dostyk dostyk = Dostyk.find.byId(id);
		AuthorisedUser user = find.where().eq("roles.name", "moderator").eq("status", "active").eq("dostyks", dostyk).findUnique();
		return user.profile.lastName + " " + user.profile.firstName;
	}

	public static List<AuthorisedUser> getTeachersByDostyk(Dostyk dostyk) {
		return find.where().eq("roles.name", "teacher").eq("dostyks", dostyk)
				.findList();
	}

	public static List<AuthorisedUser> getByDostyk(Dostyk dostyk) {
		return find.where().eq("dostyks", dostyk).findList();
	}

	public static int getCountWhichIdStarts(String dostykCode) {
		return find.where().ilike("profile.idNumber", dostykCode + "%")
				.findRowCount();
	}
	public static String getStudentId(){
		AuthorisedUser user = AuthorisedUser.find.where().eq("email",session("connected")).findUnique();
		return user.profile.idNumber;
	}



    public String getFirmName(){
        return this.firm==null?"":this.firm.name==null?"":this.firm.name;
    }
}