package models.entities.ten;

import models.entities.social.Dostyk;
import models.entities.user.AuthorisedUser;
import models.entities.user.Profile;
import play.db.ebean.Model;

import javax.persistence.*;
import javax.validation.constraints.Past;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by technoDev on 31/03/2016.
 */
@Entity
@Table(name = "exam_sessions")
public class ExamSession extends Model {
    private static final long serialVersionUID = 1L;

    public static final Model.Finder<Long, ExamSession> find = new Model.Finder<Long, ExamSession>(
            Long.class, ExamSession.class);

    @Id
    public Long id;

    @ManyToOne
    public Dostyk dostyk;

//
//    @OneToMany
//    java.util.List<AuthorisedUser> authorisedUsers = new ArrayList<>();
/*
    public String sessionTime1;
    public String sessionTime2;
    public String sessionTime3;
*/
    public Integer session = 0;
    public Integer sessionMax = 0;

    public String status = "available";

    @OneToMany(targetEntity = Profile.class)
    public List<Profile> profiles = new ArrayList<>();
//    @Transient
//    public static java.util.List<ExamSession> findList(){
//        return ExamSession.find.where().order("dostyk.name").findList();
//    }

    @Transient
    public static  java.util.List<ExamSession>  findByDostyk(Dostyk dostyk){
        return ExamSession.find.where().eq("dostyk",dostyk).order("session").findList();
    }
}

