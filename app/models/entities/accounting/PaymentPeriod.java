package models.entities.accounting;

/**
 * @author balancy
 */

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import play.data.format.Formats;
import play.db.ebean.Model;
import play.i18n.Messages;

@Entity
@Table(name = "payment_periods")
public class PaymentPeriod extends Model {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Id
	public Long id;

	@ManyToOne
	public Payment payment;

	public int period;

	public double plannedPayment = 0.0;

	@Formats.DateTime(pattern = "dd/MM/yy")
	public Date plannedPaymentDate = new Date();

	public double payedPayment = 0.0;

	@Formats.DateTime(pattern = "dd/MM/yy")
	public Date payedPaymentDate = new Date();

	public double debtPayment = 0.0;

	public String status = "PLANNED"; // DO NOT EDIT

	public String comment = Messages.get("comment");

	@OneToOne
	public PaymentReceipt receipt;
}
