package models.entities.accounting;

import play.data.format.Formats;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by User on 23-Feb-17.
 */
@Entity
@Table(name = "holiday")
public class Holiday extends Model {

    private static final long serialVersionUID = 1L;
    public static Model.Finder<Long,Holiday> find = new Model.Finder<Long, Holiday>(Long.class,Holiday.class);

    @Id
    public Long id;

    @Formats.DateTime(pattern = "yyyy-MM-dd")
    public Date startDate = new Date();

    @Formats.DateTime(pattern = "yyyy-MM-dd")
    public Date endDate = new Date();

}
