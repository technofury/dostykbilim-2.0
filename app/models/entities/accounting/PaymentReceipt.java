package models.entities.accounting;

/**
 * @author balancy
 */

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import models.entities.social.Dostyk;
import models.entities.user.AuthorisedUser;
import play.data.format.Formats;
import play.db.ebean.Model;

@Entity
@Table(name = "payment_receipts")
public class PaymentReceipt extends Model {
	private static final long serialVersionUID = 1L;

	@Id
	public Long id;

	public int number = 0;

	@ManyToOne
	public AuthorisedUser user;

	@ManyToOne
	public Payment payment;

	@ManyToOne
	public Dostyk dostyk;

	@Formats.DateTime(pattern = "dd/MM/yy")
	public Date createdDate = new Date();

	@OneToOne
	public PaymentPeriod period;
}
