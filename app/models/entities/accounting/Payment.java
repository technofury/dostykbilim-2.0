package models.entities.accounting;

/**
 * @author balancy
 */

import java.util.Date;
import java.util.List;

import javax.persistence.*;

import models.entities.user.AuthorisedUser;
import models.entities.user.Profile;
import models.logic.accounting.PaymentReceiptLogic;
import play.data.format.Formats;
import play.db.ebean.Model;
import play.i18n.Messages;

@Entity
@Table(name = "payments")
public class Payment extends Model {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Id
	public Long id;

	@ManyToOne
	public AuthorisedUser user;

	public String contract = "";
	
	//@Formats.DateTime(pattern = "dd/MM/yy")
	public Date contractDate = new Date();
	
	public String status = "CURRENT";

	@Formats.DateTime(pattern = "dd/MM/yy")
	public Date startDate = new Date();

	@Formats.DateTime(pattern = "dd/MM/yy")
	public Date endDate = new Date();

	public double materialCost = 0.0;

	public double plannedPayment = 0.0;

	public double payedPayment = 0.0;

	public double debtPayment = 0.0;

	public int discount;

	public double discountBonus;

	public double hoursPerWeek = 0.0;

	public double pricePerWeek = 0.0;

	public int weeks = 0;

	public String comment = Messages.get("comment");

	@OneToMany(cascade = CascadeType.PERSIST, mappedBy = "payment", targetEntity = PaymentPeriod.class)
	public List<PaymentPeriod> periods;

	@OneToOne
	public DiscountMax discountsMaxes;

	@OneToMany(cascade = CascadeType.PERSIST, mappedBy = "payment", targetEntity = PaymentReceipt.class)
	public List<PaymentReceipt> receipts;

	@OneToMany
	public List<PaymentSuspend> paymentSuspends;

	@Transient
	public List<PaymentSuspend> findSuspends(){
		return PaymentSuspend.find.where().eq("payment",this).order("startDate").findList();
	}

	@Transient
	public PaymentReceipt findLast(){
		return PaymentReceiptLogic.find.where()
				.eq("payment", this).order().desc("id").setMaxRows(1).findUnique();
	}
}
