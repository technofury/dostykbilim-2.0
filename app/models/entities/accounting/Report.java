package models.entities.accounting;

/**
 * Written by rsa on 14.01.2015.
 */
public class Report {
	public Report() {
	}

	public long id, dostykId;
	public String name, phone;
	public int studentSize;
	public double plannedPayment, payedPayment, debtPayment;
	public String sPlannedPayment, sPayedPayment, sDebtPayment;
	public int percentage;
}
