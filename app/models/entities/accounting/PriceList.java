package models.entities.accounting;

/**
 * @author balancy
 */

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import models.entities.education.CourseType;
import models.entities.social.Dostyk;
import play.db.ebean.Model;

@Entity
@Table(name = "price_list")
public class PriceList extends Model {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Id
	public Long id;

	@ManyToOne
	public Dostyk dostyk;

	@ManyToOne
	public CourseType courseType;

	public int hours = 0;

	public double price = 0.0;
}
