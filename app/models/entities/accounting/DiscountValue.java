package models.entities.accounting;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import models.entities.social.Dostyk;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;

/**
 * Created with IntelliJ IDEA. User: Арсен Date: 18.05.13 Time: 16:39 To change
 * this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "discounts_values")
public class DiscountValue extends Model {
	private static final long serialVersionUID = 1L;

	@Id
	public Long id;

	@Required
	@ManyToOne
	public Dostyk dostyk;

	@Required
	public int value;

	@Required
	@ManyToOne
	public DiscountMax dmax;
}
