package models.entities.accounting;

import models.entities.accounting.Payment;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by techno on 8/4/16.
 */
@Entity
@Table(name="payment_suspends")
public class PaymentSuspend extends Model{
    private static final long serialVersionUID = 1L;

    public static Model.Finder<Long, PaymentSuspend> find = new Model.Finder<Long, PaymentSuspend>(
            Long.class, PaymentSuspend.class);

    @Id
    public Long id;

    @ManyToOne
    public Payment payment;

    @ManyToOne
    public PaymentPeriod paymentPeriod;

    public String reason;

    public Date startDate = new Date();

    public Date endDate;

    public Double suspendPayment = 0.0;

    public Double periodPayment = 0.0;

    public Double studyPrice = 0.0;

    public Double diffPayment = 0.0;

}
