package models.entities.accounting;

/**
 * @author balancy
 */

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import play.data.validation.Constraints.Required;
import play.db.ebean.Model;

@Entity
@Table(name = "discounts_maxes")
public class DiscountMax extends Model {

	private static final long serialVersionUID = 1L;

	@Id
	public Long id;

	@Required
	@Column(unique = true)
	public String nameKz;

	@Required
	@Column(unique = true)
	public String nameRu;

	@Required
	public Integer value;

	public Integer maxUserSize;

	public Integer active;

	public DiscountMax() {
	}
}