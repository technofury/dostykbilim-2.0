package models.rest;

import models.entities.social.DostykGroup;
import models.entities.user.AuthorisedUser;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.Date;
import java.util.List;

/**
 *
 */
@Entity
@Table(name = "notifications")
public class Notification extends Model {
    private static final long serialVersionUID = 1L;
    //entity
    @Id
    public Long id;

    @Constraints.Required
    public String message;

    public Date date;

    //relations
    @OneToOne
    public AuthorisedUser user;

    @OneToOne
    public AuthorisedUser teacher;
    //logic
   public static final Finder<Long, Notification> find = new Finder<Long, Notification>(Long.class, Notification.class);
    public static List<Notification> getByInfo(AuthorisedUser teacher, AuthorisedUser student){
        return find.where().eq("user",student).eq("teacher",teacher).findList();
    }
    public static List<Notification> getByInfo(Date date, AuthorisedUser teacher, AuthorisedUser student, String message){
        return find.where().eq("user",student).eq("teacher",teacher).eq("date",date).eq("message",message).findList();
    }
    public static List<Notification> getByInfo(AuthorisedUser teacher, AuthorisedUser student, String message){
        return find.where().eq("user",student).eq("teacher",teacher).eq("message",message).findList();
    }
}