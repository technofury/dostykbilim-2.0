package models.rest;

import models.entities.user.AuthorisedUser;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Written by Arsen on 15.04.2015.
 */
@Entity
@Table(name = "rest_users")
public class User extends Model {
    private static final long serialVersionUID = 1L;
    //entity
    @Id
    public Long id;

    @Constraints.Required
    public String token;

    public String deviceId;

    public String osType;

    //relations
    @OneToOne
    public AuthorisedUser user;

    //logic
    public static final Finder<Long, User> find = new Finder<Long, User>(Long.class, User.class);

    public static User getByUser(AuthorisedUser user){
        return find.where().eq("user", user).findUnique();
    }
}