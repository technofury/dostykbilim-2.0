package models.printable;

import java.util.List;

/**
 * Created by arsen on 21/04/2016.
 */
public class CashBookPage {
    public String date, ostatokBegin, ostatokEnd, total, endDate, checkDate;
    public float dateTotal;
    public int bookCount = 0;
    public List<CashBook> books;
}
