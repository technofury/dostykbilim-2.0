package models.printable;

import java.util.List;

/**
 * Created by arsen on 12/04/2016.
 */
public class NewRevenue {
    public Long id;
    public String firm = "";
    public String student ="";
    public List<Deal> deals;
}
