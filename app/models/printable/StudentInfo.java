package models.printable;

/**
 * Created by arsen on 11/04/2016.
 */
public class StudentInfo {
    public Long id;
    public String index, dealNumber, beginDate, endDate, gender, firstName, lastName,
            school, region, birthDate, birthMonth, birthYear, phoneMobile,
            phoneMother,motherName, phoneFather, fatherName,phoneCustomer,customerWork, customerName, grade, language, comment,
            group, kazakhLanguage, russianLanguage, math, history, elective, mail,
    discount;
    public double sep=0, okt=0, nov=0, dec=0, jan=0, feb=0, mar=0, apr=0, may=0, refund=0,
    monthlyPayment=0, totalDebt=0, totalPayed=0, totalPlanned=0, das=0;
}