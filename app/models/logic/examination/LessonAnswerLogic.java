package models.logic.examination;

import java.util.List;

import models.entities.education.Lesson;
import models.entities.examination.LessonAnswer;
import play.db.ebean.Model.Finder;

public class LessonAnswerLogic {
	public static Finder<Long, LessonAnswer> find = new Finder<Long, LessonAnswer>(
			Long.class, LessonAnswer.class);

	public static LessonAnswer getById(Long id) {
		return find.byId(id);
	}

	public static List<LessonAnswer> getAll() {
		return find.all();
	}

	public static void deleteById(long id) {
		find.byId(id).delete();
	}

	public static LessonAnswer getBy(String variant, Lesson lesson) {
		return find.where().eq("answerKey.variant", variant)
				.eq("lesson", lesson).findUnique();
	}
}
