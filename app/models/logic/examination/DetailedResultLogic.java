package models.logic.examination;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import models.entities.education.Lesson;
import models.entities.examination.Answer;
import models.entities.examination.AnswerKey;
import models.entities.examination.DetailedResult;
import models.entities.examination.ExamResult;
import models.entities.examination.LessonAnswer;
import play.db.ebean.Model.Finder;

public class DetailedResultLogic {
	public static Finder<Long, DetailedResult> find = new Finder<Long, DetailedResult>(
			Long.class, DetailedResult.class);

	public static DetailedResult getById(Long id) {
		return find.byId(id);
	}

	public static List<DetailedResult> getAll() {
		return find.all();
	}

	public static void deleteById(long id) {
		find.byId(id).delete();
	}

	public static DetailedResult getByExamResultAndLesson(
			ExamResult examResult, Lesson lesson) {
		return find.where().eq("examResult", examResult).eq("lesson", lesson)
				.findUnique();
	}

	public static List<DetailedResult> getByExamResult(ExamResult examResult) {
		return find.where().eq("examResult", examResult).orderBy("lesson.code")
				.findList();
	}

	public static List<String> getCorrect(DetailedResult res) {
		List<AnswerKey> key = AnswerKeyLogic
				.getByVariant(res.examResult.variant);

		if (key.size() == 0)
			return new ArrayList<String>();
		AnswerKey cur = key.get(0);
		for (LessonAnswer byLesson : cur.lessonAnswers) {
			if (res.lesson.id == byLesson.lesson.id) {
				List<Answer> list = byLesson.answers;
				Collections.sort(list);
				List<String> ret = new ArrayList<String>();
				for (Answer a : list) {
					ret.add(a.answerCode + a.topic + "");
				}
				return ret;
			}
		}
		return new ArrayList<String>();
	}

	public static String getTopic(DetailedResult res, int which) {
		List<AnswerKey> key = AnswerKeyLogic
				.getByVariant(res.examResult.variant);
		if (key.size() == 0)
			return "";
		AnswerKey cur = key.get(0);
		for (LessonAnswer byLesson : cur.lessonAnswers) {
			if (res.lesson.id == byLesson.lesson.id) {
				List<Answer> list = byLesson.answers;
				for (Answer a : list) {
					if (a.questionNumber == which + 1)
						return a.topic;
				}
			}
		}
		return "";
	}

	public static Character getAnswer(DetailedResult res, int which) {
		List<AnswerKey> key = AnswerKeyLogic
				.getByVariant(res.examResult.variant);

		if (key.size() == 0)
			return '!';
		AnswerKey cur = key.get(0);
		for (LessonAnswer byLesson : cur.lessonAnswers) {
			if (res.lesson.id == byLesson.lesson.id) {
				List<Answer> list = byLesson.answers;
				for (Answer a : list) {
					if (a.questionNumber == which + 1)
						return a.answerCode.charAt(0);
				}
			}
		}
		return '!';
	}

}
