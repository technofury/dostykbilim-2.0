package models.logic.examination;

import java.util.List;

import models.entities.examination.Layout;
import play.db.ebean.Model.Finder;

public class LayoutLogic {
	public static Finder<Long, Layout> find = new Finder<Long, Layout>(
			Long.class, Layout.class);

	public static Layout getById(Long id) {
		return find.byId(id);
	}

	public static List<Layout> getAll() {
		return find.all();
	}

	public static void deleteById(long id) {
		find.byId(id).delete();
	}
}
