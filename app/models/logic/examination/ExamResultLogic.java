package models.logic.examination;

import java.util.List;

import models.entities.examination.Exam;
import models.entities.examination.ExamResult;
import models.entities.social.Dostyk;
import models.entities.social.DostykGroup;
import models.entities.user.AuthorisedUser;
import play.db.ebean.Model.Finder;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlQuery;
import com.avaje.ebean.SqlRow;

public class ExamResultLogic {
	public static Finder<Long, ExamResult> find = new Finder<Long, ExamResult>(
			Long.class, ExamResult.class);

	public static ExamResult getById(Long id) {
		return find.byId(id);
	}

	public static List<ExamResult> getAll() {
		return find.all();
	}

	public static void deleteById(long id) {
		find.byId(id).delete();
	}

	public static int getAttendedStudentsSize(Exam exam, Dostyk dostyk) {
		String sql = "SELECT count(id) as count FROM exam_results er WHERE exam_id="
				+ exam.id
				+ "AND student_id in (SELECT users_id FROM dostyks_users WHERE dostyks_id = "
				+ dostyk.id + ")";
		SqlQuery sqlQuery = Ebean.createSqlQuery(sql);
		SqlRow row = sqlQuery.findUnique();
		return row.get("count") != null ? Integer.parseInt(row.get("count")
				.toString()) : 0;
	}

	public static ExamResult getByExamAndUser(Exam exam, AuthorisedUser user) {
		return find.where().eq("exam", exam).eq("student", user).findUnique();
	}

	public static List<ExamResult> getByExamAndStudents(Exam exam,
			List<AuthorisedUser> students) {
		return find.where().eq("exam", exam).in("student", students).findList();
	}

	public static List<ExamResult> getByExam(Exam exam) {
		return find.where().eq("exam", exam).findList();
	}

	public static List<ExamResult> getByExamAndByDostykGroup2(Exam exam,
			DostykGroup group) {
		return find.where().eq("exam", exam)
				.eq("student.profile.dostykGroup", group).order().desc("total")
				.findList();
	}

	public static List<ExamResult> getByExamAndByDostyk2(Exam exam,
			Dostyk dostyk) {
		return find.where().eq("exam", exam).eq("student.lessons", dostyk)
				.order().desc("total").findList();
	}

	public static double getDostykAverage(Exam exam, Dostyk dostyk) {
		String sql = "SELECT AVG(er.total) as avg FROM exam_results er WHERE exam_id="
				+ exam.id
				+ "AND student_id in (SELECT users_id FROM dostyks_users WHERE dostyks_id = "
				+ dostyk.id + ")";
		SqlQuery sqlQuery = Ebean.createSqlQuery(sql);
		SqlRow row = sqlQuery.findUnique();
		double average = row.get("avg") != null ? Double.parseDouble(row.get(
				"avg").toString()) : 0.0;
		return average;
	}

	public static List<ExamResult> getByUser(AuthorisedUser user) {
		return find.where().eq("student", user).order("exam.endDate")
				.findList();
	}

}
