package models.logic.examination;

import java.util.List;

import models.entities.examination.ExamType;
import play.db.ebean.Model.Finder;

public class ExamTypeLogic {
	public static Finder<Long, ExamType> find = new Finder<Long, ExamType>(
			Long.class, ExamType.class);

	public static ExamType getById(Long id) {
		return find.byId(id);
	}

	public static List<ExamType> getAll() {
		return find.all();
	}

	public static void deleteById(long id) {
		find.byId(id).delete();
	}
}
