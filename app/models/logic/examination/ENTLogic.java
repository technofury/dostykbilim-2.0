package models.logic.examination;

import java.util.List;

import models.entities.examination.ENT;
import models.entities.user.AuthorisedUser;
import play.db.ebean.Model.Finder;

public class ENTLogic {
	public static Finder<Long, ENT> find = new Finder<Long, ENT>(Long.class,
			ENT.class);

	public static ENT getById(Long id) {
		return find.byId(id);
	}

	public static List<ENT> getAll() {
		return find.all();
	}

	public static void deleteById(long id) {
		find.byId(id).delete();
	}

	public static ENT getByUser(AuthorisedUser u) {
		return find.where().eq("user", u).findUnique();
	}
}
