package models.logic.examination;

import java.util.List;

import models.entities.examination.AnswerKey;
import models.entities.examination.Exam;
import play.db.ebean.Model.Finder;

public class AnswerKeyLogic {
	public static Finder<Long, AnswerKey> find = new Finder<Long, AnswerKey>(
			Long.class, AnswerKey.class);

	public static AnswerKey getById(Long id) {
		return find.byId(id);
	}

	public static List<AnswerKey> getAll() {
		return find.all();
	}

	public static void deleteById(long id) {
		find.byId(id).delete();
	}

	public static List<AnswerKey> getByVariant(String ss) {
		return find.where().eq("variant", ss).findList();
	}

	public static List<AnswerKey> getByExam(Exam exam){ return find.where().eq("exam", exam).findList();}

	public static List<AnswerKey> getByInfo(Exam exam, String ss){
		return find.where()
				.eq("exam", exam)
				.eq("variant", ss)
				.findList();
	}
}
