package models.logic.examination;

import java.util.List;

import models.entities.examination.Block;
import models.entities.examination.Layout;
import play.db.ebean.Model.Finder;

public class BlockLogic {
	public static Finder<Long, Block> find = new Finder<Long, Block>(
			Long.class, Block.class);

	public static Block getById(Long id) {
		return find.byId(id);
	}

	public static List<Block> getAll() {
		return find.all();
	}

	public static List<Block> getByLayout(Layout layout) {
		return find.where().eq("layout", layout).findList();
	}

	public static void deleteById(long id) {
		find.byId(id).delete();
	}
}
