package models.logic.examination;

import java.util.List;

import models.entities.examination.DetailedResult;
import models.entities.examination.Exam;
import models.entities.examination.ExamResult;
import models.entities.examination.ExamType;
import play.db.ebean.Model.Finder;

public class ExamLogic {
	public static Finder<Long, Exam> find = new Finder<Long, Exam>(Long.class,
			Exam.class);

	public static Exam getById(Long id) {
		return find.byId(id);
	}

	public static List<Exam> getAll() {
		return find.where().order().desc("startDate, endDate ").findList();
	}

	public static List<Exam> findAll() {
		return find.where().ne("status", "deleted").order().desc("startDate")
				.findList();
	}

	public static List<Exam> getByGrade(int grade) {
		return find.where().eq("grade", grade).findList();
	}

	public static List<Exam> getByGradeSorted(int grade) {
		return find.where().eq("grade", grade).orderBy("examType, startDate")
				.findList();
	}

	public static void deleteById(long id) {
		Exam exam = find.byId(id);
		List<ExamResult> examResults = ExamResultLogic.getByExam(exam);
		for (ExamResult er : examResults) {
			for (DetailedResult dr : er.detailedResults) {
				dr.delete();
			}
			er.delete();
		}
		exam.delete();
	}

	public static int getCount(ExamType examType) {
		return find.where().eq("examType", examType).findList().size();
	}

	public static List<Exam> getActive() {
		return find.where().eq("status", "active").findList();
	}

	public static List<Exam> getByGradeAndType(int grade, int type) {
		if (type == 0) {
			return find.where().eq("grade", grade).ilike("name", "%П%")
					.orderBy("examType, startDate").findList();
		} else {
			List<Exam> l = find.where().eq("grade", grade)
					.ilike("name", "%ДАС%").orderBy("examType, startDate")
					.findList();
			l.addAll(find.where().eq("grade", grade).ilike("name", "%ДАБ%")
					.orderBy("examType, startDate").findList());
			return l;
		}
	}
}
