package models.logic.accounting;

import models.entities.accounting.Payment;
import models.entities.user.AuthorisedUser;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by Arsen on 4/1/2016.
 */
@Entity
@Table(name="contracts")
public class Contract extends Model {

    @Id
    public Long id;

    @ManyToOne
    public AuthorisedUser student;

    @ManyToOne
    public Payment payment;
    public int refund;
    public String number,prefix,comment;
    public Date beginDate, endDate;
    //private istegen sebebim, baska jerden durus emes koldanbasyn dep: Nullpointer, IndexOutsOfBounds degendei
    //exceptiondardy azaitu maksatynda. Tek berilgen funciyalardy koldanu kerek. jetispese osynda jazu kerek
    private static Finder<Long, Contract> find = new Finder<>(Long.class, Contract.class);
    public static Contract getById(Long id){
        return find.byId(id);
    }
    public static List<Contract> getByStudent(AuthorisedUser student){
        return find.where().eq("student", student).orderBy("beginDate").findList();
    }
    public static List<Contract> getByPayment(Payment payment){
        return find.where().eq("payment", payment).orderBy("beginDate").findList();
    }
    public static Contract getByNumber(String number){
        return find.where().eq("number",number).findUnique();
    }
    public static List<Contract> getAll(){
        return find.all();
    }
    public static List<Contract> getByInfo(AuthorisedUser student, String prefix){
        return find.where().eq("student", student).eq("prefix", prefix).findList();
    }
}