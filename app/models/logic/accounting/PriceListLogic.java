package models.logic.accounting;

import java.util.List;

import models.entities.accounting.PriceList;
import models.entities.education.CourseType;
import models.entities.social.Dostyk;
import play.db.ebean.Model.Finder;

public class PriceListLogic {

	public static Finder<Long, PriceList> find = new Finder<Long, PriceList>(
			Long.class, PriceList.class);

	public static List<PriceList> getAll(Dostyk dostyk, CourseType courseType) {
		return find.where().eq("dostyk_id", dostyk.id)
				.eq("course_type_id", courseType.id).orderBy("hours")
				.findList();
	}

	public static PriceList getById(long id) {
		return find.byId(id);
	}

	public static PriceList getByHours(int hours) {
		return find.where().eq("hours", hours).findUnique();
	}

	public static List<PriceList> getByDostykAndCourse(Dostyk dostyk,
			CourseType courseType) {
		return find.where().eq("dostyk", dostyk).eq("courseType", courseType)
				.findList();
	}

	public static boolean delete(PriceList t) {
		try {
			t.delete();
		} catch (Exception e) {
		}
		return false;
	}

	public static boolean update(PriceList t) {
		t.update();
		return false;
	}

	public static boolean create(PriceList t) {
		try {
			t.save();
		} catch (Exception ex) {
			PriceList lh = getById(t.id);
			lh.hours = t.hours;
			lh.price = t.price;
			lh.save();
		}

		return false;
	}

}
