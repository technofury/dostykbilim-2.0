package models.logic.accounting;

import java.util.List;

import models.entities.accounting.DiscountMax;
import play.db.ebean.Model.Finder;

public class DiscountMaxLogic {
	public static Finder<Long, DiscountMax> find = new Finder<Long, DiscountMax>(
			Long.class, DiscountMax.class);

	public static List<DiscountMax> getAllOrderByValue() {
		return find.where().eq("active",1).orderBy("value").findList();
	}

	public static List<DiscountMax> getAllOrderById() {
		return find.where().eq("active",1).orderBy("id desc").findList();
	}

	public static DiscountMax getByName(String name) {
		return find.where().eq("active",1).eq("name_kz", name).findUnique();
	}

	public static DiscountMax getById(Long id) {
		return find.where().eq("active",1).eq("id",id).findUnique();
	}
}
