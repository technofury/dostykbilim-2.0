package models.logic.accounting;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import models.entities.accounting.Payment;
import models.entities.accounting.PaymentPeriod;
import models.entities.accounting.PaymentReceipt;
import models.entities.social.Dostyk;
import models.entities.social.Firm;
import models.entities.user.AuthorisedUser;
import play.db.ebean.Model.Finder;

/**
 * Created with IntelliJ IDEA. User: dostyk5 Date: 31.07.13 Time: 12:59 To
 * change this template use File | Settings | File Templates.
 */
public class PaymentReceiptLogic {
	public static Finder<Long, PaymentReceipt> find = new Finder<Long, PaymentReceipt>(
			Long.class, PaymentReceipt.class);

	public static PaymentReceipt getById(Long id) {
		return find.byId(id);
	}

	public static List<PaymentReceipt> getByUser(AuthorisedUser user) {
		return find.where().eq("user", user).orderBy("createdDate")
				.orderBy("id").findList();
	}

	public static List<PaymentReceipt> getReceipts(Payment payment) {
		return find.where().eq("payment", payment).orderBy("createdDate")
				.findList();
	}

	public static PaymentReceipt getByPeriod(PaymentPeriod per) {
		return find.where().eq("paymentPeriod", per).findUnique();
	}

	public static List<PaymentReceipt> getByDostyk(Dostyk dostyk, String year)
			throws Exception {
		DateFormat dt = new SimpleDateFormat("yy-MM-dd");
		Date jj = dt.parse(year + "-01-01");
		return find.where().eq("dostyk", dostyk).gt("createdDate", jj)
				.orderBy("createdDate").findList();
	}
	public static List<PaymentReceipt> getInRange(Dostyk dostyk, Date begin, Date end, Firm firm, String firmStatus){

		if(!firmStatus.equals("all")) {

			return find.where()
					.eq("dostyk", dostyk)
					.eq("user.dostyks",dostyk)
					.eq("user.firm", firm)
					.between("createdDate", begin, end)
					.orderBy("createdDate")
					.findList();
		}else{

			return find.where()
					.eq("dostyk", dostyk)
					.eq("user.dostyks",dostyk)
					.between("createdDate", begin, end)
					.orderBy("createdDate")
					.findList();
		}
	}
}
