package models.logic.accounting;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import models.entities.accounting.DiscountMax;
import models.entities.accounting.DiscountValue;
import models.entities.accounting.Payment;
import models.entities.social.Dostyk;
import models.entities.user.AuthorisedUser;
import play.db.ebean.Model;

import static play.mvc.Controller.session;

/**
 * Created with IntelliJ IDEA. User: Арсен Date: 18.05.13 Time: 16:40 To change
 * this template use File | Settings | File Templates.
 */
public class DiscountValueLogic {
	public static Model.Finder<Long, DiscountValue> find = new Model.Finder<Long, DiscountValue>(
			Long.class, DiscountValue.class);

	public static List<DiscountValue> getAll() {
		List<DiscountMax> discountMaxList = DiscountMaxLogic.getAllOrderById();
		return find.where().in("dmax",discountMaxList).orderBy("value").findList();
	}

	public static List<DiscountValue> getAllByDostykById(Dostyk dos) {
		List<DiscountMax> discountMaxList = DiscountMaxLogic.getAllOrderById();
		return find.where().eq("dostyk", dos).in("dmax",discountMaxList).orderBy("id").findList();
	}

	public static List<DiscountValue> getAllByDostykByValue(Dostyk dos) {

		List<DiscountValue> disList = new ArrayList<>();

		List<AuthorisedUser> users = AuthorisedUser.getByDostyk(dos);
		List<DiscountMax> discountMaxList = DiscountMaxLogic.getAllOrderById();
		List<DiscountValue> discountValues = find.where().eq("dostyk", dos).in("dmax",discountMaxList).orderBy("value").findList();
		for(DiscountValue discounts:discountValues){
			if(discounts.dmax.maxUserSize != null && !discounts.dmax.maxUserSize.toString().isEmpty()){

				int usedUserSize = PaymentLogic.find.where().in("user",users).eq("discountsMaxes",discounts.dmax).eq("status","CURRENT").findList().size();
				//int usedUserSize = PaymentLogic.find.findRowCount();


				if(usedUserSize < discounts.dmax.maxUserSize){
					disList.add(discounts);
				}
			}else{

				disList.add(discounts);
			}

		}


		return disList;

	}

	public static DiscountValue getById(Long id) {
		return find.byId(id);
	}

	public static DiscountValue getByMax(DiscountMax dmax, Dostyk dos) {
		return find.where().eq("dostyk", dos).eq("dmax", dmax).findUnique();
	}

	public static List<DiscountValue> getAllByMax(DiscountMax dmax) {
		return find.where().eq("dmax", dmax).findList();
	}

	public static DiscountValue getBonusByDostyk(Dostyk dostyk) {
		return find.where().eq("dostyk", dostyk).eq("dmax", DiscountMaxLogic.getByName("Bonus")).findUnique();
	}

	public static Map<DiscountMax, Integer> getDiscountCount(
			List<AuthorisedUser> students) {
		List<DiscountMax> discounts = DiscountMaxLogic.getAllOrderByValue();

		Map<DiscountMax, Integer> discountCount = new HashMap<DiscountMax, Integer>();
		for (DiscountMax discount : discounts) {
			int count = PaymentLogic.find.where().in("user", students).eq("user.status", "active").eq("discountsMaxes", discount)
					.findRowCount();
			discountCount.put(discount, count);

		}

		return discountCount;
	}

	public static List<Payment> getDiscountUsers(List<AuthorisedUser> students,DiscountMax discount){

		return PaymentLogic.find.where().in("user", students).eq("user.status", "active").eq("discountsMaxes", discount).orderBy("user.profile.lastName").findList();
	}

}
