package models.logic.accounting;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import models.entities.accounting.Payment;
import models.entities.accounting.PaymentPeriod;
import models.entities.accounting.Report;
import models.entities.social.Dostyk;
import models.entities.social.DostykGroup;
import models.entities.user.AuthorisedUser;
import play.db.ebean.Model.Finder;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlQuery;
import com.avaje.ebean.SqlRow;

/**
 * Code writen using IntelliJ IDEA. User: Ð�Ñ€Ñ�ÐµÐ½ Date: 22.07.13 Time: 11:35
 * To change this template use File | Settings | File Templates.
 */
public class PaymentLogic {
	public static Finder<Long, Payment> find = new Finder<Long, Payment>(
			Long.class, Payment.class);

	public static Payment getById(Long id) {
		return find.byId(id);
	}

	public static int getUnpaidPeriodsNumber(Payment p) {
		int result = 0;
		List<PaymentPeriod> periods = PaymentPeriodLogic.getPeriods(p);
		for (PaymentPeriod period : periods) {
			if (period.payedPayment == 0) {
				result++;
			}
		}
		return result;
	}

	public static List<Payment> getAllByUser(AuthorisedUser user) {
		return find.where().eq("user", user).orderBy("endDate").findList();
	}

	public static void saveMekemeStudentPayment(DostykGroup dostykGroup) {
		Payment studentPayment = new Payment();
		/*studentPayment.startDate = dostykGroup.startDate;
		studentPayment.endDate = dostykGroup.endDate;
		studentPayment.plannedPayment = dostykGroup.paymentPerStudent;
		studentPayment.payedPayment = dostykGroup.paymentPerStudent;*/
		studentPayment.debtPayment = 0.0;
	}

	public static void fillPayments(Report report, Date startDate,
			Date endDate, String paymentStatus, List<AuthorisedUser> users) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		ArrayList<Integer> paymentIDs = new ArrayList<Integer>();
		StringBuilder sb = new StringBuilder();
		List<Payment> payments;
		if (!paymentStatus.equals("all"))
			payments = find.select("id, discount").where().in("user", users)
			.eq("status", paymentStatus).findList();
		else
			payments = find.select("id, discount").where().in("user", users)
			.findList();
		for (Payment userPayment : payments) {
			paymentIDs.add(userPayment.id.intValue());
			if (sb.length() > 0) {
				sb.append(',');
			}
			sb.append(userPayment.id.intValue());
		}
		String result = sb.toString();
		if (result.length() > 0) {
			String sql;
			sql = "select " + "sum(planned_payment) as planned, "
					+ "sum(payed_payment) as payed, "
					+ "sum(debt_payment) as debt from payment_periods "
					+ "where " + "payed_payment_date between '"
					+ sdf.format(startDate) + "' and '" + sdf.format(endDate)
					+ "' " + " and payment_id in (" + result + ")";
			SqlQuery sqlQuery = Ebean.createSqlQuery(sql);
			SqlRow row = sqlQuery.findUnique();
			if (row != null) {
				if (row.getDouble("planned") != null)
					report.plannedPayment = row.getDouble("planned");
					report.sPlannedPayment = NumberFormat.getIntegerInstance().format((int)report.plannedPayment);
				if (row.getDouble("payed") != null)
					report.payedPayment = row.getDouble("payed");
					report.sPayedPayment = NumberFormat.getIntegerInstance().format((int)report.payedPayment);
				if (row.getDouble("debt") != null)
					report.debtPayment = row.getDouble("debt");
					report.sDebtPayment = NumberFormat.getIntegerInstance().format((int)report.debtPayment);
			}
		}

	}

	public static Payment getPaymentCount(Date startDate, Date endDate,
			String paymentStatus, List<AuthorisedUser> users) {
		Payment payment = new Payment();
		List<PaymentPeriod> paymentPeriods;
		if (!paymentStatus.equals("all")) {
			paymentPeriods = PaymentPeriodLogic.find
					.where()
					.eq("status", paymentStatus)
					.in("payment",
							PaymentLogic.find.where().in("user", users)
							.findList())
							.between("payedPaymentDate", startDate, endDate).findList();
		} else {
			paymentPeriods = PaymentPeriodLogic.find
					.where()
					.in("payment",
							PaymentLogic.find.where().in("user", users)
							.findList())
							.between("payedPaymentDate", startDate, endDate).findList();
		}
		for (PaymentPeriod pp : paymentPeriods) {
			payment.discount = PaymentLogic.find.where().in("user", users)
					.findRowCount();
			payment.plannedPayment += pp.plannedPayment;
			payment.payedPayment += pp.payedPayment;
			payment.debtPayment += pp.debtPayment;
		}
		return payment;
	}

	public static Payment getActualPayment(AuthorisedUser user) {
		List<Payment> payments = find.where().eq("user", user)
				.orderBy("endDate").findList();
		if (payments.size() == 0) {
			return null;
		} else if (payments.size() > 1) {
			for (int i = 0; i < payments.size() - 1; i++) {
				Payment p = payments.get(i);
				p.status = "PAYED";
				p.save();
			}
			return payments.get(payments.size() - 1);
		} else
			return find.where().eq("user", user).findUnique();
	}

	public static Payment getLastPayment(AuthorisedUser user) {
		List<Payment> payments = find.where().eq("user", user)
				.orderBy("endDate").findList();
		if (payments.size() == 0) {
			return null;
		} else if (payments.size() > 1) {
			return payments.get(payments.size() - 1);
		} else
			return find.where().eq("user", user).findUnique();
	}

	public static boolean checkBill(AuthorisedUser user) {
		boolean result = true;
		List<Payment> payments = PaymentLogic.getAllByUser(user);
		if (payments.size() == 0) {
			result = false;
		} else {
			for (Payment p : payments) {
				if (p.status.equalsIgnoreCase("CURRENT")
						|| p.status.equalsIgnoreCase("PAYED")) {
					result = true;
					break;
				} else {
					result = false;
				}
			}
		}
		return result;
	}

	public static boolean checkPayment(AuthorisedUser user) {
		boolean result = true;
		List<Payment> payments = PaymentLogic.getAllByUser(user);
		if (payments.size() == 0) {
			result = false;
		}
		return result;
	}

	public static int getDiscount(AuthorisedUser user) {
		Payment payment = PaymentLogic.getActualPayment(user);
		return payment.discount;
	}

	public static String getNextMonday() {
		String result = "";
		Calendar now = Calendar.getInstance();
		int weekday = now.get(Calendar.DAY_OF_WEEK);
		if (weekday != Calendar.MONDAY) {
			int days = (Calendar.SATURDAY - weekday + 2) % 7;
			now.add(Calendar.DAY_OF_YEAR, days);
		}
		Date date = now.getTime();
		String format = new SimpleDateFormat("yyyy-MM-dd").format(date);
		result = format;
		return result;
	}

	public static TreeMap<String, Double> sumDostykPayments(
			Map<Dostyk, Payment> map) {
		double plannedPayment = 0.0;
		double payedPayment = 0.0;
		double debtPayment = 0.0;
		for (Map.Entry<Dostyk, Payment> entry : map.entrySet()) {
			plannedPayment += entry.getValue().plannedPayment;
			payedPayment += entry.getValue().payedPayment;
			debtPayment += entry.getValue().debtPayment;
		}
		TreeMap<String, Double> totals = new TreeMap<String, Double>();
		totals.put("1", (double) Math.round(plannedPayment));
		totals.put("2", (double) Math.round(payedPayment));
		totals.put("3", (double) Math.round(debtPayment));
		return totals;
	}

	public static TreeMap<String, String> sumPayments(List<Report> list) {
		Double plannedPayment = 0.0;
		Double payedPayment = 0.0;
		Double debtPayment = 0.0;
		for (Report entry : list) {
			plannedPayment += entry.plannedPayment;
			payedPayment += entry.payedPayment;
			debtPayment += entry.debtPayment;
		}
		TreeMap<String, String> totals = new TreeMap<String, String>();
		totals.put("1",  NumberFormat.getIntegerInstance().format(plannedPayment));
		totals.put("2", NumberFormat.getIntegerInstance().format(payedPayment));
		totals.put("3", NumberFormat.getIntegerInstance().format(debtPayment));
		return totals;
	}

	public static int sumCurrents(List<Report> reports) {
		int result = 0;
		for (Report report : reports)
			result += report.percentage;
		return result;
	}

	public static int sumStudents(List<Report> reports) {
		int result = 0;
		for (Report report : reports)
			result += report.studentSize;
		return result;
	}

	public static TreeMap<String, Double> sumDostykGroupPayments(
			Map<DostykGroup, Payment> map) {
		Double plannedPayment = 0.0;
		Double payedPayment = 0.0;
		Double debtPayment = 0.0;
		for (Map.Entry<DostykGroup, Payment> entry : map.entrySet()) {
			plannedPayment += entry.getValue().plannedPayment;
			payedPayment += entry.getValue().payedPayment;
			debtPayment += entry.getValue().debtPayment;
		}
		TreeMap<String, Double> totals = new TreeMap<String, Double>();
		totals.put("1", plannedPayment);
		totals.put("2", payedPayment);
		totals.put("3", debtPayment);

		return totals;
	}

	public static TreeMap<String, Double> sumStudentPayments(
			Map<AuthorisedUser, Payment> map) {
		Double plannedPayment = 0.0;
		Double payedPayment = 0.0;
		Double debtPayment = 0.0;
		for (Map.Entry<AuthorisedUser, Payment> entry : map.entrySet()) {
			plannedPayment += entry.getValue().plannedPayment;
			payedPayment += entry.getValue().payedPayment;
			debtPayment += entry.getValue().debtPayment;
		}
		TreeMap<String, Double> totals = new TreeMap<String, Double>();
		totals.put("1", plannedPayment);
		totals.put("2", payedPayment);
		totals.put("3", debtPayment);

		return totals;
	}

}
