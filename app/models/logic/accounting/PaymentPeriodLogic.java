package models.logic.accounting;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import models.entities.accounting.Payment;
import models.entities.accounting.PaymentPeriod;
import play.db.ebean.Model.Finder;

/**
 * Created with IntelliJ IDEA. User: dostyk5 Date: 29.07.13 Time: 14:52 To
 * change this template use File | Settings | File Templates.
 */
public class PaymentPeriodLogic {
	public static Finder<Long, PaymentPeriod> find = new Finder<Long, PaymentPeriod>(
			Long.class, PaymentPeriod.class);

	public static List<PaymentPeriod> getPeriods(Payment payment) {
		return find.where().eq("payment", payment).orderBy("period").findList();
	}

	public static List<PaymentPeriod> getAll() {
		return find.all();
	}

	public static String getStatus(PaymentPeriod period) {
		String result = "";
		Date now = new Date();
		Date due = period.plannedPaymentDate;
		DateFormat format = new SimpleDateFormat("yy");
		int nowYear = Integer.parseInt(format.format(now));
		int dueYear = Integer.parseInt(format.format(due));
		DateFormat format2 = new SimpleDateFormat("MM");
		int nowMonth = Integer.parseInt(format2.format(now));
		int dueMonth = Integer.parseInt(format2.format(due));
		if (dueYear - nowYear > 0 && !period.status.equalsIgnoreCase("PAYED")) {
			result = "success";
		} else if (dueYear - nowYear == 0
				&& !period.status.equalsIgnoreCase("PAYED")) {
			if (dueMonth - nowMonth > 0) {
				result = "success";
			} else if (dueMonth - nowMonth == 0) {
				result = "warning";
			} else {
				result = "error";
			}
		} else if (dueYear - nowYear < 0
				&& !period.status.equalsIgnoreCase("PAYED")) {
			result = "error";
		}
		return result;
	}

	public static PaymentPeriod getCurrentPeriod(Payment payment) {
		if (find.where().eq("payment", payment).eq("status", "CURRENT")
				.orderBy("period").findList().size() == 1) {
			return find.where().eq("payment", payment).eq("status", "CURRENT")
					.orderBy("period").findUnique();
		} else if (find.where().eq("payment", payment).eq("status", "CURRENT")
				.orderBy("period").findList().size() == 0) {
			List<PaymentPeriod> periods = getPeriods(payment);
			for (PaymentPeriod period : periods) {
				if (period.status.equalsIgnoreCase("PLANNED")) {
					period.status = "CURRENT";
					period.save();
					break;
				} else if (period.status.equalsIgnoreCase("CURRENT")) {
					break;
				}
			}
			return find.where().eq("payment", payment).eq("status", "CURRENT")
					.orderBy("period").findUnique();
		} else {
			return find.where().eq("payment", payment).eq("status", "CURRENT")
					.orderBy("period").findList().get(0);
		}
	}
	public static double getPaymentByMonth(Payment payment, String month){
		List<PaymentPeriod> periods = PaymentPeriodLogic.getPeriods(payment);
        double result=0;
        DateFormat df = new SimpleDateFormat("MM");
        for(PaymentPeriod period: periods)
            if(df.format(period.payedPaymentDate).equals(month))
                result+=period.payedPayment;
        return result;
	}
    public static int countPeriodsByPayment(Payment payment, double amount){
        return find.where().eq("payment", payment).eq("plannedPayment",amount).findList().size();
    }
	public static double getAveragePayment(Payment payment){
        List<PaymentPeriod> periods = PaymentPeriodLogic.getPeriods(payment);
        double result=0;
        for(PaymentPeriod period: periods) {
            result = period.plannedPayment;
            if (PaymentPeriodLogic.countPeriodsByPayment(payment, period.plannedPayment) > 1)
                break;
        }
        return result;
	}
}
