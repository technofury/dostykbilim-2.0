package models.logic.education;

import java.util.List;

import models.entities.accounting.PriceList;
import models.entities.education.CourseType;
import models.entities.education.Lesson;
import models.entities.education.LessonHours;
import models.entities.social.Dostyk;
import models.entities.user.AuthorisedUser;
import models.logic.accounting.PriceListLogic;
import play.db.ebean.Model.Finder;

public class LessonHoursLogic {

	public static Finder<Long, LessonHours> find = new Finder<Long, LessonHours>(
			Long.class, LessonHours.class);

	public static LessonHours getById(long id) {
		return find.byId(id);
	}

	public static int getByLessonAndDostyk(Lesson lesson, Dostyk dostyk) {
		boolean tf = checkByLessonAndDostyk(lesson, dostyk);
		if (tf == true) {
			LessonHours lh = find.where().eq("lesson", lesson)
					.eq("dostyk", dostyk).findUnique();
			return lh.hours;
		} else
			return 0;
	}

	public static LessonHours getFromDostyk(Lesson lesson, Dostyk dostyk) {
		return find.where().eq("lesson", lesson).eq("dostyk", dostyk)
				.findUnique();
	}

	public static boolean checkByLessonAndDostyk(Lesson lesson, Dostyk dostyk) {
		List<LessonHours> lhs = find.where().eq("lesson", lesson)
				.eq("dostyk", dostyk).findList();
		if (lhs.size() == 1)
			return true;
		else if (lhs.size() == 0)
			return false;
		else {
			for (int i = 1; i < lhs.size(); i++) {
				LessonHours lh = lhs.get(i);
				lh.lesson = null;
				lh.dostyk = null;
				lhs.get(i).delete();
			}
			return true;
		}
	}

	public static boolean delete(LessonHours t) {
		return false;
	}

	public static boolean create(LessonHours t) {

		try {
			boolean result = checkByLessonAndDostyk(t.lesson, t.dostyk);
			LessonHours lh;
			if (result == false)
				lh = new LessonHours();
			else
				lh = getFromDostyk(t.lesson, t.dostyk);
			lh.lesson = t.lesson;
			lh.dostyk = t.dostyk;
			lh.hours = t.hours;
			lh.save();
		} catch (Exception ex) {
			t.save();
		}

		return false;
	}

	public static int getHours(Dostyk dostyk, Lesson lesson) {
		for (LessonHours lh : dostyk.lessonHours) {
			if (lh.lesson.equals(lesson)) {
				return lh.hours;
			}
		}

		return 0;
	}

	public static List<LessonHours> getByDostyk(Dostyk dostyk) {
		return find.where().eq("dostyk", dostyk).findList();
	}

	public static int getHoursByStudent(Dostyk dostyk, List<Lesson> lessons) {
		int result = 0;
		for (LessonHours lh : dostyk.lessonHours) {
			for (Lesson l : lessons) {
				if (lh.lesson.equals(l)) {
					result += lh.hours;
				}
			}
		}
		return result;
	}

	public static double getPricePerWeekForStudent(AuthorisedUser user) {
		double result = 0;
		Dostyk dostyk = user.dostyks.get(0);
		CourseType course = user.profile.course;
		List<PriceList> objects = PriceListLogic.getByDostykAndCourse(dostyk,
				course);
		int hours = getHoursByStudent(user.dostyks.get(0), user.profile.lessons);
		for (PriceList pl : objects) {
			if (hours == pl.hours) {
				result = pl.price;
			}
		}

		return result;
	}

	public static Long getLessonHoursId(Dostyk dostyk, Lesson lesson) {
		for (LessonHours lh : dostyk.lessonHours) {
			if (lh.lesson.equals(lesson)) {
				return lh.id;
			}
		}

		return 0L;
	}

}
