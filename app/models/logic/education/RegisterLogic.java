package models.logic.education;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import models.entities.education.Lesson;
import models.entities.education.Register;
import models.entities.user.AuthorisedUser;
import play.db.ebean.Model.Finder;
import models.timetable.Timetable;

public class RegisterLogic {

	public static Finder<Long, Register> find = new Finder<Long, Register>(
			Long.class, Register.class);

	public static List<Register> getAll() {
		return find.all();
	}

	public static List<String> list() {
		ArrayList<String> a = new ArrayList<String>();
		return a;
	}

	public static Register findById(long id) {
		return find.byId(id);
	}

	public static Register getByFields(AuthorisedUser user,
			Timetable timetable, Date date) {
		return find.where().eq("student", user).eq("timetable", timetable)
				.eq("date", date).findUnique();
	}

	public static List<Register> getByUserAndLesson(AuthorisedUser u, Lesson l) {
		return find.where().eq("student", u).eq("timetable.lesson", l)
				.eq("attendance", "notPresent").order("date").findList();
	}

	public static List<Register> getByDates(AuthorisedUser user, Date first,
			Date last) {
		if(user.roles.get(0).name.equals("student"))
        	return find.where().eq("student", user).between("date", first, last)
				.order("date").findList();
		else
			return find.where().eq("teacher", user).between("date", first, last)
					.order("date").findList();
	}

	public static List<Register> getByTimetable(Timetable cur) {
		return find.where().eq("timetable", cur).findList();
	}

	public static int getCount(AuthorisedUser user, String type) {
		return find.where().eq("student", user).eq("attendance", type)
				.findRowCount();
	}

	public static List<Register> getByTimetables(List<Timetable> timetables,
			long teacherId) {
		return find.where().in("timetable", timetables)
				.eq("student.id", teacherId).orderBy("date desc").findList();
	}

	public static Register getByFields(AuthorisedUser u, Lesson lesson,
			Date date, AuthorisedUser teacher) {
		return find.where().eq("student", u).eq("lessonid", lesson.id)
				.eq("date", date).eq("teacher", teacher).order("date")
				.findUnique();
	}

	public static List<Register> getExtrasByUser(AuthorisedUser user) {
		return find.where().eq("student", user).isNull("timetable")
				.orderBy("date").findList();
	}

}
