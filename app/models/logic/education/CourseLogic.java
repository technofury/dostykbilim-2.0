package models.logic.education;

import models.entities.education.Course;
import play.db.ebean.Model;
import java.util.List;

/**
 * Created by User on 15-Feb-17.
 */
public class CourseLogic {

    public static Model.Finder<Long,Course> find = new Model.Finder<Long, Course>(Long.class,Course.class);

    public static List<Course> getAll(){
        return find.where().orderBy("title").findList();
    }
    public static Course getById(Long id){
        return find.byId(id);
    }
}
