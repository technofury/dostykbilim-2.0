package models.logic.education;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import models.entities.education.Lesson;
import play.db.ebean.Model.Finder;

public class LessonLogic {

	public static Finder<Long, Lesson> find = new Finder<Long, Lesson>(
			Long.class, Lesson.class);

	public static List<Lesson> getAll() {
		return find.where().orderBy("language").orderBy("name").findList();
	}

	public static Lesson getbyCodeAndLang(String code, String language) {
		return find.where().eq("code", code).eq("language", language).findUnique();
	}

	public static List<String> list() {
		List<Lesson> Lessons = find.all();
		List<String> names = new ArrayList<String>();
		for (int i = 0; i < Lessons.size(); i++) {
			names.add(Lessons.get(i).language + " " + Lessons.get(i).name);
		}
		return names;
	}

	public static Map<String, String> map() {
		List<Lesson> Lessons = find.all();
		Map<String, String> names = new HashMap<String, String>();
		HashSet<String> st = new HashSet<String>();
		st.add("01");
		st.add("03");
		st.add("02");
		st.add("04");
		st.add("05");
		for (int i = 0; i < Lessons.size(); i++) {
			if (!st.contains(Lessons.get(i).code)) {
				names.put("" + Lessons.get(i).id, Lessons.get(i).language + " "
						+ Lessons.get(i).name);
			}
		}
		return names;
	}

	public static Lesson findByName(String name) {
		return find.where().eq("name", name).findUnique();
	}

	public static Lesson findByNameAndLang(String name, String lang) {
		return find.where().eq("name", name).eq("language", lang).findUnique();
	}

	public static Lesson getById(long id) {
		return find.byId(id);
	}

	public static boolean delete(Lesson t) {
		return false;
	}
}
