package models.logic.education;

import java.util.ArrayList;
import java.util.List;

import models.entities.education.CourseType;
import play.db.ebean.Model.Finder;

public class CourseTypeLogic {

	public static Finder<Long, CourseType> find = new Finder<Long, CourseType>(
			Long.class, CourseType.class);

	public static List<CourseType> getAll() {
		return find.where().orderBy("id").findList();
	}

	public static CourseType getByName(String name) {
		return find.where().eq("name", name).findUnique();
	}

	public static CourseType getById(long id) {
		return find.byId(id);
	}

	public static List<String> list() {
		List<CourseType> courseTypes = find.all();
		List<String> names = new ArrayList<String>();
		for (int i = 0; i < courseTypes.size(); i++) {
			names.add(courseTypes.get(i).name);
		}

		return names;
	}

	public static CourseType getByCode(String code) {
		return find.where().eq("code", code).findUnique();
	}
}
