package models.logic.user;

import java.util.ArrayList;
import java.util.List;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlUpdate;
import models.entities.social.Dostyk;
import models.entities.social.DostykGroup;
import models.entities.user.AuthorisedUser;
import models.entities.user.Profile;
import models.entities.user.SecurityRole;
import play.db.ebean.Model.Finder;
import play.mvc.Controller;

/**
 * Created with IntelliJ IDEA. User: dostyk5 Date: 19.07.13 Time: 11:30 To
 * change this template use File | Settings | File Templates.
 */
public class AuthorisedUserLogic {
	public static Finder<Long, AuthorisedUser> find = new Finder<Long, AuthorisedUser>(
			Long.class, AuthorisedUser.class);

	public static AuthorisedUser getById(Long id) {
		return find.byId(id);
	}

	public static List<AuthorisedUser> getAll() {
		return find.all();
	}
	
	/*
	public static List<AuthorisedUser> getAll() {
		return find.all();
	}*/

	public static List<AuthorisedUser> getByProfiles(List<Profile> students) {
		return find.where().in("profile", students).findList();
	}

	public static List<AuthorisedUser> getStudents(Dostyk d) {
		return AuthorisedUserLogic.find.where()
				.eq("roles", SecurityRole.getByName("student"))
				.eq("dostyks", d).ne("status", "deleted").findList();
	}

	public static List<AuthorisedUser> getStudents(Dostyk d, String status) {


		return AuthorisedUserLogic.find.where().eq("roles.name", "student")
				.eq("dostyks", d).eq("status", status).findList();
	}

    public static List<AuthorisedUser> getAllStudents(Dostyk d,
                                                      String paymentStatus, List<String> statuses) {
        if (!paymentStatus.equals("all"))
            return AuthorisedUserLogic.find.where()
                    .eq("roles", SecurityRole.getByName("student"))
                    .eq("dostyks", d).eq("payments.status", paymentStatus)
                    .in("status", statuses).findList();
        else
            return AuthorisedUserLogic.find.where()
                    .eq("roles", SecurityRole.getByName("student"))
                    .eq("dostyks", d).in("status", statuses).findList();
    }

	public static List<String> jobs() {
		List<String> jobs = new ArrayList<String>();
		if (Controller.lang().language().equals("ru")) {
			jobs.add("Научный сотрудник");
			jobs.add("Военный");
			jobs.add("Полиция");
			jobs.add("Таможенник");
			jobs.add("Прокурор");
			jobs.add("Судья");
			jobs.add("Продавец");
			jobs.add("Банковский работник");
			jobs.add("Журналист");
			jobs.add("Администратор");
			jobs.add("Парикмахер");
			jobs.add("Агроном");
			jobs.add("Стоматолог");
			jobs.add("Врач");
			jobs.add("Аптекарь");
			jobs.add("Медсестра");
			jobs.add("Частный предприниматель");
			jobs.add("Домохозяйка(ин)");
			jobs.add("Секретарь");
			jobs.add("Овеллр");
			jobs.add("Издательство");
			jobs.add("Религиозный сотрудник");
			jobs.add("Госслужащий");
			jobs.add("Депутат");
			jobs.add("Бухгалтер");
			jobs.add("Инженер");
			jobs.add("Человек искусства");
			jobs.add("Архитектор");
			jobs.add("Строитель");
			jobs.add("Директор школы");
			jobs.add("Учитель");
			jobs.add("Водитель");
			jobs.add("Спортсмен");
			jobs.add("Туризм");
			jobs.add("Безработный");
			jobs.add("Пенсионер");
			jobs.add("Другое");
		} else if (Controller.lang().language().equals("kk")) {
			jobs.add("Ғылыми қызметкер");
			jobs.add("Әскер");
			jobs.add("Полиция");
			jobs.add("Кеден қызметшісі");
			jobs.add("Прокурор");
			jobs.add("Сот");
			jobs.add("Дүкенде сатушы");
			jobs.add("Банк қызметкері");
			jobs.add("Журналист");
			jobs.add("Әкімшілік");
			jobs.add("Шаштараз");
			jobs.add("Егінші");
			jobs.add("Тіс дәрігері");
			jobs.add("Дәрігер");
			jobs.add("Дәріханашы");
			jobs.add("Медбике");
			jobs.add("Жеке кәсіпкер");
			jobs.add("Үй шаруасында");
			jobs.add("Хатшы");
			jobs.add("Зергер");
			jobs.add("Баспа ісі");
			jobs.add("Діни қызметкер");
			jobs.add("Мемлекеттік");
			jobs.add("қызметкер");
			jobs.add("Депутат");
			jobs.add("Бухгалтер");
			jobs.add("Инженер");
			jobs.add("Өнер адамы");
			jobs.add("Сәулетші");
			jobs.add("Құрылысшы");
			jobs.add("Мектеп директоры");
			jobs.add("Оқытушы");
			jobs.add("Жүргізуші");
			jobs.add("Спортшы");
			jobs.add("Туризм");
			jobs.add("Жумыссыз");
			jobs.add("Зейнеткер");
			jobs.add("Басқа");
		}

		return jobs;
	}

	public static List<AuthorisedUser> getByElective(DostykGroup dostykGroup) {
		return find.where().eq("profile.electiveGroup.id", dostykGroup.id)
				.findList();
	}

	public static List<AuthorisedUser> getByGroup(DostykGroup dostykGroup) {
		return find.where().eq("profile.dostykGroup.id", dostykGroup.id)
				.findList();
	}

	public static List<AuthorisedUser> teachersByLesson(long id) {
		return find.where().eq("roles.name", "teacher")
				.eq("profile.lessons.id", id).findList();
	}

	public static List<AuthorisedUser> teachersByLessonAndDostyk(long id,
			Dostyk dostyk) {
		return find.where().eq("roles.name", "teacher")
				.eq("profile.lessons.id", id).eq("dostyks", dostyk).findList();
	}

	public static int getStudentsSize(DostykGroup group, List<String> statuses) {
		return find.where().eq("profile.dostykGroup", group)// .in("payments.status",
				// paymentStatuses)
				.in("status", statuses).findRowCount();
	}

	public static int getStudentsSize(DostykGroup group, String paymentStatus,
			List<String> statuses) {
		if (!paymentStatus.equals("all"))
			return find.where().eq("profile.dostykGroup", group)
					.eq("payments.status", paymentStatus)
					.in("status", statuses).findRowCount();
		else
			return find.where().eq("profile.dostykGroup", group)
					.in("status", statuses).findRowCount();
	}

	public static List<AuthorisedUser> getStudentsIds(DostykGroup group,
			String paymentStatus, List<String> statuses) {
		if (!paymentStatus.equals("all"))
			return find.select("id").where().eq("profile.dostykGroup", group)
					.eq("payments.status", paymentStatus)
					.in("status", statuses).findList();
		else
			return find.select("id").where().eq("profile.dostykGroup", group)
					.in("status", statuses).findList();
	}

	public static List<AuthorisedUser> getTeachersByDostyk(Dostyk dostyk) {
		return find.where().eq("roles.name", "teacher").in("dostyks", dostyk).findList();
	}

	public static List<AuthorisedUser> getDeletedUsers(int code) {
		return find.where().eq("status", "deleted").ilike("profile.idNumber", code + "%").findList();
	}
	public static List<AuthorisedUser> getAllTeachers(){
		return find.where().eq("roles.name","teacher").findList();
	}
	public static List<AuthorisedUser> getAllStudents(){
		return find.where().eq("roles.name","student").findList();
	}

	public static void deleteStudentsDostyk(Long id){
		String s = "delete from dostyks_users du where du.users_id = (select u.id from users u where u.id ="+id+" )";
		SqlUpdate delete = Ebean.createSqlUpdate(s);
		Ebean.execute(delete);

	}
}
