package models.logic.user;

import java.util.Date;

import models.entities.education.CourseType;
import models.entities.social.DostykGroup;

public class MovementUser {

	public String name;
	public Date startDate;
	public String disposalDate;
	public CourseType courseType;
	public Date completeDate;
	public DostykGroup dostykGroup;
	
}
