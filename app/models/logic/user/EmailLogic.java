package models.logic.user;

import models.entities.user.AuthorisedUser;
import play.i18n.Messages;

import com.typesafe.plugin.MailerAPI;
import com.typesafe.plugin.MailerPlugin;
import play.libs.Crypto;

public class EmailLogic {

	public static String sendTenEmail(AuthorisedUser user) {
		String fullName = user.profile.lastName+ " " + user.profile.firstName;
		String email = user.email;
		String password = Crypto.decryptAES(user.password);
		String idNumber = user.profile.idNumber;
		String session = "";
		if(user.profile.examSession.session==1){
			session = "09:00-12:30";
		}
		else if(user.profile.examSession.session==2){
			session = "12:30-16:00";
		}
		else if(user.profile.examSession.session==3){
			session = "16:00-19:30";
		}

		MailerAPI mail = play.Play.application().plugin(MailerPlugin.class)
				.email();
		mail.setSubject("dostykbilim.kz: "+Messages.get("ten.name"));
		mail.setRecipient(email);
		mail.setFrom("Dostykbilim <cabinet.dostykbilim@gmail.com>");
		mail.send("text", "<!DOCTYPE html>"
				+ "<html>" + "<body>"
				+ "<p>Құрметті," +  fullName  + ". Сіз 17 сәуірде сағат "+session+"-де өтетін, \"ҰБТ-ға дайындықтың Алғашқы Қадам Сынағына\" тіркелдіңіз. Сынаққа өзіңізбен бірге қарындаш, өшіргіш, қара сия қалам әкелуді ұмытпаңыз. Сәттілік тілейміз! Құрметпен, \"Достық Білім беру орталығы\"</p>"
				+ "<p>Уважаемый," +  fullName  + ". Вы зарегистрировались на участие в тестировании \"Тест – Первый Шаг по подготовке к ЕНТ\", которое  состоится 17 апреля в "+session+" часов. С собой обязательно иметь карандаш, ластик, ручку с черной пастой. Желаем удачи! С уважением \"Образовательный центр Достық\"</p>"
				+ "<p>" + Messages.get("YouCanLoginToOur")+ " <a href='http://cabinet.dostykbilim.kz'> " + Messages.get("site")+ "</a> " + "</p>"
				+ "<p>" + Messages.get("Email") + ":    <b>" + email + "</b></p>"
				+ "<p>" + Messages.get("Password") + ": <b>" + password + "</b></p>"
				+ "<p>" + Messages.get("idNumber") + ": <b>" + idNumber + "</b></p>"
				+ "<p>" + Messages.get("ten.session") + ": <b>" + session + "</b></p>"
				+ "</body>" + "</html>");

		return "";
	}

	public static String sendNewPassword(AuthorisedUser user) {
		String generatedPassword = new java.math.BigInteger(130,
				new java.security.SecureRandom()).toString(32).substring(0, 10);
		String email = user.email;
		MailerAPI mail = play.Play.application().plugin(MailerPlugin.class)
				.email();
		mail.setSubject("dostykbilim.kz: "+Messages.get("user.restorePassword"));
		mail.setRecipient(email);
		mail.setFrom("Dostykbilim <cabinet.dostykbilim@gmail.com>");
		mail.send("text", "<!DOCTYPE html>" + "<html>" + "<body>" +

		"<p>" + Messages.get("YouCanLoginToOur")
		+ " <a href='http://cabinet.dostykbilim.kz'> " + Messages.get("site")
		+ "</a> " + "</p>" + "<p>"
		+ Messages.get("Email") + ":    <b>" + email + "</b></p>"
		+ "<p>" + Messages.get("Password") + ": <b>"
		+ generatedPassword + "</b></p>" + "</body>" + "</html>");

		return generatedPassword;
	}

	public static Integer supportMail(AuthorisedUser user, String moderEmail,
			String title, String body) {

		String email;
		try {
			email = user.email;
		} catch (Exception e) {
			email = "unauthorized";
		}
		String dostyk;
		try {
			dostyk = user.dostyks.get(0).name;
		} catch (Exception e) {
			dostyk = "достығы жоқ";
		}
		MailerAPI mail = play.Play.application().plugin(MailerPlugin.class)
				.email();
		mail.setSubject("dostykbilim.kz: support");
		mail.setRecipient(" technosupport <support@technovision.kz>");
		mail.setFrom("Dostykbilim <cabinet.dostykbilim@gmail.com>");
		mail.send("text", "<!DOCTYPE html><html><body>" + "<p><b>Достық:</b> "
				+ dostyk + "<p>" + "<p><b>Модератор email:</b> " + moderEmail
				+ "<p>" + "<p><b>Email:</b> " + email + "<p>"
				+ "<p><b>Тақырыбы:</b> " + title + "<p>" + "<p><b>Текст:</b> "
				+ body + "<p>" + "</body></html>");

		return 0;
	}

}
