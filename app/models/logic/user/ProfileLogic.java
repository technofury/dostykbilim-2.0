package models.logic.user;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.avaje.ebean.Expr;

import models.entities.education.CourseType;
import models.entities.social.Dostyk;
import models.entities.social.DostykGroup;
import models.entities.user.AuthorisedUser;
import models.entities.user.Profile;
import models.entities.user.SecurityRole;
import models.logic.accounting.PaymentLogic;
import models.logic.accounting.PaymentPeriodLogic;
import models.logic.education.CourseTypeLogic;
import play.db.ebean.Model.Finder;

public class ProfileLogic {

	public static Finder<Long, Profile> find = new Finder<Long, Profile>(
			Long.class, Profile.class);

	public static List<AuthorisedUser> getElectiveGroupUsers(DostykGroup dg) {
		return AuthorisedUser.find.where().eq("profile.electiveGroup", dg)
				.findList();
	}

	public static int getSessionCount(Dostyk dostyk, int type) {
			return ProfileLogic.find.where()
					.eq("user.roles", SecurityRole.getByName("ten"))
					.eq("user.lessons", dostyk).eq("user.status", "active")
					.eq("examSession.session", type).findRowCount();
	}

	public static int getGenderCount(Object obj, String type) {
		if (obj instanceof Dostyk) {
			return ProfileLogic.find.where()
					.eq("user.roles", SecurityRole.getByName("student"))
					.eq("user.dostyks", obj).eq("user.status", "active")
					.eq("gender", type).findRowCount();
		} else if (obj instanceof DostykGroup) {
			DostykGroup dg = (DostykGroup) obj;
			return /*dg.type.equals("E") ? ProfileLogic.find.where()
					.eq("user.roles", SecurityRole.getByName("student"))
					.eq("electiveGroup", dg).eq("user.status", "active")
					.eq("gender", type).findRowCount() : */ProfileLogic.find
					.where()
					.eq("user.roles", SecurityRole.getByName("student"))
					.eq("dostykGroup", dg).eq("user.status", "active")
					.eq("gender", type).findRowCount();
		}
		return 0;
	}

	public static int getUlgerimiCount(Object obj, String type) {
		if (obj instanceof Dostyk) {
			return ProfileLogic.find.where()
					.eq("user.roles", SecurityRole.getByName("student"))
					.eq("user.dostyks", obj)
					.eq("user.status", "active")
					.eq("ulgerimi", type).findRowCount();
		} else if (obj instanceof DostykGroup) {
			DostykGroup dg = (DostykGroup) obj;
			return /*dg.type.equals("E") ? ProfileLogic.find.where()
					.eq("user.roles", SecurityRole.getByName("student"))
					.eq("electiveGroup", dg).eq("user.status", "active")
					.eq("ulgerimi", type).findRowCount() :*/ ProfileLogic.find
					.where()
					.eq("user.roles", SecurityRole.getByName("student"))
					.eq("dostykGroup", dg).eq("user.status", "active")
					.eq("ulgerimi", type).findRowCount();
		}
		return 0;
	}

	public static List<Dostyk> findUlgerimCount(List<Dostyk> dostyks) {
		String ulgerimi[] = { "jaksy", "kizilAttestat", "altynBelgi" };
		int count = 0;
		for (String type : ulgerimi) {
			for (Dostyk dostyk : dostyks) {
				count = getUlgerimiCount(dostyk, type);

				dostyk.ulgerimi.put(type, count);
				//if (dostyks.size() == 1)
				//	for (DostykGroup dostykGroup : dostyk.dostykGroups) {
				//		count = getUlgerimiCount(dostykGroup, type);
						//dostykGroup.ulgerimi.put(type, count);
				//	}
			}
		}
		return dostyks;
	}

	public static List<Dostyk> findRegistrationCount(List<Dostyk> dostyks, Date start, Date end) {
		for (Dostyk dostyk : dostyks) {
			int count = 0;
			count = ProfileLogic.find.where()
					.eq("user.roles", SecurityRole.getByName("student"))
					//.eq("user.lessons", dostyk)
					.eq("user.dostyks", dostyk)
					// .eq("user.status", "active")
					.between("user.createdDate", start, end).findRowCount();
			dostyk.code = count;
		}
		return dostyks;
	}

	public static Map<CourseType, Integer> getCourseTypeCount(
			List<AuthorisedUser> users) {
		Map<CourseType, Integer> courseCount = new HashMap<CourseType, Integer>();
		for (CourseType courseType : CourseTypeLogic.getAll()) {
			courseCount.put(
					courseType,
					ProfileLogic.find.where().in("user", users)
							.eq("user.status", "active")
							.eq("course", courseType).findRowCount());
		}

		return courseCount;
	}

	public static List<Dostyk> findCourseTypeCount(List<Dostyk> dostyks) {
		int count = 0;
		for (CourseType courseType : CourseTypeLogic.getAll()) {
			for (Dostyk dostyk : dostyks) {

				count = ProfileLogic.find.where()
						.eq("user.roles", SecurityRole.getByName("student"))
						.eq("user.dostyks", dostyk).eq("user.status", "active")
						.eq("course", courseType).findRowCount();
				dostyk.courseTypeCount.put(courseType, count);
				if (dostyks.size() == 1)
					for (DostykGroup dostykGroup : dostyk.dostykGroups) {
						count =/* dostykGroup.type.equals("E") ? ProfileLogic.find
								.where()
								.eq("user.roles",
										SecurityRole.getByName("student"))
								.eq("electiveGroup", dostykGroup)
								.eq("user.status", "active")
								.eq("course", courseType).findRowCount()
								: */ProfileLogic.find
										.where()
										.eq("user.roles",
												SecurityRole
														.getByName("student"))
										.eq("dostykGroup", dostykGroup)
										.eq("user.status", "active")
										.eq("course", courseType)
										.findRowCount();
						//dostykGroup.courseTypeCount.put(courseType, count);
					}
			}
		}
		return dostyks;
	}

	public static DostykGroup isCurator(AuthorisedUser user) {
		if (user.roles.contains(SecurityRole.getByName("teacher"))) {
			for (DostykGroup group : user.dostyks.get(0).dostykGroups) {
				if (user.equals(group.curator))
					return group;
			}
		}
		return null;
	}

	public static List<Profile> getByUsers(List<AuthorisedUser> students) {
		return find.where().in("user", students).findList();
	}

	public static List<Dostyk> findMovementCount(List<Dostyk> dostyks) {
		Date months[] = movementMonths();
		int count = 0;
		for (int i = 0; i < months.length - 1; i++) {
			for (Dostyk dostyk : dostyks) {
				count = getMovementCount(dostyk, months[i], months[i + 1]);
				dostyk.movement.put(months[i], count);
				/*
				 * if (lessons.size() == 1) for (DostykGroup dostykGroup :
				 * dostyk.dostykGroups) { count = getUlgerimiCount(dostykGroup,
				 * type); dostykGroup.ulgerimi.put(type, count); }
				 */
			}
		}

		return dostyks;
	}

	public static int getMovementCount(Dostyk dostyk, Date start, Date end) {

		int count = PaymentLogic.find
				.where()
				.in("user_id",
						ProfileLogic.find
								.where()
								.eq("user.roles",
										SecurityRole.getByName("student"))
								.eq("user.dostyks", dostyk)
								.eq("user.status", "active").findIds())
				.between("endDate", start, end).eq("status", "COMPLETED")
				.findRowCount();

		return count;

		/*
		 * ProfileLogic.find.where() .eq("user.roles",
		 * SecurityRole.getByName("student")) .eq("user.lessons", obj)
		 * .eq("user.status", "active") .findRowCount();
		 */
	}

	public static List<Dostyk> findLeftCount(List<Dostyk> dostyks) {
		Date months[] = movementMonths();
		int count = 0;
		for (int i = 0; i < months.length - 1; i++) {
			for (Dostyk dostyk : dostyks) {
				count = getLeftCount(dostyk, months[i], months[i + 1]);
				dostyk.movement.put(months[i], count);

			}
		}

		return dostyks;
	}

	public static int getLeftCount(Dostyk dostyk, Date start, Date end) {
		int count = PaymentLogic.find.where()
				.in("user_id",ProfileLogic.find.where()
								.eq("user.roles",SecurityRole.getByName("student"))
								.eq("user.dostyks", dostyk)
								.eq("user.status", "completed").findIds())
				.between("endDate", start, end)
				.eq("status", "COMPLETED")
				.findRowCount();

		return count;

	}


	public static Date[] movementMonths() {

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		// Date d = sdf.parse("21/12/2012");
		Date months[] = new Date[10];
		try {
			months[0] = sdf.parse("01/09/2017");
			months[1] = sdf.parse("01/10/2017");
			months[2] = sdf.parse("01/11/2017");
			months[3] = sdf.parse("01/12/2017");
			months[4] = sdf.parse("01/01/2018");
			months[5] = sdf.parse("01/02/2018");
			months[6] = sdf.parse("01/03/2018");
			months[7] = sdf.parse("01/04/2018");
			months[8] = sdf.parse("01/05/2018");
			months[9] = sdf.parse("30/06/2018");

		} catch (ParseException e) {// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return months;
	}

	public static List<Dostyk> findRevenueCount(List<Dostyk> dostyks) {
		Date months[] = movementMonths();
		int count = 0;
		for (int i = 0; i < months.length - 1; i++)
		for (Dostyk dostyk : dostyks) {
			count = getRevenueCount(dostyk, months[i], months[i + 1]);
			dostyk.code = count ;
			dostyk.revenue.put(months[i], count);
		}
		

		return dostyks;
	}

	private static int getRevenueCount(Dostyk dostyk, Date start, Date end) {
		int count = PaymentLogic.find
				.where()
				.in("user.profile.id",
						ProfileLogic.find
								.where()
								.eq("user.roles", SecurityRole.getByName("student"))
								.eq("user.lessons", dostyk)
								.eq("user.status", "active").findIds())
				.in("periods.id", PaymentPeriodLogic.find.where().between("payedPaymentDate", start, end)
						.gt("payedPayment", 0)
						.findIds())
				.findRowCount();
		return count;

	}
}
