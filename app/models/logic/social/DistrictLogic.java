package models.logic.social;

import java.util.List;

import models.entities.social.District;
import models.entities.social.Region;
import play.db.ebean.Model.Finder;

public class DistrictLogic {
	public static Finder<Long, District> find = new Finder<Long, District>(
			Long.class, District.class);

	public static District getByCode(String code, Region reg) {
		return find.where().eq("region", reg).eq("code", code).findUnique();
	}

	public static List<District> getAll() {
		return find.all();
	}

	public static List<District> getAllSortedByRegions(){
		return find.where().orderBy("region.code").orderBy("code").findList();
	}

	public static List<District> getAllByRegion(Region region){
		return find.where().eq("region",region).orderBy("code").findList();
	}
}
