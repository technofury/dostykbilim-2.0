package models.logic.social;

import javax.persistence.Id;

/**
 * Created by arsen on 4/4/2016.
 */
//@Entity
//@Table(name = "images")
public class Image { //extends Model{
    //save images locally, in order to make database dump lightweight

    @Id
    public Long id;

    public String path;

    public String extension;
}