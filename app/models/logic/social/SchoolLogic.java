package models.logic.social;

import java.util.List;

import models.entities.social.District;
import models.entities.social.Region;
import models.entities.social.School;
import play.db.ebean.Model.Finder;

public class SchoolLogic {
	public static Finder<Long, School> find = new Finder<Long, School>(
			Long.class, School.class);

	public static School getByCode(String code, Region reg, District dis) {
		return find.where().eq("region", reg).eq("district", dis)
				.eq("code", code).findUnique();
	}

	public static List<School> getAll() {
		return find.all();
	}
	public static List<School> getAllSortedByRegionsAndDistricts(){
		return find.orderBy("region.code").orderBy("district.code").orderBy("code").findList();
	}
	public static List<School> getAllByInfo(Region region, District district){
		return find.where().eq("region",region).eq("district",district).orderBy("code").findList();
	}
}
