package models.logic.social;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import models.entities.examination.Exam;
import models.entities.social.Dostyk;
import models.entities.user.AuthorisedUser;
import models.entities.user.SecurityRole;
import models.logic.user.AuthorisedUserLogic;
import play.db.ebean.Model.Finder;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.SqlQuery;
import com.avaje.ebean.SqlRow;

public class DostykLogic {
	public static Finder<Long, Dostyk> find = new Finder<Long, Dostyk>(
			Long.class, Dostyk.class);

	public static List<Dostyk> getAll() {
		return find.where().eq("status",1).orderBy("name").findList();
	}

	public static int getStudentSize(Dostyk d, List<String> statuses) {
		return AuthorisedUserLogic.find.where()
				.eq("roles", SecurityRole.getByName("student"))
				.eq("dostyks", d)
				// .in("payments.status", paymentStatuses)
				.in("status", statuses).findRowCount();
	}

	public static int getStudentSize(Dostyk d, String paymentStatus,
			List<String> statuses) {
		if (!paymentStatus.equals("all"))
			return AuthorisedUserLogic.find.where()
					.eq("roles", SecurityRole.getByName("student"))
					.eq("dostyks", d).eq("payments.status", paymentStatus)
					.in("status", statuses).findRowCount();
		else
			return AuthorisedUserLogic.find.where()
					.eq("roles", SecurityRole.getByName("student"))
					.eq("dostyks", d).in("status", statuses).findRowCount();
	}

	public static int getStudentsSize(Dostyk d, String status) {
		return AuthorisedUserLogic.find.where()
				.eq("roles", SecurityRole.getByName("student"))
				.eq("dostyks", d).eq("status", status).findRowCount();
	}

	public static int getTenStudentsSize(Dostyk d, String status) {
		return AuthorisedUserLogic.find.where()
				.eq("roles", SecurityRole.getByName("ten"))
				.eq("dostyks", d).eq("status", status).findRowCount();
	}

	public static int getStudents(Exam e, Dostyk d, String status) {
		String sql = "SELECT count(u.id) count FROM users u, profiles p "
				+ "WHERE u.id=p.id "
				+ "AND u.status = '"
				+ status
				+ "' "
				+ "AND u.id in (SELECT users_id FROM dostyks_users WHERE dostyks_id = "
				+ d.id
				+ ") "
				+ "AND (SELECT c.class_number FROM course_types c WHERE c.id=p.course_id)="
				+ e.grade;
		SqlQuery sqlQuery = Ebean.createSqlQuery(sql);
		SqlRow row = sqlQuery.findUnique();
		return row.get("count") != null ? Integer.parseInt(row.get("count")
				.toString()) : 0;
	}

	public static Dostyk getByUser(AuthorisedUser user) {
		for (Dostyk cur : find.all()) {
			if (cur.id == user.dostyks.get(0).id) {
				return cur;
			}
		}
		return null;
	}

	public static Dostyk getByUser2(AuthorisedUser student) {
		for (Dostyk dostyk : find.all()) {
			for (AuthorisedUser user : dostyk.users)
				if (user.id == student.profile.id)
					return dostyk;
		}
		return null;
	}

	public static Dostyk getByCode(int code) {
		return find.where().eq("code", code).findUnique();
	}

	public static int countByCode(Long id,int code) {
		return find.where().ne("id",id).eq("code", code).findRowCount();
	}

	public static Dostyk getByRnn(String rnn) {
		return find.where().eq("rnn", rnn).findUnique();
	}

	public static int countByRnn(String rnn) {
		return find.where().eq("rnn", rnn).findRowCount();
	}

	public static Dostyk getById(long id) {
		return find.byId(id);
	}

	public static List<AuthorisedUser> getTeacherList(Dostyk dostyk) {
		return AuthorisedUserLogic.getTeachersByDostyk(dostyk);
	}

	public static Map<String, String> getTeacherMap(Dostyk dostyk) {
		Map<String, String> options = new HashMap<String, String>();
		List<AuthorisedUser> teachers = AuthorisedUserLogic
				.getTeachersByDostyk(dostyk);
		for (AuthorisedUser user : teachers) {
			options.put("" + user.id, user.profile.firstName + " "
					+ user.profile.lastName);
		}
		return options;
	}

	public static List<String> list() {
		List<Dostyk> dostyks = find.all();
		List<String> names = new ArrayList<String>();
		for (int i = 0; i < dostyks.size(); i++) {
			names.add(dostyks.get(i).name);
		}
		return names;
	}

	public static Dostyk getByName(String name) {
		return find.where().eq("name", name).findUnique();
	}

}
