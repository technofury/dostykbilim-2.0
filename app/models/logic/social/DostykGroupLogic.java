package models.logic.social;

import java.util.*;

import models.entities.education.Lesson;
import models.entities.social.Dostyk;
import models.entities.social.DostykGroup;
import models.entities.user.AuthorisedUser;
import models.logic.user.AuthorisedUserLogic;
import models.timetable.TimetableLesson;
import play.db.ebean.Model.Finder;
import models.timetable.Timetable;

public class DostykGroupLogic {

	public static Finder<Long, DostykGroup> find = new Finder<Long, DostykGroup>(
			Long.class, DostykGroup.class);

	public static List<DostykGroup> getAllCompanyGroups() {
		//return find.where().eq("type", "M").findList();
		return find.where().findList();
	}

	public static List<DostykGroup> getMekeme(Dostyk dostyk) {
		//return find.where().eq("type", "M").eq("dostyk", dostyk).findList();
		return find.where().eq("dostyk", dostyk).findList();
	}

	public static List<DostykGroup> getAllByDostyk(Dostyk dostyk) {
		return find.where().eq("dostyk_id", dostyk.id).orderBy("name").findList();
	}
	public static List<DostykGroup> getAllWithUser(AuthorisedUser user) throws Exception{
		Set<DostykGroup> hs = new HashSet<>();
		String role = user.roles.get(0).name;
		if(role.equals("student")){
			hs.add(user.profile.dostykGroup);
		}else if(role.equals("teacher")){
			List<TimetableLesson> timetableLessons = TimetableLesson.byUser(user);
			List<Timetable> timetables = null;
			for (int i=0; i<timetableLessons.size(); i++){
				timetables.add(timetableLessons.get(i).timetable);

			}

			if(timetables.size()>0)
				for(Timetable timetable: timetables)
					if(timetable.grade!=null)
						hs.add(timetable.grade);
		}
		List<DostykGroup> result = new ArrayList<>();
		result.addAll(hs);
		return result;
	}
	public static DostykGroup getById(long id) {
		return find.byId(id);
	}

	public static boolean delete(DostykGroup t) {
		t.delete();
		return false;
	}

	public static boolean create(DostykGroup t) {
		try {
			t.save();
			return true;
		} catch (Exception ex) {
			DostykGroup dg = getById(t.id);
			dg.name = "000";
			dg.save();
			return false;
		}

	}

	public static Map<String, String> option(Dostyk dostyk) {
		Map<String, String> map = new HashMap<String, String>();
		List<DostykGroup> dostykGroups = getAllByDostyk(dostyk);
		for (DostykGroup dostykGroup : dostykGroups) {
			map.put(dostykGroup.id + "", dostykGroup.name);
		}
		return map;
	}

	public static List<AuthorisedUser> getUsersByDostykGroup(DostykGroup dg) {
		return AuthorisedUserLogic.getByProfiles(dg.students);
	}

	public static Map<String, String> optionMekeme(Dostyk dostyk) {
		Map<String, String> map = new HashMap<String, String>();
		List<DostykGroup> dostykGroups = getAllByDostyk(dostyk);
		for (DostykGroup dostykGroup : dostykGroups) {
				map.put(dostykGroup.id + "", dostykGroup.name);
		}
		return map;
	}

	public static List<DostykGroup> getByCurator(AuthorisedUser user) {
		return find.where().eq("curator", user).findList();
	}

	public static List<DostykGroup> getByNameAndDostyk(String name,
			Dostyk dostyk) {
		return find.where().eq("name", name).eq("dostyk.id", dostyk.id)
				.findList();
	}

	public static List<DostykGroup> getByConnected() {
		AuthorisedUser u = AuthorisedUser.getUserByEmail(play.mvc.Controller
				.session("connected"));
		if (u.roles.get(0).name.equals("admin")) {
			//return find.where().ne("type", "E").orderBy("name").findList();
			return find.where().orderBy("name").findList();
		} else
			return getByDostykNotElective(u.dostyks.get(0));
	}

	private static List<DostykGroup> getByDostykNotElective(Dostyk dostyk) {
		//return find.where().eq("dostyk_id", dostyk.id).ne("type", "E").orderBy("name").findList();
		return find.where().eq("dostyk_id", dostyk.id).orderBy("name").findList();
	}

	public static List<DostykGroup> getNotElectives(Dostyk dostyk) {
		//return find.where().eq("dostyk.id", dostyk.id).ne("type", "E").orderBy("name").findList();
		return find.where().eq("dostyk.id", dostyk.id).orderBy("name").findList();
	}

	public static List<DostykGroup> getElectives(Dostyk dostyk, Lesson lesson) {
		//return find.where().eq("dostyk.id", dostyk.id).eq("type", "E").eq("lessonid", lesson.id).findList();
		return find.where().eq("dostyk.id", dostyk.id).eq("lessonid", lesson.id).findList();
		// type deleted from table
	}

}