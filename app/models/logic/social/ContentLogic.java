package models.logic.social;

import java.util.Date;
import java.util.List;

import models.entities.social.Content;
import models.entities.social.Dostyk;
import play.db.ebean.Model;

/**
 * Created with IntelliJ IDEA. User: balancy Date: 7/10/13 Time: 1:34 PM To
 * change this template use File | Settings | File Templates.
 */

public class ContentLogic {
	public static Model.Finder<Long, Content> find = new Model.Finder<Long, Content>(
			Long.class, Content.class);

	public static List<Content> getAll(Dostyk dostyk) {
		return find.where().eq("dostyk_id", dostyk.id).findList();
	}

	public static Content getById(long id) {
		return find.byId(id);
	}

	public static boolean create(Content t) {

		try {
			t.save();
		} catch (Exception ex) {
			Content lh = getById(t.id);

			lh.title = t.title;
			lh.body = t.body;
			lh.changedDate = new Date();
			lh.save();
		}
		return false;
	}

	public static boolean delete(Content t) {
		t.delete();
		return false;
	}
}
