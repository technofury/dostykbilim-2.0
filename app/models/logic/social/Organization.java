package models.logic.social;

import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Created by arsen on 4/4/2016.
 */
//@Entity
//@Table(name="organizations")
public class Organization{ //extends Model {
    @Id
    public Long id;

    public String name;

    @OneToOne
    public Theme theme;
}
