package models.logic.social;

import java.util.List;

import models.entities.social.Region;
import play.db.ebean.Model.Finder;

public class RegionLogic {
	public static Finder<Long, Region> find = new Finder<Long, Region>(
			Long.class, Region.class);

	public static List<Region> getAll() {
		return find.orderBy("code").findList();
	}

	public static Region getByCode(String code) {
		return find.where().eq("code", code).findUnique();
	}
}
