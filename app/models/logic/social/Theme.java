package models.logic.social;

import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Created by arsen on 4/4/2016.
 */
//@Entity
//@Table(name = "themes")
public class Theme{ //extends Model {
    @Id
    public Long id;

    @OneToOne
    public Image logo;

    @OneToOne
    public Image backgroundPattern;

    public String primaryColor;

    public String secondaryColor;

    public String tetriaryColor;
}
