package sms;

import play.mvc.Controller;
import play.mvc.Result;
import play.twirl.api.Content;
import flexjson.JSONSerializer;

public class Response extends Controller {

	public static Result json(Object obj) {
		JSONSerializer json = new JSONSerializer().prettyPrint(true)
//				.include("order.*")
//				.include("parents.*")
//				.include("parents.phoneNumber1")
				
//				.include("order.foodAmounts")
				;

		return ok(json.serialize(obj));
	}

	public static Result htmlJson(Content html, Object data) {
		if (request().accepts("text/html"))
			return ok(html);
		else
			return json(data);

	}
}