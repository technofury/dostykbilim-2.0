package sms;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import models.entities.education.Register;
import models.entities.social.DostykGroup;
import models.entities.user.AuthorisedUser;
import models.entities.user.Parent;
import models.logic.education.RegisterLogic;
import models.logic.social.DostykGroupLogic;
import models.logic.user.AuthorisedUserLogic;
import play.mvc.Controller;
import play.mvc.Result;
import models.timetable.Timetable;
import com.fasterxml.jackson.databind.JsonNode;

public class Users extends Controller {

	public static Result groupStudents(Long id) {
		DostykGroup dostykGroup = DostykGroupLogic.getById(id);
		Object[] obj = new Object[2];
		List<AuthorisedUser> students = AuthorisedUserLogic
				.getByProfiles(dostykGroup.students);
		List<String> phones = new ArrayList<String>();
		for (AuthorisedUser user : students) {
			boolean inserted = false;
			for (Parent p : user.parents) {
				if (p.responsible) {
					phones.add(p.phoneNumber1);
					inserted = true;
				}
			}
			if (!inserted)
				phones.add("");
		}
		obj[0] = students;
		obj[1] = phones;
		return Response.json(obj);
	}

	public static Result groups(String moder) {
		AuthorisedUser user = AuthorisedUser.getUserByEmail(moder);
		List<DostykGroup> dostykGroups = null;
		if (user.id == 1L) {
			dostykGroups = DostykGroupLogic.getAllCompanyGroups();
		} else {
			dostykGroups = user.dostyks.get(0).dostykGroups;
		}
		Collections.sort(dostykGroups, new Comparator<DostykGroup>() {
			@Override
			public int compare(DostykGroup a, DostykGroup b) {
				return a.name.compareTo(b.name);
			}
		});
		return Response.json(dostykGroups);
	}

	public static Result save() throws Exception {
		JsonNode json = request().body().asJson();
		Long timetableId = json.findPath("timetableId").asLong();
		String dateStr = json.findPath("date").asText();
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");

		Timetable timetable = Timetable.byId(timetableId);
		Date date = format.parse(dateStr);
		JsonNode node = json.findPath("users");

		Iterator<JsonNode> it = node.elements();
		while (it.hasNext()) {
			JsonNode cur = it.next();
			Iterator<String> names = cur.fieldNames();
			String userId = names.next();
			int presence = cur.findPath(userId).asInt();

			AuthorisedUser user = AuthorisedUser.getUserById(Long
					.parseLong(userId));
			Register register = RegisterLogic
					.getByFields(user, timetable, date);
			if (register == null)
				register = new Register();
			register.student = user;
			register.timetable = timetable;
			//register.lessonid = timetable.lesson.id;
			register.date = date;
			String att = "";
			if (presence == 0)
				att = "present";
			if (presence == 1)
				att = "late";
			if (presence == 2)
				att = "notPresent";
			register.attendance = att;
			register.save();
		}

		return ok("ok");
	}

}
