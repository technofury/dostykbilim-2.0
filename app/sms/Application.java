package sms;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import models.entities.education.Register;
import models.entities.social.Dostyk;
import models.entities.user.AuthorisedUser;
import models.entities.user.Parent;
import models.entities.user.SecurityRole;
import models.logic.education.RegisterLogic;
import play.libs.Crypto;
import play.mvc.Controller;
import play.mvc.Result;
import models.timetable.Timetable;

public class Application extends Controller {

	public static Result index() {
		return Response.json("ok");
	}

	public static Result logIn(String mail, String pass) {
		pass = Crypto.encryptAES(pass);
		AuthorisedUser user = AuthorisedUser.getUser(mail, pass);

		if (user == null)
			return Response.json("no");
		if (!user.status.equals("active"))
			return Response.json("inactive");
		if (!user.roles.contains(SecurityRole.getByName("moderator")))
			return Response.json("moderator");
		if (user.dostyks == null || user.dostyks.size() == 0)
			return Response.json("noDostyk");
		return Response.json(user.dostyks.get(0).name);
	}

	public static Result getSMS(String mail) {
		AuthorisedUser moder = AuthorisedUser.getUserByEmail(mail);
		if (moder.dostyks.size() > 0) {
			return Response.json(moder.dostyks.get(0).smsLeft);
		} else {
			return Response.json("no");
		}
	}

	public static Result decSMS(String mail) {
		AuthorisedUser moder = AuthorisedUser.getUserByEmail(mail);
		if (moder.dostyks.size() > 0) {
			Dostyk d = moder.dostyks.get(0);
			d.smsLeft -= 1;
			d.save();
			return Response.json("ok");
		} else {
			return Response.json("no");
		}
	}

	public static Result getDay(String mail, String date) throws ParseException {
		if (date.equals("-1")) {
			DateFormat format = new SimpleDateFormat("EE dd MM yyyy", Locale.US);
			Date current = new Date();
			date = format.format(current);
		} else {
			DateFormat format = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
			Date cur = format.parse(date);
			format = new SimpleDateFormat("EE dd MM yyyy", Locale.US);
			date = format.format(cur);
		}

		String[] splitted = date.split(" ");
		String[] weekDays = { "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun" };
		int day = -1;
		for (int i = 0; i < weekDays.length; i++)
			if (splitted[0].equals(weekDays[i])) {
				day = i + 1;
				break;
			}
		AuthorisedUser moder = AuthorisedUser.getUserByEmail(mail);
		List<Timetable> lessons = null;//TimetableLogic.getByWeekDay(day, moder);

		return Response.json(lessons);
	}

	public static Result getGroup(String date, Long timetableId) {
		Timetable current = Timetable.byId(timetableId);
		List<AuthorisedUser> students = null;
		AuthorisedUser teacher = null;
		/*if (current.electiveGroup != null) {
			students = AuthorisedUser.getByElectiveGroup(current.electiveGroup);
			teacher = current.electiveGroup.curator;
		} else {
			students = AuthorisedUser.getByDostykGroup(current.dostykGroup);
			for(AuthorisedUser s:students){
				//s.profile.parentNumber =
				Parent parent = Parent.find.where().eq("child", s).eq("responsible", true).findUnique();
				if(parent!=null){
					s.profile.parentNumber = parent.phoneNumber1;
				}
				
			}
			teacher = current.teacher;
		}*/
		new SimpleDateFormat("HH:mm");
		List<String> attendance = new ArrayList<String>();
		List<String> phones = new ArrayList<String>();

		DateFormat format2 = new SimpleDateFormat("dd-MM-yyyy");
		Date dates = null;
		try {
			dates = format2.parse(date);
		} catch (ParseException e) {

		}
		Register cur = RegisterLogic.getByFields(teacher, current, dates);
		phones.add(teacher.profile.phoneNumber);
		if (cur == null)
			attendance.add("present");
		else
			attendance.add(cur.attendance);
		for (AuthorisedUser user : students) {
			cur = RegisterLogic.getByFields(user, current, dates);
			if (cur == null)
				attendance.add("present");
			else
				attendance.add(cur.attendance);
			boolean inserted = false;
			Parent parent = Parent.find.where().eq("child", user).eq("responsible", true).findUnique();
			parent.phoneNumber1 = (parent!=null)?parent.phoneNumber1 :"";
			parent.phoneNumber1 = parent.phoneNumber1.replace(" ", "");
			parent.phoneNumber1 = parent.phoneNumber1.replace("(", "");
			parent.phoneNumber1 = parent.phoneNumber1.replace(")", "");
			phones.add(parent.phoneNumber1);
			
			/*
			for (Parent p : user.parents) {
				if (p.responsible) {
					phones.add(p.phoneNumber1);
					inserted = true;
				}
			}
			if (!inserted)
				phones.add("");
				*/
		}

		Object[] obj = new Object[3];
		students.add(0, teacher);
		obj[0] = students;
		obj[1] = attendance;
		obj[2] = phones;
		return Response.json(obj);
	}

}
