package controllers.accounting;

import models.entities.accounting.Holiday;
import play.data.Form;

import play.i18n.Messages;
import play.mvc.Result;
import play.mvc.Controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import static play.data.Form.form;
import views.html.accounting.holidays;
import views.html.accounting.editHoliday;
import views.html.helps.messenger;


/**
 * Created by User on 23-Feb-17.
 */
public class Holidays extends Controller {

    final static Form<Holiday> hForm = form(Holiday.class);

    public static Result getHolidays(){
        List<Holiday> holidayList = Holiday.find.findList();
        return ok(holidays.render(holidayList,hForm));
    }

    public static Result addHoliday(){

        Form<Holiday> holidayForm = form(Holiday.class).bindFromRequest();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        try {
            if(!dateFormat.parse(holidayForm.data().get("startDate")).before(dateFormat.parse(holidayForm.data().get("endDate")))){
                holidayForm.reject("endDate", Messages.get("date.error"));
            }

            if (holidayForm.hasErrors()) {
                List<Holiday> holidayList = Holiday.find.findList();
                return badRequest(holidays.render(holidayList,holidayForm));
            } else {
                Holiday holiday = new Holiday();
                holiday.startDate = dateFormat.parse(holidayForm.data().get("startDate"));
                holiday.endDate = dateFormat.parse(holidayForm.data().get("endDate"));
                holiday.save();
            }
        } catch (Exception e){
            e.printStackTrace();
        }

        List<Holiday> holidayList = Holiday.find.findList();
        return ok(holidays.render(holidayList,hForm));
    }

    public static Result showHolidayById(Long id){

        Holiday holiday = Holiday.find.byId(id);
        return ok(editHoliday.render(hForm.fill(holiday),id));
    }

    public static Result editHoliday(Long id){

        Form<Holiday> holidayForm = form(Holiday.class).bindFromRequest();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        try {
            if(!dateFormat.parse(holidayForm.data().get("startDate")).before(dateFormat.parse(holidayForm.data().get("endDate")))){
                holidayForm.reject("endDate", Messages.get("date.error"));
            }
            if (holidayForm.hasErrors()) {
                return badRequest(editHoliday.render(holidayForm,id));
            } else {
                Holiday holiday = Holiday.find.byId(id);
                holiday.startDate = dateFormat.parse(holidayForm.data().get("startDate"));
                holiday.endDate = dateFormat.parse(holidayForm.data().get("endDate"));
                holiday.update();
            }
        } catch (Exception e){
            e.printStackTrace();
        }

        List<Holiday> holidayList = Holiday.find.findList();
        return ok(holidays.render(holidayList,hForm));
    }

    public static Result deleteHoliday(Long id){
        Holiday holiday = Holiday.find.byId(id);
        holiday.delete();
        return ok(messenger.render("alert alert-success","congratulations",Messages.get("dostyk.successfullyDeleted")));
    }


}
