package controllers.accounting;

import static play.data.Form.form;

import java.util.List;

import models.entities.accounting.DiscountMax;
import models.entities.accounting.DiscountValue;
import models.entities.social.Dostyk;
import models.entities.user.AuthorisedUser;
import models.entities.user.SecurityRole;
import models.logic.accounting.DiscountMaxLogic;
import models.logic.accounting.DiscountValueLogic;
import models.logic.social.DostykLogic;
import play.data.DynamicForm;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.accounting.discountReport;
import views.html.accounting.discounts;

public class DiscountValues extends Controller {

	public static Result discountReport() {
		String lang = session("lang");
		AuthorisedUser activeUser = AuthorisedUser.getUserByEmail(session("connected"));
		String userRole = activeUser.getRoles().get(0).getName();
		AuthorisedUser user = AuthorisedUser.getUserByEmail(session("connected"));
		List<Dostyk> dostyks = null;
		if (user.roles.contains(SecurityRole.getByName("admin"))) {
			dostyks = DostykLogic.getAll();
		} else if (user.roles.contains(SecurityRole.getByName("moderator"))) {
			dostyks = user.dostyks;
		}
		return ok(discountReport.render(dostyks, lang, userRole));
	}

	public static boolean checkDiscounts() {
		boolean result = false;
		String email = session("connected");
		AuthorisedUser con = AuthorisedUser.getUserByEmail(email);
		Dostyk d = con.dostyks.get(0);
		List<DiscountValue> dVals = DiscountValueLogic.getAllByDostykById(d);
		List<DiscountMax> dMaxs = DiscountMaxLogic.getAllOrderById();
		boolean tabylmady = true;
		int counter = 0;
		for (DiscountMax max : dMaxs) {
			tabylmady = true;
			counter = 0;
			for (DiscountValue value : dVals) {
				if (max.nameKz.equals(value.dmax.nameKz)) {
					tabylmady = false;
					counter++;
				}
			}
			if (tabylmady == true) {
				result = true;
				break;
			} else if (counter > 1) {
				result = true;
				break;
			}
		}
		return result;
	}

	public static void refreshDiscounts() {
		String email = session("connected");
		AuthorisedUser con = AuthorisedUser.getUserByEmail(email);
		Dostyk d = con.dostyks.get(0);
		List<DiscountValue> dVals = DiscountValueLogic.getAllByDostykById(d);
		List<DiscountMax> dMaxs = DiscountMaxLogic.getAllOrderById();
		for (DiscountValue discount : dVals) {
			discount.delete();
		}
		for (DiscountMax max : dMaxs) {
			DiscountValue value = new DiscountValue();
			value.value = max.value;
			value.dostyk = d;
			value.dmax = max;
			value.save();
		}
	}

	public static Result showDiscountValues() {
		String email = session("connected");
		String lang = session("lang");

		AuthorisedUser con = AuthorisedUser.getUserByEmail(email);
		Dostyk d = con.dostyks.get(0);
		List<DiscountValue> dVals = DiscountValueLogic.getAllByDostykById(d);
		List<DiscountMax> dMaxs = DiscountMaxLogic.getAllOrderById();
		if (dMaxs.size() == 0) {
			return badRequest(discounts.render(dVals, "alert alert-warning",
					"discount.adminDoesNotSetYet",lang));
		} else {
			boolean something_wrong = checkDiscounts();
			if (something_wrong == true) {
				refreshDiscounts();
			}
			dVals = DiscountValueLogic.getAllByDostykByValue(d);
			return ok(discounts.render(dVals, "", "",lang));
		}
	}

	public static Result saveDiscountValues() {
		String email = session("connected");
		String lang = session("lang");
		AuthorisedUser con = AuthorisedUser.getUserByEmail(email);
		Dostyk d = con.dostyks.get(0);
		List<DiscountValue> dVals = DiscountValueLogic.getAllByDostykById(d);
		try {
			DynamicForm requestData = form().bindFromRequest();
			for (int i = 0; i < dVals.size(); i++) {
				DiscountValue toSave = DiscountValueLogic
						.getById(dVals.get(i).id);
				toSave.value = Integer.parseInt(requestData
						.get(toSave.dmax.nameKz));
				toSave.save();
			}
			dVals = DiscountValueLogic.getAllByDostykByValue(d);
		} catch (Exception e) {
			return badRequest(discounts.render(dVals, "alert alert-success",	e.getMessage(),lang));
		}
		return ok(discounts.render(dVals, "alert alert-success",	"discount.successfullySaved",lang));
	}
}
