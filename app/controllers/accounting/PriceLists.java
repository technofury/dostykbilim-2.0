package controllers.accounting;

import static play.data.Form.form;
import models.entities.accounting.PriceList;
import models.entities.user.AuthorisedUser;
import models.logic.accounting.PriceListLogic;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.accounting.priceList;
import views.html.accounting.priceListForm;

public class PriceLists extends Controller {

	public static Result set() {
		AuthorisedUser user = AuthorisedUser
				.getUserByEmail(session("connected"));
		return ok(priceListForm.render(user.dostyks.get(0)));
	}

	public static Result save() {
		AuthorisedUser user = AuthorisedUser
				.getUserByEmail(session("connected"));
		Form<PriceList> form = form(PriceList.class).bindFromRequest();
		if (form.hasErrors()) {
			return badRequest(priceListForm.render(user.dostyks.get(0)));
		} else {
			PriceList created = form.get();
			created.dostyk = user.dostyks.get(0);
			PriceListLogic.create(created);
			return ok(priceList.render(created.courseType, created.dostyk));
		}

	}

	public static Result update() {
		AuthorisedUser user = AuthorisedUser
				.getUserByEmail(session("connected"));
		Form<PriceList> form = form(PriceList.class).bindFromRequest();
		if (form.hasErrors()) {
			return badRequest(priceListForm.render(user.dostyks.get(0)));
		} else {
			PriceList created = form.get();
			created.dostyk = user.dostyks.get(0);
			PriceListLogic.update(created);
			return ok(priceList.render(created.courseType, created.dostyk));
		}

	}

	public static Result delete() {
		AuthorisedUser user = AuthorisedUser
				.getUserByEmail(session("connected"));
		Form<PriceList> form = form(PriceList.class).bindFromRequest();
		if (form.hasErrors()) {
			return badRequest(priceListForm.render(user.dostyks.get(0)));
		} else {
			PriceList created = form.get();
			created.dostyk = user.dostyks.get(0);
			PriceListLogic.delete(created);
			return ok(priceList.render(created.courseType, created.dostyk));
		}

	}

}
