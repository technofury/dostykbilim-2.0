package controllers.accounting;

import static play.data.Form.form;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;

import models.entities.accounting.*;
import models.entities.education.Lesson;
import models.entities.social.Dostyk;
import models.entities.social.DostykGroup;
import models.entities.user.AuthorisedUser;
import models.entities.user.Parent;
import models.entities.user.Requisites;
import models.entities.user.SecurityRole;
import models.logic.accounting.Contract;
import models.logic.accounting.PaymentLogic;
import models.logic.accounting.PaymentPeriodLogic;
import models.logic.accounting.PaymentReceiptLogic;
import models.logic.education.LessonLogic;
import models.logic.social.DostykGroupLogic;
import models.logic.social.DostykLogic;
import models.logic.user.AuthorisedUserLogic;
import models.logic.user.ProfileLogic;
import play.data.DynamicForm;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.accounting._dostykReport;
import views.html.accounting._groupReport;
import views.html.accounting._studentReport;
import views.html.accounting.outlays;
import views.html.accounting.paymentReport;
import views.html.accounting.pko;
import views.html.accounting.rko;
import views.html.helps.messenger;
import views.html.user.profilePack.paymentList;
import be.objectify.deadbolt.java.actions.Group;
import be.objectify.deadbolt.java.actions.Restrict;
import controllers.user.AuthorisedUsers;
import controllers.utils.Response;

public class Payments extends Controller {
    @Restrict({@Group("director"), @Group("admin")})
    public static Result outlays() throws Exception {
        List<String> dcodes = new ArrayList<String>(Arrays.asList("100", "101",
                "110", "111", "112", "120", "121", "122", "130", "131", "132",
                "135", "150", "140", "160", "170", "180", "190", "200", "210",
                "220", "900", "901", "910"));
        String date_range;
        List<Payment> outs;
        int sPrice, ePrice;
        AuthorisedUser user = AuthorisedUsers.getConnectedUser();
        DynamicForm form = form().bindFromRequest();
        String code;
        if (form.get("delete") != null) {
            long id = Long.parseLong(form.get("delete"));
            PaymentLogic.find.byId(id).delete();
            date_range = form.get("gRange");
            code = form.get("code");
            sPrice = Integer.parseInt(form.get("sPrice"));
            ePrice = Integer.parseInt(form.get("ePrice"));
            StringTokenizer st = new StringTokenizer(date_range, " - ");
            String from = st.nextToken();
            String to = st.nextToken();
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            Date dFrom = df.parse(from);
            Date dTo = df.parse(to);
            List<String> codes = new ArrayList<String>();
            if (code.equals("all"))
                codes = dcodes;
            else
                codes.add(code);
            if (user.roles.get(0).name.equals("admin")) {
                List<AuthorisedUser> users = AuthorisedUserLogic.find.where()
                        .in("roles", SecurityRole.getByName("director"))
                        .findList();
                outs = PaymentLogic.find
                        .where()
                        .in("status", codes)
                        .in("user", users)
                        .between("payedPayment", (double) sPrice,
                                (double) ePrice)
                        .between("startDate", dFrom, dTo).orderBy("startDate")
                        .findList();
            } else
                outs = PaymentLogic.find
                        .where()
                        .in("status", codes)
                        .eq("user", user)
                        .between("payedPayment", (double) sPrice,
                                (double) ePrice)
                        .between("startDate", dFrom, dTo).orderBy("startDate")
                        .findList();
        } else if (form.get("delete") == null && form.get("gRange") != null) {
            date_range = form.get("gRange");
            code = form.get("code");
            sPrice = Integer.parseInt(form.get("sPrice"));
            ePrice = Integer.parseInt(form.get("ePrice"));
            StringTokenizer st = new StringTokenizer(date_range, " - ");
            String from = st.nextToken();
            String to = st.nextToken();
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            Date dFrom = df.parse(from);
            Date dTo = df.parse(to);
            List<String> codes = new ArrayList<String>();
            if (code.equals("all"))
                codes = dcodes;
            else
                codes.add(code);
            if (user.roles.get(0).name.equals("admin")) {
                List<AuthorisedUser> users = AuthorisedUserLogic.find.where()
                        .in("roles", SecurityRole.getByName("director"))
                        .findList();
                outs = PaymentLogic.find
                        .where()
                        .in("status", codes)
                        .in("user", users)
                        .between("payedPayment", (double) sPrice,
                                (double) ePrice)
                        .between("startDate", dFrom, dTo).orderBy("startDate")
                        .findList();
            } else
                outs = PaymentLogic.find
                        .where()
                        .in("status", codes)
                        .eq("user", user)
                        .between("payedPayment", (double) sPrice,
                                (double) ePrice)
                        .between("startDate", dFrom, dTo).orderBy("startDate")
                        .findList();
            // return ok(outlays.render(date_range, 0, Integer.MAX_VALUE, outs,
            // "all", dcodes));
        } else if (form.get("price") != null) {
            code = form.get("code");
            int price = Integer.parseInt(form.get("price"));
            String comment = form.get("comment");
            Payment out = new Payment();
            out.startDate = new Date();
            out.comment = comment;
            out.payedPayment = price;
            out.status = code;
            out.user = user;
            out.save();
            DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
            Date today = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(today);
            int days = 1;
            cal.add(Calendar.DATE, days);
            Date tomorrow = cal.getTime();
            date_range = format.format(today) + " - " + format.format(tomorrow);
            outs = null;
            sPrice = -1;
            ePrice = -1;
            // return ok(outlays.render(date_range, 0, Integer.MAX_VALUE, outs,
            // "all", dcodes));
        } else {
            DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
            Date today = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(today);
            int days = 1;
            cal.add(Calendar.DATE, days);
            Date tomorrow = cal.getTime();
            date_range = format.format(today) + " - " + format.format(tomorrow);
            outs = null;
            sPrice = -1;
            ePrice = -1;
            code = "all";
            // return ok(outlays.render(range, 0, Integer.MAX_VALUE, null,
            // "all", dcodes));
        }
        if (sPrice != -1)
            return ok(outlays.render(date_range, sPrice, ePrice, outs, code,
                    dcodes));
        else
            return ok(outlays
                    .render(date_range, null, null, outs, code, dcodes));
    }

    public static Result showPaymentList(Long id) {
        AuthorisedUser user;
        if (id == -1) {
            user = AuthorisedUser.getUserByEmail(session().get("connected"));
        } else {
            user = AuthorisedUser.getUserById(id);
        }
        if (user.status.equalsIgnoreCase("deleted")) {

            return badRequest(messenger.render("alert alert-warning",
                    Messages.get("paymentListError"), Messages.get("Deleted")));
        } else {

            Payment payment = PaymentLogic.getActualPayment(user);
            if(payment != null) {
                List<PaymentPeriod> periods = PaymentPeriodLogic
                        .getPeriods(payment);
                for (PaymentPeriod period : periods) {
                    if (period.status.equalsIgnoreCase("PLANNED")) {
                        period.status = "CURRENT";
                        period.save();
                        break;
                    } else if (period.status.equalsIgnoreCase("CURRENT")) {
                        break;
                    }
                }
                payment = PaymentLogic.getActualPayment(user);
                List<PaymentReceipt> receipts = PaymentReceiptLogic.getByUser(user);
                Date today = getTodayDate();
                String stoday = new SimpleDateFormat("yyyy-MM-dd").format(today);
                return ok(paymentList.render(user, payment, PaymentPeriodLogic.getPeriods(payment), receipts, user.roles.get(0).name,stoday));
            }else{
                return ok(Messages.get("payment.not.exists"));
            }
        }
    }

    private static Date getTodayDate() {
        Calendar gc = Calendar.getInstance();
        gc.set(Calendar.HOUR_OF_DAY, 0);
        gc.set(Calendar.MINUTE, 0);
        gc.set(Calendar.SECOND, 0);
        gc.set(Calendar.MILLISECOND, 0);
        return gc.getTime();
    }

    public static Result getDiscountReport(){

        return ok();
    }

    public static Result savePaymentList(Long id) {
        try {
            DynamicForm df = form().bindFromRequest();
            AuthorisedUser user;
            if (id == -1) {
                user = AuthorisedUser
                        .getUserByEmail(session().get("connected"));
            } else {
                user = AuthorisedUser.getUserById(id);
            }

            String payedDate = df.get("payedPaymentDate");
            Date d = new Date();
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            DateFormat form = new SimpleDateFormat("yy");

            d = format.parse(payedDate);
            PaymentReceipt receipt = new PaymentReceipt();
            receipt.createdDate = d;
            receipt.dostyk = user.dostyks.get(0);
            receipt.user = user;
            List<PaymentReceipt> receipts = PaymentReceiptLogic.getByDostyk(
                    receipt.dostyk, form.format(d));
            double payed = Double.parseDouble(df.get("payedPayment"));
            Payment p = PaymentLogic.getActualPayment(user);
            if (p.status.equalsIgnoreCase("PAYED")) {
                Date today = getTodayDate();
                String stoday = new SimpleDateFormat("yyyy-MM-dd").format(today);
                return ok(paymentList.render(user, p,
                        PaymentPeriodLogic.getPeriods(p),
                        PaymentReceiptLogic.getReceipts(p),
                        user.roles.get(0).name,stoday));
            } else {
                PaymentPeriod period = PaymentPeriodLogic.getCurrentPeriod(p);
                if (period.debtPayment > payed) {
                    period.debtPayment = 0;
                    period.payedPayment += payed;
                    addNewPeriod(p, period, period.plannedPayment - payed);
                    period.plannedPayment = payed;
                } else if (period.debtPayment < payed) {
                    return badRequest(messenger.render("alert alert-error",
                            Messages.get("paymentListError"),
                            Messages.get("canNotPayMoreThanDue")));
                } else {
                    period.debtPayment -= payed;
                    period.payedPayment += payed;
                }
                period.payedPaymentDate = d;
                period.comment = df.get("comment");
                period.status = "PAYED";
                period.payment = p;
                boolean baredi = false;
                if (period.receipt != null) {
                    baredi = true;
                    receipt = period.receipt;
                }
                receipt.payment = p;
                receipt.period = period;
                receipt.save();
                period.receipt = receipt;
                period.save();
                if (baredi == false)
                    receipt.number = receipts.size();
                receipt.save();

                List<PaymentPeriod> periods = PaymentPeriodLogic.getPeriods(p);
                boolean full = true;
                for (PaymentPeriod per : periods) {

                    if (per.status.equalsIgnoreCase("PLANNED")) {
                        per.status = "CURRENT";
                        per.save();
                        full = false;
                        break;
                    } else if (period.status.equalsIgnoreCase("CURRENT")) {
                        full = false;
                        break;
                    }
                }
                updatePayment(p);
                if (p.debtPayment == 0 && full == true)
                    p.status = "PAYED";
                p.save();
                Contract contract = new Contract();
                contract.number = df.get("deal_number");
                contract.payment = p;
                if(df.get("update_date")!=null)
                contract.beginDate = format.parse(df.get("update_date"));
                contract.endDate = p.endDate;
                contract.prefix = "ДЕ";
                contract.student = p.user;
                contract.save();
                // p = PaymentLogic.getActualPayment(user);
                return redirect(routes.Payments.showPaymentList(id));
            }
        } catch (Exception e) {

            e.printStackTrace();
            return badRequest(messenger.render("alert alert-error",
                    Messages.get("paymentListError"), e.getMessage()));
        }
    }

    public static void addNewPeriod(Payment p, PaymentPeriod period,
                                    double price) {
        p.periods = null;
        PaymentPeriod period2 = new PaymentPeriod();
        period2.payment = p;
        period2.debtPayment = price;
        period2.plannedPaymentDate = period.plannedPaymentDate;
        period2.plannedPayment = price;
        period2.status = "PLANNED";
        period2.comment = period.comment;
        period2.payedPayment = 0.0;
        period2.period = period.period + 1;
        List<PaymentPeriod> prds = PaymentPeriodLogic.getPeriods(p);
        for (int i = period.period + 1; i < prds.size(); i++) {
            PaymentPeriod per = prds.get(i);
            per.period = i + 1;
            per.save();
        }
        period2.save();
        p.periods = PaymentPeriodLogic.getPeriods(p);
        p.save();
    }

    public static Result undoPaymentList() {
        try {
            DynamicForm df = form().bindFromRequest();
            Long uid = Long.parseLong(df.get("user_id"), 10);
            AuthorisedUser user;
            if (uid == -1) {
                user = AuthorisedUser
                        .getUserByEmail(session().get("connected"));
            } else {
                user = AuthorisedUser.getUserById(uid);
            }
            List<Payment> payments = PaymentLogic.getAllByUser(user);
            Payment last = payments.get(payments.size() - 1);
            undoPayment(last);
            return redirect(routes.Payments.showPaymentList(uid));
        } catch (Exception e) {
            return badRequest(messenger.render("alert alert-error",
                    Messages.get("paymentListError"), e.getMessage()));
        }
    }

    public static Result resetPaymentList() {
        try {
            DynamicForm df = form().bindFromRequest();
            Long uid = Long.parseLong(df.get("user_id"), 10);
            AuthorisedUser user;
            if (uid == -1) {
                user = AuthorisedUser
                        .getUserByEmail(session().get("connected"));
            } else {
                user = AuthorisedUser.getUserById(uid);
            }
            Payment payment = PaymentLogic.getAllByUser(user).get(
                    PaymentLogic.getAllByUser(user).size() - 1);
            resetPayment(payment);
            return redirect(routes.Payments.showPaymentList(uid));
        } catch (Exception e) {

            return badRequest(messenger.render("alert alert-error",
                    Messages.get("paymentListError"), e.getMessage()));
        }
    }

    public static Result deletePaymentList() {
        try {
            DynamicForm df = form().bindFromRequest();
            Long uid = Long.parseLong(df.get("user_id"), 10);
            AuthorisedUser user;
            if (uid == -1) {
                user = AuthorisedUser
                        .getUserByEmail(session().get("connected"));
            } else {
                user = AuthorisedUser.getUserById(uid);
            }
            Payment payment = PaymentLogic.getAllByUser(user).get(
                    PaymentLogic.getAllByUser(user).size() - 1);
            deletePayment(payment);
            return redirect(routes.BillForms.showBillForm(uid));
        } catch (Exception e) {

            return badRequest(messenger.render("alert alert-error",
                    Messages.get("paymentListError"), e.getMessage()));
        }
    }

    public static void resetPayment(Payment p) {
        List<PaymentPeriod> periods = PaymentPeriodLogic.getPeriods(p);
        for (PaymentPeriod period : periods) {
            period.debtPayment = period.plannedPayment;
            period.payedPayment = 0;
            period.status = "PLANNED";
            period.save();
        }
        p.status = "CURRENT";
        updatePayment(p);
    }

    public static void undoPayment(Payment p) {
        List<PaymentPeriod> periods = PaymentPeriodLogic.getPeriods(p);
        for (int i = periods.size() - 1; i >= 0; i--) {
            PaymentPeriod period = periods.get(i);
            if (period.payedPayment != 0) {
                period.debtPayment = period.plannedPayment;
                period.payedPayment = 0;
                period.status = "PLANNED";
                period.save();
                break;
            } else {
                period.status = "PLANNED";
                period.save();
            }
        }
        p.status = "CURRENT";
        updatePayment(p);
    }

    public static void deletePayment(Payment p) {
        List<PaymentPeriod> periods = PaymentPeriodLogic.getPeriods(p);
        List<PaymentReceipt> receipts = PaymentReceiptLogic.getReceipts(p);
        List<PaymentSuspend> suspends = PaymentSuspend.find.where().eq("payment",p).findList();
        List<Contract> contracts = Contract.getByPayment(p);
        for (PaymentReceipt receipt : receipts) {
            receipt.payment = null;
            receipt.period = null;
            receipt.save();
        }
        for (PaymentSuspend suspend : suspends){
            suspend.paymentPeriod = null;
            suspend.delete();
        }
        for (PaymentPeriod period : periods) {
            period.receipt = null;
            period.payment = null;
            period.delete();
        }
        for (Contract contract : contracts) {
            contract.delete();
        }

        p.periods = null;
        p.receipts = null;
        p.delete();
    }

    public static void updatePayment(Payment p) {
        List<PaymentPeriod> periods = PaymentPeriodLogic.getPeriods(p);
        p.debtPayment = 0;
        p.payedPayment = 0;
        p.plannedPayment = 0;
        for (PaymentPeriod per : periods) {
            p.debtPayment += per.debtPayment;
            p.payedPayment += per.payedPayment;
            p.plannedPayment += per.plannedPayment;
        }
        p.save();
    }

    public static Result printReceipt(Long userId, Long receiptId) {
        try {
            AuthorisedUser user = AuthorisedUser.getUserById(userId);
            PaymentReceipt receipt = PaymentReceiptLogic.getById(receiptId);


            if (receipt.period.payedPayment > 0) {
                return ok(pko.render(user, receipt));
            } else {
                return ok(rko.render(user, receipt));
            }


            //return ok(printReceipt2.render(user, receipt));
        } catch (Exception e) {

            return badRequest(messenger.render("alert alert-error",
                    Messages.get("printReceiptError"), e.getMessage()));
        }
    }

    public static Result changeCourse(Long id, Long paymentId) {
        try {
            DynamicForm df = form().bindFromRequest();
            AuthorisedUser user;
            if (id == -1) {
                user = AuthorisedUser
                        .getUserByEmail(session().get("connected"));
            } else {
                user = AuthorisedUser.getUserById(id);
            }
            Payment payment = PaymentLogic.getById(paymentId);
            PaymentReceipt receipt = new PaymentReceipt();
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            DateFormat form = new SimpleDateFormat("yy");
            String d = df.get("real_end_date");
            int payed = Integer.parseInt(df.get("total_price"));
            Date endDate = format.parse(d);
            List<PaymentReceipt> receipts = PaymentReceiptLogic.getByDostyk(
                    user.dostyks.get(0), form.format(endDate));
            receipt.number = receipts.size();
            receipt.dostyk = user.dostyks.get(0);
            receipt.user = user;
            receipt.payment = payment;
            receipt.period = PaymentPeriodLogic.getCurrentPeriod(payment);
            if (receipt.period == null) {
                receipt.period = receipt.payment.periods
                        .get(receipt.payment.periods.size() - 1);
            }
            receipt.period.payedPayment += payed;
            receipt.period.debtPayment = 0;
            receipt.period.status = "PAYED";
            receipt.period.save();
            receipt.save();
            receipt.period.receipt = receipt;
            receipt.period.save();
            for (PaymentPeriod period : PaymentPeriodLogic.getPeriods(payment)) {
                if (period.debtPayment != 0) {
                    period.debtPayment = 0;
                    period.status = "PAYED";
                    period.save();
                }
            }
            payment.status = "COMPLETED";
            payment.endDate = endDate;
            payment.save();
            updatePayment(payment);
            user.status = "completed";
            user.profile.dostykGroup = null;
            user.profile.electiveGroup = null;
            user.profile.save();
            user.save();
            return redirect(request().getHeader("referer"));
        } catch (Exception e) {

            return badRequest(messenger.render("alert alert-error",
                    Messages.get("endCourseError"), e.getMessage()));
        }
    }

    public static Result updateLessons() {
        try {
            DynamicForm df = form().bindFromRequest();
            Long uid = Long.parseLong(df.get("user_id"), 10);
            AuthorisedUser user;
            if (uid == -1) {
                user = AuthorisedUser
                        .getUserByEmail(session().get("connected"));
            } else {
                user = AuthorisedUser.getUserById(uid);
            }
            List<Payment> payments = PaymentLogic.getAllByUser(user);
            Payment payment = payments.get(payments.size() - 1);
            payment.status = "CURRENT";
            int total = Integer.parseInt(df.get("total"));
            int per = Integer.parseInt(df.get("per"));
            int unpaid = PaymentLogic.getUnpaidPeriodsNumber(payment);
            int hours = Integer.parseInt(df.get("hours_per_week"));
            int price = Integer.parseInt(df.get("price_per_week"));
            //String updateDateS = df.get("update_date");
            //String newDealNumberS = df.get("new_deal");
            payment.hoursPerWeek = hours;
            payment.pricePerWeek = price;
            List<PaymentPeriod> periods = PaymentPeriodLogic
                    .getPeriods(payment);
            boolean bari_tolendi = true;
            for (int i = 0; i < periods.size(); i++) {
                PaymentPeriod period = periods.get(i);
                if (!period.status.equals("PAYED")) {
                    bari_tolendi = false;
                }
            }
            if (bari_tolendi == false) {
                for (int i = periods.size() - unpaid; i < periods.size(); i++) {
                    PaymentPeriod period = periods.get(i);
                    period.plannedPayment += per;
                    period.debtPayment += per;
                    period.save();
                }
            } else {
                PaymentPeriod period = periods.get(periods.size() - 1);
                period.plannedPayment += total;
                if (total >= 0) {
                    if (periods.size() > 1) {
                        PaymentPeriod previous = periods
                                .get(periods.size() - 2);
                        previous.payedPayment += period.payedPayment;
                        previous.save();
                    }
                    period.debtPayment = total;
                    period.payedPayment = 0;
                    period.status = "CURRENT";
                } else {
                    for (int i = periods.size() - unpaid; i < periods.size(); i++) {
                        period = periods.get(i);
                        period.plannedPayment += per;
                        period.debtPayment += per;
                        period.save();
                    }
                }
                period.save();
            }
            updatePayment(payment);
            List<Lesson> list = new ArrayList<Lesson>();
            for (Entry<String, String> field : df.data().entrySet()) {
                if (field.getKey().startsWith("lesson[")) {
                    list.add(LessonLogic.getById(Long.parseLong(field
                            .getValue())));
                }
            }
            user.profile.lessons = list;
            user.profile.save();
            user.profile.saveManyToManyAssociations("lessons");
            payment.save();
            return redirect(routes.Payments.showPaymentList(uid));
        } catch (Exception e) {

            return badRequest(messenger.render("alert alert-error",
                    Messages.get("updateLessonsError"), e.getMessage()));
        }
    }

    public static Result suspendPayment(Long id, Long paymentId) {
        try {
            DynamicForm df = form().bindFromRequest();
            AuthorisedUser user;
            if (id == -1) {
                user = AuthorisedUser
                        .getUserByEmail(session().get("connected"));
            } else {
                user = AuthorisedUser.getUserById(id);
            }
            PaymentSuspend paymentSuspend = new PaymentSuspend();
            Payment payment = PaymentLogic.getById(paymentId);
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
//			DateFormat form = new SimpleDateFormat("yy");
            String reason = df.get("reason");
            Date startDate = format.parse(df.get("startDate"));
            Date endDate = format.parse(df.get("endDate"));


            Double studyPrice = Double.parseDouble(df.get("studyPrice"));
            paymentSuspend.studyPrice = studyPrice;
            PaymentPeriod paymentPeriod = PaymentPeriodLogic.find.byId(Long.parseLong(df.get("period_id")));
            //Double periodPayment = paymentPeriod.plannedPayment;

            for (int i = 0; ; i++) {
                PaymentPeriod nextPeriod = PaymentPeriodLogic.find.where().eq("payment", payment).eq("period", paymentPeriod.period + i).findUnique();
                if (nextPeriod == null)
                    break;
                if (nextPeriod.plannedPayment < studyPrice) {
                    studyPrice -= nextPeriod.plannedPayment;
                    nextPeriod.plannedPayment = 0;
                    nextPeriod.update();
                } else {
                    nextPeriod.plannedPayment -= studyPrice;
                    nextPeriod.update();
                    break;
                }
            }

            paymentSuspend.reason = reason;
            paymentSuspend.startDate = startDate;
            paymentSuspend.endDate = endDate;
            paymentSuspend.payment = payment;
            paymentSuspend.paymentPeriod = paymentPeriod;
            //paymentSuspend.periodPayment = periodPayment;
            //paymentSuspend.suspendPayment = suspendPayment;
//			paymentSuspend.diffPayment = diffPayment;
            paymentSuspend.save();


            return redirect(request().getHeader("referer"));
        } catch (Exception e) {

            return badRequest(messenger.render("alert alert-error",
                    Messages.get("endCourseError"), e.getMessage()));
        }
    }

    public static Result paymentReport() {
        AuthorisedUser user = AuthorisedUser.getUserByEmail(session().get("connected"));

        if (user.roles.contains(SecurityRole.getByName("admin")) ||
                user.roles.contains(SecurityRole.getByName("accountant")) || user.roles.contains(SecurityRole.getByName("director"))
                || user.roles.contains(SecurityRole.getByName("moderator"))) {
            flash("paymentReport", "dostyk");
        } else if (!ProfileLogic.isCurator(user).equals(null)) {
            flash("paymentReport", "student");
            flash("groupid", "" + ProfileLogic.isCurator(user).id);
        }

        return ok(paymentReport.render());
    }

    @Restrict({@Group("admin"), @Group("moderator"), @Group("accountant"), @Group("director")})
    public static Result dostykReport() throws ParseException {
        DynamicForm requestData = form().bindFromRequest();
        String paymentStatus,sort="";
        Date startDate = null, endDate = null;
        try {
            String[] date = requestData.get("reservation").split("-");
            startDate = new SimpleDateFormat("dd/MM/yyyy").parse(date[0]);
            endDate = new SimpleDateFormat("dd/MM/yyyy").parse(date[1]);
            paymentStatus = requestData.get("status");
            sort = requestData.get("sort");
        } catch (Exception e) {
            paymentStatus = "all";
        }


        AuthorisedUser user = AuthorisedUser.getUserByEmail(session("connected"));
        List<Report> dostks = new ArrayList<Report>();
        int students = 0, currents = 0;
        TreeMap<String, String> totals = null;
        Report dreport;
        List<String> statuses = new ArrayList<String>();
        if (paymentStatus.equals("all")) {
            statuses.add("active");
            statuses.add("completed");
        } else if (paymentStatus.equals("COMPLETED"))
            statuses.add("completed");
        else
            statuses.add("active");
        if ((user.roles.contains(SecurityRole.getByName("moderator")) || user.roles.contains(SecurityRole.getByName("director")))
                && user.dostyks.get(0) != null) {
            dreport = new Report();
            dreport.id = user.dostyks.get(0).id;
            dreport.name = user.dostyks.get(0).name;
            dreport.studentSize = DostykLogic.getStudentSize(user.dostyks.get(0), statuses);
            dreport.percentage = DostykLogic.getStudentSize(user.dostyks.get(0), paymentStatus, statuses);
            PaymentLogic.fillPayments(dreport, startDate, endDate,
                    paymentStatus, AuthorisedUserLogic.getAllStudents(user.dostyks.get(0), paymentStatus, statuses));
            dostks.add(dreport);
        } else if (user.roles.contains(SecurityRole.getByName("admin")) || user.roles.contains(SecurityRole.getByName("accountant"))) {
            List<Dostyk> dostyks = DostykLogic.getAll();
            for (Dostyk dostyk : dostyks) {
                dreport = new Report();
                dreport.id = dostyk.id;
                dreport.name = dostyk.name;
                dreport.studentSize = DostykLogic.getStudentSize(dostyk,
                        statuses);
                dreport.percentage = DostykLogic.getStudentSize(dostyk,
                        paymentStatus, statuses);
                PaymentLogic.fillPayments(dreport, startDate, endDate,
                        paymentStatus, AuthorisedUserLogic.getAllStudents(dostyk,
                                paymentStatus, statuses));
                dostks.add(dreport);
            }
            currents = PaymentLogic.sumCurrents(dostks);
            students = PaymentLogic.sumStudents(dostks);
            totals = PaymentLogic.sumPayments(dostks);

            if(sort.equals("planed")) {
                Collections.sort(dostks, Collections.reverseOrder(new Comparator<Report>() {
                    @Override
                    public int compare(Report c1, Report c2) {
                        return Double.compare(c1.plannedPayment, c2.plannedPayment);
                    }
                }));
            }else if(sort.equals("payed")) {
                Collections.sort(dostks, Collections.reverseOrder(new Comparator<Report>() {
                    @Override
                    public int compare(Report c1, Report c2) {
                        return Double.compare(c1.payedPayment, c2.payedPayment);
                    }
                }));
            }else if(sort.equals("debt")) {
                Collections.sort(dostks, Collections.reverseOrder(new Comparator<Report>() {
                    @Override
                    public int compare(Report c1, Report c2) {
                        return Double.compare(c1.debtPayment, c2.debtPayment);
                    }
                }));
            }else if(sort.equals("studsize")) {
                Collections.sort(dostks, Collections.reverseOrder(new Comparator<Report>() {
                    @Override
                    public int compare(Report c1, Report c2) {
                        return Double.compare(c1.studentSize, c2.studentSize);
                    }
                }));
            }


        }
        flash("paymentReport", "dostyk");
        return ok(_dostykReport.render(dostks, currents, students, totals));
    }



    @Restrict({@Group("admin"), @Group("moderator"), @Group("accountant")})
    public static Result groupReport(Long dostykId) throws ParseException {
        DynamicForm requestData = form().bindFromRequest();
        String paymentStatus;
        Date startDate = null, endDate = null;
        try {
            String[] date = requestData.get("reservation").split("-");
            startDate = new SimpleDateFormat("dd/MM/yyyy").parse(date[0]);
            endDate = new SimpleDateFormat("dd/MM/yyyy").parse(date[1]);
            paymentStatus = requestData.get("status");
        } catch (Exception e) {
            paymentStatus = "all";
        }
        Dostyk dostyk = DostykLogic.getById(dostykId);
        List<Report> groupas = new ArrayList<Report>();
        Report greport;
        List<String> statuses = new ArrayList<String>();
        if (paymentStatus.equals("all")) {
            statuses.add("active");
            statuses.add("completed");
        } else if (paymentStatus.equals("COMPLETED"))
            statuses.add("completed");
        else
            statuses.add("active");
        List<DostykGroup> groups = DostykGroupLogic.getAllByDostyk(dostyk);
        for (DostykGroup group : groups) {
            greport = new Report();
            greport.id = group.id;
            greport.name = group.name;
            greport.studentSize = AuthorisedUserLogic.getStudentsSize(group,
                    statuses);
            greport.percentage = AuthorisedUserLogic.getStudentsSize(group,
                    paymentStatus, statuses);
            PaymentLogic.fillPayments(greport, startDate, endDate,
                    paymentStatus, AuthorisedUserLogic.getStudentsIds(group,
                            paymentStatus, statuses));
            groupas.add(greport);
        }
        TreeMap<String, String> totals = PaymentLogic.sumPayments(groupas);
        flash("paymentReport", "dostyk");
        return ok(_groupReport.render(groupas, totals));
    }

    @Restrict({@Group("admin"), @Group("moderator"), @Group("accountant")})
    public static Result studentReport(Long groupId) throws ParseException {
        DynamicForm requestData = form().bindFromRequest();
        String paymentStatus;
        Date startDate = null, endDate = null;
        try {
            String[] date = requestData.get("reservation").split("-");
            startDate = new SimpleDateFormat("dd/MM/yyyy").parse(date[0]);
            endDate = new SimpleDateFormat("dd/MM/yyyy").parse(date[1]);
            paymentStatus = requestData.get("status");
        } catch (Exception e) {
            paymentStatus = "all";
        }
        List<Report> userler = new ArrayList<Report>();
        Report ureport;
        List<String> statuses = new ArrayList<String>();
        if (paymentStatus.equals("all")) {
            statuses.add("active");
            statuses.add("completed");
        } else if (paymentStatus.equals("COMPLETED"))
            statuses.add("completed");
        else
            statuses.add("active");
        DostykGroup dostykGroup = DostykGroupLogic.getById(groupId);
        List<AuthorisedUser> users = AuthorisedUser.getByDostykGroup(
                dostykGroup, paymentStatus, statuses);
        List<AuthorisedUser> temp;
        for (AuthorisedUser user : users) {
            temp = new ArrayList<AuthorisedUser>();
            ureport = new Report();
            ureport.id = user.id;
            ureport.name = user.profile.lastName + " " + user.profile.firstName;
            ureport.phone = "";
            if (user.profile.phoneNumber != null)
                if (user.profile.phoneNumber.length() > 0)
                    ureport.phone += user.profile.phoneNumber;
            for (Parent parent : user.parents) {
                if ((parent.phoneNumber1 != null && parent.phoneNumber1.length() > 0) ||
                        (parent.phoneNumber2 != null && parent.phoneNumber2.length() > 0)) {
                    ureport.phone += "<br>(<b>" + Messages.get(Controller.lang(), parent.parentType) + "</b>:";
                    if (parent.phoneNumber1 != null && parent.phoneNumber1.length() > 0)
                        ureport.phone += " " + parent.phoneNumber1 + ",";
                    if (parent.phoneNumber2 != null && parent.phoneNumber2.length() > 0)
                        ureport.phone += " " + parent.phoneNumber2;
                    ureport.phone += ") ";
                }

            }
            temp.add(user);
            PaymentLogic.fillPayments(ureport, startDate, endDate,
                    paymentStatus, temp);
            userler.add(ureport);
        }
        TreeMap<String, String> totals = PaymentLogic.sumPayments(userler);
        flash("paymentReport", "dostyk");
        return ok(_studentReport.render(userler, totals));
    }

    public static Map<String, Integer> sumDostykPayments(
            Map<Dostyk, Payment> map) {
        int plannedPayment = 0;
        int payedPayment = 0;
        int debtPayment = 0;
        for (Entry<Dostyk, Payment> entry : map.entrySet()) {
            plannedPayment += entry.getValue().plannedPayment;
            payedPayment += entry.getValue().payedPayment;
            debtPayment += entry.getValue().debtPayment;
        }
        Map<String, Integer> totals = new HashMap<String, Integer>();
        totals.put("plannedPayment", plannedPayment);
        totals.put("payedPayment", payedPayment);
        totals.put("debtPayment", debtPayment);
        return totals;
    }

    @Restrict({@Group("student"), @Group("teacher")})
    public static Result getPaymentStatus(long studentId) {
        AuthorisedUser student = AuthorisedUser.getUserById(studentId);
        Payment payment = PaymentLogic.getActualPayment(student);
        payment.periods = PaymentPeriodLogic.getPeriods(payment);
        return Response.sendPayments(payment);
    }


    public static Result getDogovor(long id) {
        AuthorisedUser student = AuthorisedUser.find.byId(id);
        Payment payment = PaymentLogic.getActualPayment(student);
        Dostyk dostyk = student.dostyks.get(0);
        AuthorisedUser moderator = AuthorisedUser.find.where().eq("dostyks", dostyk)
                .eq("roles", SecurityRole.getByName("moderator"))
                .findUnique();
        DateFormat day = new SimpleDateFormat("dd");
        DateFormat month = new SimpleDateFormat("MM");
        DateFormat year = new SimpleDateFormat("yyyy");
        if (moderator.requisite == null)
            moderator.requisite = new Requisites();

        Map<String, String> map = new HashMap<>();
        Date dogovorDate = new Date();
        Contract contract = new Contract();
        contract.beginDate = new Date();
        if (Contract.getByPayment(payment).size() > 0)
            contract = Contract.getByPayment(payment).get(0);
        dogovorDate = contract.beginDate;//payment.beginDate;
        if (dogovorDate == null)
            dogovorDate = payment.startDate;
        Date dogovorEnd = payment.endDate;
        Parent parent = Parent.find.where().eq("child", student).eq("responsible", true).findUnique();
        parent.lastName = parent.lastName != null ? parent.lastName : "";
        parent.firstName = parent.firstName != null ? parent.firstName : "";

        student.profile.lastName = student.profile.lastName != null ? student.profile.lastName : "";
        student.profile.firstName = student.profile.firstName != null ? student.profile.firstName : "";
        student.profile.secondName = student.profile.secondName != null ? student.profile.secondName : "";


        map.put("dogovorNO", contract.number);
        map.put("dogovorCity", moderator.requisite.dostykCity);

        map.put("parentName", parent.lastName + " " + parent.firstName);
        map.put("parentINN", parent.inn);

        map.put("studentName", student.profile.lastName + " "
                + student.profile.firstName + " "
                + student.profile.secondName);

        map.put("studentINN", student.profile.inn);


//		map.put("totalPayment", payment.plannedPayment+"");
//		map.put("totalPaymentString", "kooop kop akwa");

        map.put("parentNation", "____________");

        map.put("dostykAddress", moderator.requisite.dostykAddress);
        map.put("dostykOfficial", moderator.requisite.dostykOfficial);
        map.put("dostykTel", moderator.requisite.dostykTel);
        map.put("dostykBIK", moderator.requisite.dostykBIK);
        map.put("dostykIIK", moderator.requisite.dostykIIK);
        map.put("dostykBIN", moderator.requisite.dostykBIN);
        map.put("dostykBANK", moderator.requisite.dostykBANK);
        map.put("dostykKBE", moderator.requisite.dostykKBE);
        map.put("dostykDirector", moderator.requisite.dostykDirector);


        return ok(views.html.accounting.dogovorPrint.render(map, dogovorDate, dogovorEnd, payment.plannedPayment));
    }

    @Restrict({@Group("admin"), @Group("moderator"), @Group("accountant")})
    public static Result printChanges(Long userId, Long paymentId, String lesson_names, String hours, String total,
                                      String date, String number) {
        try {
            AuthorisedUser student = AuthorisedUser.find.byId(userId);
            Payment payment = PaymentLogic.getById(paymentId);
            List<PaymentPeriod> periods = PaymentPeriodLogic.getPeriods(payment);
            int forPeriod = (int) periods.get(periods.size() - 1).plannedPayment;
            Dostyk dostyk = student.dostyks.get(0);
            List<AuthorisedUser> mods = AuthorisedUser.find.where().eq("dostyks", dostyk)
                    .eq("roles", SecurityRole.getByName("moderator"))
                    .findList();
            AuthorisedUser mod = null;
            if (mods.size() > 0)
                mod = mods.get(0);
            Map<String, String> map = new HashMap<>();
            Contract contract = new Contract();
            contract.beginDate = new Date();
            contract.number = "";
            if (Contract.getByPayment(payment).size() > 0)
                contract = Contract.getByPayment(payment).get(0);
            Date dogovorDate = contract.beginDate;
            if (dogovorDate == null)
                dogovorDate = payment.startDate;
            Date dogovorEnd = payment.endDate;
            Parent parent = Parent.find.where().eq("child", student).eq("responsible", true).findUnique();
            parent.lastName = parent.lastName != null ? parent.lastName : "";
            parent.firstName = parent.firstName != null ? parent.firstName : "";
            student.profile.lastName = student.profile.lastName != null ? student.profile.lastName : "";
            student.profile.firstName = student.profile.firstName != null ? student.profile.firstName : "";
            student.profile.secondName = student.profile.secondName != null ? student.profile.secondName : "";
            map.put("studentName", student.profile.lastName + " "
                    + student.profile.firstName + " "
                    + student.profile.secondName);
            map.put("studentINN", student.profile.inn != null ? student.profile.inn : "");
            map.put("studentTel", student.profile.phoneNumber != null ? student.profile.phoneNumber : "");
            map.put("dogovorNO", contract.number);
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            map.put("dogovorDate", df.format(contract.beginDate));
            DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
            map.put("dogovorNewDate", df.format(df2.parse(date)));
            map.put("dogovorNewNO", number);
            map.put("parentName", parent.lastName + " " + parent.firstName);
            map.put("parentINN", parent.inn);
            map.put("lessons", lesson_names);
            map.put("hours", hours);
            map.put("totalChanges", total);
            map.put("forPeriod", forPeriod + "");
            map.put("parentNation", "____________");
            if (mod != null) {
                if (mod.requisite != null) {
                    map.put("dogovorCity", mod.requisite.dostykCity != null ? mod.requisite.dostykCity : "");
                    map.put("dostykAddress", mod.requisite.dostykAddress != null ? mod.requisite.dostykAddress : "");
                    map.put("dostykOfficial", mod.requisite.dostykOfficial != null ? mod.requisite.dostykOfficial : "");
                    map.put("dostykTel", mod.requisite.dostykTel != null ? mod.requisite.dostykTel : "");
                    map.put("dostykBIK", mod.requisite.dostykBIK != null ? mod.requisite.dostykBIK : "");
                    map.put("dostykIIK", mod.requisite.dostykIIK != null ? mod.requisite.dostykIIK : "");
                    map.put("dostykBIN", mod.requisite.dostykBIN != null ? mod.requisite.dostykBIN : "");
                    map.put("dostykBANK", mod.requisite.dostykBANK != null ? mod.requisite.dostykBANK : "");
                    map.put("dostykKBE", mod.requisite.dostykKBE != null ? mod.requisite.dostykKBE : "");
                    map.put("dostykDirector", mod.requisite.dostykDirector != null ? mod.requisite.dostykDirector : "");
                }
            }
            return ok(views.html.printable.printLessonChanges.render(student, payment, map));
        } catch (Exception e) {
            e.printStackTrace();
            return ok("Exception. Check out lessons requisites info or student or parent's profile. " +
                    "Check out all phone numbers, addresses, KBE's, BIK's and BIN's and etc... " +
                    "There are missing info. Find them and fill before printing this deal");
        }
    }
}