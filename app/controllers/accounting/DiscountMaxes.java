package controllers.accounting;

import static play.data.Form.form;

import java.util.List;

import models.entities.accounting.DiscountMax;
import models.entities.accounting.DiscountValue;
import models.entities.social.Dostyk;
import models.logic.accounting.DiscountMaxLogic;
import models.logic.accounting.DiscountValueLogic;
import models.logic.social.DostykLogic;
import play.data.Form;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.accounting.discountMaxes;
import views.html.helps.message;

public class DiscountMaxes extends Controller {
	final static Form<DiscountMax> dForm = form(DiscountMax.class);

	public static Result showDiscountMaxes() {
		List<DiscountMax> dList = DiscountMaxLogic.getAllOrderById();
		return ok(discountMaxes.render(dList, dForm));
	}

	public static Result saveDiscountMax() {
		Form<DiscountMax> filledForm = dForm.bindFromRequest();
		// Check name for emptiness
		if (filledForm.field("nameKz").valueOr("").isEmpty()) {
			filledForm.reject("nameKz", Messages.get("discountMax.typeName"));
		} else if (DiscountMaxLogic.getByName(filledForm.field("nameKz").value()) != null) {
			filledForm.reject("nameKz",
					Messages.get("discountMax.nameMustBeUnique"));
		}

		if (filledForm.field("nameRu").valueOr("").isEmpty()) {
			filledForm.reject("nameRu", Messages.get("discountMax.typeName"));
		} else if (DiscountMaxLogic.getByName(filledForm.field("nameRu").value()) != null) {
			filledForm.reject("nameRu",
					Messages.get("discountMax.nameMustBeUnique"));
		}
		// Check discount for zero
		if (filledForm.field("value").valueOr("0").isEmpty()) {
			filledForm
			.reject("value", Messages.get("discountMax.zeroDiscount"));
		}
		List<DiscountMax> dList = DiscountMaxLogic.getAllOrderById();
		DiscountMax toSave;
		if (filledForm.hasErrors()) {
			return badRequest(discountMaxes.render(dList, filledForm));
		} else {
			toSave = filledForm.get();
			toSave.nameKz = filledForm.field("nameKz").value();
			toSave.nameRu = filledForm.field("nameRu").value();
			toSave.value = Integer.parseInt(filledForm.field("value").value());
			toSave.active = 1;
			if(!filledForm.field("maxUserSize").value().isEmpty()) {
				toSave.maxUserSize = Integer.parseInt(filledForm.field("maxUserSize").value());
			}
			toSave.save();
			dList = DiscountMaxLogic.getAllOrderByValue();
			return ok(discountMaxes.render(dList, dForm));
		}
	}

	public static Result deleteDiscountMax(Long id) {
		try {
			DiscountMax toDelete = DiscountMaxLogic.getById(id);
			//List<DiscountValue> dList = DiscountValueLogic.getAllByMax(toDelete);
			//for (int i = 0; i < dList.size(); i++) {
			//	DiscountValue del = dList.get(i);
			//	del.delete();
			//}
			toDelete.active = 0;
			toDelete.save();
			return ok(message.render("alert alert-success",
					"discountMax.successfullyDeleted"));
		} catch (Exception e) {
			return badRequest(message.render("alert alert-error",
					e.getMessage()));
		}
	}

	public static Result editDiscountMax(Long id) {
		Form<DiscountMax> filledForm = form(DiscountMax.class);
		DiscountMax toEdit = DiscountMaxLogic.getById(id);
		return ok(views.html.accounting.editDiscountMax.render(filledForm.fill(toEdit), id));

	}

	public static Result saveExistingDiscount(Long id) {
		Form<DiscountMax> filledForm = dForm.bindFromRequest();


		if (filledForm.field("nameKz").valueOr("").isEmpty()) {
			filledForm.reject("nameKz", Messages.get("discountMax.typeName"));
		} else if (DiscountMaxLogic.getByName(filledForm.field("nameKz").value()) != null && !DiscountMaxLogic.getByName(filledForm.field("nameKz").value()).id.equals(id)) {
			filledForm.reject("nameKz", 	Messages.get("discountMax.nameMustBeUnique"));
		}

		if (filledForm.field("nameRu").valueOr("").isEmpty()) {
			filledForm.reject("nameRu", Messages.get("discountMax.typeName"));
		} else if (DiscountMaxLogic.getByName(filledForm.field("nameRu").value()) != null && !DiscountMaxLogic.getByName(filledForm.field("nameRu").value()).id.equals(id)) {
			filledForm.reject("nameRu",
					Messages.get("discountMax.nameMustBeUnique"));
		}
		// Check discount for zero
		if (filledForm.field("value").valueOr("0").isEmpty()) {
			filledForm
					.reject("value", Messages.get("discountMax.zeroDiscount"));
		}


		if (filledForm.hasErrors()) {
			return badRequest(views.html.accounting.editDiscountMax.render(filledForm,id));
		} else {
			DiscountMax toSave = DiscountMaxLogic.getById(id);
			toSave.nameKz = filledForm.field("nameKz").value();
			toSave.nameRu = filledForm.field("nameRu").value();
			toSave.value = Integer.parseInt(filledForm.field("value").value());
			if(!filledForm.field("maxUserSize").value().isEmpty()) {
				toSave.maxUserSize = Integer.parseInt(filledForm.field("maxUserSize").value());
			}else{
				toSave.maxUserSize = null;
			}
			toSave.update();
			List<DiscountMax> dList = DiscountMaxLogic.getAllOrderById();
			return ok(discountMaxes.render(dList, dForm));
		}
	}
}