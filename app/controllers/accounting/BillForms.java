package controllers.accounting;

import static play.data.Form.form;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import models.entities.accounting.Payment;
import models.entities.accounting.PaymentPeriod;
import models.entities.social.Dostyk;
import models.entities.user.AuthorisedUser;
import models.entities.user.Parent;
import models.logic.accounting.Contract;
import models.logic.accounting.DiscountValueLogic;
import models.logic.accounting.PaymentLogic;
import models.logic.user.AuthorisedUserLogic;
import play.Logger;
import play.data.DynamicForm;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.helps.message;
import views.html.helps.messenger;
import views.html.user.profilePack.billForm;
import views.html.printable.deal;

public class BillForms extends Controller {

	public static Result showBillForm(long id) {
		Logger.info("BillForms.showBillForm");
		String lang = session("lang");
		try {
			AuthorisedUser user;
			if (id == -1) {
				user = AuthorisedUser
						.getUserByEmail(session().get("connected"));
			} else {
				user = AuthorisedUser.getUserById(id);
			}
			if (DiscountValueLogic.getBonusByDostyk(user.dostyks.get(0)) == null) {
				return ok(messenger.render("alert alert-danger",
						Messages.get("discountValuesDidntBeenInitialised"),
						Messages.get("pleaseGoAndAddDiscountValues")));
			} else {
				return ok(billForm.render(user, user.roles.get(0).name, id,lang));
			}
		} catch (Exception e) {
			Logger.error("BillForms.showBillForm:"+e.getMessage());
			return badRequest(messenger.render("alert alert-error",
					Messages.get("billForm"),
					Messages.get("emptyDostykProbably")));
		}
	}

	public static Result saveMekeme() {
		try {
			Logger.info("BillForms.saveMekeme");
			DynamicForm df = form().bindFromRequest();

			Long.parseLong(df.field("group").value());
			AuthorisedUser user = AuthorisedUserLogic.getById(Long.parseLong(df
					.field("userId").value()));
			PaymentPeriod period = new PaymentPeriod();

			Payment p = new Payment();
			p.user = user;
			String date = df.get("start_date");
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			p.startDate = format.parse(date);
			date = df.get("end_date");
			p.endDate = format.parse(date);
			int payment = Integer.parseInt(df.get("payment_per_student"));
			p.plannedPayment = payment;
			p.debtPayment = payment;
			p.payedPayment = 0;
			p.status = "CURRENT";
			p.discount = DiscountValueLogic.getAll().get(0).value;
			p.save();
			period.period = 0;
			period.plannedPaymentDate = p.startDate;
			period.payedPaymentDate = p.startDate;
			period.plannedPayment = p.plannedPayment;
			period.payment = p;
			period.status = "PLANNED";
			period.debtPayment = p.plannedPayment;
			period.save();
			p.periods.add(period);
			p.save();
			flash("success", "SavedSuccessfully");
			return redirect(request().getHeader("referer"));
		} catch (Exception e) {
			Logger.error("BillForms.saveMekeme:"+e.getMessage());
			return badRequest(message.render("alert alert-error",
					e.getMessage()));
		}
	}

	public static Result saveBillForm(long id) {
		try {
			Logger.info("BillForms.saveBillForm");
			DynamicForm df = form().bindFromRequest();
			AuthorisedUser user;
			if (id == -1) {
				user = AuthorisedUser
						.getUserByEmail(session().get("connected"));
			} else {
				user = AuthorisedUser.getUserById(id);
			}
			Payment p = PaymentLogic.getActualPayment(user);
			if (p != null) {
				return badRequest(message.render("alert alert-error",
						"youHaveSavedBillFormAlready"));
			}
			p = new Payment();
			p.user = AuthorisedUserLogic.getById(id);
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			String date = df.get("startDate");
			p.startDate = format.parse(date);
			date = df.get("endDate");
			format = new SimpleDateFormat("yyyy-MM-dd");
			p.endDate = format.parse(date);
			p.plannedPayment = Integer.parseInt(df.get("totalCashForPayment"));
			p.debtPayment = Integer.parseInt(df.get("totalCashForPayment"));
			int periods = Integer.parseInt(df.get("periods"));
			double discount = 0.0;
			long disc = Long.parseLong(df.get("discount"));
			p.discount = DiscountValueLogic.getById(disc).value;
			p.discountsMaxes = DiscountValueLogic.getById(disc).dmax;
			p.hoursPerWeek = Double.parseDouble(df.get("hoursPerWeek"));
			p.pricePerWeek = Double.parseDouble(df.get("pricePerWeek"));
			p.weeks = Integer.parseInt(df.get("weeks"));
			if (df.get("discount_bonus") != null) {
				discount = Double.parseDouble(df.get("discount_bonus"));
				p.discountBonus = discount;
			}
			String comments = df.get("comments");
			p.comment = comments;
			String m = df.get("material_cost");
			p.materialCost = Double.parseDouble(m);
			//p.contract = df.get("contract");
			//p.contractDate = format.parse(df.get("beginDate"));
			p.save();
			Contract contract = new Contract();
			contract.number = df.get("contract");
			contract.payment = p;
			contract.beginDate = format.parse(df.get("beginDate"));
            contract.endDate = p.endDate;
            contract.prefix = "ДКЗ";
			contract.student = p.user;
			contract.save();
			PaymentPeriod payment;
			for (int i = 0; i < periods; i++) {
				date = df.get("date" + i);

				payment = new PaymentPeriod();
				payment.period = i;
				payment.status = "PLANNED";
				payment.plannedPayment = Double.parseDouble(df.get("price" + i));
				payment.debtPayment = Double.parseDouble(df.get("price" + i));
				format = new SimpleDateFormat("dd/MM/yyyy");
				payment.plannedPaymentDate = format.parse(date);
				payment.payedPaymentDate = format.parse(date);
				payment.payment = p;
				if(payment.plannedPayment > 0.0) {
					payment.save();
					p.periods.add(payment);
				}

			}
			p.save();

			return ok(message.render("alert alert-success",
					"billFormSuccessfullySaved"));
		} catch (Exception e) {
			Logger.error("BillForms.saveBillForm:"+e.getMessage());

			return badRequest(message.render("alert alert-error",
					e.getMessage()));
		}
	}
	public static Result printDeal(String no, String date, Long sid) throws Exception{
		//@(no: String, date: Date, student: AuthorisedUser, parent: Parent, dostyk: Dostyk, director: AuthorisedUser)
		Date dd = new SimpleDateFormat("yyyy-MM-dd").parse(date);
		AuthorisedUser student = AuthorisedUser.getUserById(sid);
		Parent parent = null;
		if(student.parents.size()>0)
			parent = student.parents.get(0);

		Dostyk dostyk = null;
		AuthorisedUser director = null;
		if(student.dostyks.size()>0) {
			dostyk = student.dostyks.get(0);
			if(AuthorisedUser.getDirectorsByDostyk(dostyk).size()>0)
				director = AuthorisedUser.getDirectorsByDostyk(dostyk).get(0);
		}

		return ok(deal.render(no, dd, student, parent, dostyk, director));
	}
}