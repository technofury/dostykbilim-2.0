package controllers.social;

import static play.data.Form.form;

import java.util.List;

import models.entities.social.Dostyk;
import models.logic.social.DostykLogic;
import play.data.Form;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.helps.groups;
import views.html.helps.messenger;
import views.html.social.dostyks;
import views.html.social.editDostyk;
import views.html.sms.smsPage;

public class Dostyks extends Controller {
	final static Form<Dostyk> dForm = form(Dostyk.class);

	public static Result saveSMS(int val, Long dostykId) {
		Dostyk dostyk = DostykLogic.getById(dostykId);
		dostyk.smsLeft = val;
		dostyk.save();
		return ok("OK");
	}

	public static Result sms() {
		List<Dostyk> dostyks = DostykLogic.getAll();
		return ok(smsPage.render(dostyks));
	}

	public static Result getGroups(Long dostykId) {
		Dostyk d = DostykLogic.getById(dostykId);
		return ok(groups.render(d));
	}

	public static Result showDostyks() {
		List<Dostyk> dList = DostykLogic.getAll();
		return ok(dostyks.render(dList, dForm));
	}

	public static Result saveDostyk() {
		Form<Dostyk> filledForm = dForm.bindFromRequest();
		if (filledForm.field("name").valueOr("").isEmpty()) {
			filledForm.reject("name", Messages.get("dostyk.errorName"));
		}
		if (filledForm.field("code").valueOr("0").isEmpty()) {
			filledForm.reject("code", Messages.get("dostyk.errorCode"));
		} else if (DostykLogic.getByCode(Integer.parseInt(filledForm.field(
				"code").value())) != null) { // check if such code exists
			filledForm.reject("code", Messages.get("dostyk.usedCode"));
		}
		List<Dostyk> dList = DostykLogic.getAll();
		Dostyk toSave;
		if (filledForm.hasErrors()) {
			return badRequest(dostyks.render(dList, filledForm));
		} else {
			toSave = filledForm.get();
			toSave.name = filledForm.field("name").value();
			toSave.code = Integer.parseInt(filledForm.field("code").value());
			toSave.status = 1;
			toSave.save();
			dList = DostykLogic.getAll();
			return ok(dostyks.render(dList, dForm));
		}
	}

	public static Result saveExistingDostyk(Long id) {
		Form<Dostyk> filledForm = dForm.bindFromRequest();
		if (filledForm.field("name").valueOr("").isEmpty()) {
			filledForm.reject("name", Messages.get("dostyk.errorName"));
		}
		if (filledForm.field("code").valueOr("0").isEmpty()) {
			filledForm.reject("code", Messages.get("dostyk.errorCode"));
		} else if (DostykLogic.countByCode(id,Integer.parseInt(filledForm.field(
				"code").value())) > 0) { // check if such code exists
			filledForm.reject("code", Messages.get("dostyk.usedCode"));
		}
		List<Dostyk> dList = DostykLogic.getAll();

		if (filledForm.hasErrors()) {
			return badRequest(editDostyk.render(filledForm,id));
		} else {
			Dostyk toSave = DostykLogic.getById(id);
			toSave.name = filledForm.field("name").value();
			toSave.code = Integer.parseInt(filledForm.field("code").value());
			toSave.status = 1;
			toSave.update();
			dList = DostykLogic.getAll();
			return ok(dostyks.render(dList, dForm));
		}
	}

	public static Result deleteDostyk(Long id) {
		try {
			Dostyk toDelete = DostykLogic.getById(id);
			toDelete.status = 0;
			toDelete.update();
			return ok(messenger.render("alert alert-success","congratulations",Messages.get("dostyk.successfullyDeleted")));
		} catch (Exception e) {
			return badRequest(messenger.render("alert alert-error",
					Messages.get("saveDostykError"), e.getMessage()));
		}
	}

	public static Result editDostyk(Long id) {
		Form<Dostyk> filledForm = form(Dostyk.class);
		Dostyk toEdit = DostykLogic.getById(id);
		return ok(editDostyk.render(filledForm.fill(toEdit), id));

	}
}
