package controllers.social;

import com.fasterxml.jackson.databind.JsonNode;
import models.entities.social.Dostyk;
import models.entities.user.AuthorisedUser;
import models.logic.social.DostykGroupLogic;
import models.logic.social.DostykLogic;
import models.logic.user.AuthorisedUserLogic;
import play.Play;
import play.data.Form;
import play.libs.F.Promise;
import play.libs.ws.WS;
import play.libs.ws.WSRequestHolder;
import play.libs.ws.WSResponse;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.sms.smsPage;
import views.html.sms.writePage;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import static play.data.Form.form;

/**
 * Created by techno on 8/2/17.
 */
public class SMS extends Controller {
    public static Result smsWrite() {
        AuthorisedUser user = AuthorisedUser.getUserByEmail(session().get("connected"));
        Dostyk dostyk = user.dostyks.get(0);
        return ok(writePage.render(dostyk));
    }

    public static Result smsSubmit() {
        Form<models.entities.social.SMS> filledForm = form(models.entities.social.SMS.class).bindFromRequest();
        if (filledForm.hasErrors()) {
            flash("bad", "bad");
            return redirect(request().getHeader("referer"));
        }

        models.entities.social.SMS sms = filledForm.get();
        if (sms.text.equals("") || sms.text == null) {
            flash("notext", "notext");
            return redirect(request().getHeader("referer"));
        }

        List<String> phones = new ArrayList<>();
        if (sms.groupIDs != null) {
            for (Long groupID : sms.groupIDs) {
                List<AuthorisedUser> students = AuthorisedUserLogic.getByGroup(DostykGroupLogic.getById(groupID));
                for (AuthorisedUser user : students) {
                    try {
                        String phone = user.parents.get(0).getPhoneNumber1();
                        phones.add(phone);
                    } catch (Exception e) {e.printStackTrace();}
                }
            }
        }

        if (sms.studentIDs != null) {
            for (Long studentID : sms.studentIDs) {
                AuthorisedUser user = AuthorisedUserLogic.getById(studentID);
                try {
                    String phone = user.parents.get(0).getPhoneNumber1();
                    phones.add(phone);
                } catch (Exception e) {e.printStackTrace();}
            }
        }

        String url = "http://smsc.kz/sys/send.php";
        String login = Play.application().configuration().getString("sms.login");
        String psw = Play.application().configuration().getString("sms.psw");
        String mes = sms.text;
        String SMSC_CHARSET = "UTF-8";
        try {
            login = URLEncoder.encode(login, SMSC_CHARSET);
            psw = URLEncoder.encode(psw, SMSC_CHARSET);
        } catch (Exception e) {e.printStackTrace();}

        AuthorisedUser user = AuthorisedUser.getUserByEmail(session().get("connected"));
        Dostyk dostyk = user.dostyks.get(0);

        String list = "";
        for (int i = 0; i < phones.size(); i++) {
            String phone = phones.get(i);
            dostyk.smsLeft -= 1;
            dostyk.update();
            phone = phone.replaceAll("[()]", "");
            phone = phone.replaceAll(" ", "");
            phone = phone.replaceAll("[+]", "");

            phone = phone.replaceFirst("7", "8");
            try {
                list += phone + ":" + mes;
                if (i < phones.size() - 1)
                    list += "\n";
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        WSRequestHolder holder = WS.url(url);

        WSRequestHolder complexHolder = holder
                .setTimeout(1000)
                .setQueryParameter("login", login)
                .setQueryParameter("psw", psw)
                .setQueryParameter("fmt", "3")
                .setQueryParameter("charset", SMSC_CHARSET)
                .setQueryParameter("list", list);
        Promise<WSResponse> responsePromise = complexHolder.get();

        Promise<JsonNode> jsonPromise = responsePromise.map(
                response -> {
                    JsonNode json = response.asJson();

                    return json;
                }
        );
        flash("sms", "sms");

        return redirect(request().getHeader("referer"));
    }

    public static Result smsAdmin() {
        List<Dostyk> dostyks = DostykLogic.getAll();
        return ok(smsPage.render(dostyks));
    }

    public static Result saveSMS(int val, Long dostykId) {
        Dostyk dostyk = DostykLogic.getById(dostykId);
        dostyk.smsLeft = val;
        dostyk.save();
        return ok("OK");
    }

}