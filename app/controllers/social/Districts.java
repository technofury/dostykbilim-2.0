package controllers.social;

import java.util.Collections;
import java.util.Comparator;

import models.entities.social.District;
import models.entities.social.School;
import models.logic.social.DistrictLogic;
import models.logic.social.RegionLogic;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.helps.selectSchools;

public class Districts extends Controller {
	public static Result getSchools(String code, String reg) {
		District district = DistrictLogic.getByCode(code,
				RegionLogic.getByCode(reg));
		Collections.sort(district.schools,
				new Comparator<School>() {
					public int compare(
							School m1,
							School m2) {
						return m1.name_kz
								.compareTo(m2.name_kz);
					}
				});
		return ok(selectSchools.render(reg, code, district.schools));
	}
}
