package controllers.social;

import static play.data.Form.form;
import models.entities.social.Content;
import models.entities.user.AuthorisedUser;
import models.logic.social.ContentLogic;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.social.contentForm;
import views.html.social.contentList;
import views.html.social.contentPage;

public class Contents extends Controller {

	public static Result all() {
		Form<Content> contentForm = form(Content.class);
		AuthorisedUser user = AuthorisedUser
				.getUserByEmail(session("connected"));
		return ok(contentPage.render(
				models.logic.social.ContentLogic.getAll(user.dostyks.get(0)),
				contentForm, user));
	}

	public static Result edit(Long id) {
		Form<Content> form = form(Content.class).fill(ContentLogic.getById(id));
		AuthorisedUser user = AuthorisedUser
				.getUserByEmail(session("connected"));
		return ok(contentForm.render(form, user));
	}

	public static Result delete(Long id) {
		ContentLogic.delete(ContentLogic.getById(id));

		AuthorisedUser user = AuthorisedUser
				.getUserByEmail(session("connected"));
		return ok(contentList.render(
				models.logic.social.ContentLogic.getAll(user.dostyks.get(0)),
				user));
	}

	public static Result save() {
		Form<models.entities.social.Content> form = form(
				models.entities.social.Content.class).bindFromRequest();
		AuthorisedUser user = AuthorisedUser
				.getUserByEmail(session("connected"));
		if (form.hasErrors()) {
			return badRequest(contentForm.render(form, user));
		} else {

			Content created = form.get();
			ContentLogic.create(created);
			return ok(contentList.render(models.logic.social.ContentLogic
					.getAll(user.dostyks.get(0)), user));
		}
	}
}
