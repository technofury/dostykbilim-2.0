package controllers.social;

import static play.data.Form.form;

import java.util.*;

import models.entities.education.Course;
import models.timetable.Timetable;
import models.entities.education.Lesson;
import models.entities.examination.Exam;
import models.entities.examination.ExamResult;
import models.entities.social.DostykGroup;
import models.entities.user.AuthorisedUser;
import models.logic.education.LessonLogic;
import models.logic.examination.ExamLogic;
import models.logic.examination.ExamResultLogic;
import models.logic.social.DostykGroupLogic;
import models.logic.user.AuthorisedUserLogic;
import play.data.validation.ValidationError;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.social.groupForm;
import views.html.social.groupList;
import views.html.social.groupStudents;
import views.html.social.groupTabel;
import views.html.social.groupTabelByExam;
import views.html.social.groupTableBody;
import be.objectify.deadbolt.java.actions.Group;
import be.objectify.deadbolt.java.actions.Restrict;

public class DostykGroups extends Controller {

	public static Result groupStudents(Long id) {
		DostykGroup dostykGroup = DostykGroupLogic.getById(id);

			List<AuthorisedUser> users = AuthorisedUserLogic
					.getByElective(dostykGroup);
		return ok(groupStudents.render(dostykGroup,
				AuthorisedUserLogic.getByProfiles(dostykGroup.students)));
	}

	public static Result groupTabel(Long id) {
		DostykGroup dostykGroup = DostykGroupLogic.getById(id);
		return ok(groupTabel.render(id, ExamLogic.getByGrade(dostykGroup.courses.title)));
	}

	public static Result groupTabelByExam(Long id, Long examId) {
		DostykGroup dostykGroup = DostykGroupLogic.getById(id);
		List<AuthorisedUser> users = AuthorisedUserLogic
				.getByProfiles(dostykGroup.students);
		List<ExamResult> examResults = new ArrayList<ExamResult>();
		Exam exam = ExamLogic.getById(examId);
		for (AuthorisedUser u : users) {
			ExamResult res = ExamResultLogic.getByExamAndUser(exam, u);
			examResults.add(res);
		}

		List<AuthorisedUser> users2 = new ArrayList<AuthorisedUser>();
		List<ExamResult> res = new ArrayList<ExamResult>();

		for (int i = 0; i < users.size(); i++)
			if (examResults.get(i) != null) {
				users2.add(users.get(i));
				res.add(examResults.get(i));
			}

		for (int i = 0; i < users.size(); i++)
			if (examResults.get(i) == null) {
				users2.add(users.get(i));
				res.add(examResults.get(i));
			}

		return ok(groupTabelByExam.render(id, res, users2, dostykGroup.courses.title+""));
	}

	public static Result all() {
		AuthorisedUser user = AuthorisedUser
				.getUserByEmail(session("connected"));
		List<DostykGroup> dostykGroups = null;

		if (user.id == 1L) {
			dostykGroups = DostykGroupLogic.getAllCompanyGroups();
		} else {
			dostykGroups = user.dostyks.get(0).dostykGroups;
		}

		Collections.sort(dostykGroups, new Comparator<DostykGroup>() {
			@Override
			public int compare(DostykGroup a, DostykGroup b) {
				return a.name.compareTo(b.name);
			}
		});

		Form<DostykGroup> groupForm = form(DostykGroup.class);
		return ok(groupList.render(dostykGroups, groupForm));
	}

	public static Result changeStatus(Long id) {
		DostykGroup dostykGroup = DostykGroupLogic.getById(id);
		dostykGroup.save();
		List<DostykGroup> dostykGroups = DostykGroupLogic.getAllCompanyGroups();
		return ok(groupTableBody.render(dostykGroups));
	}

	public static String lastInt(String s) {
		int i = s.length();
		while (i > 0 && Character.isDigit(s.charAt(i - 1))) {
			i--;
		}
		return s.substring(i);
	}

	public static Result edit(Long id) {
		Form<DostykGroup> form = form(DostykGroup.class).fill(DostykGroupLogic.getById(id));
		return ok(groupForm.render(form, lastInt(form.field("name").value())));
	}

	public static Result submit() {
		Form<DostykGroup> form = form(DostykGroup.class).bindFromRequest();
		AuthorisedUser user = AuthorisedUser.getUserByEmail(session("connected"));
			String id = form.field("id").value();
			String name = form.field("name").value();

			//int coursesTitle = Integer.parseInt(grade);
			//Course course = null;
			//if (coursesTitle!=0) { course = Course.byTitle(coursesTitle); }

			//List<DostykGroup> exists = DostykGroupLogic.getByNameAndDostyk(name, user.dostyks.get(0));




			if (form.hasErrors()) {
				return badRequest(groupForm.render(form, form.field("count").value()));
			}


			DostykGroup cur = form.get();

			if (id.equals("")) {
				DostykGroup dg = new DostykGroup();
				dg.name = name;
				dg.dostyk = user.dostyks.get(0);
				dg.language = cur.language;
				dg.curator = cur.curator;
				dg.courses = cur.courses;
				dg.save();
			} else {
				DostykGroup dg = DostykGroupLogic.getById(cur.id);
				dg.name = name;
				dg.dostyk = user.dostyks.get(0);
				dg.language = cur.language;
				dg.curator = cur.curator;
				dg.courses = cur.courses;
				dg.update();
			}
		List<DostykGroup> dostykGroups = user.dostyks.get(0).dostykGroups;
		Collections.sort(dostykGroups, new Comparator<DostykGroup>() {
			@Override
			public int compare(DostykGroup a, DostykGroup b) {
				return a.name.compareTo(b.name);
			}
		});
		return ok(groupTableBody.render(dostykGroups));

	}

	@Restrict({ @Group("admin"),@Group("moderator") })
	public static Result delete(Long id) {
		DostykGroup dg = DostykGroupLogic.getById(id);
		List<Timetable> tables = Timetable.byGrade(dg);
		for (Timetable table : tables)
			table.delete();
		dg.delete();
		return ok("deleted successfully");
	}
}
