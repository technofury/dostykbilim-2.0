package controllers.social;

import be.objectify.deadbolt.java.actions.Group;
import be.objectify.deadbolt.java.actions.Restrict;
import controllers.user.AuthorisedUsers;
import models.entities.social.Dostyk;
import models.entities.social.Firm;
import models.entities.user.AuthorisedUser;
import models.logic.social.DostykLogic;
import play.data.DynamicForm;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.social.firms;

/**
 * Created by Droid on 3/28/2016.
 */
public class Firms extends Controller {
    @Restrict({ @Group("moderator") })
    public static Result viewAll(){
        AuthorisedUser moder = AuthorisedUsers.getConnectedUser();
        Dostyk dostyk = moder.dostyks.get(0);
        return ok(firms.render(moder, dostyk));
    }
    @Restrict({ @Group("moderator") })
    public static Result save(Long dostykId,Long firmId,Boolean isNew){
        try{
            DynamicForm form = Form.form().bindFromRequest();
            Firm object;
            if(isNew==true)
                object = new Firm();
            else
                object = Firm.getById(firmId);
            object.address = form.get("address");
            object.bank = form.get("bank");
            object.bik = form.get("bik");
            object.bin = form.get("bin");
            object.city = form.get("city");
            object.director = form.get("director");
            object.dostyk = DostykLogic.getById(dostykId);
            object.iik = form.get("iik");
            object.kbe = form.get("kbe");
            object.name = form.get("name");
            object.phone = form.get("phone");

            if((isNew==true&&Firm.countByName(object.dostyk, object.name)>0))
                return ok("name_already_exists");
            else if((isNew==false&&Firm.countByName(object.dostyk, object.name)==1)&&
                    !Firm.getByName(object.dostyk, object.name).id.equals(object.id)) {
                return ok("name_already_exists");
            }else {
                object.save();
                return ok(object.id + "");
            }
        }catch(Exception e){
            e.printStackTrace();
            return ok("exception");
        }
    }
    @Restrict({ @Group("moderator")})
    public static Result delete(Long firmId){
        try{
            Firm toDelete = Firm.getById(firmId);
            toDelete.delete();
            return ok("success");
        }catch(Exception e){
            e.printStackTrace();
            return ok("exception");
        }
    }
}
