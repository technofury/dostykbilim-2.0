package controllers.social;

import java.util.Collections;
import java.util.Comparator;

import models.entities.social.District;
import models.entities.social.Region;
import models.logic.social.RegionLogic;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.helps.selectDistricts;

public class Regions extends Controller {
	public static Result getDistricts(String code) {
		Region region = RegionLogic.getByCode(code);
		//region.districts
		Collections.sort(region.districts,
				new Comparator<District>() {
					public int compare(
							District m1,
							District m2) {
						return m1.name_kz
								.compareTo(m2.name_kz);
					}
				});
		//Collections.sort(region.districts);
		return ok(selectDistricts.render(code, region.districts));
	}
}
