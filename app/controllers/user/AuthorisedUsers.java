package controllers.user;

import static play.data.Form.form;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import be.objectify.deadbolt.java.actions.Group;
import be.objectify.deadbolt.java.actions.Restrict;
import models.entities.education.Lesson;
import models.entities.social.Dostyk;
import models.entities.social.DostykGroup;
import models.entities.social.Firm;
import models.entities.user.AuthorisedUser;
import models.entities.user.Parent;
import models.entities.user.Requisites;
import models.entities.user.SecurityRole;
import models.logic.education.CourseTypeLogic;
import models.logic.education.LessonLogic;
import models.logic.social.DistrictLogic;
import models.logic.social.DostykGroupLogic;
import models.logic.social.DostykLogic;
import models.logic.social.RegionLogic;
import models.logic.social.SchoolLogic;
import models.logic.user.EmailLogic;
import play.Application;
import play.data.DynamicForm;
import play.data.Form;
import play.i18n.Messages;
import play.libs.Crypto;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.user.*;
import views.html.user.profilePack.editEmail;
import views.html.user.profilePack.editPassword;
import views.html.user.profilePack.profileView;

import com.avaje.ebean.Ebean;

public class AuthorisedUsers extends Controller {
	public static AuthorisedUser getConnectedUser() {
		String mail = session().get("connected");
		if (mail == null)
			return null;
		else {
			return AuthorisedUser.getUserByEmail(mail);
		}
	}

	public static Result restorePasswordPage() {
		return ok(restorePassword.render());
	}

	public static Result restorePassword() {
		DynamicForm requestData = form().bindFromRequest();
		String email = requestData.get("email");

		AuthorisedUser user = AuthorisedUser.getUserByEmail(email);

		if (user == null) {
			flash("request", "noUserWithSuchEmail");
			return badRequest(restorePassword.render());
		}

		if (user.roles.get(0).name.equalsIgnoreCase("moderator")
				|| user.roles.get(0).name.equalsIgnoreCase("admin")) {
			flash("request", "youCannotChangeYourPasswordCallToAdmin");
			return badRequest(restorePassword.render());
		}
		user.password = Crypto.encryptAES(EmailLogic.sendNewPassword(user));
		user.save();
		flash("request", "checkEmailForNewPassword");
		return ok(restorePassword.render());
	}

	public static Result signIn() {
		Form<AuthorisedUser> signinForm = form(AuthorisedUser.class);
		return ok(signin.render(signinForm));
	}

	public static Result signUp(String whom) {
		Form<AuthorisedUser> signupForm = form(AuthorisedUser.class);
		return ok(signup.render(signupForm, whom, RegionLogic.getAll()));
	}

	public static boolean checkWhetherId(String login) {
		String regex = "[0-9]+";
		if (login.length() != 8)
			return false;
		else if (!login.matches(regex))
			return false;
		else
			return true;
	}

	public static boolean checkWhetherEmail(String login) {
		return login.contains("@");
	}

	public static Result userCheck() {
		DynamicForm df = form().bindFromRequest();
		String login = df.get("email");
		String password = df.get("password");
		boolean isId = false;
		boolean isEmail = false;
		isId = checkWhetherId(login);
		isEmail = checkWhetherEmail(login);
		if (isId == false && isEmail == false) {
			flash("authorization", Messages.get("authorization.error"));
			return redirect(controllers.user.routes.AuthorisedUsers.signIn());
		} else if (isId == true && isEmail == false) {
			password = Crypto.encryptAES(password);
			AuthorisedUser user = AuthorisedUser.getUserById(login, password);
			if (user != null) {
				if (!user.status.equals("active")) {
					flash("authorization", Messages.get("Your.status.inactive"));
					return redirect(controllers.user.routes.AuthorisedUsers
							.signIn());
				} else {
					session("connected", user.email);
					return redirect(controllers.routes.Application.index());
				}
			} else {
				flash("authorization", Messages.get("authorization.error"));
				return redirect(controllers.user.routes.AuthorisedUsers
						.signIn());
			}
		} else if (isId == false && isEmail == true) {
			password = Crypto.encryptAES(password);
			AuthorisedUser user = AuthorisedUser.getUser(login, password);
			if (user != null) {
				if (!user.status.equals("active")) {
					flash("authorization", Messages.get("Your.status.inactive"));
					return redirect(controllers.user.routes.AuthorisedUsers
							.signIn());
				} else {
					session("connected", user.email);
					return redirect(controllers.routes.Application.index());
				}
			} else {
				flash("authorization", Messages.get("authorization.error"));
				return redirect(controllers.user.routes.AuthorisedUsers
						.signIn());
			}
		} else {
			flash("authorisation", Messages.get("authorization.error"));
			return redirect(controllers.user.routes.AuthorisedUsers.signIn());
		}

	}

	public static Result signOut() {
		session().remove("connected");
		String osType = System.getProperty("os.name");
		if(osType.equalsIgnoreCase("Linux"))
			return redirect("http://dostykbilim.kz");
		else
			return redirect(controllers.routes.Application.index());
	}

	public static Result submit() {
		Form<AuthorisedUser> signupForm = form(AuthorisedUser.class);
		Form<AuthorisedUser> filledForm = signupForm.bindFromRequest();

		String roleName = filledForm.data().get("role");
		if (!"true".equals(filledForm.field("accept").value())) {
			filledForm.reject("accept",
					"You must accept the terms and conditions");

		}

		/*if (AuthorisedUser.getUserByEmail(filledForm.data().get("email")) != null) {
			filledForm.reject("email", "this email is already taken");

		}*/
		if ((filledForm.data().get("course") == null || filledForm.data()
				.get("course").equals(""))
				&& roleName.equals("student")) {
			filledForm.reject("course", "select one");

		}

		if ((filledForm.data().get("region") == null || filledForm.data()
				.get("region").equals(""))
				&& roleName.equals("student")) {
			filledForm.reject("region", "select one");

		}

		if ((filledForm.data().get("district") == null || filledForm.data()
				.get("district").equals(""))
				&& roleName.equals("student")) {
			filledForm.reject("district", "select one");

		}
		if ((filledForm.data().get("school") == null || filledForm.data()
				.get("school").equals(""))
				&& roleName.equals("student")) {
			filledForm.reject("school", "select one");

		}
		if (!filledForm.data().get("password")
				.equals(filledForm.data().get("repeatPassword"))
				|| filledForm.data().get("password").length() < 6) {
			filledForm.reject("password",
					Messages.get("incorrect.password.or.less.than.6.chars"));

		}
		if (!filledForm.data().get("password")
				.equals(filledForm.data().get("repeatPassword"))
				|| filledForm.data().get("password").length() < 6) {

			filledForm.reject("password",
					Messages.get("incorrect.password.or.less.than.6.chars"));
		}
		if(roleName.equals("student"))
			if(filledForm.data().get("sfirm") == null || filledForm.data()
					.get("sfirm").equals("")){
				filledForm.reject("sfirm",
						Messages.get("RequiredField"));

			}

		if (filledForm.hasErrors()) {

			return badRequest(signup.render(filledForm, roleName,RegionLogic.getAll()));
		} else {
			AuthorisedUser user = filledForm.get();
			user.roles = new ArrayList<SecurityRole>();
			user.roles.add(SecurityRole.getRoleByName(roleName));
			user.password = filledForm.data().get("password");
			user.password = Crypto.encryptAES(user.password);// MD5

			if (roleName.equals("student")) {
				String parentTypes[] = { /*"mother", "father",*/ "guardian" };
				List<Parent> parents = new ArrayList<Parent>();
				for (String p : parentTypes) {
					Parent parent = new Parent();
					parent.parentType = p;
					parent.firstName = filledForm.data().get(p + ".firstName");
					parent.lastName = filledForm.data().get(p + ".lastName");
					parent.inn = filledForm.data().get(p + ".inn");
					parent.phoneNumber1 = filledForm.data().get(p + ".phoneNumber1");
					parent.phoneNumber2 = filledForm.data().get(p + ".phoneNumber2");
					parent.work = filledForm.data().get(p + ".work");
					if (filledForm.data().get("responsible").equalsIgnoreCase(p))
						parent.responsible = true;
					parents.add(parent);
				}
				user.parents = parents;

				user.profile.ulgerimi = filledForm.data().get("ulgerimi");

				user.profile.lang = filledForm.data().get("lang");


				user.profile.course = CourseTypeLogic.getByName(filledForm.data().get("course"));
				user.profile.region = RegionLogic.getByCode(filledForm.data().get("region"));
				user.profile.district = DistrictLogic.getByCode(filledForm.data().get("district"), RegionLogic
						.getByCode(filledForm.data().get("region")));
				user.profile.save();
				user.profile.school = SchoolLogic.getByCode(filledForm.data()
						.get("school"), user.profile.region,user.profile.district);
			}

			if (roleName.equals("teacher") || roleName.equals("student")) {
				user.profile.lessons = new ArrayList<Lesson>();
				for (Entry<String, String> field : filledForm.data().entrySet()) {
					if (field.getKey().startsWith("lesson[")) {
						user.profile.lessons.add(LessonLogic.getById(Long.parseLong(field.getValue())));
					}
				}
			}
            user.profile.save();
            user.save();
			String who = session().get("connected");
			Dostyk dostyk;
			if (filledForm.data().get("dostyk") == null
					|| filledForm.data().get("dostyk").length() == 0) {
				AuthorisedUser moder = AuthorisedUser.getUserByEmail(who);
				dostyk = moder.dostyks.get(0);
			} else {
				dostyk = DostykLogic.getByName(filledForm.data().get("dostyk"));
			}
			dostyk.users.add(user);
			dostyk.save();
			if(roleName.equals("student"))
            	user.firm = Firm.getById(Long.parseLong(filledForm.data().get("sfirm")));
            user.profile.save();
            user.save();
			Ebean.saveManyToManyAssociations(user, "roles");

			if (who == null || who.length() == 0) {
				return redirect(controllers.routes.Application.index());
			} else if (who.indexOf("admin") >= 0) {
				return redirect(controllers.user.routes.UserManagement.moderators());
			} else {
				return redirect(controllers.user.routes.UserManagement.list(0,"name", "asc", "","all","all"));
			}
		}
	}

	public static Result showProfile() {
		AuthorisedUser user = AuthorisedUser.getUserByEmail(session().get(
				"connected"));
		return ok(profile.render(user));
	}

	public static Result saveProfile(long id) throws ParseException {

		AuthorisedUser user, moder;
		if (id == -1) {
			user = AuthorisedUser.getUserByEmail(session().get("connected"));
		} else {
			user = AuthorisedUser.getUserById(id);
		}
		moder = AuthorisedUser.getUserByEmail(session().get("connected"));
		String roleName = user.roles.get(0).name;
		Form<AuthorisedUser> userForm = form(AuthorisedUser.class)
				.bindFromRequest();
		if (userForm.hasErrors()) {

		} else {
			AuthorisedUser food = userForm.get();
			food.update();
		}
		if(userForm.data().get("sfirm")!=null) {

			user.firm = Firm.getById(Long.parseLong(userForm.data().get("sfirm")));
		}
		user.profile.firstName = userForm.data().get("profile.firstName");
		user.profile.lastName = userForm.data().get("profile.lastName");
		user.profile.secondName = userForm.data().get("profile.secondName");
		user.profile.inn = userForm.data().get("profile.inn");
		user.profile.gender = userForm.data().get("profile.gender");
		user.profile.lang = userForm.data().get("lang");
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		if (!userForm.data().get("profile.birthDate").equals("")) {
			user.profile.birthDate = format.parse(userForm.data().get(
					"profile.birthDate"));
		}

		user.profile.phoneNumber = userForm.data().get("profile.phoneNumber");
		if (userForm.data().containsKey("profile.parentNumber")) {
			user.profile.parentNumber = userForm.data().get(
					"profile.parentNumber");
		}

		if (userForm.data().containsKey("ulgerimi")) {
			user.profile.ulgerimi = userForm.data().get("ulgerimi");
		}

		if (userForm.data().containsKey("course")) {
			user.profile.course = CourseTypeLogic.getByName(userForm.data()
					.get("course"));
		}

		if (userForm.data().containsKey("dostyk")) {
			user.status = "inactive";
			Dostyk d = DostykLogic.getByName(userForm.data().get("dostyk"));
			if (d == null) {
				d = moder.dostyks.get(0);
			}
			for (int i = 0; i < d.users.size(); i++) {
				if (d.users.get(i).id == user.id) {
					d.users.remove(i);
					d.save();
					break;
				}
			}

			d = DostykLogic.getByName(userForm.data().get("dostyk"));

			if (!d.users.contains(user)) {
				user.dostyks = new ArrayList<Dostyk>();
				user.dostyks.add(d);
				user.profile.save();
				d.saveManyToManyAssociations("users");
				d.users.add(user);
				d.save();
				// moder.lessons.set(0,d);

				d.saveManyToManyAssociations("users");
			}

		}

		if (roleName.equals("teacher") || roleName.equals("student")) {
			List<Lesson> list = new ArrayList<Lesson>();
			for (Entry<String, String> field : userForm.data().entrySet()) {
				if (field.getKey().startsWith("lesson[")) {
					list.add(LessonLogic.getById(Long.parseLong(field
							.getValue())));
				}
			}
			user.profile.lessons = list;
			user.profile.save();
			user.profile.saveManyToManyAssociations("lessons");
		}
		user.profile.save();

		if (roleName.equals("student")) {
			for (Entry<String, String> field : userForm.data().entrySet()) {
				if (field.getKey().startsWith("parents[")
						&& field.getKey().length() > 8) {
					int parentId = Integer.parseInt(field.getKey().charAt(8)
							+ "");
					String key = "parents[" + parentId + "].";
					user.parents.get(parentId).firstName = userForm.data().get(
							key + "firstName");
					user.parents.get(parentId).lastName = userForm.data().get(
							key + "lastName");
					user.parents.get(parentId).inn  = userForm.data().get(
							key + "inn");
					//parent.inn = filledForm.data().get(p + ".inn");
					user.parents.get(parentId).phoneNumber1 = userForm.data()
							.get(key + "phoneNumber1");
					user.parents.get(parentId).phoneNumber2 = userForm.data()
							.get(key + "phoneNumber2");
					user.parents.get(parentId).work = userForm.data().get(
							key + "work");
					user.parents.get(parentId).responsible = Boolean
							.parseBoolean(userForm.data().get(
									key + "responsible"));
				}
			}
		}
		
		if (roleName.equals("moderator")) {
			Requisites requisite = new Requisites();
//			requisites.user = user;
			requisite.dostykOfficial = userForm.data().get("requisite.dostykOfficial");
			requisite.dostykCity     = userForm.data().get("requisite.dostykCity");
			requisite.dostykAddress  = userForm.data().get("requisite.dostykAddress");	
			requisite.dostykTel      = userForm.data().get("requisite.dostykTel");	
			requisite.dostykDirector = userForm.data().get("requisite.dostykDirector");
			requisite.dostykBIK      = userForm.data().get("requisite.dostykBIK");
			requisite.dostykIIK      = userForm.data().get("requisite.dostykIIK");
			requisite.dostykBIN      = userForm.data().get("requisite.dostykBIN");
			requisite.dostykBANK     = userForm.data().get("requisite.dostykBANK");
			requisite.dostykKBE      = userForm.data().get("requisite.dostykKBE");
			requisite.save();
			user.requisite = requisite;
			

			
		}
		user.save();
		flash("success", "Your profile changes saved successfully");
		return redirect(request().getHeader("referer"));

	}

	public static Result editMail(long id) {
		AuthorisedUser user;
		if (id == -1) {
			user = AuthorisedUser.getUserByEmail(session().get("connected"));
		} else {
			user = AuthorisedUser.getUserById(id);
		}
		Form<AuthorisedUser> userForm = form(AuthorisedUser.class).fill(user);
		return ok(editEmail.render(user, userForm, user.roles.get(0).name, id));
	}

	public static Result changeMail(long id) {
		AuthorisedUser user;
		if (id == -1) {
			user = AuthorisedUser.getUserByEmail(session().get("connected"));
		} else {
			user = AuthorisedUser.getUserById(id);
		}

		Form<AuthorisedUser> userForm = form(AuthorisedUser.class)
				.bindFromRequest();
		AuthorisedUser other = AuthorisedUser.getUserByEmail(userForm.data()
				.get("email"));

		if (other != null && other.id != user.id) {
			flash("error", "This email is already taken by other user.");
			return redirect(request().getHeader("referer"));
		}

		user.email = userForm.data().get("email");
		user.save();
		session("connected", user.email);
		flash("success", "Your email changed successfully");
		return redirect(request().getHeader("referer"));
	}

	public static Result changePassword(long id) {
		AuthorisedUser user;
		if (id == -1) {
			user = AuthorisedUser.getUserByEmail(session().get("connected"));
		} else {
			user = AuthorisedUser.getUserById(id);
		}
		Form<AuthorisedUser> userForm = form(AuthorisedUser.class)
				.bindFromRequest();

		if (id == -1) {
			String password = Crypto.encryptAES(userForm.data().get(
					"oldPassword"));
			if (!password.equals(user.password)) {

				userForm.reject("oldPassword", "Incorrect password");
				flash("error", "Incorrect old password");
				return badRequest(editPassword.render(user, userForm,
						user.roles.get(0).name, id));
			}
		}
		if (!userForm.data().get("newPassword")
				.equals(userForm.data().get("repeatPassword"))
				|| userForm.data().get("newPassword").length() < 6) {
			userForm.reject("repeatPassword",
					"Passwords doesn't match or less than 6 chars");
			flash("error", "Passwords doesn't match or less than 6 chars");
			return badRequest(editPassword.render(user, userForm,
					user.roles.get(0).name, id));
		}

		user.password = Crypto.encryptAES(userForm.data().get("newPassword"));
		user.save();
		flash("success", "Your password changed successfully");
		return redirect(request().getHeader("referer"));
	}

	public static Result editPassword(long id) {
		AuthorisedUser user;
		if (id == -1) {
			user = AuthorisedUser.getUserByEmail(session().get("connected"));
		} else {
			user = AuthorisedUser.getUserById(id);
		}
		Form<AuthorisedUser> userForm = form(AuthorisedUser.class).fill(user);
		return ok(editPassword.render(user, userForm, user.roles.get(0).name,
				id));
	}

	public static Result editProfile(long id) {
		AuthorisedUser user;
		if (id == -1) {
			user = AuthorisedUser.getUserByEmail(session().get("connected"));
		} else {
			user = AuthorisedUser.getUserById(id);
		}
		Form<AuthorisedUser> userForm = form(AuthorisedUser.class).fill(user);

		return ok(profileView.render(user, userForm, user.roles.get(0).name,
				RegionLogic.getAll(), id));
	}

	public static String format(int x) {
		String s = "" + x;
		while (s.length() < 6)
			s = "0" + s;
		return s;
	}

	public static Result changeStatus(long id) {
		AuthorisedUser user = AuthorisedUser.getUserById(id);
		if (user.status.equals("active")) {
			user.status = "inactive";
			user.save();
			return ok("inactive");
		} else {
			user.status = "active";
			if (user.profile.idNumber != null
					&& user.profile.idNumber.length() > 0) {
			} else {
				String dostykCode = Integer.toString(user.dostyks.get(0).code);
				int alreadyHave = AuthorisedUser
						.getCountWhichIdStarts(dostykCode);
				//System.err.println(alreadyHave);				
				String tempId = ""; 
				for(int i = 1;;i++){					 
					 tempId = dostykCode + format(alreadyHave + i);
					 if(AuthorisedUser.getByIdNumber(tempId)==null){
						 break;
					 }
				}
				user.profile.idNumber = tempId;
				user.profile.save();

			}
			user.save();
			return ok(user.profile.idNumber);
		}
	}

	public static Result deleteFromDostyk(long id) {
		AuthorisedUser user = AuthorisedUser.getUserById(id);
		user.status = "deleted";
		long dostykId = user.dostyks.get(0).id;
		Dostyk dostyk = DostykLogic.getById(dostykId);

		for (int i = 0; i < user.dostyks.size(); i++) {
			if (dostyk.code == user.dostyks.get(i).code) {
				user.dostyks.remove(i);
				user.profile.dostykGroup = null;
				user.profile.electiveGroup = null;
				break;
			}
		}

		for (int i = 0; i < dostyk.users.size(); i++) {
			if (user.id == dostyk.users.get(i).id) {
				dostyk.users.remove(i);
				break;
			}
		}
		dostyk.save();

		user.profile.save();
		user.save();
		return ok();
	}

	@Restrict({@Group("moderator")})
	public static Result diactivateAllStudents() {
		int page = 0;
		String sortBy = "name";
		String order = "asc";
		String filter = "";
		AuthorisedUser moderator = AuthorisedUser.getUserByEmail(session("connected"));
		List<AuthorisedUser> userList =  moderator.dostyks.get(0).users;


			for (int i = 0; i < userList.size(); i++) {
				if (userList.get(i).roles.get(0).name.equals("student")) {
					AuthorisedUser user = AuthorisedUser.getUserById(userList.get(i).id);
					if (user.status.equals("active")) {
						user.status = "inactive";
						user.save();
					}
				}
			}
		List<DostykGroup> dostykGroups = new ArrayList<DostykGroup>();
		List<DostykGroup> electiveGroups = new ArrayList<DostykGroup>();

		for (DostykGroup group : moderator.dostyks.get(0).dostykGroups) {
			//if (!group.type.equals("E")) {
				dostykGroups.add(group);
			/*} else {
				electiveGroups.add(group);
			}*/
		}
		// System.err.println(moderator.lessons.get(0).users.size());
		return ok(list.render(AuthorisedUser.getJaiPage(
				moderator.dostyks.get(0).users, filter, page, sortBy, order,"all","all"),
				page, sortBy, order, filter, "all","all",dostykGroups, electiveGroups));
	}

	public static Result changeDostykGroup(long userId, long dostykGroupId) {
		AuthorisedUser user = AuthorisedUser.getUserById(userId);
		DostykGroup dostykGroup = DostykGroupLogic.getById(dostykGroupId);
		dostykGroup.students.add(user.profile);
		dostykGroup.save();
		user.profile.dostykGroup = dostykGroup;
		user.profile.save();
		user.save();
		return ok();
	}

	public static Result changeElectiveGroup(long userId, long electiveGroupId) {
		AuthorisedUser user = AuthorisedUser.getUserById(userId);
		DostykGroup electiveGroup = DostykGroupLogic.getById(electiveGroupId);
		electiveGroup.students.add(user.profile);
		electiveGroup.save();
		user.profile.electiveGroup = electiveGroup;
		user.profile.save();
		user.save();
		return ok();
	}

}