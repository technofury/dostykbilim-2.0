package controllers.user;

import static play.libs.F.Promise.promise;
import static play.data.Form.form;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.io.FileOutputStream;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;

import be.objectify.deadbolt.java.actions.Group;
import be.objectify.deadbolt.java.actions.Restrict;
import com.avaje.ebean.Page;
import jxl.Sheet;
import jxl.Workbook;
import models.entities.accounting.Payment;
import models.entities.accounting.PaymentPeriod;
import models.entities.accounting.PaymentReceipt;
import models.entities.education.CourseType;
import models.entities.education.Lesson;
import models.entities.social.*;
import models.entities.user.AuthorisedUser;
import models.entities.user.Parent;
import models.entities.user.Profile;
import models.entities.user.SecurityRole;
import models.logic.accounting.*;
import models.logic.education.CourseTypeLogic;
import models.logic.education.LessonLogic;
import models.logic.social.DistrictLogic;
import models.logic.social.DostykGroupLogic;
import models.logic.social.DostykLogic;
import models.logic.social.RegionLogic;
import models.logic.user.*;
import models.printable.*;
import play.Logger;
import play.data.DynamicForm;
import play.data.Form;
import play.i18n.Messages;
import play.libs.Crypto;
import play.mvc.Controller;
import play.mvc.Http.MultipartFormData;
import play.mvc.Http.MultipartFormData.FilePart;
import play.mvc.Result;
import views.html.accounting.cashBookChooser;
import views.html.education.courseTypeReport;
import views.html.helps.messenger;
import views.html.helps.studentsList;
import views.html.printable.cashBook;
import views.html.printable.myRegistration;
import views.html.printable.myRevenue;
import views.html.social.genderReport;
import views.html.social.studentSchoolReport;
import views.html.social.registrationReport;
import views.html.social.movementReport;
import views.html.social.leftReport;
import views.html.social.movementReportPrint;
import views.html.social.jurnalReportPrint;
import views.html.accounting.revenueReport;
import views.html.social._dostykRegistration;
import views.html.social.ulgerimiReport;
import views.html.user.directorsList;
import views.html.user.list;
import views.html.user.list2;
import views.html.user.modersList;
import views.html.user.studentList;
import views.html.user.uploadExcel;
import play.libs.F.Promise;

public class UserManagement extends Controller {


	public  static Result memoryUsage(){
		int mb = 1024*1024;
		Runtime runtime = Runtime.getRuntime();
		Logger.info("** Used Memory:  " + (runtime.totalMemory() - runtime.freeMemory()) / mb);
		Logger.info("** Free Memory:  " + runtime.freeMemory() / mb);
		Logger.info("** Total Memory: " + runtime.totalMemory() / mb);
		Logger.info("** Max Memory:   " + runtime.maxMemory() / mb);
		Logger.info("** Available Processors:   " + runtime.availableProcessors());

		String str = "** Used Memory:  " + (runtime.totalMemory() - runtime.freeMemory()) / mb +"\n";
		str  = str + ("** Free Memory:  " + runtime.freeMemory() / mb +"\n");
		str  = str + ("** Total Memory: " + runtime.totalMemory() / mb +"\n");
		str  = str + ("** Max Memory:   " + runtime.maxMemory() / mb +"\n");
		str  = str + ("** Available Processors:   " + runtime.availableProcessors() +"\n");

		return ok(str);
	}

	public static Result garbageCollector(){
		Runtime runtime = Runtime.getRuntime();
		runtime.gc();
		return ok("Garbage Collector");
	}

	@Restrict({@Group("admin")})
	public static Result deleteUser(Long id){
		AuthorisedUser admin = AuthorisedUser.getUserByEmail(session("connected"));
		if(admin.roles.contains(SecurityRole.getByName("admin"))) {
			try{
				AuthorisedUser user = AuthorisedUserLogic.getById(id);
				AuthorisedUserLogic.deleteStudentsDostyk(id);
				String name = user.profile.lastName + " "+ user.profile.firstName + " : " + user.id;
			return ok(name+"dostyk deleted !!!");
			}catch (Exception e){
				return badRequest(e.getMessage());
			}

		}else{
			return badRequest();
		}
	}


	public static Result list(int page, String sortBy, String order,
			String filter, String grade, String role) {

			grade = grade.replace(" ", "");
			AuthorisedUser moderator = AuthorisedUser.getUserByEmail(session("connected"));
			List<DostykGroup> dostykGroups = new ArrayList<DostykGroup>();
			List<DostykGroup> electiveGroups = new ArrayList<DostykGroup>();
			Page<AuthorisedUser> pages = null;
		try {


			for (DostykGroup group : moderator.dostyks.get(0).dostykGroups) {
				//if (!group.type.equals("E")) {
				dostykGroups.add(group);
			/*} else {
				electiveGroups.add(group);
			}*/
			}
			pages = AuthorisedUser.getJaiPage(moderator.dostyks.get(0).users, filter, page, sortBy, order, grade, role);
		}catch(Exception e){
			Logger.error("Exception in UserManagment.list: "+e.getMessage());
		}

		return ok(list.render(pages,
				page, sortBy, order, filter, grade, role, dostykGroups, electiveGroups));
	}

	public static Result listAll(int page, String sortBy, String order,
			String filter) {
		return ok(list2.render(AuthorisedUser.getPage(
				AuthorisedUserLogic.getAll(), filter, page, sortBy, order),
				page, sortBy, order, filter));
	}

	public static Result moderators() {
		List<AuthorisedUser> moders = AuthorisedUser.getModers();
		return ok(modersList.render(moders));
	}

	public static Result directors() {
		List<AuthorisedUser> directors = AuthorisedUser.getDirectors();
		return ok(directorsList.render(directors));
	}

	public static Result showStudentList(Long id) {
		int studentSize = 0;
		AuthorisedUser user;
		if (id == -1) {
			user = AuthorisedUser.getUserByEmail(session().get("connected"));
		} else {
			user = AuthorisedUser.getUserById(id);
		}

		DostykGroup dg = new DostykGroup();
		for (DostykGroup cur : DostykGroupLogic.getByCurator(user)) {
			if (cur.students.size() > 0) {
				dg = cur;
				studentSize = cur.students.size();
				break;
			}
		}
		return ok(studentList.render(studentSize,AuthorisedUserLogic.getByProfiles(dg.students)));
	}

	public static Result gradeReport(String a) {
		return ok("lol");
	}

	public static Result genderReport() {
		AuthorisedUser user = AuthorisedUser.getUserByEmail(session().get("connected"));
		List<Dostyk> dostyks = null;
		if (user.roles.contains(SecurityRole.getByName("admin"))) {
			dostyks = DostykLogic.getAll();
		}
		if (user.roles.contains(SecurityRole.getByName("moderator")) || user.roles.contains(SecurityRole.getByName("director"))) {
			dostyks = user.dostyks;
		}
		return ok(genderReport.render(dostyks));
	}

	public static Result ulgerimiReport() {
		AuthorisedUser user = AuthorisedUser.getUserByEmail(session().get("connected"));
		List<Dostyk> dostyks = null;
		if (user.roles.contains(SecurityRole.getByName("admin"))) {
			dostyks = DostykLogic.getAll();
		}
		if (user.roles.contains(SecurityRole.getByName("moderator")) || user.roles.contains(SecurityRole.getByName("director"))) {
			dostyks = user.dostyks;
		}


		dostyks = models.logic.user.ProfileLogic.findUlgerimCount(dostyks);
		return ok(ulgerimiReport.render(dostyks));
	}

	public static Result courseTypeReport() {
		AuthorisedUser user = AuthorisedUser.getUserByEmail(session().get("connected"));
		List<Dostyk> dostyks = null;
		if (user.roles.contains(SecurityRole.getByName("admin"))) {
			dostyks = DostykLogic.getAll();
		}
		if (user.roles.contains(SecurityRole.getByName("moderator")) || user.roles.contains(SecurityRole.getByName("director"))) {
			dostyks = user.dostyks;
		}

		dostyks = models.logic.user.ProfileLogic.findCourseTypeCount(dostyks);
		return ok(courseTypeReport.render(dostyks));
	}

	public static Result uploadExcel() {
		return ok(uploadExcel.render());
	}

	public static Result uploadStudents() {
		MultipartFormData body = request().body().asMultipartFormData();
		FilePart filePart = body.getFile("reg_file");
		Set<String> st = new HashSet<String>();
		if (filePart != null) {

			try {
				File file = filePart.getFile();
				Workbook book = Workbook.getWorkbook(file);
				Sheet sheet = book.getSheet(0);
				boolean ok = true;
				for (int i = 9; i < sheet.getRows(); i++) {
					String tegi = sheet.getCell(1, i).getContents();
					String aty = sheet.getCell(2, i).getContents();
					String dostykCode = sheet.getCell(3, i).getContents();
					String gender = sheet.getCell(7, i).getContents();
					String okuTili = sheet.getCell(9, i).getContents();
					String tuganKuni = sheet.getCell(11, i).getContents();
					String parentNumber = sheet.getCell(12, i).getContents();
					String phoneNumber = sheet.getCell(13, i).getContents();
					String courseType = sheet.getCell(14, i).getContents()
							.trim();
					Dostyk dostyk = DostykLogic.getByCode(Integer
							.parseInt(dostykCode));
					CourseType course = CourseTypeLogic.getByCode(courseType);
					DateFormat fr = new SimpleDateFormat("ddMMyyyy");
					try {
						fr.parse(tuganKuni);
					} catch (Exception e) {
						ok = false;
					}
					if (tegi.length() == 0 || aty.length() == 0
							|| dostyk == null)
						ok = false;
					if (gender.length() == 0) {
						ok = false;
					}
					if (okuTili.length() == 0) {
						ok = false;
					}
					if (parentNumber.length() != 10
							|| phoneNumber.length() != 10) {
						ok = false;
					}
					if (course == null) {
						ok = false;
					}
					if (st.contains(phoneNumber)) {
						ok = false;
					}
					st.add(phoneNumber);
					AuthorisedUser user = AuthorisedUser
							.getUserByEmail(phoneNumber + "@email");
					if (user != null) {
						ok = false;
					}
				}

				if (!ok) {
					return badRequest(messenger.render("alert alert-error",
							Messages.get("FileParseError"),
							Messages.get("correctAndReupload")));
				} else {
					for (int i = 9; i < sheet.getRows(); i++) {
						String tegi = sheet.getCell(1, i).getContents();
						String aty = sheet.getCell(2, i).getContents();
						String dostykCode = sheet.getCell(3, i).getContents();
						String gender = sheet.getCell(7, i).getContents();
						String ulgerimi = sheet.getCell(8, i).getContents();
						String okuTili = sheet.getCell(9, i).getContents();
						String tuganKuni = sheet.getCell(11, i).getContents();
						String parentNumber = sheet.getCell(12, i)
								.getContents();
						String phoneNumber = sheet.getCell(13, i).getContents();
						String courseType = sheet.getCell(14, i).getContents()
								.trim();
						String lessons = sheet.getCell(15, i).getContents();
						Dostyk dostyk = DostykLogic.getByCode(Integer
								.parseInt(dostykCode));
						CourseType course = CourseTypeLogic
								.getByCode(courseType);
						AuthorisedUser user = new AuthorisedUser();
						user.email = phoneNumber + "@email";
						user.password = phoneNumber;
						user.password = Crypto.encryptAES(user.password);
						user.roles = new ArrayList<SecurityRole>();
						user.roles.add(SecurityRole.getByName("student"));
						user.save();
						Profile profile = new Profile();
						profile.firstName = aty;
						profile.lastName = tegi;
						profile.course = course;
						profile.parentNumber = parentNumber;
						profile.phoneNumber = phoneNumber;

						ulgerimi.trim();
						if (ulgerimi.length() == 0 || ulgerimi.equals(" "))
							profile.ulgerimi = "jaksy";
						if (ulgerimi.equals("А"))
							profile.ulgerimi = "altynBelgi";
						if (ulgerimi.equals("Ү"))
							profile.ulgerimi = "kizilAttestat";
						DateFormat fr = new SimpleDateFormat("ddMMyyyy");
						profile.birthDate = fr.parse(tuganKuni);

						if (gender.equals("Қ"))
							profile.gender = "female";
						else {
							profile.gender = "male";
						}

						if (okuTili.equals("Қ"))
							okuTili = "KZ";
						else
							okuTili = "RU";
						List<Lesson> l = new ArrayList<Lesson>();
						for (int k = 0; k < lessons.length(); k++) {
							if (lessons.charAt(k) != ' ') {
								String code = lessons.charAt(k) - 'A' + 1 + "";
								if (code.length() == 1)
									code = "0" + code;

								l.add(LessonLogic.getbyCodeAndLang(code,
										okuTili));
							}
						}
						profile.lessons = l;
						profile.save();
						user.profile = profile;
						profile.save();
						user.profile = profile;

						user.save();
						dostyk.users.add(user);
						dostyk.saveManyToManyAssociations("users");
						dostyk.save();
					}
				}
				return ok(messenger.render("alert alert-success",
						Messages.get("Congrats"),
						Messages.get("fileUploadedSuccessfully")));
			} catch (Exception e) {

				return badRequest(messenger.render("alert alert-error",
						Messages.get("fileUploadError"), e.getMessage()));
			}
		} else {
			flash("error", "Missing file");
			return badRequest(messenger.render("alert alert-error",
					Messages.get("fileUploadError"),
					Messages.get("fileNotFound")));
		}
	}

	public static Result registrationReport() {
		AuthorisedUser user = AuthorisedUser.getUserByEmail(session().get(
				"connected"));
		List<Dostyk> dostyks = null;
		if (user.roles.contains(SecurityRole.getByName("admin"))) {
			dostyks = DostykLogic.getAll();
		}
		if (user.roles.contains(SecurityRole.getByName("moderator"))) {
			dostyks = user.dostyks;
		}
		return ok(registrationReport.render());
	}

	public static Result registrationReportSubmit() {
		DynamicForm requestData = form().bindFromRequest();
		String paymentStatus;
		Date startDate = null, endDate = null;
		try {
			String[] date = requestData.get("reservation").split("-");
			startDate = new SimpleDateFormat("dd/MM/yyyy").parse(date[0]);
			endDate = new SimpleDateFormat("dd/MM/yyyy").parse(date[1]);
		} catch (Exception e) {
            e.printStackTrace();
		}
		AuthorisedUser user = AuthorisedUser.getUserByEmail(session().get("connected"));
		List<Dostyk> dostyks = null;
		if (user.roles.contains(SecurityRole.getByName("admin"))) {
			dostyks = DostykLogic.getAll();
		}
		if (user.roles.contains(SecurityRole.getByName("moderator")) || user.roles.contains(SecurityRole.getByName("director"))) {
			dostyks = user.dostyks;
		}
		dostyks = models.logic.user.ProfileLogic.findRegistrationCount(dostyks, startDate, endDate);
		Collections.sort(dostyks, new Comparator<Dostyk>() {
			public int compare(Dostyk m1, Dostyk m2) {
				return m1.name.compareTo(m2.name);
			}
		});
		return ok(_dostykRegistration.render(dostyks));
	}

	public static Result movementReport() {
		AuthorisedUser user = AuthorisedUser.getUserByEmail(session().get("connected"));
		List<Dostyk> dostyks = null;
		if (user.roles.contains(SecurityRole.getByName("admin"))) {
			dostyks = DostykLogic.getAll();
		}
		if (user.roles.contains(SecurityRole.getByName("moderator")) || user.roles.contains(SecurityRole.getByName("director"))) {
			dostyks = user.dostyks;
		}
		dostyks = models.logic.user.ProfileLogic.findMovementCount(dostyks);
		// for(Dostyk dostyk: lessons){
		// for(Entry<Date, Integer> i :dostyk.movement.entrySet()){

		// }
		// }
		return ok(movementReport.render(dostyks));
		// return ok(movementReport.render());
	}

	public static Result leftReport() {
		AuthorisedUser user = AuthorisedUser.getUserByEmail(session().get("connected"));
		List<Dostyk> dostyks = null;
		if (user.roles.contains(SecurityRole.getByName("admin"))) {
			dostyks = DostykLogic.getAll();
		}
		if (user.roles.contains(SecurityRole.getByName("moderator")) || user.roles.contains(SecurityRole.getByName("director"))) {
			dostyks = user.dostyks;
		}
		dostyks = models.logic.user.ProfileLogic.findLeftCount(dostyks);
		// for(Dostyk dostyk: lessons){
		// for(Entry<Date, Integer> i :dostyk.movement.entrySet()){

		// }
		// }
		return ok(leftReport.render(dostyks));
		// return ok(movementReport.render());
	}

	public static Result movementReportPrint(Long dostykId, int monthIndex) {
		Dostyk dostyk = DostykLogic.find.byId(dostykId);
		AuthorisedUser moderator =  AuthorisedUser.find.where().eq("dostyks", dostyk)
				.eq("roles", SecurityRole.getByName("moderator"))
				.findUnique();
		dostyk.organization = (moderator.requisite!=null)?moderator.requisite.dostykOfficial:"";
		Date start = ProfileLogic.movementMonths()[monthIndex];
		Date end = ProfileLogic.movementMonths()[monthIndex + 1];

		List<Payment> payments = PaymentLogic.find
				.where()
				.in("user_id",
						ProfileLogic.find
								.where()
								.eq("user.roles",
										SecurityRole.getByName("student"))
								.eq("user.lessons", dostyk)
								.eq("user.status", "active").findIds())
				.between("endDate", start, end).eq("status", "COMPLETED")
				.findList();

		List<MovementUser> mus = new ArrayList<MovementUser>();
		for (Payment payment : payments) {
			MovementUser mu = new MovementUser();
			payment.user.profile.lastName = payment.user.profile.lastName != null ? payment.user.profile.lastName
					: "";
			payment.user.profile.firstName = payment.user.profile.firstName != null ? payment.user.profile.firstName
					: "";
			payment.user.profile.secondName = payment.user.profile.secondName != null ? payment.user.profile.secondName
					: "";
			mu.name = payment.user.profile.lastName + " "
					+ payment.user.profile.firstName + " "
					+ payment.user.profile.secondName;
			mu.startDate = payment.startDate;
			// mu.disposalDate = payment.findLast().period.payedPayment +" тг.";
			mu.courseType = payment.user.profile.course;
			mu.completeDate = payment.endDate;
			mu.dostykGroup = payment.user.profile.dostykGroup;

			mus.add(mu);
		}
		return ok(movementReportPrint.render(dostyk, mus,
				ProfileLogic.movementMonths()[monthIndex]));
	}

	/*
	public static Result revenueReport() {
		AuthorisedUser user = AuthorisedUser.getUserByEmail(session().get(
				"connected"));
		List<Dostyk> lessons = null;
		if (user.roles.contains(SecurityRole.getByName("admin"))) {
			lessons = DostykLogic.getAll();
		}
		if (user.roles.contains(SecurityRole.getByName("moderator"))) {
			lessons = user.lessons;
		}

		Date months[] = ProfileLogic.movementMonths();
		Date start = months[0];
		Date end = months[months.length - 1];
		lessons = models.logic.user.ProfileLogic.findRevenueCount(lessons);
		// for(Dostyk dostyk: lessons){
		// for(Entry<Date, Integer> i :dostyk.movement.entrySet()){

		// }
		// }
		return ok(revenueReport.render(lessons));
		// return ok(movementReport.render());
	}
	*/
	
	 public static Promise<Result> revenueReport() {
	        return promise(() -> longComputation())
	                  .map((List<Dostyk> dostyks) -> ok(revenueReport.render(dostyks)));
	 }
	 
	 public static List<Dostyk> longComputation(){
		 AuthorisedUser user = AuthorisedUser.getUserByEmail(session().get(
					"connected"));
			List<Dostyk> dostyks = null;
			if (user.roles.contains(SecurityRole.getByName("admin"))) {
				dostyks = DostykLogic.getAll();
			}
			if (user.roles.contains(SecurityRole.getByName("moderator"))) {
				dostyks = user.dostyks;
			}

			/*Date months[] = ProfileLogic.movementMonths();
			Date start = months[0];
			Date end = months[months.length - 1];*/
			dostyks = models.logic.user.ProfileLogic.findRevenueCount(dostyks);
		 return dostyks;
	 }

	public static Result revenueReportPrint(Long dostykId, int monthIndex) {
		/*Dostyk dostyk = DostykLogic.find.byId(dostykId);
		AuthorisedUser moderator =  AuthorisedUser.find.where().eq("lessons", dostyk)
				.eq("roles", SecurityRole.getByName("moderator"))
				.findUnique();
		dostyk.organization = (moderator.requisite!=null)?moderator.requisite.dostykOfficial:"";

		Date months[] = ProfileLogic.movementMonths();
		Date start = months[monthIndex];
		Date end = months[monthIndex + 1];
		DateFormat df = new SimpleDateFormat("dd/MM/yyy");

//		PaymentLogic.find
//		.where()
//		.in("user.profile.id",
//				ProfileLogic.find
//						.where()
//						.eq("user.roles", SecurityRole.getByName("student"))
//						.eq("user.lessons", dostyk)
//						.eq("user.status", "active").findIds())
//		.in("periods.id", PaymentPeriodLogic.find.where().between("payedPaymentDate", start, end)
//				.gt("payedPayment", 0)
//				.findIds())
//		.findRowCount();
		List<Payment> payments = PaymentLogic.find
				.where()
				.in("user.profile.id",
						ProfileLogic.find
								.where()
								.eq("user.roles", SecurityRole.getByName("student"))
								.eq("user.lessons", dostyk)
								.eq("user.status", "active").findIds())
				.in("periods.id", PaymentPeriodLogic.find.where().between("payedPaymentDate", start, end)
						.gt("payedPayment", 0)
						.findIds())				
				//.between("endDate", start, end)
								
				// .eq("status", "COMPLETED")
				.findList();

		List<RevenueUser> mus = new ArrayList<RevenueUser>();
		for (Payment payment : payments) {

			RevenueUser ru = new RevenueUser();
			payment.user.profile.lastName = payment.user.profile.lastName != null ? payment.user.profile.lastName
					: "";
			payment.user.profile.firstName = payment.user.profile.firstName != null ? payment.user.profile.firstName
					: "";
			payment.user.profile.secondName = payment.user.profile.secondName != null ? payment.user.profile.secondName
					: "";
            ru.id = payment.id;
			ru.firm = payment.user.getFirmName();
			ru.name = payment.user.profile.lastName + " "
					+ payment.user.profile.firstName + " "
					+ payment.user.profile.secondName;

			ru.contracts = Contract.getByPayment(payment);
			ru.endDate = df.format(payment.endDate);
			ru.comment = payment.comment;
			ru.total = payment.payedPayment+"";

			//receipt
			List<PaymentPeriod> paymentPeriods = PaymentPeriodLogic.find.where().eq("payment", payment)
			.between("payedPaymentDate", months[monthIndex], months[monthIndex + 1]).findList();
			
			int periodCount = paymentPeriods.size();
			
			double initPayment = (payment.plannedPayment - payment.materialCost)
					* 100 / (100 - payment.discountBonus);
			initPayment = initPayment * 100 / (100 - payment.discount);
			
			ru.courseIncome = initPayment - payment.materialCost;
			if(periodCount>0){
				ru.materialIncome = payment.materialCost;
				ru.discountIncome = initPayment - payment.plannedPayment + payment.materialCost;
			}
			ru.totalIncome = ru.courseIncome + payment.materialCost - ru.discountIncome;
			for(PaymentPeriod pp: paymentPeriods){
				ru.coursePlanned += pp.plannedPayment;
				ru.coursePayed +=pp.payedPayment;
			}			
			//ru.coursePayed = payment.payedPayment != 0 ? payment.payedPayment - payment.materialCost : 0;
			if(periodCount>0){
				ru.materialPayed = payment.payedPayment != 0 ? payment.materialCost : 0;
			}
			ru.totalPayed = ru.coursePayed + ru.materialPayed;

			ru.lessonChange = 0.0;
			ru.disposal = 0.0;

			ru.returned = 0.0;
			ru.addpaid = 0.0;
			ru.residue = 0.0;

			mus.add(ru);
		}
		return ok(revenueReportPrint.render(dostyk, mus, months[monthIndex]));*/
		return TODO;
	}
    @Restrict({@Group("moderator"),@Group("accountant")})
    public static Result newRevenue(){
        try {
            Dostyk dostyk = AuthorisedUsers.getConnectedUser().dostyks.get(0);
            Payment payment;
            List<PaymentPeriod> periods;
            List<NewRevenue> revenues = new ArrayList<>();
            NewRevenue revenue;
            Deal deal;
            DateFormat monthFormat = new SimpleDateFormat("MM");
            DateFormat yearFormat = new SimpleDateFormat("yyyy");
            DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
            int startYear, endYear, generalTotal = 0;
            for (AuthorisedUser student : AuthorisedUser.getByDostyk(dostyk)) {
                payment = PaymentLogic.getActualPayment(student);
                if (payment != null) {
                    periods = PaymentPeriodLogic.getPeriods(payment);
                    startYear = Integer.parseInt(yearFormat.format(payment.startDate));
                    endYear = Integer.parseInt(yearFormat.format(payment.endDate));
                    if (format.parse("01/09/" + startYear).before(payment.startDate)
                            || format.parse("01/07/" + endYear).after(payment.endDate)) {
                        revenue = new NewRevenue();
                        revenue.id = payment.id;
                        revenue.firm = payment.user.getFirmName();
                        revenue.student = (payment.user.profile.lastName != null ? payment.user.profile.lastName : "") + " "
                                + (payment.user.profile.firstName != null ? payment.user.profile.firstName : "") + " "
                                + (payment.user.profile.secondName != null ? payment.user.profile.secondName : "");
                        revenue.deals = new ArrayList<>();
                        for (Contract contract : Contract.getByPayment(payment)) {
                            deal = new Deal();
                            deal.name = contract.number;
                            deal.prefix = contract.prefix;
                            deal.beginDate = format.format(contract.beginDate);
                            deal.endDate = format.format(contract.endDate);
                            deal.total += deal.sep = (int) PaymentPeriodLogic.getPaymentByMonth(payment, "09");
                            deal.total += deal.okt = (int) PaymentPeriodLogic.getPaymentByMonth(payment, "10");
                            deal.total += deal.nov = (int) PaymentPeriodLogic.getPaymentByMonth(payment, "11");
                            deal.total += deal.dec = (int) PaymentPeriodLogic.getPaymentByMonth(payment, "12");
                            deal.total += deal.jan = (int) PaymentPeriodLogic.getPaymentByMonth(payment, "01");
                            deal.total += deal.feb = (int) PaymentPeriodLogic.getPaymentByMonth(payment, "02");
                            deal.total += deal.mar = (int) PaymentPeriodLogic.getPaymentByMonth(payment, "03");
                            deal.total += deal.apr = (int) PaymentPeriodLogic.getPaymentByMonth(payment, "04");
                            generalTotal += deal.total;
                            deal.comment = contract.comment;
                            revenue.deals.add(deal);
                        }
                        revenues.add(revenue);
                    }
                }
            }
            /*File file = new File("Temp.csv");
            PrintWriter pw = new PrintWriter(new FileWriter(file));
            pw.print("\"№\",");
            pw.print("\"Фирма\",");
            pw.print("\"Ф.И.О. ученика\",");
            pw.print("\"№ Договора\",");
            pw.print("\"Префикс Дог\",");
            pw.print("\"Дата Договора\",");
            pw.print("\"Дата окончания курса\",");
            pw.print("\"За раздаточный материал+Всего начислено за курсы (указанная сумма в договоре)\",");
            pw.print("\"Сентябрь\",");
            pw.print("\"Октябрь\",");
            pw.print("\"Ноябрь\",");
            pw.print("\"Декабрь\",");
            pw.print("\"Январь\",");
            pw.print("\"Февраль\",");
            pw.print("\"Март\",");
            pw.print("\"Апрель\",");
            pw.print("\"Примечание\",");
            pw.println();
            int counter=1;
            for(NewRevenue rev: revenues){

            }*/
            return ok(myRevenue.render(dostyk, revenues, generalTotal));
        }catch (Exception e){
            e.printStackTrace();
            return ok("exception");
        }
    }

    private static void setPeriodPayment(Contract contract, Deal deal, PaymentPeriod period, DateFormat format)
            throws Exception{
        int result;
        /*if(period.payedPaymentDate.after(contract.beginDate)
                &&period.payedPaymentDate.before(contract.endDate)) {*/
            deal.total += period.payedPayment;
            result = (int)period.payedPayment;//new DecimalFormat("##.##").format(period.payedPayment) + "";
        /*}else
            result = 0;*/
        if (format.format(period.plannedPaymentDate).equals("09"))
            deal.sep += result;
        else if (format.format(period.plannedPaymentDate).equals("10"))
            deal.okt += result;
        else if (format.format(period.plannedPaymentDate).equals("11"))
            deal.nov += result;
        else if (format.format(period.plannedPaymentDate).equals("12"))
            deal.dec += result;
        else if (format.format(period.plannedPaymentDate).equals("01"))
            deal.jan += result;
        else if (format.format(period.plannedPaymentDate).equals("02"))
            deal.feb += result;
        else if (format.format(period.plannedPaymentDate).equals("03"))
            deal.mar += result;
        else if (format.format(period.plannedPaymentDate).equals("04"))
            deal.apr += result;
    }

	public static Result jurnalReport() {
		return ok();
	}
	
	public static Result jurnalReportPrint(Long dostykId, String rangeDate) {
		Dostyk dostyk = DostykLogic.find.byId(dostykId);
		AuthorisedUser moderator =  AuthorisedUser.find.where().eq("dostyks", dostyk)
				.eq("roles", SecurityRole.getByName("moderator"))
				.findUnique();
		dostyk.organization = (moderator.requisite!=null)?moderator.requisite.dostykOfficial:"";
		
		Date startDate = null, endDate = null;
		try {
			String[] date = rangeDate.split("-");
			startDate = new SimpleDateFormat("dd/MM/yyyy").parse(date[0]);
			endDate = new SimpleDateFormat("dd/MM/yyyy").parse(date[1]);
		} catch (Exception e) {

		}
		
		List<Profile> profiles = ProfileLogic.find.where()
		.eq("user.roles", SecurityRole.getByName("student"))
		.eq("user.lessons", dostyk)
		.between("user.createdDate", startDate, endDate).findList();
		
		List<JurnalUser> jus = new ArrayList<JurnalUser>();
		Contract contract;
		for (Profile profile : profiles) {
			JurnalUser ju = new JurnalUser();
			profile.lastName = profile.lastName != null ? profile.lastName : "";
			profile.firstName = profile.firstName != null ? profile.firstName : "";
			profile.secondName = profile.secondName != null ? profile.secondName : "";
			contract = new Contract();
			contract.number="";
			if(Contract.getByStudent(profile.user).size()>0)
				contract = Contract.getByStudent(profile.user).get(0);
			ju.dogovorNo = profile.user.payments.size()>0&&profile.user.payments!= null ? contract.number : "";
			
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			ju.dogovorDate= profile.user.payments.size()>0&&profile.user.payments!= null&&contract.beginDate !=null ? df.format(contract.beginDate) : "";
			ju.fioStudent = profile.lastName + " "
					+ profile.firstName + " "
					+ profile.secondName;
			
			Parent parent = Parent.find.where().eq("child", profile.user).eq("responsible", true).findUnique();
			parent.lastName = parent.lastName != null ? parent.lastName : "";
			parent.firstName = parent.firstName != null ? parent.firstName : "";
			ju.parentStudent=parent.lastName + " " + parent.firstName;
			
			NumberFormat formatter = new DecimalFormat("#0");
			ju.totalPayment = profile.user.payments.size()>0&&profile.user.payments!= null ? formatter.format(profile.user.payments.get(0).plannedPayment) : "";
			ju.grade = profile.course.classNumber+"";
			ju.dogovorEndDate = profile.user.payments.size()>0&&profile.user.payments!= null ? new SimpleDateFormat("dd/MM/yyyy").format(profile.user.payments.get(0).endDate): "";
//			mu.disposalDate = payment.findLast().period.payedPayment +" тг.";
//			mu.courseType = payment.user.profile.course;
//			mu.completeDate = payment.endDate;
//			mu.dostykGroup = payment.user.profile.dostykGroup;
			jus.add(ju);
		}
		return ok(jurnalReportPrint.render(dostyk, jus,startDate, endDate));
	}

	@Restrict({@Group("moderator"),@Group("accountant"),@Group("director")})
    public static Result newRegistration(){
        try {
            Dostyk dostyk = AuthorisedUsers.getConnectedUser().dostyks.get(0);
            DateFormat dateFormat = new SimpleDateFormat("dd");
            DateFormat monthFormat = new SimpleDateFormat("MM");
            DateFormat yearFormat = new SimpleDateFormat("yyyy");
            DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
            Payment payment;
            List<PaymentPeriod> periods;
            List<StudentInfo> infos = new ArrayList<>();
            List<Contract> contracts;
            StudentInfo info;
            for (AuthorisedUser student : AuthorisedUserLogic.getStudents(dostyk)) {
                info = new StudentInfo();
                info.id = student.id;
                contracts = Contract.getByStudent(student);
                Contract last = null;

                if (contracts.size() > 0) {
                    last = contracts.get(0);
                }
                payment = PaymentLogic.getLastPayment(student);
                info.dealNumber = last != null ? last.number != null ? last.number : "" : "";
                info.beginDate = last != null ? last.beginDate != null ? format.format(last.beginDate) : "" : "";
                info.endDate = last != null ? last.endDate != null ? format.format(last.endDate) : "" : "";
                info.gender = student.profile.gender;
                info.firstName = student.profile.firstName;
                info.lastName = student.profile.lastName;
                info.school = "";
                if(student.profile.school!=null)
                	if(student.profile.school.name_kz!=null)
                		info.school = student.profile.school.name_kz;
                info.birthDate = student.profile.birthDate != null ? dateFormat.format(student.profile.birthDate) : "";
                info.birthMonth = student.profile.birthDate != null ? monthFormat.format(student.profile.birthDate) : "";
                info.birthYear = student.profile.birthDate != null ? yearFormat.format(student.profile.birthDate) : "";
                info.phoneMobile = student.profile.phoneNumber;
                for (Parent parent : student.parents) {
                    /*if (parent.parentType.equalsIgnoreCase("father")) {
                        info.phoneFather = parent.getPhoneNumber1();
                        if(info.phoneFather == null){
                        	info.phoneFather = parent.getPhoneNumber2();
						}
                        info.fatherName = parent.lastName + " " + parent.firstName;
                        if(info.fatherName == null) info.fatherName = "";
                    } else if (parent.parentType.equalsIgnoreCase("mother")) {
						info.motherName = parent.getPhoneNumber1();
						if(info.motherName == null){
							info.motherName = parent.getPhoneNumber2();
						}
                        info.motherName = parent.lastName + " " + parent.firstName;
						if(info.motherName == null) info.motherName = "";
                    }
                    */
					if (parent.parentType.equalsIgnoreCase("guardian")) {
						info.phoneCustomer = parent.getPhoneNumber1();
						if(info.phoneCustomer == null || info.phoneCustomer.equals("")){
							info.phoneCustomer = parent.getPhoneNumber2();
						}else{
							info.phoneCustomer = info.phoneCustomer+" , "+parent.getPhoneNumber2();
						}
						info.customerName = parent.getLastName() + " " + parent.getFirstName();
						if(info.customerName == null) info.customerName = "";
						info.customerWork = parent.getWork() + "";
					}
				}
                info.grade="";
                if(student.profile.course!=null)
                	info.grade = student.profile.course.classNumber + "";
                info.elective = "";
                int kzs = 0, rus = 0;
                info.russianLanguage = "";
                info.kazakhLanguage = "";
                info.math = "";
                info.history = "";
                if(student.profile.lessons!=null){
                for (Lesson lesson : student.profile.lessons) {
                    if (lesson.language.equalsIgnoreCase("ru"))
                        rus++;
                    else
                        kzs++;
                    if (lesson.code.equals("01"))
                        info.kazakhLanguage = "+";
                    else if (lesson.code.equals("02"))
                        info.russianLanguage = "+";
                    else if (lesson.code.equals("03"))
                        info.history = "+";
                    else if (lesson.code.equals("04"))
                        info.math = "+";
                    else
                        info.elective = lesson.name;
                }
            	}
                if (kzs >= rus)
                    info.language = "каз";
                else
                    info.language = "рус";
                info.comment = "";
                if (student.profile.dostykGroup != null)
                    info.group = student.profile.dostykGroup.name;
                else
                    info.group = "В";
                info.refund = last != null ? last.refund : 0;
                info.sep = payment != null ? PaymentPeriodLogic.getPaymentByMonth(payment, "09") : 0;
                info.okt = payment != null ? PaymentPeriodLogic.getPaymentByMonth(payment, "10") : 0;
                info.nov = payment != null ? PaymentPeriodLogic.getPaymentByMonth(payment, "11") : 0;
                info.dec = payment != null ? PaymentPeriodLogic.getPaymentByMonth(payment, "12") : 0;
                info.jan = payment != null ? PaymentPeriodLogic.getPaymentByMonth(payment, "01") : 0;
                info.feb = payment != null ? PaymentPeriodLogic.getPaymentByMonth(payment, "02") : 0;
                info.mar = payment != null ? PaymentPeriodLogic.getPaymentByMonth(payment, "03") : 0;
                info.apr = payment != null ? PaymentPeriodLogic.getPaymentByMonth(payment, "04") : 0;
                info.may = payment != null ? PaymentPeriodLogic.getPaymentByMonth(payment, "05") : 0;
                info.monthlyPayment = payment != null ? PaymentPeriodLogic.getAveragePayment(payment) : 0;
                info.totalPlanned = payment != null ? payment.plannedPayment : 0;
                info.totalPayed = payment != null ? payment.payedPayment : 0;
                info.totalDebt = payment != null ? payment.debtPayment : 0;
                info.region = student.profile.region.name_kz;
                info.mail = student.email;
                info.das = 0;
                info.discount = payment != null ? payment.discount + "" : "0";
                infos.add(info);
            }
            String f = myRegistration.render(infos).toString();
            /*File file = new File("Temp.xls");
            PrintWriter pw = new PrintWriter(new FileWriter(file));
            pw.print(f);
            pw.flush();
            pw.close();*/
            //************
			String directory = play.Play.application().path().getAbsolutePath() + ""+File.separator+"temp"+File.separator;
			File layoutPackage = new File(directory);
			if(!layoutPackage.exists()){
				layoutPackage.mkdirs();
			}
            File fileDir = new File(directory + "Temp.xls");
			Writer out = new BufferedWriter(new OutputStreamWriter(
			new FileOutputStream(fileDir), "UTF8"));
			out.append(f).append("\r\n");

			out.flush();
			out.close();

            return ok(fileDir);
            //return ok(f);
        }catch (Exception e){

			return ok("exception: "+ e.getMessage());
        }
    }

	@Restrict({@Group("moderator"),@Group("director"),@Group("admin")})
    public static Result getStudentBySchool(){
		return ok(studentSchoolReport.render(RegionLogic.getAll()));
	}

	public static Result getStudentList(String reg, String dis, String  school) {
		List<AuthorisedUser> studList = AuthorisedUser.getBySchool(reg, dis, school);
		return ok(studentsList.render(studList));
	}


    private static String mf(double input){
		return new DecimalFormat("#.00").format(input);
	}

	private static String mf2(double input){
        return new DecimalFormat("#.00").format(input);
    }

    @Restrict({@Group("moderator"),@Group("accountant")})
    public static Result cashBook(){
		Dostyk dostyk = AuthorisedUsers.getConnectedUser().dostyks.get(0);
		List<Firm> firm = dostyk.firms;
        return ok(cashBookChooser.render(firm));
    }



    @Restrict({@Group("moderator"),@Group("accountant")})
    public static Result getCashBook(){
        try {
			Logger.info("UserManagment.getCashBook");
            Dostyk dostyk = AuthorisedUsers.getConnectedUser().dostyks.get(0);
            DynamicForm form = Form.form().bindFromRequest();
            String beginDate = form.get("dateStart");
            String endDate = form.get("dateEnd");
            String firmStr = form.get("firm");
            Long firmId = 0l;
            Firm firm = new Firm();
            if(!firmStr.equals("all")){
            	firmId = Long.parseLong(firmStr);
            	firm=Firm.find.byId(firmId);
			}

            DateFormat ymd = new SimpleDateFormat("dd.MM.yyyy");
            DateFormat ymdhm = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            DateFormat yy = new SimpleDateFormat("yyyy");
            DateFormat mm = new SimpleDateFormat("MM");
            DateFormat dd = new SimpleDateFormat("dd");
            Date begin = ymdhm.parse(beginDate+" 00:00");
            Date end = ymdhm.parse(endDate+" 00:00");
            Calendar c = Calendar.getInstance();
            c.setTime(begin);
            c.add(Calendar.DATE, 1);
            //Date end = c.getTime();

			/*
            String[] date = form.get("reservation").split("-");
			Date startDate = new SimpleDateFormat("dd/MM/yyyy").parse(date[0]);
			Date endDate = new SimpleDateFormat("dd/MM/yyyy").parse(date[1]);

			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

			*/




            List<PaymentReceipt> receipts = PaymentReceiptLogic.getInRange(dostyk, begin, end, firm, firmStr);
            CashBookPage page = new CashBookPage();
            page.books = new ArrayList<>();
            page.date = ymd.format(begin);
            page.endDate = ymd.format(end);
            String year = yy.format(begin);
            Date extra;
            if(begin.after(ymd.parse("01.09."+year))) {
                extra = ymd.parse("01.09." + year);
            }else{
                extra = ymd.parse("01.09."+(Integer.parseInt(year)-1));
            }
            //List<PaymentReceipt> forOstatok = PaymentReceiptLogic.getInRange(dostyk, extra, begin);
            List<PaymentReceipt> forOstatok = PaymentReceiptLogic.getInRange(dostyk, begin, end, firm, firmStr);
            double ostatok=0;
            for(PaymentReceipt receipt: forOstatok)
                if(receipt.period!=null)
                    ostatok += receipt.period.payedPayment;
            page.ostatokBegin = mf2(ostatok);
            double tot=0;
            CashBook book;
            for(PaymentReceipt receipt: receipts){
                if(receipt.period!=null) {
                	//if()
                	book = new CashBook();
                    book.number = receipt.number + "";
                    for (int i = 0; i < (11 - book.number.length()); i++)
                        book.number = "0" + book.number;
                    book.owner = receipt.payment.user.profile.firstName + " " + receipt.payment.user.profile.lastName;
                    book.firm = receipt.payment.user.firm.getName();
					List<Contract> contract = Contract.getByPayment(receipt.payment);
                    book.dogovorNo = contract.get(0).number;
					DateFormat outputFormatter = new SimpleDateFormat("MM/dd/yyyy");
					String outputDate = outputFormatter.format(contract.get(0).beginDate); // Output : 01/20/2012
					book.dogovorDate = outputDate;
                    book.income = "";
                    book.outcome = "";
                    if (receipt.period.payedPayment > 0)
                        book.income = mf2(receipt.period.payedPayment);
                    else
                        book.outcome = mf2(receipt.period.payedPayment);
                    book.score = "";
                    tot += receipt.period.payedPayment;
                    book.date = ymd.format(receipt.createdDate);
                    page.books.add(book);
                }
            }
            page.ostatokEnd = mf2(ostatok+tot);
            page.total = mf2(tot);



            String f = cashBook.render(page).toString();
			String directory = play.Play.application().path().getAbsolutePath() + ""+File.separator+"temp"+File.separator;
			File layoutPackage = new File(directory);
			if(!layoutPackage.exists()){
				layoutPackage.mkdirs();
			}

			//File file = new File(directory + "Temp.xls");
            //PrintWriter pw = new PrintWriter(new FileWriter(file));
            //pw.print(f);
            //pw.flush();
            //pw.close();

			File fileDir = new File(directory + "Temp.xls");
			Writer out = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(fileDir), "UTF8"));
			out.append(f).append("\r\n");
			out.flush();
			out.close();

			return ok(fileDir);

            /*if(!play.api.Play.current().mode().toString().equals("Dev")) {
                return ok(fileDir);
            }else {
                return ok(cashBook.render(page));
            }*/
        }catch (Exception e){
			Logger.error("UserManagment.getCashBook: "+e.getMessage());
            e.printStackTrace();
            return ok("exception");
        }
    }
}