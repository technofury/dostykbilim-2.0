package controllers;

import be.objectify.deadbolt.java.actions.Group;
import be.objectify.deadbolt.java.actions.Restrict;
import be.objectify.deadbolt.java.actions.SubjectPresent;
import models.entities.social.Dostyk;
import models.entities.user.AuthorisedUser;
import models.logic.social.DostykLogic;
import models.timetable.LessonPeriod;
import models.timetable.Settings;
import models.timetable.Timetable;
import play.data.DynamicForm;
import play.data.Form;
import play.db.ebean.Transactional;
import play.mvc.Controller;
import play.mvc.Result;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@SubjectPresent
public class CTimetableSettings extends Controller {

    public static Form formFactory;
	
	//@Restrict({@Group("sudo"), @Group("admin"), @Group("principal"), @Group("vice principal"), @Group("register")})
	public static Result edit(String settingsId) {
		Settings settings;
		List<LessonPeriod> periods;
		List<Settings> all = Settings.all();
		if (settingsId.equals("new")) {
			settings = new Settings();
			periods = new ArrayList<>();
		} else {
			settings = Settings.byId(Long.parseLong(settingsId));
			periods = LessonPeriod.bySettings(settings);
		}
		return ok(views.html.timetable.editSettings.render(settings, periods, all, "edit"));
	}
	
	//@Transactional
	//@Restrict({@Group("sudo"), @Group("admin"), @Group("principal"), @Group("vice principal"), @Group("register")})
	public static Result save(String settingsId) {
		AuthorisedUser user = AuthorisedUser.getUserByEmail(session("connected"));
		Dostyk dostyk = DostykLogic.getByUser(user);

		DynamicForm form = formFactory.form().bindFromRequest();
		Settings result = getSettingsFrom(form, settingsId);
		result.organization = dostyk;
		List<LessonPeriod> periods = getPeriodsFrom(form, result);
		String status = "saved";
		if (result.name==null || periods.size() == 0) {
			status = "unsaved";
		} else {
			Settings activeSettings = Settings.findActive();
			if (activeSettings == null) result.isActive = true;
			if (settingsId.equals("new")) result.save();
			else result.update();
			for (LessonPeriod period : periods) {
				period.settings = result;
				period.organization = dostyk;
				if (period.id == null) period.save();
				else period.update();
			}
		}
		List<Settings> all = Settings.all(); 
		return ok(views.html.timetable.editSettings.render(result, periods, all, status));
	}
	
	//@Transactional
	//@Restrict({@Group("sudo"), @Group("admin"), @Group("principal"), @Group("vice principal"), @Group("register")})
	public static Result active(Long settingsId) {
		Settings activeSettings = Settings.findActive();
		Settings settings = Settings.byId(settingsId);
		if (!settings.isActive) {
			List<Timetable> list = Timetable.bySettings(activeSettings);
			settings.isActive = true;
			settings.update();			
			activeSettings.isActive = false;
			activeSettings.update();
			for (Timetable tt : list) {
				if (tt.isActive) {
					tt.isActive = false;
					tt.update();
				}
			}
		} 
		List<Settings> all = Settings.all(); 
		return ok(views.html.timetable.editSettings.render(settings, LessonPeriod.bySettings(settings), all, "activated"));
	}
	
	//@Transactional
	//@Restrict({@Group("sudo"), @Group("admin"), @Group("principal"), @Group("vice principal"), @Group("register")})
	public static Result delete(Long settingsId) {
		String status = "undeleted";
		Settings settings = Settings.byId(settingsId);
		if (!settings.isActive && Timetable.bySettings(settings).size() == 0) {
			status = "deleted";
			List<LessonPeriod> periods = LessonPeriod.bySettings(settings);
			for (LessonPeriod period : periods) {
				period.delete();
			}
			settings.delete();
			settings = Settings.findActive();
		}
		List<Settings> all = Settings.all();
		List<LessonPeriod> periods = LessonPeriod.bySettings(settings);
		if (settings == null) {
			settings = new Settings();
			periods = new ArrayList<>();
		}
		return ok(views.html.timetable.editSettings.render(settings, periods, all, status));
	}
	
	private static Settings getSettingsFrom(DynamicForm form, String settingsId) {
		Settings result;
		if (settingsId.equals("new")) result = new Settings();
		else result = Settings.byId(Long.parseLong(settingsId));
		if (form.get("lesson-days") != null) {
            if (form.get("lesson-days").matches("[0-9]+"))
                result.lessonDays = Integer.parseInt(form.get("lesson-days"));
        }
		if (form.get("max-lessons") != null) {
            if (form.get("max-lessons").matches("[0-9]+"))
                result.maxLessons = Integer.parseInt(form.get("max-lessons"));
        }
		result.name = form.get("name");
		return result;
	}
	
	private static List<LessonPeriod> getPeriodsFrom(DynamicForm form, Settings result) {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		List<LessonPeriod> periods = new ArrayList<>();
		for (int i = 1; i <= result.maxLessons; ++i) {
			LessonPeriod period = LessonPeriod.byNumber(i, result);
			if (period == null) {
				period = new LessonPeriod();
				period.number = i;				
			}
			try {
				period.startTime = df.parse("2017-01-01 " + form.get("start-time_" + i));
				period.endTime = df.parse("2017-01-01 " + form.get("end-time_" + i));
			} catch (ParseException e) {			
				return new ArrayList<>();
			}
			periods.add(period);
		}
		return periods;
	}
}
