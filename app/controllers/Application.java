package controllers;

import static play.data.Form.form;
import static play.libs.F.Promise.promise;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import controllers.education.*;
import controllers.education.routes;
import flexjson.JSONException;
import models.entities.examination.LessonAnswer;
import models.entities.social.Dostyk;
import models.entities.user.AuthorisedUser;
import models.logic.examination.LessonAnswerLogic;
import models.logic.user.EmailLogic;
import play.Play;
import play.Routes;
import play.data.DynamicForm;
import play.data.Form;
import play.i18n.Messages;
import play.libs.F.Promise;
import play.libs.Json;
import play.libs.ws.WS;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.user.signin;
import javax.imageio.ImageIO;

public class Application extends Controller {


	public static Result index() {
		Controller.changeLang("kk");
		session("lang", "kk");
		if (session("connected") == null) {
			Form<AuthorisedUser> signinForm = form(AuthorisedUser.class);
			return ok(signin.render(signinForm));

		} else
			return redirect(controllers.user.routes.AuthorisedUsers
					.editProfile(-1));
	}

	public static Result getStudId() throws JSONException {
		ObjectNode objectNode = Json.newObject();
		JsonNode json = request().body().asJson();
		if(json == null){
			return badRequest("Expecting Json data");
		}else{
			String token = json.findPath("token").textValue();
			String email = json.findPath("email").textValue();
			if(token.equals("Asc545LQw542xer")){
				if(AuthorisedUser.find.where().eq("email",email).findRowCount() > 0){
					AuthorisedUser user = AuthorisedUser.find.where().eq("email",email).findUnique();
					Dostyk dostyk = user.dostyks.get(0);
					List<AuthorisedUser> studList = AuthorisedUser.getByDostyk(dostyk);

					JsonArray jsonArray = new JsonArray();
					for(AuthorisedUser a:studList){

						if(a.getRoles().get(0).getName().equals("student")) {
							JsonObject jsonObject = new JsonObject();
							jsonObject.addProperty("id", a.id);
							jsonObject.addProperty("name", a.profile.firstName);
							jsonObject.addProperty("surname", a.profile.lastName);
							jsonObject.addProperty("studId", a.profile.idNumber);
							jsonArray.add(jsonObject);
						}

					}

					objectNode.put("students",jsonArray.toString());
					return ok(objectNode);

				}else{
					return badRequest("User not found");
				}

			}else{
				return badRequest("Token is incorrect");
			}

		}

	}

	public static Result changeLanguage(String s) {
		Controller.changeLang(s);
		session("lang", s);
		if (request().getHeader("referer") == null)
			return index();
		return redirect(request().getHeader("referer"));
	}
	public static String getActiveLanguage(){
		return session().get("lang");
	}


	public static String getCurrentSchoolyear(){
		try {
			String result;
			DateFormat df1 = new SimpleDateFormat("dd/MM/yyyy");
			DateFormat df3 = new SimpleDateFormat("yyyy");
			Date now = new Date();
			int currentYear = Integer.parseInt(df3.format(now));
			String firstSeptember = "01/09";
			Date currentScienceDate = df1.parse(firstSeptember+"/"+currentYear);
			if(now.before(currentScienceDate))
				result = (currentYear-1)+"-"+currentYear;
			else
				result = currentYear+"-"+(currentYear+1);
			return result;
		}catch (Exception e){
			e.printStackTrace();
			return "yyyy-yyyy";
		}
	}

	public static String getRootPath() {
		return Play.application().path().getPath();
	}

	public static Result getFile(String file) {
		try {
			System.setProperty("file.encoding", "UTF-8");
			LessonAnswer a = LessonAnswerLogic.getById(Long.parseLong(file));
			String url = a.pathToVideo
					.substring(0, a.pathToVideo.indexOf("##"));
			File myfile = new File(System.getenv("PWD") + "/FILES/" + url);
			String name = a.pathToVideo
					.substring(a.pathToVideo.indexOf("##") + 2);
			response().setHeader("Content-Disposition",
					"inline; filename=\"" + name + "\"");
			response().setContentType("video/mp4");
			response().setHeader("Cache-Control", "public");
			response().setHeader(
					"Content-Range",
					"bytes " + 0 + "-" + myfile.length() + "/"
							+ myfile.length());
			return ok(myfile);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return TODO;
	}

	public static Result getImage(String fileName) throws IOException {

		BufferedImage image = ImageIO.read(new File(play.Play.application().path().getAbsolutePath() + "\\orderimage\\"+ fileName));
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(image, "jpg", baos);
		String file = play.Play.application().path().getAbsolutePath() + "\\orderimage\\"+ fileName;


		return ok(baos.toByteArray()).as("image/jpeg");
		//return ok(fileName);
		//return ok(file);

	}

	public static Promise<Result> support() {

		DynamicForm data = form().bindFromRequest();

		flash("email", Messages.get("support.send"));
		// return redirect(request().getHeader("referer"));

		return promise(
				() -> EmailLogic.supportMail(AuthorisedUser
						.getUserByEmail(session().get("connected")), data
						.get("moderEmail"), data.get("title"), data.get("body")))
				.map((Integer i) -> redirect(request().getHeader("referer")));
	}
	public static Result isAlive(){
		response().setHeader("Access-Control-Allow-Origin","*");
		return ok("alive");
	}

	public static Result javascriptRoutes() {
		response().setContentType("text/javascript");

		return ok(Routes
				.javascriptRouter(
						"myJsRoutes",
						controllers.ten.routes.javascript.UserTen.printXLS(),
						controllers.ten.routes.javascript.UserTen.getSessions(),
						controllers.user.routes.javascript.UserManagement
								.movementReportPrint(),
						controllers.user.routes.javascript.UserManagement
								.revenueReportPrint(),
						controllers.user.routes.javascript.UserManagement
								.jurnalReportPrint(),
						controllers.examination.routes.javascript.AnswerKeys
								.options(),
						controllers.education.routes.javascript.TopicRegisters
								.save(),
						controllers.education.routes.javascript.ExtraJournals
								.save(),
						controllers.education.routes.javascript.ExtraJournals
								.getTeachers(),
						controllers.education.routes.javascript.ExtraJournals
								.teachersByLesson(),
						controllers.education.routes.javascript.ExtraJournals
								.teachersByDostyk(),
						controllers.education.routes.javascript.ExtraJournals
								.extraLessonsByTeacher(),
						// controllers.education.routes.javascript.ExtraJournals.getRegisters(),
						controllers.education.routes.javascript.Topics.getBy(),
						controllers.education.routes.javascript.Topics
								.saveUploadedFile(),
						controllers.examination.routes.javascript.ATTUTTs
								.getUTT(),
						controllers.examination.routes.javascript.ATTUTTs
								.getATT(),
						controllers.examination.routes.javascript.ATTUTTs
								.save(),
						controllers.examination.routes.javascript.ATTUTTs
								.reportByLesson(),
						controllers.education.routes.javascript.Registers
								.save2(),
						controllers.education.routes.javascript.Registers
								.getGroups(),
						controllers.education.routes.javascript.Registers
								.getByLessonAndGroup(),
						controllers.education.routes.javascript.ExtraJournals
								.getByLessonAndGroup(),
						controllers.education.routes.javascript.ExtraJournals
								.saveMarks(),
						controllers.routes.javascript.CTimetable.edit(),
						controllers.examination.routes.javascript.LessonAnswers
								.getVideo(),
						controllers.examination.routes.javascript.LessonAnswers
								.saveUploadedFile(),
						controllers.social.routes.javascript.SMS.saveSMS(),
						controllers.education.routes.javascript.Topics
								.getReport(),
						controllers.social.routes.javascript.Dostyks
								.getGroups(),
						controllers.education.routes.javascript.Lessons
								.deleteLesson(),
						controllers.social.routes.javascript.Regions
								.getDistricts(),
						controllers.social.routes.javascript.Districts.getSchools(),
						controllers.user.routes.javascript.UserManagement.getStudentList(),
						controllers.education.routes.javascript.CourseTypes.getByName(),
						controllers.accounting.routes.javascript.DiscountMaxes
								.deleteDiscountMax(),
						controllers.accounting.routes.javascript.Payments
								.dostykReport(),
						controllers.accounting.routes.javascript.Payments
								.groupReport(),
						controllers.accounting.routes.javascript.Payments
								.studentReport(),
						controllers.education.routes.javascript.CourseTypes
								.deleteCourseType(),
						controllers.social.routes.javascript.Dostyks
								.deleteDostyk(),
						controllers.social.routes.javascript.DostykGroups
								.submit(),
						controllers.social.routes.javascript.DostykGroups
								.edit(),
						controllers.social.routes.javascript.DostykGroups
								.changeStatus(),
						controllers.social.routes.javascript.DostykGroups
								.delete(),
						controllers.education.routes.javascript.LessonHourss
								.save(),
						controllers.accounting.routes.javascript.PriceLists
								.save(),
						controllers.accounting.routes.javascript.PriceLists
								.update(),
						controllers.accounting.routes.javascript.PriceLists
								.delete(),
						controllers.accounting.routes.javascript.BillForms
								.saveBillForm(),
						controllers.user.routes.javascript.AuthorisedUsers
								.changeStatus(),
						controllers.user.routes.javascript.AuthorisedUsers
								.deleteFromDostyk(),
						controllers.user.routes.javascript.AuthorisedUsers
								.changeDostykGroup(),
						controllers.user.routes.javascript.AuthorisedUsers
								.changeElectiveGroup(),
						controllers.user.routes.javascript.UserManagement
								.uploadStudents(),
						controllers.social.routes.javascript.Contents.save(),
						controllers.social.routes.javascript.Contents.edit(),
						controllers.social.routes.javascript.Contents.delete(),
						controllers.examination.routes.javascript.Exams
								.changeStatus(),
						controllers.examination.routes.javascript.Exams.changeName(),
						controllers.examination.routes.javascript.Exams
								.changeCode(),
						controllers.examination.routes.javascript.Answers
								.delete(),
						controllers.examination.routes.javascript.Exams
								.submitResult(),
						controllers.examination.routes.javascript.Exams
								.submitResultD5(),
						controllers.examination.routes.javascript.ExamResults
								.examReportByDostyk(),
						controllers.user.routes.javascript.UserManagement
								.registrationReportSubmit(),
						controllers.education.routes.javascript.Topics.save(),
						controllers.education.routes.javascript.Topics.delete(),
						controllers.education.routes.javascript.Topics.getTopic(),
						controllers.accounting.routes.javascript.Holidays.deleteHoliday(),
						controllers.routes.javascript.CTimetable.delete(),
						controllers.routes.javascript.CTimetable.active(),
						controllers.examination.routes.javascript.ExamResults
								.examReportByDostykGroup(),
						controllers.education.routes.javascript.Attendances.update()));
	}

}