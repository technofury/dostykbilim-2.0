package controllers.utils;

import play.mvc.Controller;
import play.mvc.Result;
import play.twirl.api.Html;
import flexjson.JSONSerializer;

public class Response extends Controller {
	public static Result sendPayments(Object obj) {
		JSONSerializer json = new JSONSerializer().prettyPrint(true)
				.exclude("*.user").exclude("*.class").exclude("*.receipt")
				.exclude("*.comment").exclude("*.status").exclude("discount")
				.exclude("*.id").exclude("discountBonus")
				.exclude("hoursPerWeek").exclude("materialCost")
				.exclude("pricePerWeek")

				.exclude("weeks").include("periods");
		return ok(json.serialize(obj));
	}

	public static Result sendCategories(Object obj) {
		JSONSerializer json = new JSONSerializer().prettyPrint(true)
				// .exclude("*.class")
				.exclude("*.organization").exclude("*.canteen").exclude("icon")
				.exclude("class").exclude("product.category.icon");
		return ok(json.serialize(obj));
	}

	public static Result sendInfos(Object obj) {
		JSONSerializer json = new JSONSerializer()
		.prettyPrint(true)
		// .exclude("*.class")
		.exclude("*.organization").exclude("*.canteen")
		.exclude("*.class").exclude("product.category.icon")
		.exclude("product.barcode").exclude("product.category.class")
		.exclude("product.category.icon")
		.exclude("product.category.name").exclude("product.icon.class")
		.exclude("product.icon.contentType");
		return ok(json.serialize(obj));
	}

	public static Result json(Object obj) {
		JSONSerializer json = new JSONSerializer().prettyPrint(true);
		return ok(json.serialize(obj));
	}

	public static Result htmlJson(Html html, Object data) {
		if (request().accepts("text/html"))
			return ok(html);
		else
			return json(data);
	}
}