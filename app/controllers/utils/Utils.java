package controllers.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import play.mvc.Controller;

public class Utils extends Controller {

	public static int getWeekDay(String str) throws Exception {
		Date date = new SimpleDateFormat("yyyy-MM-dd").parse(str);
		DateFormat format = new SimpleDateFormat("EE dd MM yyyy", Locale.US);

		str = format.format(date);
		String[] splitted = str.split(" ");
		String[] weekDays = { "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun" };
		int day = -1;
		for (int i = 0; i < weekDays.length; i++)
			if (splitted[0].equals(weekDays[i])) {
				day = i + 1;
				break;
			}
		return day;
	}
	public static boolean checkWhetherId(String login) {
		String regex = "[0-9]+";
		if (login.length() != 8)
			return false;
		else if (!login.matches(regex))
			return false;
		else
			return true;
	}

	public static boolean checkWhetherEmail(String login) {
		return login.contains("@");
	}
	/*private static final char[] durus = new char[]{'q','w','e','r','t','y','u','i','o','p','a','s','d','f','g','h','j',
			'k','l','z','x','c','v','b','n','m'};
	private static final char[] burus = new char[]{'w','e','r','t','y','u','i','o','p','a','s','d','f','g','h','j','k',
			'l','z','x','c','v','b','n','m','q'};
	public static String encrypt(String message){
		String encrypted="";
		for(int i=0;i<message.length();i++)
			for(int j=0;j<durus.length;j++)
				if(message.charAt(i)==durus[j]){
					encrypted+=burus[j];
					break;
				}
		return encrypted;
	}
	public static String decrypt(String encrypted){
		String message="";
		for(int i=0;i<encrypted.length();i++)
			for(int j=0;j<burus.length;j++)
				if(message.charAt(i)==burus[j]){
					encrypted+=durus[j];
					break;
				}
		return message;
	}*/
}