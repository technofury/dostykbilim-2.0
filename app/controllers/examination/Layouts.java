package controllers.examination;

import static play.data.Form.form;
import models.entities.examination.Layout;
import models.logic.examination.LayoutLogic;
import play.data.DynamicForm;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.examination.layoutList;

public class Layouts extends Controller {
	public static Result manage() {
		return ok(layoutList.render(LayoutLogic.getAll()));
	}

	public static Result save() {
		DynamicForm form = form().bindFromRequest();
		long id = Long.parseLong(form.field("id").value());
		String name = form.field("name").value();

		Layout layout;
		if (id == -1) {
			layout = new Layout();
		} else {
			layout = LayoutLogic.getById(id);
		}

		layout.name = name;
		layout.save();
		return redirect(request().getHeader("referer"));
	}

	public static Result delete(long id) {
		LayoutLogic.deleteById(id);
		return redirect(request().getHeader("referer"));
	}
}