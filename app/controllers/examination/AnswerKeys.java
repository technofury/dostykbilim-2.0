package controllers.examination;

import static play.data.Form.form;
import models.entities.examination.AnswerKey;
import models.entities.examination.Exam;
import models.logic.examination.AnswerKeyLogic;
import models.logic.examination.ExamLogic;
import play.data.DynamicForm;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.examination.answerKeysList;
import views.html.helps.answerKeys;

public class AnswerKeys extends Controller {
	public static Result manage() {
		return ok(answerKeysList.render(AnswerKeyLogic.getAll(),
				ExamLogic.getAll()));
	}

	public static Result save() {
		DynamicForm form = form().bindFromRequest();
		long id = Long.parseLong(form.field("id").value());
		String variant = form.field("variant").value();
		int grade = Integer.parseInt(form.field("grade").value());
		Exam exam = null;
		if (!form.field("exam").value().equals(""))
			exam = ExamLogic
			.getById(Long.parseLong(form.field("exam").value()));
		// System.err.println(form.field("exam").value() + " " + exam);
		AnswerKey answerKey;
		if (id == -1) {
			answerKey = new AnswerKey();
		} else {
			answerKey = AnswerKeyLogic.getById(id);
		}

		answerKey.variant = variant;
		answerKey.grade = grade;
		answerKey.lessonAnswers = null;
		answerKey.exam = exam;
		answerKey.save();
		return redirect(request().getHeader("referer"));
	}

	public static Result delete(long id) {
		AnswerKeyLogic.deleteById(id);
		return redirect(request().getHeader("referer"));
	}

	public static Result options(long examId) {
		Exam exam = ExamLogic.getById(examId);
		// System.err.println(exam.answerKeys.size() + " " + examId);
		return ok(answerKeys.render(exam));
	}
}