package controllers.examination;

import static play.data.Form.form;
import models.entities.examination.ExamType;
import models.entities.examination.Layout;
import models.logic.examination.ExamTypeLogic;
import models.logic.examination.LayoutLogic;
import play.data.DynamicForm;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.examination.examTypeList;

public class ExamTypes extends Controller {
	public static Result manage() {
		return ok(examTypeList.render(ExamTypeLogic.getAll()));
	}

	public static Result save() {
		DynamicForm form = form().bindFromRequest();
		long id = Long.parseLong(form.field("id").value());
		String name = form.field("name").value();

		long layoutId = Long.parseLong(form.field("layout").value());

		Layout layout;
		if (layoutId == -1)
			layout = null;
		else
			layout = LayoutLogic.getById(layoutId);

		ExamType examType;
		if (id == -1) {
			examType = new ExamType();
		} else {
			examType = ExamTypeLogic.getById(id);
		}

		examType.name = name;
		examType.layout = layout;
		examType.save();
		return redirect(request().getHeader("referer"));
	}

	public static Result delete(long id) {
		ExamTypeLogic.deleteById(id);
		return redirect(request().getHeader("referer"));
	}
}