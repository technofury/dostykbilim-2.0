package controllers.examination;

import java.util.ArrayList;
import java.util.List;

import models.entities.education.Lesson;
import models.entities.examination.ATTUTT;
import models.entities.social.Dostyk;
import models.entities.social.DostykGroup;
import models.entities.user.AuthorisedUser;
import models.logic.education.LessonLogic;
import models.logic.social.DostykLogic;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.examination.attuttBody;
import views.html.examination.attuttMain;

public class ATTUTTs extends Controller {

	public static Result save(String users, String marks, long lessonId,
			int number, String type) {
		String[] uArr = users.split(" ");
		String[] mArr = marks.split(" ");
		Lesson lesson = LessonLogic.getById(lessonId);

		for (int i = 1; i < uArr.length; i++) {
			ATTUTT cur = ATTUTT.getByFields(lessonId, Long.parseLong(uArr[i]),
					number, type);
			if (cur == null) {
				cur = new ATTUTT();
				cur.lesson = lesson;
				cur.mark = Integer.parseInt(mArr[i]);
				cur.number = number;
				cur.type = type;
				cur.user = AuthorisedUser.getUserById(Long.parseLong(uArr[i]));
			} else {
				cur.mark = Integer.parseInt(mArr[i]);
			}
			cur.save();
		}
		return ok();
	}

	public static Result getATT(String users, long lessonId, int number) {
		String[] uArr = users.split(" ");
		StringBuilder sb = new StringBuilder();
		for (int i = 1; i < uArr.length; i++) {
			ATTUTT cur = ATTUTT.getByFields(lessonId, Long.parseLong(uArr[i]),
					number, "ATT");
			if (cur == null) {
				sb.append(" 0");
			} else
				sb.append(" " + cur.mark);
		}
		return ok(sb.toString());
	}

	public static Result getUTT(String users, long lessonId, int number) {
		String[] uArr = users.split(" ");
		StringBuilder sb = new StringBuilder();
		for (int i = 1; i < uArr.length; i++) {
			ATTUTT cur = ATTUTT.getByFields(lessonId, Long.parseLong(uArr[i]),
					number, "UTT");
			if (cur == null) {
				sb.append(" 0");
			} else
				sb.append(" " + cur.mark);
		}
		return ok(sb.toString());
	}

	public static Result report() {
		return ok(attuttMain.render());
	}

	public static Result reportByLesson(long lessonId) {
		AuthorisedUser user = AuthorisedUser
				.getUserByEmail(session("connected"));

		Double[][] a = null;
		ArrayList<String> names = new ArrayList<String>();
		;

		if (user.roles.get(0).name.equals("admin")) {
			List<Dostyk> dostyks = DostykLogic.getAll();
			a = new Double[dostyks.size()][30];
			int k = 0;
			for (Dostyk dostyk : dostyks) {
				names.add(dostyk.name);
				for (int i = 1; i <= 30; i++) {
					int sum = 0;
					int cnt = 0;
					for (DostykGroup dg : dostyk.dostykGroups)
						if (dg.courses.title==11 || dg.courses.title==10) {
							List<ATTUTT> list = ATTUTT.getByFields(dg, i,
									"ATT", lessonId);
							if (!list.isEmpty()) {
								for (ATTUTT p : list) {
									sum += p.mark;
									if (p.mark != 0)
										cnt++;
								}
							}
						}
					if (cnt != 0)
						a[k][i - 1] = (sum + .0) / cnt;
					else
						a[k][i - 1] = 0.0;
				}
				k++;
			}
		} else {
			Dostyk dostyk = user.dostyks.get(0);
			int k = 0;
			for (DostykGroup dg : dostyk.dostykGroups)
				if (dg.courses.title==11 || dg.courses.title==10)
					k++;
			a = new Double[k][30];
			k = 0;
			for (DostykGroup dg : dostyk.dostykGroups)
				if (dg.courses.title==11 || dg.courses.title==10) {
					names.add(dg.name);
					for (int i = 1; i <= 30; i++) {
						List<ATTUTT> list = ATTUTT.getByFields(dg, i, "ATT",
								lessonId);
						a[k][i - 1] = 0.0;
						if (!list.isEmpty()) {
							int sum = 0;
							int cnt = 0;
							for (ATTUTT p : list) {
								sum += p.mark;
								if (p.mark != 0)
									cnt++;
							}
							if (cnt != 0)
								a[k][i - 1] = (sum + .0) / cnt;
						}
					}
					k++;
				}
		}
		return ok(attuttBody.render(names, a));
	}
}