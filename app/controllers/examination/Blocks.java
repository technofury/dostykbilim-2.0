package controllers.examination;

import static play.data.Form.form;
import models.entities.examination.Block;
import models.logic.examination.BlockLogic;
import models.logic.examination.LayoutLogic;
import play.data.DynamicForm;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.examination.blocksList;

public class Blocks extends Controller {
	public static Result showByLayout(long id) {
		return ok(blocksList.render(
				BlockLogic.getByLayout(LayoutLogic.getById(id)), id));
	}

	public static Result save() {
		DynamicForm form = form().bindFromRequest();
		long id = Long.parseLong(form.field("id").value());
		String name = form.field("name").value();
		int weight = Integer.parseInt(form.field("weight").value());
		int delimeter = Integer.parseInt(form.field("delimeter").value());
		String checking = form.field("checking").value();
		Block block;
		if (id == -1) {
			block = new Block();
			block.layout = LayoutLogic.getById(Long.parseLong(form.field(
					"layoutId").value()));
		} else {
			block = BlockLogic.getById(id);
		}

		block.name = name;
		block.weight = weight;
		block.delimeter = delimeter;
		block.checking = checking;
		block.save();
		return redirect(request().getHeader("referer"));
	}

	public static Result delete(long id) {
		BlockLogic.deleteById(id);
		return redirect(request().getHeader("referer"));
	}
}