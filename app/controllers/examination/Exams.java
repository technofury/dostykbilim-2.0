package controllers.examination;

import static play.data.Form.form;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.lang.Thread.State;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import models.entities.education.Lesson;
import models.entities.examination.Answer;
import models.entities.examination.AnswerKey;
import models.entities.examination.Block;
import models.entities.examination.DetailedResult;
import models.entities.examination.Exam;
import models.entities.examination.ExamResult;
import models.entities.examination.ExamType;
import models.entities.examination.LessonAnswer;
import models.entities.social.Dostyk;
import models.entities.user.AuthorisedUser;
import models.logic.education.LessonLogic;
import models.logic.examination.AnswerKeyLogic;
import models.logic.examination.DetailedResultLogic;
import models.logic.examination.ExamLogic;
import models.logic.examination.ExamResultLogic;
import models.logic.examination.ExamTypeLogic;
import models.logic.examination.LessonAnswerLogic;
import models.logic.social.DostykLogic;
import play.Logger;
import play.data.DynamicForm;
import play.mvc.Controller;
import play.mvc.Http.MultipartFormData;
import play.mvc.Http.MultipartFormData.FilePart;
import play.mvc.Result;
import views.html.progressBar;
import views.html.tryLater;
import views.html.examination.activeExams;
import views.html.examination.errorReport;
import views.html.examination.examList;

public class Exams extends Controller {

	public static Result manage() {
		return ok(examList.render(ExamLogic.getAll()));
	}

	public static Result save() throws ParseException {
		DynamicForm form = form().bindFromRequest();
		int grade = Integer.parseInt(form.field("grade").value());
		ExamType examType = ExamTypeLogic.getById(Long.parseLong(form.field(
				"examType").value()));
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		String start = form.field("startDate").value() + " "
				+ form.field("startTime").value();
		String end = form.field("endDate").value() + " "
				+ form.field("endTime").value();

		Date begin = format.parse(start);
		Date finish = format.parse(end);
		Exam exam = new Exam();
		exam.name = form.field("name").value();
		exam.grade = grade;
		exam.status = "inactive";
		exam.startDate = begin;
		exam.endDate = finish;
		exam.examType = examType;
		exam.save();
		return redirect(request().getHeader("referer"));
	}

	public static Result delete(long id) {
		ExamLogic.deleteById(id);
		return redirect(request().getHeader("referer"));
	}

	public static Result changeStatus(long id, String status) {
		Exam exam = ExamLogic.getById(id);
		exam.status = status;
		exam.save();
		return ok();
	}

	public static Result changeName(long id, String name) {
		Exam exam = ExamLogic.getById(id);
		exam.name = name;
		exam.save();
		return ok();
	}

	public static Result changeCode(long id, String code) {
		LessonAnswer la = LessonAnswerLogic.getById(id);
		la.code = code;
		la.save();
		return ok();
	}

	public static Result showActive() {
		return ok(activeExams.render(ExamLogic.getActive()));
	}

	public static class Pair implements Comparable<Pair> {
		long id;
		int correct;

		public Pair(long id, int correct) {
			this.id = id;
			this.correct = correct;
		}

		@Override
		public int compareTo(Pair other) {
			if (other.id != id) {
				return sign(id - other.id);
			}
			return other.correct - correct;
		}

		public int sign(long x) {
			if (x < 0)
				return -1;
			if (x > 0)
				return 1;
			return 0;
		}
	}

	public static class Triple implements Comparable<Triple> {
		long dostykId;
		long groupId;
		int correct;
		int order;

		public Triple(long dostykId, long groupId, int correct, int order) {
			this.dostykId = dostykId;
			this.groupId = dostykId;
			this.correct = correct;
			this.order = order;
		}

		@Override
		public int compareTo(Triple other) {
			return other.correct - correct;
		}

	}

	public static class longComp extends Thread implements Runnable {
		@Override
		public void run() {
			System.currentTimeMillis();
			Exam exam = ExamLogic.getById(examId);
			List<ExamResult> examResults = ExamResultLogic.getByExam(exam);

			int[] kz = new int[300];
			int[] kzSum = new int[300];
			int[][] dostyk = new int[100][300];
			int[][] dostykSum = new int[100][300];

			HashMap<Long, Integer> userDostyk = new HashMap<Long, Integer>();
			HashMap<Long, Long> userGroup = new HashMap<Long, Long>();

			for (ExamResult cur : examResults) {
				Dostyk ds = DostykLogic.getByUser2(cur.student);
				if (ds != null) {
					userDostyk.put(cur.student.id, ds.code);
				} else if (cur.student.dostyks != null
						&& cur.student.dostyks.size() > 0) {
					userDostyk.put(cur.student.id,
							cur.student.dostyks.get(0).code);
				} else {
					userDostyk.put(cur.student.id, -1);
				}

				if (cur.student.profile.dostykGroup != null) {
					userGroup.put(cur.student.id,
							cur.student.profile.dostykGroup.id);
				} else {
					userGroup.put(cur.student.id, -1L);
				}
			}

			ArrayList<Pair> group = new ArrayList<Pair>();
			for (ExamResult cur : examResults) {
				kz[cur.total]++;
				int ds = userDostyk.get(cur.student.id);
				if (ds != -1)
					dostyk[ds][cur.total]++;

				long gr = userGroup.get(cur.student.id);
				if (gr != -1) {
					Pair p = new Pair(gr, cur.total);
					group.add(p);
				}
			}

			for (int i = 298; i >= 0; i--) {
				kzSum[i] = kzSum[i + 1] + kz[i];
				for (int j = 0; j < 100; j++)
					dostykSum[j][i] = dostykSum[j][i + 1] + dostyk[j][i];
			}

			for (ExamResult cur : examResults) {
				cur.rankKZ = kzSum[cur.total] - kz[cur.total] + 1;
				int code = userDostyk.get(cur.student.id);
				if (code == -1) {
					cur.rankDostyk = -1;
				} else {
					cur.rankDostyk = dostykSum[code][cur.total]
							- dostyk[code][cur.total] + 1;
				}
				cur.save();
			}

			Collections.sort(group);

			TreeMap<Pair, Integer> tm = new TreeMap<Pair, Integer>();
			int before = 0;
			int last = 0;
			for (int i = 0; i < group.size(); i++) {
				tm.put(group.get(i), before + 1);

				if (i + 1 < group.size()
						&& group.get(i + 1).id != group.get(i).id) {
					before = 0;
					last = i + 1;
				} else if (i + 1 < group.size()
						&& group.get(i + 1).correct != group.get(i).correct) {
					before += i - last + 1;
					last = i + 1;
				}

			}
			for (ExamResult cur : examResults) {
				long gr = userGroup.get(cur.student.id);
				if (gr != -1) {
					Pair p = new Pair(gr, cur.total);
					cur.rankGroup = tm.get(p);
					cur.save();
				}
			}

			for (Lesson lesson : LessonLogic.getAll()) {
				ArrayList<Triple> all = new ArrayList<Triple>();
				int order = 0;
				for (ExamResult cur : examResults) {
					for (DetailedResult curDetailed : cur.detailedResults)
						if (lesson.id == curDetailed.lesson.id) {
							long dostykId = userDostyk.get(cur.student.id);
							if (dostykId == -1)
								dostykId = 99;
							long groupId = userGroup.get(cur.student.id);
							int correct = curDetailed.correct;
							all.add(new Triple(dostykId, groupId, correct,
									order));
							break;
						}
					order++;
				}

				Collections.sort(all);
				Map<Long, Integer> dostykMap = new TreeMap<Long, Integer>();
				Map<Long, Integer> groupMap = new TreeMap<Long, Integer>();
				for (int i = 0, j = 0; i < all.size(); i = j) {
					while (j < all.size()
							&& all.get(i).correct == all.get(j).correct) {
						List<DetailedResult> detailedResults = examResults
								.get(all.get(j).order).detailedResults;
						for (DetailedResult curDetailed : detailedResults)
							if (lesson.id == curDetailed.lesson.id) {
								curDetailed.rankKZ = i + 1;
								int dostykRank = 1;
								long dostykId = all.get(j).dostykId;
								if (dostykMap.containsKey(dostykId))
									dostykRank = dostykMap.get(dostykId) + 1;
								curDetailed.rankDostyk = dostykRank;

								if (all.get(j).groupId != -1) {
									int groupRank = 1;
									long groupId = all.get(j).groupId;
									if (groupMap.containsKey(groupId))
										groupRank = groupMap.get(groupId) + 1;
									curDetailed.rankGroup = groupRank;
								}
								curDetailed.save();
								break;
							}
						j++;
					}
					for (int k = i; k < j; k++) {
						if (!dostykMap.containsKey(all.get(k).dostykId))
							dostykMap.put(all.get(k).dostykId, 0);
						int cur = dostykMap.get(all.get(k).dostykId);
						dostykMap.remove(all.get(k).dostykId);
						dostykMap.put(all.get(k).dostykId, cur + 1);

						if (!groupMap.containsKey(all.get(k).groupId))
							groupMap.put(all.get(k).groupId, 0);
						cur = groupMap.get(all.get(k).groupId);
						groupMap.remove(all.get(k).groupId);
						groupMap.put(all.get(k).groupId, cur + 1);
					}
				}
			}
			System.currentTimeMillis();
		}
	}

	public static long examId;
	public static longComp thread;
	private static BufferedReader bf;

	public static Result checkExam(long examId1) {
		if (thread == null) {
			examId = examId1;
			thread = new longComp();
			thread.start();
			return ok(progressBar.render());
		}

		if (thread.isAlive()) {
			return ok(tryLater.render());
		} else {
			thread.getState();
			if (State.TERMINATED != null) {
				thread = null;
			}
			examId = examId1;
			thread = new longComp();
			thread.start();
			return ok(progressBar.render());
		}
	}

	public static Result check(Exam exam, List<String> list) {
        try {

            List<Block> blocks = exam.examType.layout.blocks;
            Collections.sort(blocks, new Comparator<Block>() {
                @Override
                public int compare(Block a, Block b) {
                    return a.weight - b.weight;
                }
            });

            @SuppressWarnings("unchecked")
            Map<String, String>[] studentData = new HashMap[list.size()];
            for (int i = 0; i < studentData.length; i++) {
                studentData[i] = new HashMap<String, String>();
            }

            int cur = 0;
            List<Map<String, String>> incorrect = new ArrayList<Map<String, String>>();

            boolean[] put = new boolean[list.size()];


            if (exam.examType.layout.name.contains("D5")) {
//			if (exam.examType.layout.name.equals("D5")) {
                d5Compute(exam, list, cur, put, blocks, studentData, incorrect);
            } else {
                defaultCompute(cur, put, exam, list, blocks, studentData, incorrect);
            }
            return report(incorrect, exam);
        }catch(Exception e){
            e.printStackTrace();
            return ok("Тексеруде қате табылды. Мына нәрселерді мұқият тексеруіңізді өтінеміз:\n" +
                    "1) Layout\n" +
                    "2) Жауаптар\n" +
                    "3) Txt файл");
        }
	}

	private static void d5Compute(Exam exam, List<String> list, int cur,
			boolean[] put, List<Block> blocks,
			Map<String, String>[] studentData,
			List<Map<String, String>> incorrect) {
		for (String s : list) {
			int bi = 0;
			int ei;

			for (int i = 0; i < blocks.size(); i++) {
				String ss;
				ei = blocks.get(i).delimeter + bi;

				ss = s.substring(bi, ei);

				studentData[cur].put(blocks.get(i).checking, ss);
				bi = ei;
				
				/*
				if (blocks.get(i).checking.equals("99")) {
					if (ss.equals(" ")) {
						studentData[cur].put("5pan", "incorrect");
						put[cur] = true;
					} else
						studentData[cur].put("5pan", "correct");
				}
				*/

				if (blocks.get(i).checking.equals("55")) {
					if (AnswerKeyLogic.getByVariant(ss).size() == 0) {
						studentData[cur].put("variant", "incorrect");
						put[cur] = true;
					} else {
						studentData[cur].put("variant", "correct");
					}
				}

				if (blocks.get(i).checking.equals("87")) {
					AuthorisedUser user = AuthorisedUser.getByIdNumber(ss);
					if (user == null) {
						studentData[cur].put("idExistence", "false");
						put[cur] = true;
					} else {
						studentData[cur].put("idExistence", "true");
					}
				}
			}
			if (!studentData[cur].containsKey("99"))
				studentData[cur].put("99", "");
			if (!studentData[cur].containsKey("01"))
				studentData[cur].put("01", "");
			if (!studentData[cur].containsKey("02"))
				studentData[cur].put("02", "");
			if (!studentData[cur].containsKey("03"))
				studentData[cur].put("03", "");
			if (!studentData[cur].containsKey("04"))
				studentData[cur].put("04", "");
			if (!studentData[cur].containsKey("05"))
				studentData[cur].put("05", "");
			if (!studentData[cur].containsKey("06"))
				studentData[cur].put("06", "");
			if (!studentData[cur].containsKey("07"))
				studentData[cur].put("07", "");
			if (!studentData[cur].containsKey("08"))
				studentData[cur].put("08", "");
			if (!studentData[cur].containsKey("09"))
				studentData[cur].put("09", "");
			if (!studentData[cur].containsKey("10"))
				studentData[cur].put("10", "");
			cur++;
		}

		for (int i = 0; i < studentData.length; i++) {
			for (int j = 0; j < studentData.length; j++)
				if (i != j) {
					if (studentData[i].get("87").equals(
							studentData[j].get("87"))) {
						studentData[i].put("dublicate", "true");
						put[i] = true;
					}
				}
			if (!studentData[i].containsKey("dublicate"))
				studentData[i].put("dublicate", "false");
			if (put[i]) {
				incorrect.add(studentData[i]);
			} else {
				String ls[] = {studentData[i].get("01"), studentData[i].get("02"),
						studentData[i].get("03"), studentData[i].get("04"),
						studentData[i].get("05"),studentData[i].get("06"), studentData[i].get("07"),
						studentData[i].get("08"), studentData[i].get("09"),
						studentData[i].get("10")};
				addResult(exam, studentData[i].get("87"),
						studentData[i].get("55"), studentData[i].get("99"),
						ls);
				/*
				addResult(exam, 
						studentData[i].get("87"),
						studentData[i].get("55"), 
						studentData[i].get("99"),
						studentData[i].get("01"), 
						studentData[i].get("02"),
						studentData[i].get("03"), 
						studentData[i].get("04"),
						studentData[i].get("05"));
						*/
			}
		}
		
	}

	public static void defaultCompute(int cur, boolean[] put, Exam exam,
			List<String> list, List<Block> blocks,
			Map<String, String>[] studentData,
			List<Map<String, String>> incorrect) {

		for (String s : list) {
      //Logger.info("s: "+s);
			int bi = 0;
			int ei;
			for (int i = 0; i < blocks.size(); i++) {
				String ss;
				ei = blocks.get(i).delimeter + bi;
				ss = s.substring(bi, ei);
				studentData[cur].put(blocks.get(i).checking, ss);
				bi = ei;
				if (blocks.get(i).checking.equals("99")) {
					if (ss.equals(" ")) {
						studentData[cur].put("5pan", "incorrect");
						put[cur] = true;
					} else
						studentData[cur].put("5pan", "correct");
				}
				if (blocks.get(i).checking.equals("55")) {
					if (AnswerKeyLogic.getByInfo(exam, ss).size() == 0) {
						studentData[cur].put("variant", "incorrect");
						put[cur] = true;
					} else {
						studentData[cur].put("variant", "correct");
					}
				}
				if (blocks.get(i).checking.equals("87")) {
					AuthorisedUser user = AuthorisedUser.getByIdNumber(ss);
					if (user == null) {
						studentData[cur].put("idExistence", "false");
						put[cur] = true;
					} else {
						studentData[cur].put("idExistence", "true");
					}
				}
			}
			if (!studentData[cur].containsKey("99"))
				studentData[cur].put("99", "");
			if (!studentData[cur].containsKey("01"))
				studentData[cur].put("01", "");
			if (!studentData[cur].containsKey("02"))
				studentData[cur].put("02", "");
			if (!studentData[cur].containsKey("03"))
				studentData[cur].put("03", "");
			if (!studentData[cur].containsKey("04"))
				studentData[cur].put("04", "");
			if (!studentData[cur].containsKey("05"))
				studentData[cur].put("05", "");
			cur++;
		}

		for (int i = 0; i < studentData.length; i++) {
			for (int j = 0; j < studentData.length; j++)
				if (i != j) {
					if (studentData[i].get("87").equals(
							studentData[j].get("87"))) {
						studentData[i].put("dublicate", "true");
						put[i] = true;
					}
				}
			if (!studentData[i].containsKey("dublicate"))
				studentData[i].put("dublicate", "false");
			if (put[i]) {
				incorrect.add(studentData[i]);
			} else {
                		addResult(exam, studentData[i].get("87"),
						studentData[i].get("55"), studentData[i].get("99"),
						studentData[i].get("01"), studentData[i].get("02"),
						studentData[i].get("03"), studentData[i].get("04"),
						studentData[i].get("05"));
			}
		}
	}

	private static void addResult(Exam exam, String idNumber, String variant,
			String pan, String[] ls) {

		AnswerKey answerKey = AnswerKeyLogic.getByVariant(variant).get(0);
		AuthorisedUser user = AuthorisedUser.getByIdNumber(idNumber);
		ExamResult examResult = ExamResultLogic.getByExamAndUser(exam, user);
		if (examResult == null)
			examResult = new ExamResult();
		for (DetailedResult dr : examResult.detailedResults)
			DetailedResultLogic.deleteById(dr.id);
		examResult.exam = exam;
		examResult.variant = variant;
		examResult.student = user;
		examResult.total = 0;
		examResult.rankDostyk = 0;
		examResult.rankGroup = 0;
		examResult.rankKZ = 0;
		examResult.save();
		for (int i = 1; i <= ls.length; i++) {

			checkOneLesson(examResult, answerKey, ls[i-1],String.format("%02d", i));
		}
	}

	private static void addResult(Exam exam, String idNumber, String variant,
			String pan, String l1, String l2, String l3, String l4, String l5) {

		AnswerKey answerKey = AnswerKeyLogic.getByVariant(variant).get(0);
    //Logger.info("answer key is null: "+(answerKey==null));
		AuthorisedUser user = AuthorisedUser.getByIdNumber(idNumber);
		ExamResult examResult = ExamResultLogic.getByExamAndUser(exam, user);
		if (examResult == null)
			examResult = new ExamResult();
		for (DetailedResult dr : examResult.detailedResults)
			DetailedResultLogic.deleteById(dr.id);
		examResult.exam = exam;
		examResult.variant = variant;
		examResult.student = user;
		examResult.total = 0;
		examResult.rankDostyk = 0;
		examResult.rankGroup = 0;
		examResult.rankKZ = 0;
		examResult.save();

		checkOneLesson(examResult, answerKey, l1, "01");
		checkOneLesson(examResult, answerKey, l2, "02");
		checkOneLesson(examResult, answerKey, l3, "03");
		checkOneLesson(examResult, answerKey, l4, "04");

		if (answerKey.lessonAnswers.size() >= 5||exam.examType.layout.name.startsWith("D7")) {
			pan = pan.charAt(0) - 'A' + 1 + "";
			if (pan.length() == 1)
				pan = "0" + pan;
			checkOneLesson(examResult, answerKey, l5, pan);
		}

	}

	private static void checkOneLesson(ExamResult examResult,
			AnswerKey answerKey, String studentAnswers, String code) {

		if (studentAnswers.length() == 0)
			return;
		for (LessonAnswer lessonAnswer : answerKey.lessonAnswers)
			if (lessonAnswer.code.equals(code)) {
				List<Answer> answers = lessonAnswer.answers;
				Collections.sort(answers);
				DetailedResult detailedResult;
				detailedResult = DetailedResultLogic.getByExamResultAndLesson(
						examResult, lessonAnswer.lesson);
				if (detailedResult == null) {
					detailedResult = new DetailedResult();
				} else {
					examResult.total -= detailedResult.correct;
					detailedResult.correct = 0;
					detailedResult.blank = 0;
					detailedResult.incorrect = 0;
				}
				detailedResult.lesson = lessonAnswer.lesson;
				detailedResult.studentAnswers = studentAnswers;

				for (int i = 0; i < studentAnswers.length(); i++) {
					if (studentAnswers.charAt(i) == ' ') {
						detailedResult.blank++;
						continue;
					}
					
					try{
					if (studentAnswers.charAt(i) == answers.get(i).answerCode
							.charAt(0)) {
						detailedResult.correct++;
					} else {
						detailedResult.incorrect++;
					}
					}catch(Exception e){}
				}
				detailedResult.save();
				examResult.total += detailedResult.correct;
				examResult.save();
				detailedResult.examResult = examResult;
				if (examResult.exam.examType.layout.name.contains("BIL")) {
					detailedResult.mark = detailedResult.correct - (detailedResult.incorrect/4);
				}
				detailedResult.save();
				break;
			}
	}

	public static Result report(List<Map<String, String>> incorrect, Exam exam) {
		return ok(errorReport.render(incorrect, exam));
	}

	public static Result submitResult(long examId, String idNumber,
			String variant, String pan, String l1, String l2, String l3,
			String l4, String l5) {
		if (AnswerKeyLogic.getByVariant(variant).size() == 0) {
			return ok("incorrectVariant");
		}
		AuthorisedUser user = AuthorisedUser.getByIdNumber(idNumber);
		if (user == null) {
			return ok("noIdNumber");
		}
		Exam exam = ExamLogic.getById(examId);
		addResult(exam, idNumber, variant, pan, l1, l2, l3, l4, l5);
		return ok("ok");
	}
	
	public static Result submitResultD5(long examId, String idNumber,
			String variant, String ls) {
		String[] lss = ls.split(",");
		if (AnswerKeyLogic.getByVariant(variant).size() == 0) {
			return ok("incorrectVariant");
		}
		AuthorisedUser user = AuthorisedUser.getByIdNumber(idNumber);
		if (user == null) {
			return ok("noIdNumber");
		}
		Exam exam = ExamLogic.getById(examId);

		addResult(exam, idNumber, variant, "", lss);
		return ok("ok");
	}

	public static Result upload(long examId) throws Exception {
		MultipartFormData body = request().body().asMultipartFormData();
		Exam exam = ExamLogic.getById(examId);
		FilePart file = body.getFile("file");
		if (file != null) {
			file.getFilename();
			file.getContentType();
			File original = file.getFile();
			String charset = "UTF-16";
			bf = new BufferedReader(new InputStreamReader(new FileInputStream(
					original), charset));
			List<String> input = new ArrayList<String>();
			String s;
			while ((s = bf.readLine()) != null) {
				if (s.length() > 0)
					input.add(s);
			}
			return check(exam, input);
		} else {
			flash("error", "Missing file");
			return redirect(request().getHeader("referer"));
		}

	}
}