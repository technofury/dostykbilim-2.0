package controllers.examination;

import static play.data.Form.form;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.avaje.ebean.Ebean;
import models.entities.education.Lesson;
import models.entities.examination.Answer;
import models.entities.examination.AnswerKey;
import models.entities.examination.LessonAnswer;
import models.logic.education.LessonLogic;
import models.logic.examination.AnswerKeyLogic;
import models.logic.examination.LessonAnswerLogic;
import play.data.DynamicForm;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import views.html.examination.lessonAnswersList;
import views.html.examination.video;
import views.html.helps.videoHelper;
import controllers.Application;

public class LessonAnswers extends Controller {

	public static Result getVideo(long examId, String variant, long lessonId) {
		//System.err.println(variant + " " + lessonId);
		LessonAnswer l = LessonAnswerLogic.getBy(variant,
				LessonLogic.getById(lessonId));
		return ok(videoHelper.render(l));
	}
	public static Result getVideo2(long examId, String variant, long lessonId){
		//System.err.println(variant + " " + lessonId);
		LessonAnswer l = LessonAnswerLogic.getBy(variant,
				LessonLogic.getById(lessonId));
		return ok(videoHelper.render(l));
	}

	public static Result video() {
		return ok(video.render());
	}

	public static Result showByAnswerKey(long id) {
		List<LessonAnswer> l = AnswerKeyLogic.getById(id).lessonAnswers;
		List<String> names = new ArrayList<String>();

		for (LessonAnswer a : l) {
			if (a.pathToVideo != null && a.pathToVideo.indexOf("##") >= 0) {
				names.add(a.pathToVideo.substring(a.pathToVideo.indexOf("##") + 2));
			} else
				names.add("none");
		}
		return ok(lessonAnswersList.render(l, names, id));
	}

	public static Result save() {
		DynamicForm form = form().bindFromRequest();
		long answerKeyId = Long.parseLong(form.field("answerKeyId").value());
		long lessonId = Long.parseLong(form.field("lessonId").value());
		Lesson lesson = LessonLogic.getById(lessonId);
		AnswerKey answerKey = AnswerKeyLogic.getById(answerKeyId);
		if (answerKey.lessonAnswers != null)
			for (LessonAnswer lessonAnswer : answerKey.lessonAnswers) {
				if (lessonAnswer.lesson.id == lesson.id) {
					return redirect(request().getHeader("referer"));
				}
			}

		LessonAnswer lessonAnswer = new LessonAnswer();
		lessonAnswer.lesson = lesson;
		lessonAnswer.answerKey = answerKey;
		lessonAnswer.code = form.field("code").value();
		lessonAnswer.save();
		if (answerKey.lessonAnswers == null)
			answerKey.lessonAnswers = new ArrayList<LessonAnswer>();
		answerKey.lessonAnswers.add(lessonAnswer);
		answerKey.save();
		return redirect(request().getHeader("referer"));
	}

	public static Result delete(long id) {
		LessonAnswerLogic.deleteById(id);
		return redirect(request().getHeader("referer"));
	}

	public static Result saveUploadedFile(long id) {
		try {
			Http.MultipartFormData body = request().body()
					.asMultipartFormData();
			Http.MultipartFormData.FilePart picture = body.getFile("file");
			if (picture != null) {
				LessonAnswer l = LessonAnswerLogic.getById(id);
				int q;
				try {
					q = Integer.parseInt(getName(picture.getFilename()));
				} catch (Exception e) {
					return ok("must be number");
				}
				java.io.File file = picture.getFile();
				String Root = Application.getRootPath();
				DateFormat sdf = new SimpleDateFormat("yyyy-MM");
				Date now = new Date();
				String current = sdf.format(now);
				String ext = getExtension(picture.getFilename());
                if(!ext.equalsIgnoreCase("mp4"))
                    return ok("use mp4 format only");
				Long time = now.getTime();
                String osName = System.getProperty("os.name");
                String pathToStore;
                if(osName.equalsIgnoreCase("Linux"))
                    pathToStore = "/var/www/files.dostykbilim.kz/public_html/";
                else
                    pathToStore = Root+"/FILES/";
                String fileName = "video_"+l.id+"_"+q + "." + ext;
				boolean res = addFileToDir(file, pathToStore, fileName);
				if (res == true) {
					Answer answer = Answer.getBy(l, q);
					answer.pathToVideo = "http://files.dostykbilim.kz/"+fileName;
					Ebean.save(answer);
					return ok("ok");
				} else
					return ok("error");
			} else {
				return ok("missing");
			}
		} catch (Exception e) {
			e.printStackTrace();
			return ok("error");
		}
	}

	private static String getName(String filename) {
		String name = filename.substring(0, filename.lastIndexOf("."));
		return name;
	}

	public static String getExtension(String filename) {
		String extension = filename.substring(filename.lastIndexOf(".") + 1,
				filename.length());
		return extension;
	}

	public static boolean addFileToDir(java.io.File file, String path,
			String newName) {
		try {
			java.io.File f = new java.io.File(path);
			f.mkdirs();
			file.renameTo(new java.io.File(path, newName));
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
    public static void migrateVideoPaths(){
        String oldName,folder,name,ext; int count=0; String [] all; File file;
        String Root = Application.getRootPath();
        for(Answer answer: Answer.getAllWithVideo()){
            oldName = answer.pathToVideo;
            if(!oldName.isEmpty()&&!oldName.contains("@")){
                //file = new File("/var/www/files.dostykbilim.kz/public_html/"+oldName);
                all = oldName.split("/");
                //folder = all[0];

                name = all[3];
                file = new File("/var/www/files.dostykbilim.kz/public_html/"+name);
                all = name.split("\\.");
                ext = all[1];
                String pathToStore;
                String osName = System.getProperty("os.name");
                if(osName.equalsIgnoreCase("Linux"))
                    pathToStore = "/var/www/files.dostykbilim.kz/public_html/";
                else
                    pathToStore = Root+"/FILES/";
                oldName = "video@"+answer.lessonAnswer.id+"@"+answer.questionNumber+"."+ext;
                file.renameTo(new File(pathToStore+oldName));
                oldName = "http://files.dostykbilim.kz/"+oldName;
                answer.pathToVideo = oldName;
                Ebean.save(answer);
                count++;
            }

        }

    }
}