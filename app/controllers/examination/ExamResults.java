package controllers.examination;

import static play.data.Form.form;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map.Entry;

import models.entities.examination.DetailedResult;
import models.entities.examination.ENT;
import models.entities.examination.Exam;
import models.entities.examination.ExamResult;
import models.entities.social.Dostyk;
import models.entities.social.DostykGroup;
import models.entities.user.AuthorisedUser;
import models.logic.education.LessonLogic;
import models.logic.education.RegisterLogic;
import models.logic.examination.ENTLogic;
import models.logic.examination.ExamLogic;
import models.logic.examination.ExamResultLogic;
import models.logic.social.DostykGroupLogic;
import models.logic.social.DostykLogic;
import models.logic.user.AuthorisedUserLogic;
import play.data.DynamicForm;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.examination.allResult;
import views.html.examination.examResults;
import views.html.examination.examinationReport;
import views.html.examination.groupReport;
import views.html.examination.showGroup;
import views.html.examination.deletedUsers;
import views.html.examination.userResult;
import views.html.user.profilePack.allResultTab;
import views.html.user.profilePack.userResultTab;
import views.html.printable.printExamByUser;

public class ExamResults extends Controller {

	public static Result showByUser(Long userId) {
		AuthorisedUser user;
		if (userId == -1) {
			user = AuthorisedUser.getUserByEmail(session().get("connected"));
		} else {
			user = AuthorisedUser.getUserById(userId);
		}

		List<ExamResult> examResults = ExamResultLogic.getByUser(user);
		if (userId == -1)
			//return ok(userResult.render(examResults));
			return ok(userResultTab.render(examResults, user));
		else
			return ok(userResultTab.render(examResults, user));
	}

	public static Result showByUser2(Long userId) {
		AuthorisedUser user;
		if (userId == -1) {
			user = AuthorisedUser.getUserByEmail(session().get("connected"));
		} else {
			user = AuthorisedUser.getUserById(userId);
		}

		List<Exam> exams = ExamLogic
				.getByGradeSorted(user.profile.course.classNumber);
		List<ExamResult> examResults = new ArrayList<ExamResult>();
		for (Exam e : exams) {
			ExamResult er = ExamResultLogic.getByExamAndUser(e, user);
			if (er != null)
				Collections.sort(er.detailedResults,
						new Comparator<DetailedResult>() {
					@Override
					public int compare(DetailedResult arg0,
							DetailedResult arg1) {
						return arg0.lesson.code
								.compareTo(arg1.lesson.code);
					}
				});
			examResults.add(er);
		}

		int k = 0;
		for (int i = 1; i < exams.size(); i++)
			if (!exams.get(i).examType.equals(exams.get(i - 1).examType))
				k = i;
		if (userId == -1)
			return ok(allResult.render(exams, examResults, user,
					RegisterLogic.getCount(user, "notPresent"), k));
		else
			return ok(allResultTab.render(exams, examResults, user,
					RegisterLogic.getCount(user, "notPresent"), k));
	}

	public static Result examReportByDostykGroup(Long examId, Long dostykGroupId) {
		DostykGroup dostykGroup = DostykGroupLogic.getById(dostykGroupId);
		Exam exam = ExamLogic.getById(examId);
		return ok(examResults.render(ExamResultLogic
				.getByExamAndByDostykGroup2(exam, dostykGroup)));
	}

	public static Result examReportByDostyk(Long examId, Long dostykId) {
		Dostyk dostyk = DostykLogic.getById(dostykId);
		Exam exam = ExamLogic.getById(examId);
		return ok(examResults.render(ExamResultLogic.getByExamAndByDostyk2(
				exam, dostyk)));
	}

	public static Result examReport() {
		AuthorisedUser user = AuthorisedUser.getUserByEmail(session().get(
				"connected"));
		return ok(examinationReport.render(user));
	}

	public static Result groupReport() {
		AuthorisedUser user = AuthorisedUser.getUserByEmail(session().get(
				"connected"));
		List<DostykGroup> dostykGroups = null;
		if (user.id == 1L) {
			dostykGroups = DostykGroupLogic.getAllCompanyGroups();
		} else {
			dostykGroups = user.dostyks.get(0).dostykGroups;
		}
		Collections.sort(dostykGroups, new Comparator<DostykGroup>() {
			@Override
			public int compare(DostykGroup a, DostykGroup b) {
				return a.name.compareTo(b.name);
			}
		});
		return ok(groupReport.render(dostykGroups));
	}

	public static Result showGroup(Long id, int type) {
		DostykGroup dostykGroup = DostykGroupLogic.getById(id);
		List<AuthorisedUser> users = AuthorisedUserLogic
				.getByProfiles(dostykGroup.students);
		List<Exam> exams = ExamLogic.getByGradeAndType(dostykGroup.courses.title, type);
		List<List<ExamResult>> res = new ArrayList<List<ExamResult>>();
		List<ENT> ent = new ArrayList<ENT>();
		Double[] srd = new Double[exams.size() * 6 + 1];

		for (int i = 0; i < srd.length; i++)
			srd[i] = 0.0;

		for (AuthorisedUser u : users) {
			int k = 0;
			List<ExamResult> examResults = new ArrayList<ExamResult>();
			for (Exam e : exams) {
				ExamResult er = ExamResultLogic.getByExamAndUser(e, u);
				if (er != null)
					Collections.sort(er.detailedResults,
							new Comparator<DetailedResult>() {
						@Override
						public int compare(DetailedResult arg0,
								DetailedResult arg1) {
							return arg0.lesson.code
									.compareTo(arg1.lesson.code);
						}
					});
				examResults.add(er);
				if (er != null) {
					for (DetailedResult dr : er.detailedResults) {
						srd[k++] += dr.correct;
					}
					srd[k++] += er.total;
				} else {
					int p = dostykGroup.courses.title == 11 ? 6 : 3;
					for (int i = 0; i < p; i++)
						k++;
				}

			}
			res.add(examResults);
			ent.add(ENTLogic.getByUser(u));
			srd[k++] += ent.get(ent.size() - 1) == null ? 0 : ent.get(ent.size() - 1).points;
		}

		for (int i = 0; i < srd.length; i++) {
			if (users.size() > 0)
				srd[i] /= users.size() + .0;
		}

		return ok(showGroup.render(users, res, exams, ent, srd, type, LessonLogic.getAll()));
	}

	public static Result saveEnt() {
		DynamicForm form = form().bindFromRequest();

		for (Entry<String, String> e : form.data().entrySet()) {
			String[] tmp = e.getKey().split("_");
			AuthorisedUser u = AuthorisedUserLogic.getById(Long.parseLong(tmp[1]));
			int points = Integer.parseInt("0" + e.getValue());
			ENT ee = ENTLogic.getByUser(u);
			if (ee == null)
				ee = new ENT();
			if (tmp[0].equals("points"))
				ee.points = points; else
			if (tmp[0].equals("kaz"))
				ee.kaz = points; else
			if (tmp[0].equals("rus"))
				ee.rus = points; else
			if (tmp[0].equals("math"))
				ee.math = points; else
			
					if (tmp[0].equals("history"))
						ee.history = points; else
					if (tmp[0].equals("selected"))
						ee.selected = points; else
					if (tmp[0].equals("lesson"))
						ee.lesson = LessonLogic.getById(Long.parseLong("0" + e.getValue()));
							
			ee.user = u;
			ee.save();
		}

		return redirect(request().getHeader("referer"));
	}
	
	public static Result deletedUsers(){
		AuthorisedUser moder = AuthorisedUser.getUserByEmail(session().get("connected"));
		List<AuthorisedUser> users = AuthorisedUserLogic.getDeletedUsers(moder.dostyks.get(0).code);
		List<ENT> ent = new ArrayList<ENT>();
		for (AuthorisedUser u : users) {
				ent.add(ENTLogic.getByUser(u));
		}
		return ok(deletedUsers.render(users, ent, LessonLogic.getAll())); 
	}
	public static Result printByUser(Long userId, Long examId){
		AuthorisedUser student = AuthorisedUser.find.byId(userId);
		ExamResult result = ExamResultLogic.find.byId(examId);
		List<ExamResult> examResults = ExamResultLogic.getByUser(student);
		return ok(printExamByUser.render(student,result,examResults));
	}
}