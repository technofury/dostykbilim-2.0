package controllers.examination;

import static play.data.Form.form;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import models.entities.education.Topic;
import models.entities.examination.Answer;
import models.entities.examination.LessonAnswer;
import models.logic.examination.LessonAnswerLogic;
import play.data.DynamicForm;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.examination.answersList;
import views.html.examination.uploadVideo;

public class Answers extends Controller {

	public static Result uploadVideo(long lessonAnswerId) {
		return ok(uploadVideo.render(LessonAnswerLogic.getById(lessonAnswerId)));
	}

	public static Result showByLessonAnswer(long id) {
		LessonAnswer lessonAnswer = LessonAnswerLogic.getById(id);
		List<Answer> answers = lessonAnswer.answers;
		Collections.sort(answers);
		List<Topic> topics = Topic.findByLesson(lessonAnswer.lesson);
		return ok(answersList.render(answers, id, topics));
	}

	public static Result save() {
		DynamicForm form = form().bindFromRequest();
		long lessonAnswerId = Long.parseLong(form.field("lessonAnswerId")
				.value());
		LessonAnswer lessonAnswer = LessonAnswerLogic.getById(lessonAnswerId);

		map: for (Map.Entry<String, String> entry : form.data().entrySet()) {

			if (entry.getKey().startsWith("topic")) {
				String topic = entry.getValue();
				String[] s = entry.getKey().split("n");
				int questionNumber = Integer.parseInt(s[1]);
				String answerCode = "";
				for (Map.Entry<String, String> entry2 : form.data().entrySet()) {
					String[] ss = entry2.getKey().split("n");
					if (!ss[0].equals("q"))
						continue;
					if (ss[1].equals(s[1])) {
						answerCode = entry2.getValue();
						break;
					}
				}
				if (lessonAnswer.answers != null) {
					for (Answer answer : lessonAnswer.answers) {
						if (answer.questionNumber == questionNumber) {
							answer.answerCode = answerCode;
							answer.topic = topic;
							answer.save();
							answer.lessonAnswer.save();
							answer.lessonAnswer.answerKey.save();
							continue map;
						}
					}
				} else {
					lessonAnswer.answers = new ArrayList<Answer>();
				}
				Answer answer = new Answer();
				answer.answerCode = answerCode;
				answer.questionNumber = questionNumber;
				answer.topic = topic;
				answer.lessonAnswer = lessonAnswer;
				answer.save();
				lessonAnswer.answers.add(answer);
				lessonAnswer.save();
				lessonAnswer.answerKey.save();
			}
		}
		return redirect(request().getHeader("referer"));
	}

	public static Result delete(int questionNumber, long lessonAnswerId) {
		LessonAnswer lessonAnswer = LessonAnswerLogic.getById(lessonAnswerId);

		if (lessonAnswer.answers != null) {

			for (Answer answer : lessonAnswer.answers) {
				if (answer.questionNumber == questionNumber) {
					answer.delete();
					lessonAnswer.save();
					break;
				}
			}

		}
		return ok();
	}
}