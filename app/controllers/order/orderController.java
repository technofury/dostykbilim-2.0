package controllers.order;

import be.objectify.deadbolt.java.actions.Group;
import be.objectify.deadbolt.java.actions.Restrict;
import models.entities.social.Dostyk;
import models.entities.social.Firm;
import models.order.*;
import net.sf.ehcache.search.expression.Or;
import play.mvc.Http.MultipartFormData;
import play.mvc.Http.MultipartFormData.FilePart;
import play.data.DynamicForm;
import play.mvc.Controller;
import play.mvc.Result;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import be.objectify.deadbolt.java.actions.Group;
import be.objectify.deadbolt.java.actions.Restrict;
import models.entities.social.Dostyk;
import models.entities.user.AuthorisedUser;
import models.logic.social.DostykLogic;
import models.order.*;
import play.data.Form;
import sun.util.calendar.BaseCalendar;
import views.html.orders.*;

import static play.data.Form.form;

/**
 * Created by Бахитжан on 11.04.2017.
 */

public class orderController extends Controller {

    @Restrict({@Group("admin")})
    public static Result Categories(){
        List<Category> category = Category.find.orderBy("name").findList();
        List<Suplier> supliers = Suplier.find.orderBy("name").findList();
        List<Dostyk> dostyks = Dostyk.find.where().order("name").findList();
        List<Firm> firms = Firm.find.where().findList();
        return ok(views.html.orders.settings.render(category,supliers,dostyks,firms));
    }

    @Restrict({@Group("admin")})
    public static Result addCategory(String name){
        int count = Category.find.where().eq("name",name).findRowCount();
        if(count ==0 ) {
            Category category = new Category();
            category.name = name;
            category.save();
            return ok(category.id + "");
        }else{
            return ok("exists");
        }
    }

    @Restrict({@Group("admin")})
    public static Result deleteCategory(Long id){
        Category category = Category.find.byId(id);
        int count = Orders.find.where().eq("category",category).findRowCount();
        if(count == 0){
            category.delete();
            return ok("deleted");
        }else{
            return ok("exists");
        }


    }

    @Restrict({@Group("admin")})
    public static Result updateCategory(Long id, String name){
        Category category = Category.find.byId(id);
        category.name = name;
        category.update();
        return ok();
    }

    @Restrict({@Group("admin")})
    public static Result save(Long suplierId,Boolean isNew){
        try{
            DynamicForm form = form().bindFromRequest();
            Suplier object;
            if(isNew==true) {
                object = new Suplier();
            }else{
                object = Suplier.find.byId(suplierId);
            }
                String oldName = object.name;
                object.address = form.get("address");
                object.bank = form.get("bank");
                object.bik = form.get("bik");
                object.bin = form.get("bin");
                object.city = form.get("city");
                object.director = form.get("director");
                object.iik = form.get("iik");
                object.kbe = form.get("kbe");
                object.name = form.get("name");
                object.phone = form.get("phone");
            if(Suplier.find.where().eq("name",form.get("name")).findRowCount() > 0 && isNew){
                return ok("name_already_exists");
            }else if(!isNew && Suplier.find.where().eq("name",form.get("name")).ne("id",suplierId).findRowCount() >0) {
                return ok("name_already_exists");
            }else{
                object.save();
                return ok(object.id + "");
            }
        }catch(Exception e){
            e.printStackTrace();
            return ok("exception");
        }
    }

    @Restrict({@Group("admin")})
    public static Result delete(Long supplierId){
        try{
            Suplier suplier = Suplier.find.byId(supplierId);
            suplier.delete();
            return ok("success");
        }catch(Exception e){
            e.printStackTrace();
            return ok("exception");
        }
    }

    @Restrict({@Group("admin")})
    public static Result addOrder(){

        List<Category> categoryList = Category.find.all();
        List<OrderStatus> statusList = OrderStatus.find.all();
        List<Orders> orders = Orders.find.where().orderBy("id desc").findList();
        return ok(views.html.orders.addOrder.render(orders,categoryList,statusList));
    }

    @Restrict({@Group("admin")})
    public static Result deleteOrder(Long id){
        Orders orders = Orders.find.byId(id);
        orders.delete();
        return ok();
    }

    @Restrict({@Group("admin")})
    public static Result editOrder(Long id){
        List<Category> categoryList = Category.find.all();
        List<OrderStatus> statusList = OrderStatus.find.all();
        Orders order = Orders.find.where().eq("id",id).findUnique();
        return ok(views.html.orders.editOrder.render(order,categoryList,statusList));
    }

    @Restrict({@Group("admin")})
    public static Result saveOrder(){
        DynamicForm requestData = form().bindFromRequest();
        String name = requestData.get("name");
        String date = requestData.get("date");
        String category = requestData.get("category");
        String price = requestData.get("price");
        String status = requestData.get("status");
        String comment = requestData.get("comment");
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date d = new Date();
        try {
            d = formatter.parse(date);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        Category cat = Category.find.byId(Long.parseLong(category));
        OrderStatus orderStatus = OrderStatus.find.byId(Long.parseLong(status));
        Orders orders = new Orders();
        orders.name = name;
        orders.date = d;
        orders.category = cat;
        orders.price = Integer.parseInt(price);
        orders.orderStatus = orderStatus;
        orders.description = comment;
        orders.save();

        MultipartFormData body = request().body().asMultipartFormData();
        FilePart fileP = body.getFile("file");
        String fileName = "";
        if (fileP != null) {
            fileName = fileP.getFilename();
            String contentType = fileP.getContentType();
            File file = fileP.getFile();

            String direcory = play.Play.application().path().getAbsolutePath() + "\\orderimage\\";
            File layoutPackage = new File(direcory);
            if(!layoutPackage.exists()){
                layoutPackage.mkdirs();
            }
            String path = play.Play.application().path().getAbsolutePath() + "\\orderimage\\"+ orders.id+"-" + fileName;
            file.renameTo(new File(path));
            orders.image = orders.id+"-"+fileName;
        }

        orders.save();

        return ok(orders.id.toString());

    }

    @Restrict({@Group("admin")})
    public static Result updateOrder(){
        DynamicForm requestData = form().bindFromRequest();
        String name = requestData.get("name");
        String date = requestData.get("date");
        String category = requestData.get("category");
        String price = requestData.get("price");
        String status = requestData.get("status");
        String comment = requestData.get("comment");
        String id = requestData.get("id");

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-mm-dd");
        Date d = new Date();
        try {
            d = formatter.parse(date);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        Category cat = Category.find.byId(Long.parseLong(category));
        OrderStatus orderStatus = OrderStatus.find.byId(Long.parseLong(status));
        Orders orders = Orders.find.byId(Long.parseLong(id));
        orders.name = name;
        orders.date = d;
        orders.category = cat;
        orders.price = Integer.parseInt(price);
        orders.orderStatus = orderStatus;
        orders.description = comment;


        MultipartFormData body = request().body().asMultipartFormData();
        FilePart fileP = body.getFile("file");
        String fileName = "";

        if (fileP != null) {
            fileName = fileP.getFilename();
            String contentType = fileP.getContentType();
            File file = fileP.getFile();

            String direcory = play.Play.application().path().getAbsolutePath() + "\\orderimage\\";
            File layoutPackage = new File(direcory);
            if(!layoutPackage.exists()){
                layoutPackage.mkdirs();
            }
            String path = play.Play.application().path().getAbsolutePath() + "\\orderimage\\"+ orders.id+"-" + fileName;
            file.renameTo(new File(path));

        }

        if(!fileName.equals("")){
            orders.image = orders.id+"-"+fileName;
        }

        orders.update();

        return ok("good");

    }

    @Restrict({@Group("admin")})
    public static Result getImage(String fileName) throws IOException {
        BufferedImage image = ImageIO.read(new File(play.Play.application().path().getAbsolutePath() + "\\orderimage\\"+ fileName));
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(image, "jpg", baos);
        return ok(baos.toByteArray()).as("image/jpg");
    }

    @Restrict({@Group("admin")})
    public static Result showOrderCount(Long id){
        Orders order = Orders.find.byId(id);
        List<OrderCount> orderCounts = OrderCount.find.where().eq("orders",order).orderBy("dostyk.name").findList();
        return ok(views.html.orders.showOrderCount.render(orderCounts,order.price));
    }

    @Restrict({@Group("admin")})
    public static Result updateOrderAmount(Long id, int amount){
        OrderCount orderCount = OrderCount.find.byId(id);
        orderCount.count = amount;
        orderCount.update();
        return ok();
    }

    @Restrict({@Group("admin")})
    public static Result debtors(){
        List<Dostyk> dostyk = Dostyk.find.all();
        List<Debt> debtList = Debt.find.all();

        if(dostyk.size() > 0){
            for(int i = 0; i < dostyk.size(); i++){
                Debt dostyksDebt = new Debt();
                List<OrderCount> orderCounts = OrderCount.find.where().eq("dostyk_id", dostyk.get(i).id).orderBy("dostyk.name").findList();
                int debt = 0;
                if(orderCounts.size() > 0){
                    for(int k = 0; k < orderCounts.size(); k++){
                        debt = debt + orderCounts.get(k).count * orderCounts.get(k).orders.price;
                    }

                    int payments = 0;
                    List<OrderPayments> orderPayments = OrderPayments.find.where().eq("dostyk_id", dostyk.get(i).id).findList();
                    if(orderPayments.size()>0){
                        for(int k = 0; k < orderPayments.size(); k++){
                            payments = payments + orderPayments.get(k).amount;
                        }
                    }

                    int total = debt - payments;
                    dostyksDebt.dostyk = dostyk.get(i);
                    dostyksDebt.debt = total;
                    debtList.add(dostyksDebt);
                }

            }
        }

        return ok(views.html.orders.debtors.render(debtList));
    }

    @Restrict({@Group("admin")})
    public static Result showDostykPayments(Long id){
        List<OrderPayments> orderPayments = OrderPayments.find.where().eq("dostyk_id",id).orderBy("date desc").findList();
        Dostyk dostyk = Dostyk.find.byId(id);
        return ok(views.html.orders.payments.render(orderPayments,dostyk.name, dostyk.id));
    }

    @Restrict({@Group("admin")})
    public static Result savePayment(){
        DynamicForm requestData = form().bindFromRequest();
        String date = requestData.get("date");
        String price = requestData.get("price");
        String id = requestData.get("id");
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date d = new Date();
        try {
            d = formatter.parse(date);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        OrderPayments orderPayments = new OrderPayments();
        orderPayments.date = d;
        orderPayments.amount = Integer.parseInt(price);
        orderPayments.dostyk = Dostyk.find.byId(Long.parseLong(id));
        orderPayments.save();

        return ok(orderPayments.id.toString());

    }

    @Restrict({@Group("admin")})
    public static Result orderReport(){
        List<Category> categoryList = Category.find.where().orderBy("name").findList();
        return ok(views.html.orders.orderReport.render(categoryList));
    }

    @Restrict({@Group("admin")})
    public static Result getOrdersByCategory(Long id){
        Category category = Category.find.byId(id);
        List<Orders> orders = Orders.find.where().eq("category",category).orderBy("date desc").findList();
        return ok(views.html.orders.orderReportTable.render(orders)).as("text/html");

    }

    @Restrict({@Group("admin")})
    public static Result paymentReport(){
        List<Category> categoryList = Category.find.all();
        return ok(views.html.orders.paymentReport.render(categoryList));
    }

    @Restrict({@Group("admin")})
    public static Result getPaymentReport(String start,String end){
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date startDate = null;
        Date endDate = null;
        try {
            startDate = format.parse(start);
            endDate = format.parse(end);
        } catch (ParseException e) {

        }

        List<OrderPayments> orderPayments = OrderPayments.find.where().between("date",startDate,endDate).orderBy("date desc").findList();
        return ok(views.html.orders.paymentReportTable.render(orderPayments)).as("text/html");
    }

    @Restrict({@Group("admin")})
    public static Result saveFirm(Long id,Long firmId){
        Dostyk dostyk = Dostyk.find.byId(id);
        dostyk.mainFirmId = firmId.toString();
        dostyk.save();
        return ok();

    }

    public boolean checkOrderDate(){

        List<Orders> orders = Orders.find.findList();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); //For declaring values in new date objects. use same date format when creating dates
        Date today = new Date();


        for(int i = 0; i< orders.size(); i++){

            if(today.after(orders.get(i).date)){
                String todayStr = sdf.format(today);
                String DateStr = sdf.format(orders.get(i).date);
                if(!todayStr.equals(DateStr)){
                    if(orders.get(i).orderStatus.id == 1){

                        Orders o = Orders.find.byId(orders.get(i).id);
                        OrderStatus os = OrderStatus.find.byId(2L);
                        o.orderStatus = os;
                        o.save();

                        List<Dostyk> dostyk = Dostyk.find.all();
                        for(int k = 0; k < dostyk.size(); k++){
                            if(OrderCount.find.where().eq("orders",o).eq("dostyk",dostyk.get(k)).findRowCount() == 0){
                                int defValue = 0;
                                if(DefaultOrderValue.find.where().eq("dostyk",dostyk.get(k)).findRowCount() != 0){
                                    defValue = DefaultOrderValue.find.where().eq("dostyk",dostyk.get(k)).findUnique().value;

                                }
                                OrderCount oc = new OrderCount();
                                oc.orders = o;
                                oc.dostyk = dostyk.get(k);
                                oc.count = defValue;
                                oc.save();;
                            }

                        }
                    }

                }
            }
        }

        return true;
    }

    //////////////////////////////////////////////////

    @Restrict({@Group("director")})
    public static Result viewAll(){
        AuthorisedUser user = AuthorisedUser.getUserByEmail(session("connected"));
        Dostyk dostyk = DostykLogic.getByUser(user);

        List<Orders> ordersList = Orders.find.all();
        List<OrderCount> orderCountList = OrderCount.find.where().eq("dostyk",dostyk).findList();

        int n = 0;
        if (DefaultOrderValue.find.where().eq("dostyk",dostyk).findRowCount() > 0){
            n = DefaultOrderValue.find.where().eq("dostyk",dostyk).findUnique().value;
        }

        return ok(viewAllOrder.render(ordersList, orderCountList, n));
    }
    @Restrict({@Group("director")})
    public static Result setDefaultValue(){
        AuthorisedUser user = AuthorisedUser.getUserByEmail(session("connected"));
        Dostyk dostyk = DostykLogic.getByUser(user);

        int n = -1;
        if (DefaultOrderValue.find.where().eq("dostyk",dostyk).findRowCount() > 0){
            n = DefaultOrderValue.find.where().eq("dostyk",dostyk).findUnique().value;
        }

                return ok(defaultOrderValue.render(n));
    }
    @Restrict({@Group("director")})
    public static Result saveDefaultValue(){
                DynamicForm requestData = form().bindFromRequest();
                String name = requestData.get("orderValue");

                 AuthorisedUser user = AuthorisedUser.getUserByEmail(session("connected"));
                Dostyk dostyk = DostykLogic.getByUser(user);
                int v = Integer.parseInt(name);
                DefaultOrderValue orderValue = DefaultOrderValue.find.where().eq("dostyk",dostyk).findUnique();
                if (orderValue != null){
                       orderValue.value = v;
                        orderValue.update();
                    }else{
                        orderValue = new DefaultOrderValue();
                        orderValue.value = v;
                        orderValue.dostyk = dostyk;
                        orderValue.save();
                    }
                return ok("good");
            }
    @Restrict({@Group("director")})
    public static Result saveOrderCount(){
                DynamicForm requestData = form().bindFromRequest();


                String orderId = requestData.get("orderId");
                String orderCount = requestData.get("orderCount");

                if (orderCount!=null && orderId!=null) {
                    AuthorisedUser user = AuthorisedUser.getUserByEmail(session("connected"));
                    Dostyk dostyk = DostykLogic.getByUser(user);
                    int v = Integer.parseInt(orderCount);
                    Long id = Long.parseLong(orderId);
                    Orders order = Orders.find.byId(id);
                    OrderCount count = OrderCount.getByOrderAndDostyk(order, dostyk);
                    if (count != null) {
                        count.count = v;
                        count.update();
                    } else {
                        count = new OrderCount();
                        count.orders = order;
                        count.dostyk = dostyk;
                        count.count = v;
                        count.save();
                    }
                }
                return ok("good");
            }

    @Restrict({@Group("director")})
    public static Result viewDostykOrderAmount() throws ParseException {
                AuthorisedUser user = AuthorisedUser.getUserByEmail(session("connected"));
                Dostyk dostyk = DostykLogic.getByUser(user);
                List<Orders> ordersList = Orders.find.all();
                List<OrderPayments> orderPaymentsList = OrderPayments.getByDostyk(dostyk);
                List<OrderPayments> orderPayments = OrderPayments.getByDostyk(dostyk);
                List<OrderCount> orderCounts = OrderCount.getByDostyk(dostyk);
                orderPayments.clear();
                DynamicForm form = Form.form().bindFromRequest();
                String date1 = form.get("date1");
                String date2 = form.get("date2");
                int totalSum = 0;
                int totalPayment = 0;
                if (date1!= null){
                        Date d1 = new SimpleDateFormat("yyyy-MM-dd").parse(date1);
                        Date d2 = new SimpleDateFormat("yyyy-MM-dd").parse(date2);
                        for (OrderPayments payment : orderPaymentsList){
                                if (payment.date.getTime()>=d1.getTime() && payment.date.getTime()<=d2.getTime()){
                                        orderPayments.add(payment);
                                    }

                                        totalPayment+= payment.amount;
                            }
                        for(Orders order: ordersList){
                                for(OrderCount count: orderCounts){
                                        if(order.id==count.orders.id){
                                                totalSum+= order.price*count.count;
                                            }
                                    }
                            }
                        return ok(viewDostykOrder.render( orderPayments, totalPayment, totalSum));

                            }else{
                        orderPayments=null;
                        return ok(viewDostykOrder.render( orderPayments, totalPayment, totalSum));
                    }

                    }

}
