package controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLEncoder;

import models.entities.education.Topic;
import models.entities.examination.Answer;
import models.entities.examination.LessonAnswer;
import models.logic.examination.LessonAnswerLogic;
import play.mvc.Controller;
import play.mvc.Result;

import com.google.common.io.Files;

public class ServeFiles extends Controller {

	public static Result video(String id) {

		if(id.contains(".mp4"))
			id = id.substring(0,id.length()-4);
		Answer a = Answer.find.byId(Long.parseLong(id));
		/*File file = new File(Application.getRootPath() + "/FILES/"
				+ a.pathToVideo);

		String[] range = request().headers().get("Range");
		// System.err.println(range);
		if (range == null) {
			return ok(file);
		}
		return videoStream(range[0], file);*/
		return redirect(a.pathToVideo);
	}

	public static Result videoStream(String range, File videoFile) {
		try {
			String[] splitRange = range.substring("bytes=".length()).split("-");
			long bytesToSkip = Long.parseLong(splitRange[0]);
			/*
			 * if(bytesToSkip == 0) { return ok(videoFile); }
			 */
			// System.err.println(bytesToSkip);
			FileInputStream stream = new FileInputStream(videoFile);
			stream.skip(bytesToSkip);
			long totalBytes = videoFile.length();
			response().getHeaders().put(
					CONTENT_RANGE,
					String.format("bytes %d-%d/%d", bytesToSkip,
							totalBytes - 1, totalBytes));
			response().setHeader(ACCEPT_RANGES, "bytes");
			response().setHeader(CONNECTION, "keep-alive");
			return status(PARTIAL_CONTENT, stream);
		} catch (IOException e) {
			e.printStackTrace();
			return notFound();
		}
	}

	public static Result downloadTest(long id) {
		Topic t = Topic.find.byId(id);
		System.setProperty("file.encoding", "UTF-8");
		java.io.File to = null;
		try {
			String fileName = Application.getRootPath() + "/FILES/"
					+ t.pathToFile;
			java.io.File from = new java.io.File(fileName);
			fileName = Application.getRootPath() + "/FILES/download/"
					+ t.fileName;
			to = new java.io.File(fileName);
			new java.io.File(Application.getRootPath() + "/FILES/download/")
			.mkdirs();
			if (to.exists()) {
				to.delete();
			}
			response().setHeader(
					"Content-Disposition",
					"attachment; filename="
							+ URLEncoder.encode(t.fileName, "UTF-8"));
			Files.copy(from, to);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ok(to);
	}

}