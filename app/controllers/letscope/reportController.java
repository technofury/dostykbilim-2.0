package controllers.letscope;

import models.entities.user.AuthorisedUser;
import play.mvc.Controller;
import play.mvc.Result;

/**
 * Created by Бахитжан on 11.04.2017.
 */

public class reportController extends Controller {

    public static Result getExamList(){
        return ok(views.html.exam.examReport.render(session("connected")));
    }
    public static Result getStudentExamList(){
        String studID = AuthorisedUser.getStudentId();
        return ok(views.html.exam.examStudentReport.render(studID));
    }

    public static Result printPdf(String id){
        String examId = id;
        return ok(views.html.exam.printPdf.render(examId));
    }

    public static Result printAllPdf(String id){
        String examId = id;
        return ok(views.html.exam.printAllPdf.render(examId));
    }

}
