package controllers.education;

import static play.data.Form.form;

import java.util.List;

import models.entities.education.CourseType;
import models.logic.education.CourseTypeLogic;
import play.data.Form;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.education.courseTypes;
import views.html.education.editCourseType;
import views.html.helps.message;

public class CourseTypes extends Controller {
	final static Form<CourseType> dForm = form(CourseType.class);

	public static Result showCourseTypes() {
		List<CourseType> dList = CourseTypeLogic.getAll();
		return ok(courseTypes.render(dList, dForm));
	}

	public static Result getByName(String name) {
		String course = "";
		if(CourseTypeLogic.getByName(name) != null){
			course = CourseTypeLogic.getByName(name).classNumber+"";
		}
		return ok(course);
		//return  CourseTypeLogic.getByName(name).classNumber;
	}

	public static Result saveCourseType() {
		Form<CourseType> filledForm = dForm.bindFromRequest();
		if (filledForm.field("classNumber").valueOr("").isEmpty()) {
			filledForm.reject("classNumber",
					Messages.get("CourseType.emptyNumber"));
		}
		if (filledForm.field("code").valueOr("").isEmpty()
				|| filledForm.field("code").value().length() != 1) {
			filledForm.reject("code", Messages.get("code.incorrect"));
		}
		if (filledForm.field("name").valueOr("").isEmpty()) {
			filledForm.reject("name", Messages.get("CourseType.emptyName"));
		} else if (CourseTypeLogic.getByName(filledForm.field("name").value()) != null) {
			filledForm.reject("name", Messages.get("CourseType.usedName"));
		}
		List<CourseType> dList = CourseTypeLogic.getAll();
		CourseType toSave;
		if (filledForm.hasErrors()) {
			return badRequest(courseTypes.render(dList, filledForm));
		} else {
			toSave = filledForm.get();
			toSave.classNumber = Integer.parseInt(filledForm.field(
					"classNumber").value());
			toSave.name = filledForm.field("name").value();
			toSave.code = filledForm.field("code").value().toUpperCase();
			toSave.save();
			dList = CourseTypeLogic.getAll();
			return ok(courseTypes.render(dList, dForm));
		}
	}

	public static Result deleteCourseType(Long id) {
		try {
			CourseType toDelete = CourseTypeLogic.getById(id);
			toDelete.delete();
			return ok(message.render("alert alert-success",
					"CourseType.successfullyDeleted"));
		} catch (Exception e) {
			return badRequest(message.render("alert alert-error",
					e.getMessage()));
		}
	}

	public static Result editCourseType(Long id) {
		Form<CourseType> filledForm = form(CourseType.class);
		CourseType toEdit = CourseTypeLogic.getById(id);
		return ok(editCourseType.render(filledForm.fill(toEdit), id));
	}

	public static Result updateCourseType(Long id) {
		Form<CourseType> filledForm = dForm.bindFromRequest();
		CourseType toEdit = CourseTypeLogic.getById(id);
		if (filledForm.field("classNumber").valueOr("").isEmpty()) {
			filledForm.reject("classNumber",
					Messages.get("CourseType.emptyNumber"));
		}
		if (filledForm.field("code").valueOr("").isEmpty()) {
			filledForm.reject("code", Messages.get("code.incorrect"));
		}
		if (filledForm.field("name").valueOr("").isEmpty()) {
			filledForm.reject("name", Messages.get("CourseType.emptyName"));
		} else if (CourseTypeLogic.getByName(filledForm.field("name").value()) != null) {
			if (!filledForm.field("name").value().equals(toEdit.name))
				filledForm.reject("name", Messages.get("CourseType.usedName"));
		}
		List<CourseType> dList = CourseTypeLogic.getAll();
		CourseType toSave;
		if (filledForm.hasErrors()) {
			return badRequest(editCourseType.render(filledForm, id));
		} else {
			toSave = toEdit;
			toSave.classNumber = Integer.parseInt(filledForm.field(
					"classNumber").value());
			toSave.name = filledForm.field("name").value();
			toSave.code = filledForm.field("code").value().toUpperCase();
			toSave.save();
			dList = CourseTypeLogic.getAll();
			return ok(courseTypes.render(dList, dForm));
		}
	}

}
