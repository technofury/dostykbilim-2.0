package controllers.education;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import models.entities.education.ExtraJournal;
import models.entities.education.ExtraLesson;
import models.entities.education.Lesson;
import models.entities.education.Register;
import models.entities.education.Topic;
import models.entities.education.TopicRegister;
import models.entities.social.Dostyk;
import models.entities.social.DostykGroup;
import models.entities.user.AuthorisedUser;
import models.logic.education.LessonLogic;
import models.logic.education.RegisterLogic;
import models.logic.social.DostykGroupLogic;
import models.logic.social.DostykLogic;
import models.logic.user.AuthorisedUserLogic;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.education.extraBody;
import views.html.education.extraMain;
import views.html.education.reportByTeacher;
import views.html.helps.extraLessons;
import views.html.helps.teachers;
import views.html.helps.teachersByLesson;

public class ExtraJournals extends Controller {

	public static Result extraLessonsByTeacher(long teacherId) {
		List<ExtraLesson> list = ExtraLesson.by(teacherId);
		return ok(extraLessons.render(list,
				AuthorisedUser.getUserById(teacherId)));
	}

	public static Result teachersByDostyk(long dostykId) {
		List<AuthorisedUser> ts = AuthorisedUser
				.getTeachersByDostyk(DostykLogic.getById(dostykId));
		List<Integer> counts = new ArrayList<Integer>();
		for (AuthorisedUser teacher : ts)
			counts.add(ExtraLesson.countByTeacher(teacher));
		return ok(teachers.render(ts, counts));
	}

	public static Result getTeachers(long groupId, long lessonId) {
		DostykGroup dg = DostykGroupLogic.getById(groupId);
		// Lesson lesson = LessonLogic.getById(lessonId);
		/*if (dg.type == "E") {
			List<AuthorisedUser> teachers = new ArrayList<AuthorisedUser>();
			teachers.add(dg.curator);
			return ok(teachersByLesson.render(teachers));
		}*/
		List<AuthorisedUser> teachers = AuthorisedUserLogic
				.teachersByLessonAndDostyk(lessonId, dg.dostyk);
		return ok(teachersByLesson.render(teachers));
	}

	public static Result reportByTeacher() {
		AuthorisedUser user = AuthorisedUser
				.getUserByEmail(session("connected"));
		List<Dostyk> dostyks = null;
		List<AuthorisedUser> teachers = new ArrayList<AuthorisedUser>();
		List<Integer> counts = new ArrayList<Integer>();
		if (user.roles.get(0).name.equals("admin")) {
			dostyks = DostykLogic.getAll();
			for (Dostyk dostyk : dostyks) {
				counts.add(ExtraLesson.countByDostyk(dostyk));
			}
		} else {
			teachers = AuthorisedUser.getTeachersByDostyk(user.dostyks.get(0));
			for (AuthorisedUser teacher : teachers) {
				counts.add(ExtraLesson.countByTeacher(teacher));
			}
		}
		return ok(reportByTeacher.render(dostyks, teachers, counts));
	}

	public static Result teachersByLesson(long id) {
		AuthorisedUser user = AuthorisedUser
				.getUserByEmail(session("connected"));
		List<AuthorisedUser> list = null;
		if (user.roles.get(0).name.equals("admin")) {
			list = AuthorisedUserLogic.teachersByLesson(id);
		} else {
			list = AuthorisedUserLogic.teachersByLessonAndDostyk(id,
					user.dostyks.get(0));
		}
		return ok(teachersByLesson.render(list));
	}

	/*
	 * public static Result getRegisters(long lessonId, long teacherId) {
	 * List<Timetable> timetables = TimetableLogic.getByFields(lessonId,
	 * teacherId); List<Register> registers =
	 * RegisterLogic.getByTimetables(timetables, teacherId); return
	 * ok(reportByTeacherBody.render(registers)); }
	 */

	public static Result main() {
		return ok(extraMain.render());
	}

	public static Result saveMarks(String users, String marks, String which,
			String strDate) throws Exception {
		String[] uArr = users.split(" ");
		String[] mArr = marks.split(" ");
		String[] wArr = which.split(" ");
		Date date = new SimpleDateFormat("yyyy-MM-dd").parse(strDate);
		AuthorisedUser u = null;
		for (int i = 1; i < uArr.length; i++) {

			if (!uArr[i].equals(uArr[i - 1]))
				u = AuthorisedUser.getUserById(Long.parseLong(uArr[i]));

			ExtraJournal cur = ExtraJournal.getBy(u, date);
			if (cur == null) {
				cur = new ExtraJournal();
				cur.user = u;
				cur.date = date;
			}

			if (wArr[i].equals("kaz"))
				cur.kaz = Integer.parseInt(mArr[i]);
			if (wArr[i].equals("rus"))
				cur.rus = Integer.parseInt(mArr[i]);
			if (wArr[i].equals("math"))
				cur.math = Integer.parseInt(mArr[i]);
			if (wArr[i].equals("history"))
				cur.history = Integer.parseInt(mArr[i]);
			if (wArr[i].equals("selected"))
				cur.selected = Integer.parseInt(mArr[i]);

			cur.save();
		}
		return ok();
	}

	public static Result getByLessonAndGroup(long lessonId, long groupId,
			String strDate, long teacherId) throws Exception {
		Lesson lesson = LessonLogic.getById(lessonId);
		List<Topic> tops = Topic.findByLesson(lesson);
		DostykGroup group = DostykGroupLogic.getById(groupId);
		AuthorisedUser teacher = AuthorisedUser.getUserById(teacherId);
		Date date = new SimpleDateFormat("yyyy-MM-dd").parse(strDate);
		/*
		 * List<Timetable> timetables; if (lessonId != -1) { timetables =
		 * TimetableLogic.getByFieldsAndExtra(lesson, group, date.getDay()); }
		 * else { timetables = TimetableLogic.getExtra(group.id, date.getDay());
		 * }
		 *
		 * if (timetables==null || timetables.size()==0) { return
		 * ok("there is no such lesson on " + strDate); }
		 */

		List<AuthorisedUser> students = AuthorisedUser.getByDostykGroup(group);

				List<String> attendance = new ArrayList<String>();

				new ArrayList<Integer>();
				new ArrayList<Integer>();

				List<ExtraJournal> marks = new ArrayList<ExtraJournal>();

				for (AuthorisedUser u : students) {
					boolean added = false;
					// for (Timetable t : timetables) {
					Register r = RegisterLogic.getByFields(u, lesson, date, teacher);
					if (r != null) {
						attendance.add(r.attendance);
						added = true;
						// break;
					}
					// }
					marks.add(ExtraJournal.getBy(u, date));
					if (!added)
						attendance.add("present");
				}

				TopicRegister tr = TopicRegister.findByFields(group, date, lesson);

				return ok(extraBody.render(tops, students, attendance, marks, tr));
	}

	public static Result save(String states, String users, long lessonId,
			long groupId, String strDate, long teacherId, long topicId)
					throws Exception {
		Lesson lesson = LessonLogic.getById(lessonId);
		DostykGroup group = DostykGroupLogic.getById(groupId);
		Date date = new SimpleDateFormat("yyyy-MM-dd").parse(strDate);
		AuthorisedUser teacher = AuthorisedUserLogic.getById(teacherId);
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		//TimetableLogic.getByFieldsAndExtra(lesson, group,c.get(Calendar.DAY_OF_WEEK));

		String[] stateArr = states.split(" ");
		String[] userArr = users.split(" ");

		int i = 0;
		for (String uId : userArr) {
			// for (Timetable t : timetables) {
			if (i == 0) {
				i++;
				continue;
			}
			AuthorisedUser u = AuthorisedUser.getUserById(Long.parseLong(uId));
			Register r = RegisterLogic.getByFields(u, lesson, date, teacher);
			if (r != null) {
				r.attendance = stateArr[i];
			} else {
				r = new Register();
				r.attendance = stateArr[i];
				r.student = u;
				r.teacher = teacher;
				r.date = date;
				r.lessonid = lessonId;
			}

			r.save();
			// }
			i++;
		}
		Topic topic = null;
		if (topicId != -1)
			topic = Topic.find.byId(topicId);
		ExtraLesson extraLesson = ExtraLesson.by(teacher, group, lesson, date);
		if (extraLesson == null) {
			extraLesson = new ExtraLesson();
			extraLesson.date = date;
			extraLesson.dostykGroup = group;
			extraLesson.lesson = lesson;
			extraLesson.teacher = teacher;
			extraLesson.topic = topic;
		} else {
			extraLesson.topic = topic;
		}
		extraLesson.save();
		/*
		 * for (Timetable t : timetables) { Register r =
		 * RegisterLogic.getByFields(t.teacher, t, date); if (r != null) {
		 * r.attendance = "present"; } else { r = new Register(); r.attendance =
		 * "present"; r.timetable = t; r.student = t.teacher; r.date = date;
		 * r.lessonid = lessonId; } r.save(); }
		 */

		return ok();
	}
}