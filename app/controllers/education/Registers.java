package controllers.education;

import static play.data.Form.form;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map.Entry;
import models.timetable.Timetable;
import models.entities.education.Lesson;
import models.entities.education.Register;
import models.entities.education.Topic;
import models.entities.education.TopicRegister;
import models.entities.examination.ATTUTT;
import models.entities.social.DostykGroup;
import models.entities.user.AuthorisedUser;
import models.logic.education.LessonLogic;
import models.logic.education.RegisterLogic;
import models.logic.social.DostykGroupLogic;
import play.data.DynamicForm;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.education.attendanceList;
import views.html.education.attendancePage;
import views.html.education.registerBody;
import views.html.education.registerMain;
import views.html.education.userAttendance;
import views.html.helps.groups2;
import views.html.user.profilePack.userAttendanceTab;

public class Registers extends Controller {

	public static Result getGroups(long lessonId) {
		AuthorisedUser u = AuthorisedUser.getUserByEmail(play.mvc.Controller
				.session("connected"));

		if (lessonId == -1) {
			return ok(groups2.render(DostykGroupLogic.getNotElectives(u.dostyks
					.get(0))));
		}

		Lesson lesson = LessonLogic.getById(lessonId);

		if (lesson.code.compareTo("05") > 0) {
			return ok(groups2.render(DostykGroupLogic.getElectives(
					u.dostyks.get(0), lesson)));
		} else {
			return ok(groups2.render(DostykGroupLogic.getNotElectives(u.dostyks
					.get(0))));
		}
	}

	public static Result save2(String states, String users, long lessonId,
			long groupId, String strDate) throws Exception {
		Lesson lesson = LessonLogic.getById(lessonId);
		DostykGroup group = DostykGroupLogic.getById(groupId);
		Date date = new SimpleDateFormat("yyyy-MM-dd").parse(strDate);
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		List<Timetable> timetables = Timetable.byGrade( group);

		String[] stateArr = states.split(" ");
		String[] userArr = users.split(" ");

		int i = 0;
		for (String uId : userArr) {
			for (Timetable t : timetables) {
				if (i == 0)
					break;
				AuthorisedUser u = AuthorisedUser.getUserById(Long
						.parseLong(uId));
				Register r = RegisterLogic.getByFields(u, t, date);
				if (r != null) {
					r.attendance = stateArr[i];
				} else {
					r = new Register();
					r.attendance = stateArr[i];
					r.timetable = t;
					r.student = u;
					r.date = date;
					r.lessonid = lessonId;
				}

				r.save();
			}
			i++;
		}

		return ok();
	}

	public static Result getByLessonAndGroup(long lessonId, long groupId,
			String strDate) throws Exception {
		Lesson lesson = LessonLogic.getById(lessonId);
		List<Topic> tops = Topic.findByLesson(lesson);
		DostykGroup group = DostykGroupLogic.getById(groupId);

		Date date = new SimpleDateFormat("yyyy-MM-dd").parse(strDate);
		
		Calendar c = Calendar.getInstance();
		c.setTime(date);

		List<Timetable> timetables = Timetable.byGrade( group);

		if (timetables == null || timetables.size() == 0) {
			return ok("there is no such lesson on " + strDate);
		}

		List<AuthorisedUser> students = AuthorisedUser.getByDostykGroup(group);

				List<String> attendance = new ArrayList<String>();

				List<Integer> att = new ArrayList<Integer>();
				List<Integer> utt = new ArrayList<Integer>();

				for (AuthorisedUser u : students) {
					boolean added = false;
					for (Timetable t : timetables) {
						Register r = RegisterLogic.getByFields(u, t, date);
						if (r != null) {
							attendance.add(r.attendance);
							added = true;
							break;
						}
					}

					ATTUTT a = ATTUTT.getByFields(lessonId, u.id, 1, "ATT");
					if (a == null)
						att.add(0);
					else
						att.add(a.mark);

					a = ATTUTT.getByFields(lessonId, u.id, 1, "UTT");
					if (a == null)
						utt.add(0);
					else
						utt.add(a.mark);

					if (!added)
						attendance.add("present");
				}

				TopicRegister tr = TopicRegister.findByFields(group, date, lesson);
				AuthorisedUser teacher = null; //timetables.get(0).teacher;
				return ok(registerBody.render(tops, students, attendance, att, utt, tr,
						teacher));
	}

	public static Result main() {
		return ok(registerMain.render());
	}

	public static Result currentDate() {
		DateFormat format = new SimpleDateFormat("dd MM yyyy", Locale.US);
		Date current = new Date();
		return showDate(format.format(current));
	}

	public static Result showDate(String date) {
		String[] splitted = date.split(" ");
		String[] weekDays = { "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun" };
		int day = -1;
		for (int i = 0; i < weekDays.length; i++)
			if (splitted[0].equals(weekDays[i])) {
				day = i + 1;
				break;
			}
		AuthorisedUser moder = AuthorisedUser.getUserByEmail(session().get(
				"connected"));
		List<Timetable> lessons = null; //TimetableLogic.getByWeekDay(day, moder);

		return ok(attendancePage.render(date, lessons));
	}

	public static Result otherDate() throws ParseException {
		DynamicForm df = form().bindFromRequest();
		Date date = new SimpleDateFormat("yyyy-MM-dd").parse(df.field("date")
				.value());
		DateFormat format = new SimpleDateFormat("EE dd MM yyyy", Locale.US);
		return showDate(format.format(date));
	}

	public static Result attendanceList(String date, long timetableId) {
		Timetable current = Timetable.byId(timetableId);
		List<AuthorisedUser> students = null;
		AuthorisedUser teacher = null;
		DateFormat format = new SimpleDateFormat("HH:mm");
		List<String> attendance = new ArrayList<String>();

		DateFormat format2 = new SimpleDateFormat("EE dd MM yyyy");
		Date dates = null;
		try {
			dates = format2.parse(date);
		} catch (ParseException e) {

		}
		Register cur = RegisterLogic.getByFields(teacher, current, dates);
		if (cur == null)
			attendance.add("present");
		else
			attendance.add(cur.attendance);
		for (AuthorisedUser user : students) {
			cur = RegisterLogic.getByFields(user, current, dates);
			if (cur == null)
				attendance.add("present");
			else
				attendance.add(cur.attendance);
		}
		//date = date + " / " + format.format(current.startTime) + "-"+ format.format(current.endTime);
		return ok(attendanceList.render(teacher, students, date, timetableId,
				attendance));
	}

	public static Result save() {
		DynamicForm data = form().bindFromRequest();
		String[] dateTime = data.field("date").value().split("/");
		String[] parsed = dateTime[0].split(" ");
		DateFormat format = new SimpleDateFormat("dd MM yyyy");
		Date date = null;
		try {
			date = format.parse(parsed[1] + " " + parsed[2] + " " + parsed[3]);
		} catch (ParseException e) {

		}
		Timetable timetable = Timetable.byId(Long.parseLong(data
				.field("timetableId").value()));
		for (Entry<String, String> entry : data.data().entrySet()) {
			if (entry.getKey().equals("timetableId")
					|| entry.getKey().equals("date"))
				continue;
			AuthorisedUser user = AuthorisedUser.getUserById(Long
					.parseLong(entry.getKey()));
			Register register = RegisterLogic
					.getByFields(user, timetable, date);
			if (register == null)
				register = new Register();
			register.student = user;
			register.timetable = timetable;
			register.date = date;
			register.attendance = entry.getValue();
			register.save();
		}

		flash("success", "Attendance saved successfully");
		return showDate(dateTime[0]);
	}

	public static Result show(long id) throws ParseException {
		AuthorisedUser user;
		if (id == -1) {
			user = AuthorisedUser.getUserByEmail(session("connected"));
		} else
			user = AuthorisedUser.getUserById(id);

		List<Register> list;
		int lastDay = Calendar.getInstance().getActualMaximum(
				Calendar.DAY_OF_MONTH);
		DateFormat format = new SimpleDateFormat("EE dd MM yyyy");
		Date current = new Date();
		String[] start = format.format(current).split(" ");
		Date first = format.parse(start[0] + " " + "01" + " " + start[2] + " "
				+ start[3]);
		Date last = format.parse(start[0] + " " + lastDay + " " + start[2]
				+ " " + start[3]);
		list = RegisterLogic.getByDates(user, first, last);
		List<Register> extras = RegisterLogic.getExtrasByUser(user);
		if (id == -1)
			return ok(userAttendance.render(user, list, extras));
		else
			return ok(userAttendanceTab.render(user, list, extras)); // if
		// moderator
		// views
		// then
		// must
		// show
		// in
		// TAB
	}

	public static Result show2(long id) throws ParseException {
		DynamicForm form = form().bindFromRequest();
		String start = form.field("start").value();
		String end = form.field("end").value();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

		AuthorisedUser user;
		if (id == -1) {
			user = AuthorisedUser.getUserByEmail(session("connected"));
		} else
			user = AuthorisedUser.getUserById(id);

		Date first = df.parse(start);
		Date last = df.parse(end);
		List<Register> list = RegisterLogic.getByDates(user, first, last);
		List<Register> extras = RegisterLogic.getExtrasByUser(user);
		if (id == -1)
			return ok(userAttendance.render(user, list, extras));
		else
			return ok(userAttendanceTab.render(user, list, extras)); // if
		// moderator
		// views
		// then
		// must
		// show
		// in
		// TAB
	}
	public static void clearRepeatingAttendances(){
		List<Register> temps;
		int count=0;
		for(Register temp: RegisterLogic.find.all()) {
			temps = RegisterLogic.find.where().eq("student", temp.student).eq("timetable", temp.timetable)
					.eq("date", temp.date).findList();
			if (temps.size() > 1) {
				for (int i = 0; i < temps.size() - 1; i++) {
					temps.get(i).delete();
					count++;
				}
			}
		}

	}
}