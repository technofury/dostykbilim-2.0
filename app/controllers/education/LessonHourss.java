package controllers.education;

import static play.data.Form.form;
import models.entities.education.LessonHours;
import models.entities.social.Dostyk;
import models.entities.user.AuthorisedUser;
import models.logic.education.LessonHoursLogic;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.education.lessonHoursForm;
import views.html.education.lessonHoursTableBody;

public class LessonHourss extends Controller {

	public static Result set() {
		AuthorisedUser user = AuthorisedUser
				.getUserByEmail(session("connected"));
		Dostyk dostyk = user.dostyks.get(0);

		Form<LessonHours> form = form(LessonHours.class);

		return ok(lessonHoursForm.render(form, dostyk));
	}

	public static Result save() {
		Form<LessonHours> form = form(LessonHours.class).bindFromRequest();
		AuthorisedUser user = AuthorisedUser
				.getUserByEmail(session("connected"));
		Dostyk dostyk = user.dostyks.get(0);

		if (form.hasErrors()) {
			return badRequest(lessonHoursForm.render(form, dostyk));
		} else {
			LessonHoursLogic.create(form.get());
			return ok(lessonHoursTableBody.render(form, dostyk));
		}

	}
}
