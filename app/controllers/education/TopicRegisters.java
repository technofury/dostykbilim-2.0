package controllers.education;

import java.text.SimpleDateFormat;
import java.util.Date;

import models.entities.education.Topic;
import models.entities.education.TopicRegister;
import models.entities.social.DostykGroup;
import models.logic.social.DostykGroupLogic;
import play.mvc.Controller;
import play.mvc.Result;

public class TopicRegisters extends Controller {

	public static Result save(long topic, String strDate, long group,
			long lesson) throws Exception {
		Date date = new SimpleDateFormat("yyyy-MM-dd").parse(strDate);
		DostykGroup dg = DostykGroupLogic.getById(group);
		Topic t = Topic.find.byId(topic);

		TopicRegister tr = TopicRegister.findByFields(dg, date, t.lesson);
		if (tr == null) {
			tr = new TopicRegister();
			tr.date = date;
			tr.dostykGroup = dg;
			tr.topic = t;
		} else {
			tr.topic = Topic.find.byId(topic);
		}

		tr.save();
		return ok();
	}

}