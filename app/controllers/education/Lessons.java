package controllers.education;

import models.entities.education.Course;
import models.entities.education.Lesson;
import models.entities.social.Dostyk;
import models.logic.education.CourseLogic;
import models.logic.education.LessonLogic;
import models.logic.social.DostykLogic;
import play.data.DynamicForm;
import play.i18n.Messages;
import play.mvc.Result;
import play.data.Form;
import play.mvc.Controller;
import views.html.education.editLesson;
import views.html.helps.messenger;



import java.util.List;

import static play.data.Form.form;


/**
 * Created by User on 15-Feb-17.
 */
public class Lessons extends Controller {
    final static Form<Lesson> dForm = form(Lesson.class);

    public static Result getCourseList(){
        List<Lesson> lessonList = LessonLogic.getAll();
        List<Course> courseList = CourseLogic.getAll();
        return ok(views.html.education.lessons.render(lessonList,courseList,dForm));
    }

    public static Result saveLesson() {
        DynamicForm dynamicForm = form().bindFromRequest();
        Form<Lesson> filledForm = form(Lesson.class);;
        Lesson lesson = new Lesson();
        lesson.name = dynamicForm.get("name");
        lesson.code = dynamicForm.get("code");
        lesson.language = dynamicForm.get("language");
        filledForm = filledForm.fill(lesson);
        if (dynamicForm.field("code").valueOr("0").isEmpty()) {
            filledForm.reject("code", Messages.get("dostyk.errorCode"));
        } else if (LessonLogic.getbyCodeAndLang(dynamicForm.get("code"),dynamicForm.get("language")) != null) { // check if such code exists
            filledForm.reject("code", Messages.get("dostyk.usedCode"));
        }
        List<Lesson> lessonList = LessonLogic.getAll();
        List<Course> courseList = CourseLogic.getAll();
        if (filledForm.hasErrors()) {
            return badRequest(views.html.education.lessons.render(lessonList, courseList, filledForm));
        } else {


            for (int i = 1; i <= CourseLogic.find.findRowCount(); i++) {
                if (dynamicForm.get("" + i) != null) {
                    lesson.course.add(CourseLogic.find.byId(Long.parseLong("" + i)));
                }
            }

            lesson.save();
            lessonList = LessonLogic.getAll();

            return ok(views.html.education.lessons.render(lessonList, courseList, dForm));
        }

    }

    public static Result saveExistingLesson(Long id) {

        DynamicForm dynamicForm = form().bindFromRequest();
        Form<Lesson> filledForm = form(Lesson.class);;
        Lesson lesson = LessonLogic.find.byId(id);
        lesson.name = dynamicForm.get("name");
        lesson.code = dynamicForm.get("code");
        lesson.language = dynamicForm.get("language");
        filledForm = filledForm.fill(lesson);
        if (dynamicForm.field("code").valueOr("0").isEmpty()) {
            filledForm.reject("code", Messages.get("dostyk.errorCode"));
        } else if (LessonLogic.find.where().eq("code",dynamicForm.get("code")).eq("language",dynamicForm.get("language")).ne("id",id).findRowCount() > 0) { // check if such code exists
            filledForm.reject("code", Messages.get("dostyk.usedCode"));
        }
        List<Lesson> lessonList = LessonLogic.getAll();
        List<Course> courseList = CourseLogic.getAll();
        if (filledForm.hasErrors()) {
            Lesson toEdit = LessonLogic.find.byId(id);
            return badRequest(views.html.education.editLesson.render(filledForm, courseList, toEdit.course,id));
        } else {
            lesson.course.clear();
            for(int i = 1; i <= CourseLogic.find.findRowCount(); i++){
                if(dynamicForm.get(""+i) != null){
                    lesson.course.add(CourseLogic.find.byId(Long.parseLong(""+i)));
                }
            }
            lesson.update();
            lessonList = LessonLogic.getAll();
            return ok(views.html.education.lessons.render(lessonList,courseList,dForm));
        }

    }

    public static Result deleteLesson(Long id) {
        try {
            Lesson toDelete = LessonLogic.getById(id);

            toDelete.delete();
            return ok(messenger.render("alert alert-success","congratulations", Messages.get("dostyk.successfullyDeleted")));
        } catch (Exception e) {
            return badRequest(messenger.render("alert alert-error",
                    Messages.get("saveDostykError"), e.getMessage()));
        }


    }

    public static Result editLesson(Long id) {
        Form<Lesson> filledForm = form(Lesson.class);
        Lesson toEdit = LessonLogic.find.byId(id);
        List<Course> courseList = CourseLogic.getAll();
        return ok(editLesson.render(filledForm.fill(toEdit),courseList,toEdit.course,id));

    }
}
