package controllers.education;

import static play.data.Form.form;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.fasterxml.jackson.databind.JsonNode;

import models.entities.education.Attendance;
import models.entities.education.CourseType;
import models.entities.social.Dostyk;
import models.entities.user.AuthorisedUser;
import models.entities.user.SecurityRole;
import models.logic.social.DostykLogic;
import models.timetable.TimetableLesson;
import play.data.DynamicForm;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.education.attendance;
import views.html.education.viewAttendance;

public class Attendances extends Controller {	

	public static Result edit(long lid, String tdy) {
		AuthorisedUser user = AuthorisedUser.getUserByEmail(session("connected"));
		if (!isTeacher(user)) return badRequest("Not Allowed"); 
		TimetableLesson lesson = TimetableLesson.byId(lid);
		Date today = getTodayDate();
		try {
		if (!tdy.isEmpty()) today = new SimpleDateFormat("yyyy-MM-dd").parse(tdy);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		List<Attendance> list = Attendance.byDateLesson(today, lesson);
		List<AuthorisedUser> students = AuthorisedUser.getByDostykGroup(lesson.timetable.grade);
		if (students.size() > 0 && (list == null || list.size() == 0)) {
			list = new ArrayList<>();
			Dostyk dostyk = DostykLogic.getByUser(user);
			for (AuthorisedUser student : students) {
				Attendance attendance = new Attendance();
				attendance.mydate = today;
				attendance.lesson = lesson;
				attendance.student = student;
				attendance.organization = dostyk;
				attendance.save();
				list.add(attendance);
			}
		}
		String stoday = new SimpleDateFormat("yyyy-MM-dd").format(today);
		return ok(attendance.render(lesson, list, stoday));
	}
	
	private static boolean isTeacher(AuthorisedUser user) {
		for (SecurityRole role : user.roles)
			if (role.name.equals("teacher")) return true;
		return false;
	}
	
	private static boolean isStudent(AuthorisedUser user) {
		for (SecurityRole role : user.roles)
			if (role.name.equals("student")) return true;
		return false;
	}

	private static Date getTodayDate() {
		Calendar gc = Calendar.getInstance();
		gc.set(Calendar.HOUR_OF_DAY, 0);
	    gc.set(Calendar.MINUTE, 0);
	    gc.set(Calendar.SECOND, 0);
	    gc.set(Calendar.MILLISECOND, 0);
	    return gc.getTime();
	}
	
	public static Result update() {
		JsonNode node = request().body().asJson();		
		
		Iterator<Map.Entry<String, JsonNode>> itr = node.fields();
		while (itr.hasNext()) {
			Entry<String, JsonNode> nxt = itr.next();
			Attendance att = Attendance.byId(Long.parseLong(nxt.getKey()));
			att.status = nxt.getValue().asInt();
			att.update();
		}
						
		return ok();
	}
	
	public static Result view() {
		AuthorisedUser user = AuthorisedUser.getUserByEmail(session("connected"));
		if (!isStudent(user)) return badRequest("Not Allowed");		
		return ok(viewAttendance.render(user, null, null, null));
	}
	
	public static Result viewBetween() {
		AuthorisedUser user = AuthorisedUser.getUserByEmail(session("connected"));
		if (!isStudent(user)) return badRequest("Not Allowed");
		
		DynamicForm form = form().bindFromRequest();
		String start = form.field("start").value();
		String end = form.field("end").value();
		
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		try {
			Date first = df.parse(start);
			Date last = df.parse(end);
			List<Attendance> list = Attendance.absentOrLate(user, first, last);			
			return ok(viewAttendance.render(user, start, end, list));		
		} catch (ParseException exc) {
			exc.printStackTrace();
			return badRequest("Exception");
		}	
	}
}
