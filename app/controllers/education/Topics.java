package controllers.education;

import static play.data.Form.form;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.JsonArray;
import models.entities.education.Course;
import models.entities.education.Lesson;
import models.entities.education.Topic;
import models.entities.examination.Answer;
import models.entities.examination.DetailedResult;
import models.entities.examination.Exam;
import models.entities.examination.ExamResult;
import models.entities.examination.LessonAnswer;
import models.entities.social.Dostyk;
import models.entities.social.DostykGroup;
import models.entities.user.AuthorisedUser;
import models.logic.education.CourseLogic;
import models.logic.education.LessonLogic;
import models.logic.examination.DetailedResultLogic;
import models.logic.examination.ExamLogic;
import models.logic.examination.ExamResultLogic;
import models.logic.examination.LessonAnswerLogic;
import models.logic.social.DostykGroupLogic;
import models.logic.social.DostykLogic;
import play.data.DynamicForm;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import play.twirl.api.Content;
import views.html.education.reportByTopics;
import views.html.education.reportByTopics2;
import views.html.education.topics;
import views.html.education.uploadBody;
import views.html.education.uploadPage;
import views.html.examination.repByTopPrint;
import views.html.examination.reportByTopics3;
import be.objectify.deadbolt.java.actions.Group;
import be.objectify.deadbolt.java.actions.Restrict;
import controllers.Application;
import controllers.examination.LessonAnswers;

public class Topics extends Controller {
	public static TreeMap<String, TreeMap<String, TreeMap<String, Pair>>> rows;

	public static Result saveUploadedFile(long id) {
		try {
			Http.MultipartFormData body = request().body()
					.asMultipartFormData();
			Http.MultipartFormData.FilePart picture = body.getFile("file");
			if (picture != null) {
				java.io.File file = picture.getFile();
				String Root = Application.getRootPath();
				DateFormat sdf = new SimpleDateFormat("yyyy-MM");
				Date now = new Date();
				String current = sdf.format(now);
				String ext = LessonAnswers.getExtension(picture.getFilename());
				Long time = now.getTime();
				boolean res = LessonAnswers.addFileToDir(file, Root + "/"
						+ "FILES" + "/" + current + "/", time + "." + ext);
				if (res == true) {
					Topic t = Topic.find.byId(id);
					t.pathToFile = current + "/" + time + "." + ext;
					t.fileName = picture.getFilename();
					t.save();
					return ok("ok");
				} else
					return ok("error");
			} else {
				return ok("missing");
			}
		} catch (Exception e) {
			e.printStackTrace();
			return ok("error");
		}
	}

	public static Result uploadPage() {
		return ok(uploadPage.render());
	}

	public static Result getBy(long lessonId) {
		List<Topic> list = Topic.findByLesson(LessonLogic.getById(lessonId));
		return ok(uploadBody.render(list));
	}

	public static void addEntry(String name, String lessonName, String topic,
			char ch, String answerCode, Boolean... ok) {
		if (!rows.containsKey(name)) {
			rows.put(name, new TreeMap<String, TreeMap<String, Pair>>());
		}
		for (String curName : rows.keySet()) {
			TreeMap<String, TreeMap<String, Pair>> lessons = rows.get(curName);
			if (!lessons.containsKey(lessonName)) {
				lessons.put(lessonName, new TreeMap<String, Pair>());
			}
			TreeMap<String, Pair> topics = lessons.get(lessonName);
			if (!topics.containsKey(topic)) {
				topics.put(topic, new Pair(0, 0));
			}

			if (ok.length == 0 && curName.equals(name)) {
				Pair cur = topics.get(topic);
				cur.a++;
				if (ch == answerCode.charAt(0))
					cur.b++;
			}
		}
	}

	public static void consolidate(int type, List<ExamResult> resList,
			Long... lessonIds) {
		long lessonId = -1;
		if (lessonIds.length > 0)
			lessonId = lessonIds[0];
		List<Answer> answers;
		for (ExamResult r : resList) {
			if (r == null)
				continue;
			String name;
			if (type == 0) {
				if (r.student.dostyks == null || r.student.dostyks.size() == 0) {
					name = "noDostyk";
				} else
					name = r.student.dostyks.get(0).name;
			} else if (type == 1) {
				if (r.student.profile.dostykGroup != null) {
					name = r.student.profile.dostykGroup.name;
				} else
					name = "noGroup";
			} else
				name = r.student.profile.firstName + " "
						+ r.student.profile.lastName;

			for (DetailedResult dr : DetailedResultLogic.getByExamResult(r)) {
				if (lessonId != -1 && lessonId != dr.lesson.id)
					continue;
				LessonAnswer lA = LessonAnswerLogic.getBy(r.variant, dr.lesson);
				Collections.sort(lA.answers);
				String lessonName = dr.lesson.name;
				answers = Answer.getByLessonAnswer(lA);
				for (int i = 0; i < answers.size(); i++) {
					char ch = dr.studentAnswers.charAt(i);
					Answer a = answers.get(i);
					addEntry(name, lessonName, a.topic, ch, a.answerCode);
				}
			}
		}

		rows.forEach((k, v) -> v.forEach((k2, v2) -> v2
				.forEach((k3, v3) -> addEntry(k, k2, k3, 'a', "a", false))));
	}

	public static Result getReport(Long examId, Long dostykId, Long groupId, Long lessonId) {
		rows = new TreeMap<String, TreeMap<String, TreeMap<String, Pair>>>();
		Exam exam = ExamLogic.getById(examId);
		if (dostykId == -1) {
			List<ExamResult> resList = ExamResultLogic.getByExam(exam);
			consolidate(0, resList, lessonId);
			return ok(reportByTopics2.render(rows, 0));
		}

		Dostyk dostyk = DostykLogic.getById(dostykId);
		if (groupId == -1) {
			List<ExamResult> resList = ExamResultLogic.getByExamAndByDostyk2(
					exam, dostyk);
			consolidate(1, resList, lessonId);
			return ok(reportByTopics2.render(rows, 0));
		}

		DostykGroup group = DostykGroupLogic.getById(groupId);
		List<ExamResult> resList = ExamResultLogic.getByExamAndByDostykGroup2(
				exam, group);
		consolidate(2, resList, lessonId);
		return ok(reportByTopics2.render(rows, 1));
	}

	public static Result getReport2(Long examId, Long userId) {
		rows = new TreeMap<String, TreeMap<String, TreeMap<String, Pair>>>();
		AuthorisedUser user = AuthorisedUser.getUserById(userId);
		Exam exam = ExamLogic.getById(examId);
		List<ExamResult> resList = new ArrayList<>();
		ExamResult res = ExamResultLogic.getByExamAndUser(exam, user);
		String language = "-1";

		language = res.detailedResults.get(0).lesson.language;

//		language = user.profile.dostykGroup != null ? user.profile.dostykGroup.language
//				: "K";
//		if (language.equals("K")) {
//			language = "KZ";
//		} else
//			language = "RU";
		resList.add(res);
		consolidate(2, resList);

		return ok(reportByTopics3.render(user, exam, language, rows,
				resList.get(0)));
	}

	public static Result printReport(String type, Long examId, Long userId) {
		rows = new TreeMap<String, TreeMap<String, TreeMap<String, Pair>>>();
		AuthorisedUser user = AuthorisedUser.getUserById(userId);
		Exam exam = ExamLogic.getById(examId);
		List<ExamResult> resList = new ArrayList<ExamResult>();
		ExamResult res = ExamResultLogic.getByExamAndUser(exam, user);
		String language = "-1";
		language = user.profile.dostykGroup.language;
		if (language.equals("K")) {
			language = "KZ";
		} else
			language = "RU";
		resList.add(res);
		consolidate(2, resList);
		String userInfo = user.profile.firstName + " " + user.profile.lastName;
		String examInfo = exam.name;
		return ok(repByTopPrint.render(type, userInfo, examInfo, language,
				rows, res));
	}

	public static Result report() {
		List<Exam> exams = ExamLogic.getAll();
		List<Dostyk> dostyks = DostykLogic.getAll();
		AuthorisedUser user = AuthorisedUser
				.getUserByEmail(session("connected"));
		Dostyk dostyk;
		if (user.dostyks.isEmpty())
			dostyk = null;
		else
			dostyk = user.dostyks.get(0);
		return ok(reportByTopics.render(exams, dostyks, dostyk));
	}

	@Restrict({ @Group("admin") })
	public static Result manage() {
		List<Lesson> lessons = LessonLogic.getAll();
		List<Course> courseList = CourseLogic.getAll();
		return ok(topics.render(lessons, courseList, new Long(-1)));
	}

	@Restrict({ @Group("admin") })
	public static Result save() {
		try {
			DynamicForm df = form().bindFromRequest();
			long id = Long.parseLong(df.get("id"));
			long cid = Long.parseLong(df.get("course"));
			Lesson lesson = LessonLogic.getById(Long.parseLong(df.get("lesson")));
			Course courses = CourseLogic.getById(cid);
			String name = df.get("name");
			int count = Topic.countByInfo(lesson, name);
			Topic topic;
			if (id == -1) {
				if (count > 0)
					return ok("error");
				topic = new Topic();
			} else {
				if (count > 1)
					return ok("error");
				topic = Topic.find.byId(id);
			}
			topic.name = name;
			topic.lesson = lesson;
			topic.courses = courses;
			topic.save();
			return ok(topic.id + "");
		} catch (Exception e) {
			e.printStackTrace();
			return ok("exception");
		}
	}

	@Restrict({ @Group("admin") })
	public static Result delete(long id) {
		try {
			Topic.find.byId(id).delete();
			return ok("success");
		} catch (Exception e) {
			e.printStackTrace();
			return ok("exception");
		}
	}
	@Restrict({ @Group("admin") })
	public static Result getTopic(long lid, long cid) {
        try {
            List<Topic> topic = Topic.getTopicList(lid, cid);
            JsonNode json = Json.toJson(topic);
            return ok(json);
        } catch (Exception e) {
            e.printStackTrace();
            return ok("exception");
        }
	}

	public static class Pair {
		public int a, b;

		public Pair(int a, int b) {
			this.a = a;
			this.b = b;
		}
	}
}
