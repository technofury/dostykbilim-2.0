package controllers.ten;

import com.avaje.ebean.Ebean;
import models.entities.education.Lesson;
import models.entities.social.Dostyk;
import models.entities.social.DostykGroup;
import models.entities.ten.ExamSession;
import models.entities.user.AuthorisedUser;
import models.entities.user.Parent;
import models.entities.user.SecurityRole;
import models.logic.education.CourseTypeLogic;
import models.logic.education.LessonLogic;
import models.logic.social.DistrictLogic;
import models.logic.social.DostykLogic;
import models.logic.social.RegionLogic;
import models.logic.social.SchoolLogic;
import models.logic.user.AuthorisedUserLogic;
import models.logic.user.EmailLogic;
import play.data.Form;
import play.i18n.Messages;
import play.libs.Crypto;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static play.data.Form.form;

/**
 * Created by technoDev on 31/03/2016.
 */
public class UserTen extends Controller {
    public static Form<AuthorisedUser> signupForm;

    public static Result genderReport() {
        AuthorisedUser user = AuthorisedUser.getUserByEmail(session().get(
                "connected"));
        List<Dostyk> dostyks = null;
        if (user.roles.contains(SecurityRole.getByName("admin"))) {
            dostyks = DostykLogic.getAll();
        }
//        if (user.roles.contains(SecurityRole.getByName("moderator"))) {
//            lessons = user.lessons;
//        }
        return ok(views.html.ten.tenReport.render(dostyks));
    }
    public static Result printXLS(){
        String who = session().get("connected");
        AuthorisedUser moder = AuthorisedUser.getUserByEmail(who);
        Dostyk dostyk = moder.dostyks.get(0);

        List<AuthorisedUser> authorisedUsers = AuthorisedUserLogic.find.where()
                .eq("roles", SecurityRole.getByName("ten"))
                .eq("dostyks", dostyk)
//                .ne("status", "deleted")
                .findList();
        return ok(views.html.ten.xls.render(authorisedUsers));
    }

    public static Result getSessions(String name) {
        List<ExamSession> examSessions = ExamSession.findByDostyk(DostykLogic.getByName(name));
        return ok(views.html.helps.selectSessions.render(examSessions));
    }
    public static Result signUp() {
        signupForm = form(AuthorisedUser.class);
        return ok(views.html.ten.signup.render(signupForm, "student", RegionLogic.getAll()));
    }

    public static Result signUpSubmit() {
        Form<AuthorisedUser> filledForm = form(AuthorisedUser.class).bindFromRequest();

        if (!"true".equals(filledForm.field("accept").value())) {
            filledForm.reject("accept",Messages.get(lang(),"Youmustacceptthetermsandconditions"));
        }
        if (AuthorisedUser.getUserByEmail(filledForm.data().get("email")) != null) {
            filledForm.reject("email", Messages.get(lang(),"thisemailisalreadytaken"));
        }
        if ((filledForm.data().get("dostyk") == null || filledForm.data()
                .get("dostyk").equals(""))) {
            filledForm.reject("dostyk", Messages.get(lang(),"selectone"));
        }

        if ((filledForm.data().get("region") == null || filledForm.data()
                .get("region").equals(""))) {
            filledForm.reject("region", Messages.get(lang(),"selectone"));
        }
        if ((filledForm.data().get("district") == null || filledForm.data()
                .get("district").equals(""))) {
            filledForm.reject("district",  Messages.get(lang(),"selectone"));
        }
        if ((filledForm.data().get("school") == null || filledForm.data()
                .get("school").equals(""))) {
            filledForm.reject("school", Messages.get(lang(),"selectone"));
        }
        if (!filledForm.data().get("password")
                .equals(filledForm.data().get("repeatPassword"))
                || filledForm.data().get("password").length() < 6) {
            filledForm.reject("password",
                    Messages.get("incorrect.password.or.less.than.6.chars"));
        }
        if (filledForm.hasErrors()) {

            return badRequest(views.html.ten.signup.render(filledForm, "student",
                    RegionLogic.getAll()));
        } else {
            AuthorisedUser user = filledForm.get();
            user.roles = new ArrayList<SecurityRole>();
//            user.roles.add(SecurityRole.getRoleByName("student"));
            user.roles.add(SecurityRole.getRoleByName("ten"));
            user.password = filledForm.data().get("password");
            user.password = Crypto.encryptAES(user.password);// MD5

            user.profile.ulgerimi = filledForm.data().get("ulgerimi");

            ExamSession examSession = ExamSession.find.byId(Long.parseLong(filledForm.data().get("profile.examSession.id").trim()));
            if(examSession.profiles.size()==examSession.sessionMax){
                examSession.status = "disabled";
                examSession.update();
            }
            user.profile.examSession = ExamSession.find.byId(Long.parseLong(filledForm.data().get("profile.examSession.id")));


//            user.profile.course = CourseTypeLogic.getByName(filledForm
//                    .data().get("course"));
            user.profile.region = RegionLogic.getByCode(filledForm.data()
                    .get("region"));
            user.profile.district = DistrictLogic.getByCode(filledForm
                    .data().get("district"), RegionLogic
                    .getByCode(filledForm.data().get("region")));
            user.profile.save();
            user.profile.school = SchoolLogic.getByCode(filledForm.data()
                            .get("school"), user.profile.region,
                    user.profile.district);
            user.profile.save();
            user.save();

            String who = session().get("connected");
            Dostyk dostyk;
            if (filledForm.data().get("dostyk") == null
                    || filledForm.data().get("dostyk").length() == 0) {
                AuthorisedUser moder = AuthorisedUser.getUserByEmail(who);
                dostyk = moder.dostyks.get(0);
            } else {
                dostyk = DostykLogic.getByName(filledForm.data().get("dostyk"));
            }
            dostyk.users.add(user);
            dostyk.save();
            Ebean.saveManyToManyAssociations(user, "roles");

            changeStatus(user.id);

            return redirect(controllers.routes.Application.index());
        }

    }

    public static Result list(int page, String sortBy, String order,
                              String filter) {
        AuthorisedUser moderator = AuthorisedUser
                .getUserByEmail(session("connected"));
        List<DostykGroup> dostykGroups = new ArrayList<DostykGroup>();
        List<DostykGroup> electiveGroups = new ArrayList<DostykGroup>();

        // System.err.println(moderator.lessons.get(0).users.size());
        return ok(views.html.ten.list.render(AuthorisedUser.getTenPage(
                moderator.dostyks.get(0).users, filter, page, sortBy, order),
                page, sortBy, order, filter, dostykGroups, electiveGroups));
    }


    public static void changeStatus(long id) {
        AuthorisedUser user = AuthorisedUser.getUserById(id);
        if (user.status.equals("active")) {
            user.status = "inactive";
            user.save();
//            return ok("inactive");
        } else {
            user.status = "active";
            if (user.profile.idNumber != null
                    && user.profile.idNumber.length() > 0) {
            } else {
                String dostykCode = Integer.toString(user.dostyks.get(0).code);
                int alreadyHave = AuthorisedUser
                        .getCountWhichIdStarts(dostykCode);
                String tempId = "";
                for (int i = 1; ; i++) {
                    tempId = dostykCode + format(alreadyHave + i);
                    if (AuthorisedUser.getByIdNumber(tempId) == null) {
                        break;
                    }
                }
                user.profile.idNumber = tempId;
                user.profile.save();

            }
            user.save();
            try {
                EmailLogic.sendTenEmail(user);
            }catch(Exception e){
                e.printStackTrace();
            }
//            return ok(user.profile.idNumber);
        }
    }

    public static Result deleteFromDostyk(long id) {
        AuthorisedUser user = AuthorisedUser.getUserById(id);
        user.status = "deleted";
        long dostykId = user.dostyks.get(0).id;
        Dostyk dostyk = DostykLogic.getById(dostykId);

        for (int i = 0; i < user.dostyks.size(); i++) {
            if (dostyk.code == user.dostyks.get(i).code) {
                user.dostyks.remove(i);
                user.profile.dostykGroup = null;
                user.profile.electiveGroup = null;
                break;
            }
        }

        for (int i = 0; i < dostyk.users.size(); i++) {
            if (user.id == dostyk.users.get(i).id) {
                dostyk.users.remove(i);
                break;
            }
        }
        dostyk.save();

        user.profile.save();
        user.save();
        return ok();
    }

    public static String format(int x) {
        String s = "" + x;
        while (s.length() < 6)
            s = "0" + s;
        return s;
    }
}
