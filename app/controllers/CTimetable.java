package controllers;

import java.awt.Color;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeCreator;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;

import be.objectify.deadbolt.java.actions.Group;
import be.objectify.deadbolt.java.actions.Restrict;
import be.objectify.deadbolt.java.actions.SubjectPresent;
import controllers.user.AuthorisedUsers;
import models.entities.education.Lesson;
import models.entities.education.LessonHours;
import models.entities.social.Dostyk;
import models.entities.social.DostykGroup;
import models.entities.user.AuthorisedUser;
import models.logic.education.LessonHoursLogic;
import models.logic.education.LessonLogic;
import models.logic.social.DostykGroupLogic;
import models.logic.social.DostykLogic;
import models.timetable.LessonPeriod;
import models.timetable.Settings;
import models.timetable.Timetable;
import models.timetable.TimetableLesson;
import play.data.DynamicForm;
import play.data.Form;
import play.db.ebean.Transactional;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.defaultpages.error;

@SubjectPresent
public class CTimetable extends Controller {
	
    Form formFactory;
	
	//@Restrict({@Group("sudo"), @Group("admin"), @Group("principal"), @Group("vice principal"), @Group("register")})
	public static Result viewAll( long gid, String st) {
		List<Timetable> list = Timetable.byParams( gid, st);
		AuthorisedUser user = AuthorisedUser.getUserByEmail(session("connected"));
		Dostyk dostyk = DostykLogic.getByUser(user);
		List<DostykGroup> group = DostykGroupLogic.getAllByDostyk(dostyk);
		if (group.size()!=0) {
			gid = group.get(0).id;
		}
		return ok(views.html.timetable.viewAllTimetables.render(list, gid, st));
	}

	public static Result viewTeacherTimetable(){
		AuthorisedUser user = AuthorisedUser.getUserByEmail(session("connected"));
		Settings settings = Settings.findActive();
		List<TimetableLesson> timetableLessons = TimetableLesson.byUser(user);
		Date today = new Date();
		String stoday = new SimpleDateFormat("yyyy-MM-dd").format(today);
		return ok(views.html.timetable.viewTeacherTimetable.render(settings, timetableLessons, stoday));
	}
	
	//@Restrict({@Group("sudo"), @Group("admin"), @Group("principal"), @Group("vice principal"), @Group("register")})
	public static Result view(Long ttid) {
		Timetable timetable = Timetable.byId(ttid);
		List<TimetableLesson> lessons = TimetableLesson.byTimetable(timetable);				
		Settings settings = timetable.settings;
		return ok(views.html.timetable.viewTimetable.render(timetable, lessons, settings));
	}

	public static Result viewGroupTimetable(Long gid) {
		DostykGroup dg = DostykGroupLogic.getById(gid);
		List<Timetable> timetable = Timetable.byGrade(dg);
		List<TimetableLesson> lessons = null;
		Timetable timetable1 = null;
		Settings settings = null;

		if (timetable.size()!=0){
			timetable1 = timetable.get(0);
			lessons = TimetableLesson.byTimetable(timetable.get(0));
			settings = timetable.get(0).settings;
		}
		return ok(views.html.timetable.viewTimetable.render(timetable1, lessons, settings));
		//return ok("error");
	}

	//@Restrict({@Group("admin"), @Group("principal"), @Group("vice principal"), @Group("register")})
	public static Result edit(Long gid, String timetableId) {

		AuthorisedUser user = AuthorisedUser.getUserByEmail(session("connected"));
		Dostyk dostyk = DostykLogic.getByUser(user);

		Map<Long, String> colorMap = new HashMap<>();
		Timetable timetable;
		List<TimetableLesson> lessons = null;
		List<AuthorisedUser> teachers = DostykLogic.getTeacherList(dostyk);
		DostykGroup grade = DostykGroupLogic.getById(gid);;
		Settings settings = null;

		if (timetableId.equals("new")) {
			timetable = new Timetable();
			lessons = new ArrayList<>();
			settings = Settings.findActive();
			timetable.grade = grade;
			timetable.organization = dostyk;
		} else {
			if (timetableId.equals("no")){
				timetable = Timetable.findActive(grade);
				if (timetable==null){
					List<Timetable> timtableList = Timetable.byGrade(grade);
					timetable = timtableList.get(0);
					lessons = TimetableLesson.byTimetable(timetable);
					settings = timetable.settings;
				}
			}
			else{
				timetable = Timetable.byId(Long.parseLong(timetableId));
				lessons = TimetableLesson.byTimetable(timetable);
				settings = timetable.settings;
			}
		}
		List<String> creditColor = new ArrayList<>();
		List<String> lessonColor = new ArrayList<>();
		//List<Lesson> lessonList = LessonLogic.getAll();
		List<LessonHours> lessonHoursList = LessonHoursLogic.getByDostyk(dostyk);
		for (LessonHours credit : lessonHoursList) {
			for (TimetableLesson tlesson : lessons) {
				if (credit.hours == 0) break;
				if (tlesson.subject.equals(credit.lesson)) {
					credit.hours -= 1;
				}
			}
			if (!colorMap.containsKey(credit.lesson.id)) {
				colorMap.put(credit.lesson.id, getRandomColor());
			}
			creditColor.add(colorMap.get(credit.lesson.id));
		}
		for (TimetableLesson tlesson : lessons) {
			lessonColor.add(colorMap.get(tlesson.subject.id));
		}
		return ok(views.html.timetable.editTimetable.render( timetable, teachers, lessons, lessonHoursList, settings, gid, creditColor, lessonColor));
	}

	private static String getRandomColor() {
		Random rand = new Random();
		int r = rand.nextInt();
		int g = rand.nextInt();
		int b = rand.nextInt();
		return "#" + toBrowserHexValue(r) + toBrowserHexValue(g) + toBrowserHexValue(b);
	}

	private static String toBrowserHexValue(int number) {
		StringBuilder builder = new StringBuilder(Integer.toHexString(number & 0xff));
		while (builder.length() < 2) {
			builder.append("0");
		}
		return builder.toString().toUpperCase();
	}
	
	//@Transactional
	//@Restrict({@Group("sudo"), @Group("admin"), @Group("principal"), @Group("vice principal"), @Group("register")})
	public static Result save( Long gid, String timetableId, String sid) {
		Timetable timetable;
		JsonNode node = request().body().asJson();


		DostykGroup grade = DostykGroupLogic.getById(gid);
		Settings settings = Settings.byId(Long.parseLong(sid));
		AuthorisedUser user = AuthorisedUser.getUserByEmail(session("connected"));
		Dostyk dostyk = DostykLogic.getByUser(user);

		for (JsonNode json : node) {
			String strArray[] = json.get(0).asText().split("-");
			if (strArray.length!=3) {
				return badRequest();
			}
		}

		if (timetableId.equals("new")) {
			timetable = new Timetable();
			timetable.grade = grade;
			timetable.settings = settings;
			timetable.organization = dostyk;
			timetable.save();
		} else {
			timetable = Timetable.byId(Long.parseLong(timetableId));
		}

		deleteLessons(timetable);

		for (JsonNode json : node) {
			String subjectId = "";
			String teacherId = "";
			String strArray[] = json.get(0).asText().split("-");
			TimetableLesson lesson = new TimetableLesson();
			subjectId = strArray[1];
			teacherId = strArray[2];

			lesson.subjectTeacher = AuthorisedUser.getUserById(Long.parseLong(teacherId));
			lesson.subject = LessonLogic.getById(Long.parseLong(subjectId));
			lesson.period = LessonPeriod.byNumber(Integer.parseInt(json.get(1).toString()), settings);
			lesson.day = Integer.parseInt(json.get(2).toString());
			lesson.timetable = timetable;
			lesson.organization = dostyk;
			lesson.save();
		}
		
		return ok(timetable.id+"");
	}
	
	//@Transactional
	//@Restrict({@Group("sudo"), @Group("admin"), @Group("principal"), @Group("vice principal"), @Group("register")})
	public static Result active(Long timetableId) {
		String activeId = "no";
		Timetable timetable = Timetable.byId(timetableId);
		if (timetable.isActive) {
			timetable.isActive = false;
		} else {
			Settings settings = Settings.findActive();
			if (!settings.equals(timetable.settings)) {
				activeId = "CantActivate";
			} else {
				Timetable active = Timetable.findActive(timetable.grade);
				if (active != null) {
					activeId = active.id.toString();
					active.isActive = false;
					active.update();
				}
				timetable.isActive = true;
			}
		}
		timetable.update();
		return ok(activeId);
	}
	
	//@Transactional
	//@Restrict({@Group("sudo"), @Group("admin"), @Group("principal"), @Group("vice principal"), @Group("register")})
	public static Result delete(Long timetableId) {
		String result = "active";
		Timetable timetable = Timetable.byId(timetableId);
		if (!timetable.isActive) {
			deleteLessons(timetable);
			timetable.delete();
			result = "ok";
		}
		return ok(result);
	}

	private static void deleteLessons(Timetable timetable) {
		if (timetable.id == null) return;
		List<TimetableLesson> list = TimetableLesson.byTimetable(timetable);
		for (TimetableLesson lesson : list) {
			lesson.delete();
		}
	}
}
