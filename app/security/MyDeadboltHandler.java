package security;

import models.entities.user.AuthorisedUser;
import play.libs.F;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import views.html.accessFailed;
import be.objectify.deadbolt.core.models.Subject;
import be.objectify.deadbolt.java.AbstractDeadboltHandler;

public class MyDeadboltHandler extends AbstractDeadboltHandler {
	@Override
	public F.Promise<Result> beforeAuthCheck(Http.Context context) {
		return F.Promise.pure(null);
	}

	@Override
	public Subject getSubject(Http.Context context) {
		String mail = Controller.session().get("connected");
		if (mail == null)
			return null;
		return AuthorisedUser.getUserByEmail(Controller.session().get(
				"connected"));
	}

	@Override
	public F.Promise<Result> onAuthFailure(Http.Context context, String content) {
		return F.Promise.promise(new F.Function0<Result>() {
			@Override
			public Result apply() throws Throwable {
				return ok(accessFailed.render());
			}
		});
	}
}