
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.concurrent.TimeUnit;

import controllers.order.orderController;
import models.entities.accounting.DiscountMax;
import models.entities.accounting.Payment;
import models.entities.education.Course;
import models.entities.education.CourseType;
import models.entities.education.Lesson;
import models.entities.examination.Block;
import models.entities.examination.Layout;
import models.entities.social.District;
import models.entities.social.Dostyk;
import models.entities.social.Region;
import models.entities.social.School;
import models.entities.ten.ExamSession;
import models.entities.user.AuthorisedUser;
import models.entities.user.Profile;
import models.entities.user.SecurityRole;
import models.logic.accounting.Contract;
import models.logic.accounting.DiscountMaxLogic;
import models.logic.accounting.PaymentLogic;
import models.logic.education.CourseLogic;
import models.logic.education.CourseTypeLogic;
import models.logic.education.LessonLogic;
import models.logic.examination.LayoutLogic;
import models.logic.social.DistrictLogic;
import models.logic.social.DostykLogic;
import models.logic.social.RegionLogic;
import models.logic.social.SchoolLogic;
import models.logic.user.AuthorisedUserLogic;
import org.joda.time.DateTime;
import org.joda.time.Seconds;
import play.Application;
import play.GlobalSettings;
import play.Logger;
import play.api.PlayException;
import play.data.format.Formats;
import play.libs.Akka;
import play.libs.Crypto;
import play.libs.F;
import play.libs.F.Promise;
import play.mvc.Http.RequestHeader;
import play.mvc.Result;
import scala.concurrent.duration.Duration;
import views.html.errorPage;
import views.html.notFoundPage;

import com.avaje.ebean.Ebean;

import static play.mvc.Results.internalServerError;
import static play.mvc.Results.notFound;

public class Global extends GlobalSettings {

    public void onStart(Application app) {
        Logger.info("Application has started");
        //Registers.clearRepeatingAttendances();
        //Groups.clearRepeatedNotifications();
        //Users.clearAllUsers();
        //LessonAnswers.migrateVideoPaths();
        try {
            //backupSchools();
            conditionZero();
        }catch(Exception e){
            Logger.error("Exception thrown on Global");
            e.printStackTrace();
        }

    }



    public void conditionZero() throws Exception{
        if(CourseLogic.find.findRowCount()==0){
            int courseTitle[] = {1,2,3,4,5,6,7,8,9,10,11,12};
            for(int i = 0; i < courseTitle.length; i++){
                Course course = new Course();
                course.title = courseTitle[i];
                course.save();
            }
        }

        if(ExamSession.find.findRowCount()==0){
            String dostykNames [] = {
                    "Алматы2", //pravda
                    "Алматы3", //bayzak
                    "Астана",
                    "Астана2",
                    "Атырау",
                    "Ақтау",
                    "Ақтөбе",
                    "Орал",
                    "Павлодар",
                    "Екібастұз",
                    "Семей",
                    "Өскемен",
                    "Қарағанды",
                    "Көкшетау",
                    "Шымкент",
                    "Тараз",
                    "Қызылорда",
                    "Қостанай",
                    "Technovision",
            };
            int s1 [] ={250,
                    180,
                    200,
                    200,
                    700,
                    300,
                    350,
                    150,
                    150,
                    90,
                    100,
                    110,
                    130,
                    250,
                    160,
                    200,
                    200,
                    140,
                    0
            };

            int s2 [] = {250,
                    180,
                    500,
                    500,
                    700,
                    300,
                    350,
                    150,
                    150,
                    90,
                    100,
                    110,
                    130,
                    250,
                    160,
                    200,
                    200,
                    140, 0
            };
            int s3[] = {
                    250,
                    180,
                    200,
                    200,
                    0,
                    0,
                    350,
                    150,
                    150,
                    90,
                    100,
                    110,
                    130,
                    250,
                    160,
                    200,
                    200,
                    140,
                    0
            };

            for(int i = 0 ; i<dostykNames.length;i++){
                Dostyk dostyk = Dostyk.find.where().eq("name",dostykNames[i]).findUnique();
                ExamSession examSession = new ExamSession();
                examSession.dostyk = dostyk;
                examSession.session = 1;
                examSession.sessionMax = s1[i];
                if(s1[i]==0)
                    examSession.status = "disabled";

                examSession.save();

                ExamSession examSession2 = new ExamSession();
                examSession2.dostyk = dostyk;
                examSession2.session = 2;
                examSession2.sessionMax = s2[i];
                if(s2[i]==0)
                    examSession.status = "disabled";
                examSession2.save();

                ExamSession examSession3 = new ExamSession();
                examSession3.dostyk = dostyk;
                examSession3.session = 3;
                examSession3.sessionMax = s3[i];
                if(s3[i]==0)
                    examSession.status = "disabled";
                examSession3.save();
            }
        }
        if (SecurityRole.getByName("ten") == null) {
            SecurityRole securityRole = new SecurityRole();
            securityRole.name = "ten";
            securityRole.save();
        }
        if (SecurityRole.getByName("accountant") == null) {
            SecurityRole sr = new SecurityRole();
            sr.name = "accountant";
            sr.save();

            AuthorisedUser user = new AuthorisedUser();
            user.email = "global@dostykbilim.kz";
            user.password = Crypto.encryptAES("global");
            user.roles = new ArrayList<SecurityRole>();
            user.roles.add(SecurityRole.getByName("accountant"));
            user.save();
            Profile profile = new Profile();
            profile.firstName = "global";
            profile.lastName = "global";
            profile.gender = "other";
            profile.phoneNumber = "100915";
            profile.save();
            user.profile = profile;
            user.status = "active";
            user.save();
            Ebean.saveManyToManyAssociations(user, "roles");
        }

        if (SecurityRole.getByName("admin") == null) {
            for (String name : Arrays.asList("admin", "moderator", "teacher", "student")) {
                SecurityRole role = new SecurityRole();
                role.name = name;
                role.save();
            }
        }
        if (SecurityRole.getByName("director") == null) {
            SecurityRole role = new SecurityRole();
            role.name = "director";
            role.save();
        }
        if (DostykLogic.find.findRowCount() == 0) {
            String datas[] = { "Ақтау", "Ақтөбе", "Алматы1", "Алматы2",
                    "Алматы3", "Астана", "Атырау", "Екібастұз", "Көкшетау",
                    "Қарағанды", "Қостанай", "Қызылорда", "Орал", "Өскемен",
                    "Павлодар", "Семей", "Тараз", "Шымкент", "Астана_6-сынып",
                    "Астана2", };
            for (int i = 0; i < datas.length; i++) {
                Dostyk dostyk = new Dostyk();
                dostyk.code = 11 + i;
                dostyk.name = datas[i];
                dostyk.organization = datas[i];
                dostyk.status = 1;
                dostyk.save();
            }
            Dostyk a = new Dostyk();
            a.code = 99;
            a.name = "Technovision";
            a.organization = "Technovision LTD";
            a.status = 1;
            a.save();
        }

        if (DiscountMaxLogic.find.findRowCount() == 0) {
            Map<String, Integer> data = new HashMap<String, Integer>();
            // data.put("Bonus", 15);
            // data.put("Жеңілдіксіз", 0);
            data.put("Басшылық шегерімі", 7);
            data.put("\"Алтын белгі\" иесі шегерімі", 15);
            data.put("\"Үздік аттестат\" иесі шегерімі", 10);
            data.put("Достық ұжымының жанұя шегерімі", 50);
            data.put("Ұжымның жақын туысқан шегерімі", 25);
            data.put("Мерзімдік шегерімі", 15);
            data.put("Топтық шегерімі", 10);
            data.put("Бауырмашыл шегерімі", 10);
            data.put("Ауылдық шегерімі", 10);
            data.put("Жартылай жетім шегерімі", 25);
            data.put("Толық жетім шегерімі", 50);
            data.put("БТС-60 Балл", 10);
            data.put("БТС-1", 70);
            data.put("БТС-2", 60);
            data.put("БТС-3", 50);
            data.put("БТС-4", 40);
            data.put("БТС-5", 35);
            data.put("БТС-6", 30);
            data.put("БТС-7", 25);
            data.put("БТС-8", 20);
            data.put("БТС-9", 15);

            DiscountMax a = new DiscountMax();
            a.nameKz = "Bonus";
            a.value = 15;
            a.save();
            a = new DiscountMax();
            a.nameKz = "Жеңілдіксіз";
            a.value = 0;
            a.save();

            for (Map.Entry<String, Integer> entry : data.entrySet()) {
                DiscountMax dm = new DiscountMax();
                dm.nameKz = entry.getKey();
                dm.value = entry.getValue();
                dm.save();
            }

        }

        if (CourseTypeLogic.find.findRowCount() == 0) {
            String datas[] = { "A:ҰБТ:11", "B:ҰБТ 11-сынып жеделдетілген:11",
                    "C:КТЛ 6-сынып:6", "D:КТЛ 6-сынып жеделдетілген:6",
                    "E:ҰБТ 10-сынып:10", "F:ҰБТ 10-сынып жеделдетілген:10",
                    "G:IELTS:0", "H:Тіл курсы:0", "I:9-сынып:9",
                    "J:Қосымша 1:0", "K:Қосымша 2:0", "L:Қосымша 3:0" };
            for (String data : datas) {
                CourseType a = new CourseType();
                a.name = data.split(":")[1];
                a.classNumber = Integer.parseInt(data.split(":")[2]);
                a.code = data.split(":")[0];
                a.save();
            }
        }
        if (RegionLogic.find.findRowCount() == 0) {
            BufferedReader in = null;
            try {
                in = new BufferedReader(new FileReader("public/csv files/regions.csv"));
                String s;
                StringTokenizer st;
                while ((s = in.readLine()) != null) {
                    st = new StringTokenizer(s,";");
                    Region reg = new Region();
                    reg.code = st.nextToken();
                    reg.name_kz = st.nextToken();
                    reg.name_ru = st.nextToken();
                    reg.save();
                }
                in = new BufferedReader(new FileReader("public/csv files/districts.csv"));
                while ((s = in.readLine()) != null) {
                    st = new StringTokenizer(s,";");
                    District d = new District();
                    d.region = RegionLogic.getByCode(st.nextToken());
                    d.code = st.nextToken();
                    d.name_kz = st.nextToken();
                    d.name_ru = st.nextToken();
                    d.save();
                }
                in = new BufferedReader(new FileReader("public/csv files/schools.csv"));
                while ((s = in.readLine()) != null) {
                    st = new StringTokenizer(s,";");
                    School sc = new School();
                    sc.region = RegionLogic.getByCode(st.nextToken());
                    sc.district = DistrictLogic.getByCode(st.nextToken(),sc.region);
                    sc.code = st.nextToken();
                    sc.name_kz = st.nextToken();
                    sc.name_ru = st.nextToken();
                    sc.save();
                }
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        if (LessonLogic.find.findRowCount() == 0) {
            String array[][] = { { "ҚАЗАҚ ТІЛІ", "KZ", "01" },
                    { "ОРЫС ТІЛІ", "KZ", "02" },
                    { "ҚАЗАҚСТАН ТАРИХЫ", "KZ", "03" },
                    { "МАТЕМАТИКА", "KZ", "04" }, { "ЛОГИКА", "KZ", "05" },
                    { "ФИЗИКА", "KZ", "06" }, { "ХИМИЯ", "KZ", "07" },
                    { "БИОЛОГИЯ", "KZ", "08" }, { "ГЕОГРАФИЯ", "KZ", "09" },
                    { "ЖАЛПЫ ТАРИХЫ", "KZ", "10" },
                    { "ҚАЗАҚ ӘДЕБИЕТІ", "KZ", "11" },
                    { "АҒЫЛШЫН ТІЛІ", "KZ", "12" }, { "ШЕТ ТІЛІ", "KZ", "13" },
                    { "ҚОСЫМША 1", "KZ", "14" }, { "ҚОСЫМША 2", "KZ", "15" },

                    { "КАЗАХСКИЙ ЯЗЫК", "RU", "01" },
                    { "РУССКИЙ ЯЗЫК", "RU", "02" },
                    { "ИСТ. КАЗАХСТАНА", "RU", "03" },
                    { "МАТЕМАТИКА", "RU", "04" }, { "ЛОГИКА", "RU", "05" },
                    { "ФИЗИКА", "RU", "06" }, { "ХИМИЯ", "RU", "07" },
                    { "БИОЛОГИЯ", "RU", "08" }, { "ГЕОГРАФИЯ", "RU", "09" },
                    { "ВСЕМ. ИСТОРИЯ", "RU", "10" },
                    { "ЛИТЕРАТУРА", "RU", "11" },
                    { "АНГЛИЙСКИЙ ЯЗЫК", "RU", "12" },
                    { "ИНОСТРАННЫЙ ЯЗЫК", "RU", "13" },
                    { "ДОПОЛНИТЕЛЬНЫЙ 1", "RU", "14" },
                    { "ДОПОЛНИТЕЛЬНЫЙ 2", "RU", "15" } };

            for (String temp[] : array) {
                Lesson lesson = new Lesson();
                lesson.name = temp[0];
                lesson.language = temp[1];
                lesson.code = temp[2];
                lesson.save();
            }
        }
        if (LessonLogic.findByName("ҚОСЫМША 1")==null) {
            String array[][] = { { "ШЕТ ТІЛІ", "KZ", "12" },
                    { "ҚОСЫМША 1", "KZ", "13" }, { "ҚОСЫМША 2", "KZ", "14" },

                    { "ИНОСТРАННЫЙ ЯЗЫК", "RU", "12" },
                    { "ДОПОЛНИТЕЛЬНЫЙ 1", "RU", "13" },
                    { "ДОПОЛНИТЕЛЬНЫЙ 2", "RU", "14" } };
            for (String temp[] : array) {
                Lesson lesson = new Lesson();
                lesson.name = temp[0];
                lesson.language = temp[1];
                lesson.code = temp[2];
                lesson.save();
            }
        }
        if(LessonLogic.findByName("САНДЫҚ СИПАТТАМАЛАР")==null){
            String array[][] = { { "САНДЫҚ СИПАТТАМАЛАР", "KZ", "16" },
                    { "КЕҢІСТІКТЕ ОЙЛАУ", "KZ", "17" },

                    { "КОЛИЧЕСТВЕННЫЕ ХАРАКТЕРИСТИКИ", "RU", "16" },
                    { "ПРОСТРАНСТВЕННОЕ МЫШЛЕНИЕ", "RU", "17" }};
            for (String temp[] : array) {
                Lesson lesson = new Lesson();
                lesson.name = temp[0];
                lesson.language = temp[1];
                lesson.code = temp[2];
                lesson.save();
            }
        }
        if (AuthorisedUser.getUserByEmail("admin@admin")==null) {
            // --------------------ADMIN---------------------------
            AuthorisedUser user = new AuthorisedUser();
            user.email = "admin@admin";
            user.password = Crypto.encryptAES("19771977");
            user.roles = new ArrayList<SecurityRole>();
            user.roles.add(SecurityRole.getByName("admin"));
            user.save();
            Profile profile = new Profile();
            profile.firstName = "admin";
            profile.lastName = "admin";
            profile.gender = "other";
            profile.phoneNumber = "9123";
            profile.save();
            user.profile = profile;
            user.status = "active";
            user.save();
            Ebean.saveManyToManyAssociations(user, "roles");
            // --------------------MODERATOR---------------------------
            user = new AuthorisedUser();
            user.email = "moder@moder";
            user.password = Crypto.encryptAES("19771977");
            user.roles = new ArrayList<SecurityRole>();
            user.roles.add(SecurityRole.getByName("moderator"));
            user.save();
            profile = new Profile();
            profile.firstName = "moder";
            profile.lastName = "moder";
            profile.gender = "other";
            profile.phoneNumber = "9123";
            user.dostyks.add(DostykLogic.getByName("Technovision"));
            profile.save();
            user.profile = profile;
            user.status = "active";
            user.save();
            Ebean.saveManyToManyAssociations(user, "roles");
            // --------------------TEACHER---------------------------
            user = new AuthorisedUser();
            user.email = "teacher@teacher";
            user.password = Crypto.encryptAES("19771977");
            user.roles = new ArrayList<SecurityRole>();
            user.roles.add(SecurityRole.getByName("teacher"));
            user.save();
            profile = new Profile();
            profile.firstName = "teacher";
            profile.lastName = "teacher";
            profile.gender = "other";
            profile.phoneNumber = "9123";
            user.dostyks.add(DostykLogic.getByName("Technovision"));
            profile.save();
            user.profile = profile;
            user.status = "active";
            user.save();
            Ebean.saveManyToManyAssociations(user, "roles");
            // --------------------STUDENT---------------------------
            user = new AuthorisedUser();
            user.email = "student@student";
            user.password = Crypto.encryptAES("19771977");
            user.roles = new ArrayList<SecurityRole>();
            user.roles.add(SecurityRole.getByName("student"));
            user.save();
            profile = new Profile();
            profile.lessons = LessonLogic.getAll();
            profile.firstName = "student";
            profile.lastName = "student";
            profile.gender = "other";
            profile.phoneNumber = "9123";
            profile.course = CourseTypeLogic.getByName("ENT");
            user.dostyks.add(DostykLogic.getByName("Technovision"));
            // profile.dostykGroup = DostykGroupLogic.getById(1);
            profile.region = RegionLogic.getAll().get(0);
            profile.district = DistrictLogic.getAll().get(0);
            // profile.school = SchoolLogic.getAll().get(0);
            profile.save();
            user.profile = profile;
            user.status = "active";
            user.save();
            Ebean.saveManyToManyAssociations(user, "roles");
            addModer();
        }
        if (LayoutLogic.find.findRowCount() == 0) {
            Layout layout = new Layout();
            layout.name = "D1";
            layout.save();
            Block block = new Block();
            block.name = "surname";
            block.delimeter = 17;
            block.weight = 1;
            block.checking = "81";
            block.layout = layout;
            block.save();
            layout.blocks.add(block);

            Block b2 = new Block();
            b2.name = "first";
            b2.delimeter = 1;
            b2.weight = 2;
            b2.checking = "82";
            b2.layout = layout;
            b2.save();
            layout.blocks.add(b2);

            Block b3 = new Block();
            b3.name = "oblast";
            b3.delimeter = 2;
            b3.weight = 3;
            b3.checking = "83";
            b3.layout = layout;
            b3.save();
            layout.blocks.add(b3);

            Block b4 = new Block();
            b4.name = "audan";
            b4.delimeter = 2;
            b4.weight = 4;
            b4.checking = "84";
            b4.layout = layout;
            b4.save();
            layout.blocks.add(b4);

            Block b5 = new Block();
            b5.name = "school";
            b5.delimeter = 3;
            b5.weight = 5;
            b5.checking = "85";
            b5.layout = layout;
            b5.save();
            layout.blocks.add(b5);

            Block b6 = new Block();
            b6.name = "group";
            b6.delimeter = 3;
            b6.weight = 6;
            b6.checking = "86";
            b6.layout = layout;
            b6.save();
            layout.blocks.add(b6);

            Block b7 = new Block();
            b7.name = "id";
            b7.delimeter = 8;
            b7.weight = 7;
            b7.checking = "87";
            b7.layout = layout;
            b7.save();
            layout.blocks.add(b7);

            Block b8 = new Block();
            b8.name = "variant";
            b8.delimeter = 4;
            b8.weight = 8;
            b8.checking = "55";
            b8.layout = layout;
            b8.save();
            layout.blocks.add(b8);

            Block b9 = new Block();
            b9.name = "tandagan pani";
            b9.delimeter = 1;
            b9.weight = 9;
            b9.checking = "99";
            b9.layout = layout;
            b9.save();
            layout.blocks.add(b9);

            Block b10 = new Block();
            b10.name = "kaz";
            b10.delimeter = 25;
            b10.weight = 10;
            b10.checking = "01";
            b10.layout = layout;
            b10.save();
            layout.blocks.add(b10);

            Block b11 = new Block();
            b11.name = "rus";
            b11.delimeter = 25;
            b11.weight = 11;
            b11.checking = "02";
            b11.layout = layout;
            b11.save();
            layout.blocks.add(b11);

            Block b12 = new Block();
            b12.name = "history";
            b12.delimeter = 25;
            b12.weight = 12;
            b12.checking = "03";
            b12.layout = layout;
            b12.save();
            layout.blocks.add(b12);

            Block b13 = new Block();
            b13.name = "math";
            b13.delimeter = 25;
            b13.weight = 13;
            b13.checking = "04";
            b13.layout = layout;
            b13.save();
            layout.blocks.add(b13);

            Block b14 = new Block();
            b14.name = "tandagan pan";
            b14.delimeter = 25;
            b14.weight = 14;
            b14.checking = "05";
            b14.layout = layout;
            b14.save();
            layout.blocks.add(b14);
            layout.save();
        }
        //addContractsToTestStudents();
        //parseMessages();
    }

    public void backupSchools() throws Exception{
        List<Region> regions = RegionLogic.getAll();
        PrintWriter pw = new PrintWriter(new FileWriter("public/csv files/regions.csv"));
        for(Region region: regions)
            pw.println(region.code+";"+region.name_kz+";"+region.name_ru);
        pw.flush();
        pw = new PrintWriter(new FileWriter("public/csv files/districts.csv"));
        for(Region region: regions)
            for(District district: DistrictLogic.getAllByRegion(region))
                pw.println(district.region.code+";"+district.code+";"+district.name_kz+";"+district.name_ru);
        pw.flush();
        pw = new PrintWriter(new FileWriter("public/csv files/schools.csv"));
        for(Region region: regions)
            for(District district: DistrictLogic.getAllByRegion(region)) {
                pw.println(region.code + ";" + district.code + ";000;Басқа;Другая");
                for (School school : SchoolLogic.getAllByInfo(region, district))
                    pw.println(school.region.code + ";" + school.district.code + ";" + school.code + ";"
                            + school.name_kz + ";" + school.name_ru);
            }
        pw.flush();
    }

    private void parseMessages() {
        //BufferedReader in = new BufferedReader(new FileReader(Play.application().resource("/views/main.scala.html").toString()));

    }

    public void addModer() {
        String dostyks[] = { "Ақтау", "Ақтөбе", "Алматы1", "Алматы2",
                "Алматы3", "Астана", "Атырау", "Екібастұз", "Көкшетау",
                "Қарағанды", "Қостанай", "Қызылорда", "Орал", "Өскемен",
                "Павлодар", "Семей", "Тараз", "Шымкент", "Астана_6-сынып",
                "Астана2", };
        String emails[] = { "aktau11", "aktobe12", "almaty13", "almaty14",
                "almaty15", "astana16", "aturay17", "ekibastuz18",
                "kokshetau19", "karaganda20", "kostanay21", "kizilorda22",
                "oral23", "oskemen24", "pavlodar25", "seme26", "taraz27",
                "shymkent28", "astana29", "astana30" };
        for (int i = 0; i < dostyks.length; i++) {
            AuthorisedUser user = new AuthorisedUser();
            user.email = emails[i] + "@dostykbilim.kz";
            user.password = Crypto.encryptAES(emails[i] + "@dostykbilim.kz");
            user.roles = new ArrayList<SecurityRole>();
            user.roles.add(SecurityRole.getByName("moderator"));
            user.status = "active";
            user.save();

            Profile profile = new Profile();
            profile.firstName = dostyks[i];
            profile.lastName = dostyks[i];
            profile.gender = "other";
            profile.phoneNumber = "";
            user.dostyks.add(DostykLogic.getByName(dostyks[i]));
            profile.save();
            user.profile = profile;
            user.save();
            Ebean.saveManyToManyAssociations(user, "roles");
        }
    }
    public void addContractsToTestStudents(){
        int count=0;
        for(Dostyk dostyk: DostykLogic.getAll())
        for(AuthorisedUser student: AuthorisedUserLogic.getStudents(dostyk)) {
            if(Contract.getByStudent(student).size()==0) {
                addContracts(student);
                count++;
            }
        }

    }
    public void addContracts(AuthorisedUser user){
        Payment active = PaymentLogic.getActualPayment(user);
        int count=0;
        if(active!=null)
            if (Contract.getByPayment(active).size() == 0) {
                Contract contract = new Contract();
                contract.beginDate = active.startDate;
                contract.number = active.contract;
                contract.endDate = active.endDate;
                contract.payment = active;
                contract.prefix = "ДКЗ";
                contract.student = user;
                contract.comment = "comment "+(count+1);
                contract.save();
                count++;
            }

    }
    public Promise<Result> onError(RequestHeader request, Throwable t) {
        String id = "";
        if(!play.api.Play.current().mode().toString().equals("Dev")) {
            if (t instanceof PlayException)
                id = ((PlayException) t).id + "";
            return F.Promise.<Result>pure(internalServerError(
                    errorPage.render(t, id)
            ));
        }else
            return super.onError(request,t);
    }

    public Promise<Result> onHandlerNotFound(RequestHeader request) {
//		List<Region> regions = RegionLogic.getAll();
//		for (Region region : regions) {
//			region.name_kz = Messages.get(region.code);
//			region.save();
//			for (District district : region.districts) {
//				district.name_kz = Messages.get(region.code+district.code);
//				district.save();
//				for(School school : district.schools) {
//					school.name_kz = Messages.get(region.code+district.code+school.code);
//					school.save();
//				}
//			}
//		}
        return Promise.<Result> pure(notFound(notFoundPage.render(request)));
    }
}