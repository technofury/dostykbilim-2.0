# Routes
# This file defines all application routes (Higher priority routes first)
# ~~~~

#TEN
GET         /ten/signup                                     controllers.ten.UserTen.signUp()
POST        /ten/signup                                     controllers.ten.UserTen.signUpSubmit()
GET         /ten/users                                      controllers.ten.UserTen.list(p:Int ?= 0, s ?= "name", o ?= "asc", f ?= "")
GET         /ten/sessions/:name                             controllers.ten.UserTen.getSessions(name:String)
GET         /ten/deleteFromDostyk                           controllers.ten.UserTen.deleteFromDostyk(id : Long)
GET         /ten/printXLS                                   controllers.ten.UserTen.printXLS
GET         /admin/ten/report                               controllers.ten.UserTen.genderReport()

# Topics
GET         /topic                                          controllers.education.Topics.report
GET         /group/:id                                      controllers.social.Dostyks.getGroups(id : Long)
GET         /topic/report                                   controllers.education.Topics.getReport(examId : Long, dostykId : Long, groupId : Long, lessonId: Long)
GET         /topic/report2                                  controllers.education.Topics.getReport2(examId : Long, userId : Long)
GET         /topic/report/print                             controllers.education.Topics.printReport(tip: String, examId: Long, userId: Long)
GET         /uploadPage                                     controllers.education.Topics.uploadPage()
GET         /getBy                                          controllers.education.Topics.getBy(lessonId : Long)
POST        /saveFile                                       controllers.education.Topics.saveUploadedFile(topicId : Long)

#SMS
GET         /decSMS                                         sms.Application.decSMS(mail : String)
GET         /getSMS                                         sms.Application.getSMS(mail : String)
GET         /smsAdmin                                       controllers.social.SMS.smsAdmin
GET         /smsWrite                                       controllers.social.SMS.smsWrite
POST         /smsSubmit                                      controllers.social.SMS.smsSubmit

GET         /saveSMS                                        controllers.social.SMS.saveSMS(res : Int, dostykId : Long)
GET         /sms                                            sms.Application.index


GET         /sms/:mail/:pass                                sms.Application.logIn(mail : String, pass : String)
GET         /sms/getDay/:mail/:date                         sms.Application.getDay(mail : String, date : String)
GET         /sms/getGroup/:date/:id                         sms.Application.getGroup(date : String, id : Long)
POST        /sms/save                                       sms.Users.save
GET         /sms/groups/all/:moder                          sms.Users.groups(moder : String)
GET         /sms/group/students/:group                      sms.Users.groupStudents(group : Long)



# Home page
GET         /                                               controllers.Application.index
POST        /support                                        controllers.Application.support
GET         /signin                                         controllers.user.AuthorisedUsers.signIn
GET         /signup/:whom                                   controllers.user.AuthorisedUsers.signUp(whom: String )
GET         /changeLanguage                                 controllers.Application.changeLanguage(lang:String)
GET         /studentList                                    controllers.user.UserManagement.showStudentList(id : Long)
GET         /uploadExcel                                    controllers.user.UserManagement.uploadExcel()
GET         /files/:file                                    controllers.ServeFiles.video(file: String)
GET         /downloadTest                                   controllers.ServeFiles.downloadTest(id: Long)

# Map static resources from the /public folder to the /assets URL path
GET         /assets/*file                                   controllers.Assets.at(path="/public", file)



# AJAX
GET         /javascript-routes                              controllers.Application.javascriptRoutes
GET         /district/:code                                 controllers.social.Regions.getDistricts(code : String)
GET         /courses/:name                                  controllers.education.CourseTypes.getByName(name : String)
POST        /user/upload                                    controllers.user.UserManagement.uploadStudents
GET         /school/:code                                   controllers.social.Districts.getSchools(code : String, reg : String)
GET         /students/:reg                                  controllers.user.UserManagement.getStudentList(reg : String, dis : String, school : String)
GET         /discount-max/delete/:id                        controllers.accounting.DiscountMaxes.deleteDiscountMax(id : Long)


#GET         /timetable                                      controllers.education.Timetables.save(lessonId : String, electiveId : String, teacherId : String, cabinet : String, groupId : Long, row : Int, startTime : String, endTime : String, day : Int, typ : Int)
#GET         /timetable/delete                               controllers.education.Timetables.delete(groupId : Long, row : Int, day : Int, isExtra : Int)
GET         /changeStatus                                   controllers.user.AuthorisedUsers.changeStatus(id : Long)
GET         /deleteFromDostyk                               controllers.user.AuthorisedUsers.deleteFromDostyk(id : Long)
GET         /changeDostykGroup                              controllers.user.AuthorisedUsers.changeDostykGroup(userId : Long, dostykGroupId : Long)
GET         /changeElectiveGroup                            controllers.user.AuthorisedUsers.changeElectiveGroup(userId : Long, electiveGroupId : Long)
GET         /deleteAnswer                                   controllers.examination.Answers.delete(questionNumber : Int, lessonAnswerId : Long)
POST        /submitResult                                   controllers.examination.Exams.submitResult(examId : Long, idNumber : String, variant : String, pan : String, l1 : String, l2 : String, l3 : String, l4 : String, l5 : String)
POST        /submitResultD5                                 controllers.examination.Exams.submitResultD5(examId : Long, idNumber : String, variant : String, ls : String)

# EXAMINATION
GET         /manageLayouts                                  controllers.examination.Layouts.manage()
GET         /manageExamTypes                                controllers.examination.ExamTypes.manage()
GET         /manageExams                                    controllers.examination.Exams.manage()
GET         /manageAnswerKeys                               controllers.examination.AnswerKeys.manage()
GET         /options                                        controllers.examination.AnswerKeys.options(id : Long)


GET         /save                                           controllers.examination.Layouts.save()
GET         /saveBlock                                      controllers.examination.Blocks.save()
GET         /saveExamType                                   controllers.examination.ExamTypes.save()
GET         /saveExam                                       controllers.examination.Exams.save()
GET         /saveAnswerKey                                  controllers.examination.AnswerKeys.save()
GET         /saveLessonAnswer                               controllers.examination.LessonAnswers.save()
GET         /video                                          controllers.examination.LessonAnswers.video()
GET         /getVideo                                       controllers.examination.LessonAnswers.getVideo(exam : Long, variant: String, lesson: Long)
GET         /get-video/:ex/:vari/:les                       controllers.examination.LessonAnswers.getVideo2(ex: Long, vari: String, les: Long)
POST        /saveUploadedFile                               controllers.examination.LessonAnswers.saveUploadedFile(id : Long)
POST        /saveAnswer                                     controllers.examination.Answers.save()

GET         /delete                                         controllers.examination.Layouts.delete(id : Long)
GET         /deleteBlock                                    controllers.examination.Blocks.delete(id : Long)
GET         /deleteExamType                                 controllers.examination.ExamTypes.delete(id : Long)
GET         /deleteExam                                     controllers.examination.Exams.delete(id : Long)
GET         /deleteAnswerKey                                controllers.examination.AnswerKeys.delete(id : Long)
GET         /deleteLessonAnswer                             controllers.examination.LessonAnswers.delete(id : Long)


GET         /showByLayout                                   controllers.examination.Blocks.showByLayout(id : Long)
GET         /showByAnswerKey                                controllers.examination.LessonAnswers.showByAnswerKey(id : Long)
GET         /showByLessonAnswer                             controllers.examination.Answers.showByLessonAnswer(id : Long)
GET         /uploadVideo                                    controllers.examination.Answers.uploadVideo(id : Long)
GET         /showByUser                                     controllers.examination.ExamResults.showByUser(id : Long)
GET         /showByUser2                                    controllers.examination.ExamResults.showByUser2(id : Long)
GET         /printByUser/user=:uid/exam=:eid                controllers.examination.ExamResults.printByUser(uid:Long,eid:Long)

POST        /upload                                         controllers.examination.Exams.upload(examId : Long)
GET         /showActive                                     controllers.examination.Exams.showActive()
GET         /checkExam                                      controllers.examination.Exams.checkExam(examId : Long)
POST        /changeStatus                                   controllers.examination.Exams.changeStatus(examId : Long, status : String)
POST        /changeName                                     controllers.examination.Exams.changeName(examId : Long, name : String)
POST        /changeCode                                     controllers.examination.Exams.changeCode(id : Long, code : String)


# ATTENDANCE & TIMETABLE
GET         /currentDate                                    controllers.education.Registers.currentDate()
POST        /otherDate                                      controllers.education.Registers.otherDate()
GET         /attendanceList                                 controllers.education.Registers.attendanceList(date : String, timetableId : Long)
POST        /save                                           controllers.education.Registers.save()
GET         /show                                           controllers.CTimetable.view(id : Long)
GET         /showRegister                                   controllers.education.Registers.show(id : Long)
POST        /showRegister2                                  controllers.education.Registers.show2(id : Long)
#GET         /timetable2                                     controllers.education.Timetables.show2()
GET         /registerMain                                   controllers.education.Registers.main()
GET         /getByLessonAndDate                             controllers.education.Registers.getByLessonAndGroup(lessonId : Long, groupId : Long, date : String)
GET         /getGroups                                      controllers.education.Registers.getGroups(lessonId : Long)
GET         /saveRegisters                                  controllers.education.Registers.save2(states : String, users : String, lessonId : Long, groupId : Long, date : String)



# Extra Schedule
GET         /extraMain                                      controllers.education.ExtraJournals.main()
GET         /getTeachers                                    controllers.education.ExtraJournals.getTeachers(groupId : Long, lessonId : Long)
GET         /reportByTeacher                                controllers.education.ExtraJournals.reportByTeacher()
#GET			/getRegisters					 controllers.education.ExtraJournals.getRegisters(lessonId : Long, teacherId : Long)
GET         /teachersByLesson                               controllers.education.ExtraJournals.teachersByLesson(id : Long)
GET         /teachersByDostyk                               controllers.education.ExtraJournals.teachersByDostyk(id : Long)
GET         /extraLessonsByTeacher                          controllers.education.ExtraJournals.extraLessonsByTeacher(id : Long)
GET         /extraGetByLessonAndDate                        controllers.education.ExtraJournals.getByLessonAndGroup(lessonId : Long, groupId : Long, date : String, teacherId : Long)
GET         /extraSaveRegisters                             controllers.education.ExtraJournals.save(states : String, users : String, lessonId : Long, groupId : Long, date : String, teacherId : Long, topicId : Long)
GET         /saveMarks                                      controllers.education.ExtraJournals.saveMarks(users : String, marks : String, which : String, date : String)

# ATTUTT
GET         /saveATTUTT                                     controllers.examination.ATTUTTs.save(users : String, marks : String, lessonId : Long, number : Int, type : String)
GET         /getATT                                         controllers.examination.ATTUTTs.getATT(users : String, lessonId : Long, number : Int)
GET         /getUTT                                         controllers.examination.ATTUTTs.getUTT(users : String, lessonId : Long, number : Int)
GET         /saveTopicRegister                              controllers.education.TopicRegisters.save(topic : Long, date : String, group : Long, lessonId : Long)
GET         /reportATTUTT                                   controllers.examination.ATTUTTs.report()
GET         /reportByLesson                                 controllers.examination.ATTUTTs.reportByLesson(lessonId : Long)






# User
POST        /user/submit                                    controllers.user.AuthorisedUsers.submit
POST        /user/check                                     controllers.user.AuthorisedUsers.userCheck
GET         /user/logout                                    controllers.user.AuthorisedUsers.signOut
GET         /profile                                        controllers.user.AuthorisedUsers.showProfile()
GET         /profile/edit                                   controllers.user.AuthorisedUsers.editProfile(id : Long = -1)
GET         /profile/edit/:id                               controllers.user.AuthorisedUsers.editProfile(id : Long)
POST        /profile/save/:id                               controllers.user.AuthorisedUsers.saveProfile(id : Long)
GET         /user/mail/edit/:id                             controllers.user.AuthorisedUsers.editMail(id: Long)
POST        /user/mail/change/:id                           controllers.user.AuthorisedUsers.changeMail(id : Long)
GET         /user/password/edit/:id                         controllers.user.AuthorisedUsers.editPassword(id : Long)
POST        /user/password/change/:id                       controllers.user.AuthorisedUsers.changePassword(id : Long)
GET         /user/password/restore                          controllers.user.AuthorisedUsers.restorePasswordPage()
POST        /user/password/restoreIt                        controllers.user.AuthorisedUsers.restorePassword()
POST        /bill-form/:id                                  controllers.accounting.BillForms.saveBillForm(id: Long)
GET         /bill-form/:id                                  controllers.accounting.BillForms.showBillForm(id: Long)
GET         /dogovor                                        controllers.accounting.Payments.getDogovor(id : Long)
GET         /outlays                                        controllers.accounting.Payments.outlays
POST        /outlays                                        controllers.accounting.Payments.outlays
GET         /payment-list/:id                               controllers.accounting.Payments.showPaymentList(id: Long)
POST        /payment-list/save/:id                          controllers.accounting.Payments.savePaymentList(id: Long)
POST        /payment-list/undo                              controllers.accounting.Payments.undoPaymentList
POST        /payment-list/reset                             controllers.accounting.Payments.resetPaymentList
POST        /payment-list                                   controllers.accounting.Payments.deletePaymentList
GET         /users                                          controllers.user.UserManagement.list(p:Int ?= 0, s ?= "name", o ?= "asc", f ?= "", g ?="all", r ?= "all")
GET         /all-users                                      controllers.user.UserManagement.listAll(p:Int ?= 0, s ?= "name", o ?= "asc", f ?= "")
GET         /moders                                         controllers.user.UserManagement.moderators()
GET         /directors                                      controllers.user.UserManagement.directors()
POST        /saveMekeme                                     controllers.accounting.BillForms.saveMekeme()
GET         /diactivateAllStudents                          controllers.user.AuthorisedUsers.diactivateAllStudents()
GET         /memoryUsage                                    controllers.user.UserManagement.memoryUsage()
GET         /gc                                             controllers.user.UserManagement.garbageCollector()
GET         /user/delete/dang/:id                           controllers.user.UserManagement.deleteUser(id: Long)

# Dostyk Group
GET         /groups                                         controllers.social.DostykGroups.all()
GET         /group/change-status                            controllers.social.DostykGroups.changeStatus(id : Long)
POST        /group/save                                     controllers.social.DostykGroups.submit()
GET         /group/edit/:id                                 controllers.social.DostykGroups.edit(id : Long)
# erinbei knopka jasa)
GET         /group/delete/:id                               controllers.social.DostykGroups.delete(id: Long)
GET         /group/:id/students                             controllers.social.DostykGroups.groupStudents(id : Long)
GET         /group/:id/tabel                                controllers.social.DostykGroups.groupTabel(id : Long)
GET         /group/tabelByExam                              controllers.social.DostykGroups.groupTabelByExam(id : Long, examId : Long)

# Dostyk Content
GET         /content/all                                    controllers.social.Contents.all()
POST        /content/save                                   controllers.social.Contents.save()
GET         /content/edit/:id                               controllers.social.Contents.edit(id : Long)
GET         /content/delete/:id                             controllers.social.Contents.delete(id : Long)

# Timetable
#GET         /groupTimetable                                 controllers.education.Timetables.edit(id : Long, isExtra : Int)

# Accounting [Bill]
GET         /discount-values                                controllers.accounting.DiscountValues.showDiscountValues
POST        /discount-values/save                           controllers.accounting.DiscountValues.saveDiscountValues
GET         /discount-maxes                                 controllers.accounting.DiscountMaxes.showDiscountMaxes
POST        /discount-maxes/save                            controllers.accounting.DiscountMaxes.saveDiscountMax
GET         /discount-maxes/edit/:id                        controllers.accounting.DiscountMaxes.editDiscountMax(id: Long)
POST        /discount-maxes/edit/:id                        controllers.accounting.DiscountMaxes.saveExistingDiscount(id : Long)
GET         /print-receipt/                                 controllers.accounting.Payments.printReceipt(userId : Long, receiptId : Long)
POST        /change-course/                                 controllers.accounting.Payments.changeCourse(id : Long, paymentId: Long)
POST        /payment-suspend/                               controllers.accounting.Payments.suspendPayment(id : Long, paymentId: Long)
POST        /update-lessons/                                controllers.accounting.Payments.updateLessons()
GET         /payment-json/:sid                              controllers.accounting.Payments.getPaymentStatus(sid: Long)
GET         /print-changes/:uid/:pid/:ls/:h/:t/:d/:n        controllers.accounting.Payments.printChanges(uid:Long,pid:Long,ls:String,h:String,t:String,d:String,n:String)

# Holidays
GET         /holiday                                        controllers.accounting.Holidays.getHolidays()
POST        /holiday/add                                    controllers.accounting.Holidays.addHoliday()
GET         /holiday/show/:id                               controllers.accounting.Holidays.showHolidayById(id:Long)
GET         /holiday/edit/:id                               controllers.accounting.Holidays.editHoliday(id:Long)
GET         /holiday/delete/:id                             controllers.accounting.Holidays.deleteHoliday(id:Long)

# Lesson Hour
GET         /lessonHours                                    controllers.education.LessonHourss.set()
POST        /lessonHours/save                               controllers.education.LessonHourss.save()

# Price List
GET         /priceList                                      controllers.accounting.PriceLists.set()
POST        /priceList/save                                 controllers.accounting.PriceLists.save()
POST        /priceList/update                               controllers.accounting.PriceLists.update()
POST        /priceList/delete                               controllers.accounting.PriceLists.delete()

# Administer
GET         /dostyk/delete/:id                              controllers.social.Dostyks.deleteDostyk(id: Long)
GET         /dostyk/edit/:id                                controllers.social.Dostyks.editDostyk(id: Long)
GET         /dostyk/list                                    controllers.social.Dostyks.showDostyks
POST        /dostyk/save                                    controllers.social.Dostyks.saveDostyk
POST        /dostyk/edit/:id                                controllers.social.Dostyks.saveExistingDostyk(id : Long)

GET         /course-type/list                               controllers.education.CourseTypes.showCourseTypes
POST        /course-type/save                               controllers.education.CourseTypes.saveCourseType
GET         /course-type/edit/:id                           controllers.education.CourseTypes.editCourseType(id: Long)
POST        /course-type/edit/:id                           controllers.education.CourseTypes.updateCourseType(id: Long)
GET         /course-type/delete/:id                         controllers.education.CourseTypes.deleteCourseType(id: Long)
GET         /topic/list                                     controllers.education.Topics.manage
POST        /topic/save                                     controllers.education.Topics.save
GET         /topic/delete/:id                               controllers.education.Topics.delete(id: Long)
GET         /topic/topics/:lid&:cid                         controllers.education.Topics.getTopic(lid: Long, cid: Long)
GET         /lesson/list                                    controllers.education.Lessons.getCourseList
GET         /lesson/delete/:id                              controllers.education.Lessons.deleteLesson(id: Long)
GET         /lesson/edit/:id                                controllers.education.Lessons.editLesson(id: Long)
POST        /lesson/save                                    controllers.education.Lessons.saveLesson
POST        /lesson/edit/:id                                controllers.education.Lessons.saveExistingLesson(id : Long)

# Letscope reports

GET        /examList                                       controllers.letscope.reportController.getExamList
GET        /examStudentList                                controllers.letscope.reportController.getStudentExamList
GET         /printPdf/:id                                  controllers.letscope.reportController.printPdf(id: String)
GET         /printAllPdf/:id                               controllers.letscope.reportController.printAllPdf(id: String)
POST        /getStudId                                       controllers.Application.getStudId

# Reports
#            /report/atynbelgi
GET         /report/ulgerimi                                controllers.user.UserManagement.ulgerimiReport()
GET         /report/gender                                  controllers.user.UserManagement.genderReport()
GET         /report/registration                            controllers.user.UserManagement.registrationReport()
POST        /report/registrationSubmit                      controllers.user.UserManagement.registrationReportSubmit()
GET         /report/new/registration                        controllers.user.UserManagement.newRegistration()
GET         /report/movement                                controllers.user.UserManagement.movementReport()
GET         /report/left                                    controllers.user.UserManagement.leftReport()
GET         /report/movementPrint                           controllers.user.UserManagement.movementReportPrint(id:Long, index:Int)
GET         /report/revenue                                 controllers.user.UserManagement.revenueReport()
GET         /report/revenuePrint                            controllers.user.UserManagement.revenueReportPrint(id:Long, index:Int)
GET         /report/jurnal                                  controllers.user.UserManagement.jurnalReport()
GET         /report/jurnalPrint                             controllers.user.UserManagement.jurnalReportPrint(id:Long, range:String)
GET         /report/new/revenue                             controllers.user.UserManagement.newRevenue()
GET         /report/cash-book                               controllers.user.UserManagement.cashBook()
POST        /report/cash-book/get                           controllers.user.UserManagement.getCashBook()
GET         /report/studentSchool                           controllers.user.UserManagement.getStudentBySchool()

GET         /report/courseType                              controllers.user.UserManagement.courseTypeReport()
GET         /report/payment/dostyk/                         controllers.accounting.Payments.dostykReport()
GET         /report/payment/group/                          controllers.accounting.Payments.groupReport(dostykId: Long)
GET         /report/payment/student/                        controllers.accounting.Payments.studentReport(groupId: Long)
GET         /report/payment/                                controllers.accounting.Payments.paymentReport()
GET         /report/discount/                               controllers.accounting.DiscountValues.discountReport()
GET         /report/exam/                                   controllers.examination.ExamResults.examReport()
GET         /report/exam/dostyk/:dostykId                   controllers.examination.ExamResults.examReportByDostyk(examId: Long, dostykId : Long)
GET         /report/exam/dostykGroup/:id                    controllers.examination.ExamResults.examReportByDostykGroup(examId: Long, id : Long)
GET         /report/group/                                  controllers.examination.ExamResults.groupReport()
GET         /report/showGroup/                              controllers.examination.ExamResults.showGroup(groupId : Long, type : Int)
POST        /report/saveEnt/                                controllers.examination.ExamResults.saveEnt()
GET         /report/inactiveUsers/                          controllers.examination.ExamResults.deletedUsers()
GET         /report/discounts                               controllers.accounting.Payments.getDiscountReport()


GET         /firms/view-all                                 controllers.social.Firms.viewAll
POST        /firms/create                                   controllers.social.Firms.save(dostykId: Long,firmId:Long,isNew:Boolean)
GET         /firms/delete                                   controllers.social.Firms.delete(firmId: Long)

#printable
GET 		/print/deal/:no/:date/:sid						controllers.accounting.BillForms.printDeal(no:String,date:String,sid:Long)

#REST API
POST        /rest/login                                     rest.controllers.Users.authUser()
POST        /rest/today                                     rest.controllers.Users.today()
POST        /rest/all-in-one                                rest.controllers.Users.allInOne()
POST        /rest/email-save                                rest.controllers.Users.saveEmail()
POST        /rest/password-save                             rest.controllers.Users.savePassword()
POST        /rest/telephone-save                            rest.controllers.Users.saveTelephone()
GET         /rest/schedule/:uid                             rest.controllers.Schedules.checkSchedule(uid: Long)
POST        /rest/schedule                                  rest.controllers.Schedules.schedule()
GET         /rest/attendance/:uid                           rest.controllers.Schedules.checkAttendance(uid: Long)
POST        /rest/attendance                                rest.controllers.Schedules.attendance()
POST        /rest/attendance/save                           rest.controllers.Schedules.saveAttendance()
GET         /rest/payment/:uid                              rest.controllers.Agreements.check(uid: Long)
POST        /rest/payment                                   rest.controllers.Agreements.get()
GET         /rest/exam-results/:uid                         rest.controllers.Exams.check(uid: Long)
POST        /rest/exam-results                              rest.controllers.Exams.results()
GET         /rest/videos/all                                rest.controllers.Exams.getAllVideos()
POST        /rest/videos/all                                rest.controllers.Exams.postAllVideos()
GET         /rest/group                                     rest.controllers.Groups.checkGroup()
POST        /rest/group                                     rest.controllers.Groups.group()
GET         /rest/group/single/:gid                         rest.controllers.Groups.checkSingle(gid: Long)
POST        /rest/group/single                              rest.controllers.Groups.single()
POST        /rest/notification                              rest.controllers.Notifications.getNotification
POST        /rest/notification/save                         rest.controllers.Notifications.saveNotification
POST        /rest/group/curator                             rest.controllers.Groups.getCuratorGroups

GET 		/is-alive										controllers.Application.isAlive

# ORDERS
GET         /addOrder                                       controllers.order.orderController.addOrder()
POST        /saveOrder                                      controllers.order.orderController.saveOrder()
POST        /updateOrder                                    controllers.order.orderController.updateOrder()
GET         /deleteOrder/:id                                controllers.order.orderController.deleteOrder(id:Long)
GET         /editOrder/:id                                  controllers.order.orderController.editOrder(id:Long)
GET         /getImage/:name                                 controllers.order.orderController.getImage(name: String)
GET         /showOrderCount/:id                             controllers.order.orderController.showOrderCount(id:Long)
GET         /updateOrderAmount/:id/:amount                  controllers.order.orderController.updateOrderAmount(id: Long, amount: Int)

GET         /settings                                       controllers.order.orderController.Categories()
GET         /addCategory/:name                              controllers.order.orderController.addCategory(name: String)
GET         /deleteCategory/:id                             controllers.order.orderController.deleteCategory(id: Long)
GET         /updateCategory/:id/:name                       controllers.order.orderController.updateCategory(id: Long, name: String)

POST        /suplier/create                                 controllers.order.orderController.save(suplierId:Long,isNew:Boolean)
GET         /suplier/delete                                 controllers.order.orderController.delete(firmId: Long)

GET         /debtors                                        controllers.order.orderController.debtors()
GET         /showDostykPayments/:id                         controllers.order.orderController.showDostykPayments(id: Long)
POST        /savePayment                                    controllers.order.orderController.savePayment()

GET         /orderReport                                    controllers.order.orderController.orderReport()
GET         /getOrdersByCategory/:id                        controllers.order.orderController.getOrdersByCategory(id:Long)

GET         /paymentReport                                  controllers.order.orderController.paymentReport()
GET         /getPaymentReport/:start/:end                   controllers.order.orderController.getPaymentReport(start:String,end:String)

GET         /saveFirm/:id/:firmId                           controllers.order.orderController.saveFirm(id:Long,firmId:Long)

GET         /viewAllOrder                                   controllers.order.orderController.viewAll()
GET         /defaultOrderValue                              controllers.order.orderController.setDefaultValue()
POST        /saveOrderValue                                 controllers.order.orderController.saveDefaultValue()
POST        /saveOrderCount                                 controllers.order.orderController.saveOrderCount()
GET         /viewDostykOrder                                controllers.order.orderController.viewDostykOrderAmount()
POST        /viewDostykOrder                                controllers.order.orderController.viewDostykOrderAmount()

#TimetableSettings
GET		  /timetable/settings/edit/:id			            controllers.CTimetableSettings.edit(id:String)
POST	  /timetable/settings/edit/:id			            controllers.CTimetableSettings.save(id:String)
GET		  /timetable/settings/active/:id		            controllers.CTimetableSettings.active(id:Long)
GET		  /timetable/settings/delete/:id		            controllers.CTimetableSettings.delete(id:Long)

#Timetable
GET		  /timetable/edit/:gid/:ttid					    controllers.CTimetable.edit(gid:Long,ttid:String)
POST	  /timetable/save/:gid/:ttid/:sid				    controllers.CTimetable.save(gid:Long,ttid:String,sid:String)
GET		  /timetable/active/:ttid							controllers.CTimetable.active(ttid:Long)
GET		  /timetable/delete/:ttid							controllers.CTimetable.delete(ttid:Long)
GET 	  /timetable/view-all/:gid/:st				        controllers.CTimetable.viewAll(gid:Long,st:String)
GET 	  /timetable/view/:ttid								controllers.CTimetable.view(ttid:Long)
GET       /groupTimetable/:gid                              controllers.CTimetable.viewGroupTimetable(gid:Long)
GET       /viewTeacherTimetable                             controllers.CTimetable.viewTeacherTimetable()

#Attendance
GET		  /attendance/:lid/:today							controllers.education.Attendances.edit(lid:Long,today:String)
POST	  /attendance/update								controllers.education.Attendances.update()
GET		  /attendance/student								controllers.education.Attendances.view()
POST	  /attendance/student								controllers.education.Attendances.viewBetween()

