
# --- !Ups

alter table profiles add lang varchar(2);
alter table topics add courses_id bigint;

create table holiday(
  id                  bigint not null,
  start_date          timestamp,
  end_date            timestamp,
  constraint pk_holiday primary key (id)
);

create sequence holiday_seq;

alter table topics add constraint fk_topics_courses_id foreign key (courses_id) references courses (id);
create index ix_topics_courses_id on topics (courses_id);

# --- !Downs

alter table profiles drop lang;
alter table topics drop courses_id;

drop sequence if exists holiday_seq;
drop table if exists holiday;

