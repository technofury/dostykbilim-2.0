# --- !Ups
drop table if exists timetable_settings cascade;
drop sequence if exists timetable_settings_seq;
create table timetable_settings (
  id                        	bigint not null,
  name							varchar(255),
  lesson_time                   integer,
  lesson_days               	integer,
  max_lessons					integer,
  is_active						boolean,
  organization_id				bigint,
  constraint pk_timetable_settings primary key (id))
;

create sequence timetable_settings_seq;

alter table timetable_settings add constraint fk_timetable_settings_organization_1 foreign key (organization_id) references dostyks (id);
create index ix_timetable_settings_organization_1 on timetable_settings (organization_id);

# --- !Downs

drop sequence if exists timetable_settings_seq;

drop table if exists timetable_settings cascade;