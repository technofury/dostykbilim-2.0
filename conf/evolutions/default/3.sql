
# --- !Ups

create table category(
  id                  bigint not null,
  name                varchar(255),
  constraint pk_category primary key (id)
);

create table suplier(
  id                        bigint not null,
  name                      varchar(255),
  city                      varchar(255),
  address                   varchar(255),
  phone                     varchar(255),
  director                  varchar(255),
  bik                       varchar(255),
  iik                       varchar(255),
  bin                       varchar(255),
  bank                      varchar(255),
  kbe                       varchar(255),
  constraint pk_suplier primary key (id));

create sequence category_seq;
create sequence suplier_seq;

# --- !Downs

drop sequence if exists category_seq;
drop sequence if exists suplier_seq;

drop table if exists category;
drop table if exists suplier;

