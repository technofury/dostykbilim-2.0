# --- !Ups

create table attendances(
  id                        	bigint not null,
  status						integer,
  mydate                        date,
  lesson_id				        bigint,
  student_id                    bigint,
  organization_id				bigint,						
  constraint pk_attendances primary key (id)
);

create sequence attendances_seq;

alter table attendances add constraint fk_attendances_lesson_1 foreign key (lesson_id) references timetable_lessons (id);
create index ix_attendances_lesson_1 on attendances (lesson_id);

alter table attendances add constraint fk_attendances_student_1 foreign key (student_id) references users (id);
create index ix_attendances_student_1 on attendances (student_id);

alter table attendances add constraint fk_attendances_organization_1 foreign key (organization_id) references dostyks (id);
create index ix_attendances_organization_1 on attendances (organization_id);

# --- !Downs

drop sequence if exists attendances_seq;

drop table if exists attendances cascade;
