
# --- !Ups
create table default_order_value(
  id                  bigint not null,
  value               int,
  dostyk_id           bigint,
  constraint pk_default_order_value primary key (id)
);

create table order_status(
  id                  bigint not null,
  status              varchar(255),
  constraint pk_order_status primary key (id)
);

create table orders(
  id                  bigint not null,
  name                varchar(255),
  category_id         bigint,
  price               bigint,
  order_status_id     bigint,
  description         varchar(500),
  date                timestamp,
  image               varchar(255),
  constraint pk_orders primary key(id)
);

create table order_count(
  id                  bigint not null,
  orders_id           bigint,
  dostyk_id           bigint,
  count               bigint,
  constraint pk_order_count primary key(id)
);

create table order_payments(
  id            bigint not null,
  dostyk_id     bigint,
  amount        bigint,
  date          timestamp,
  constraint pk_order_payments primary key(id)
);


create table debt(
  id            bigint not null,
  dostyk_id     bigint,
  debt          bigint,

  constraint pk_debt primary key(id)
);

alter table dostyks add main_firm_id VARCHAR;

create sequence default_order_value_seq;
create sequence order_status_seq;
create sequence orders_seq;
create sequence order_count_seq;
create sequence order_payments_seq;
create sequence debt_seq;

# --- !Downs

alter table dostyks drop main_firm_id;

drop sequence if exists default_order_value_seq;
drop sequence if exists order_status_seq;
drop sequence if exists orders_seq;
drop sequence if exists order_count_seq;
drop sequence if exists order_payments_seq;
drop sequence if exists debt_seq;

drop table if exists default_order_value;
drop table if exists order_status;
drop table if exists orders;
drop table if exists order_count;
drop table if exists order_payments;
drop table if exists debt;


