
# --- !Ups

create table jauaptar (
id                        bigint not null,
question_number           integer,
answer_code               varchar(255),
topic                     varchar(255),
lesson_answer_id          bigint,
path_to_video             varchar(255),
constraint pk_jauaptar primary key (id));

create table jauapkilti (
id                        bigint not null,
variant                   varchar(255),
grade                     integer,
exam_id                   bigint,
constraint uq_jauapkilti_variant unique (variant),
constraint pk_jauapkilti primary key (id));

create table users (
id                        bigint not null,
status                    varchar(255),
created_date              date,
changed_date              date,
email                     varchar(255),
password                  varchar(255),
profile_id                bigint,
requisite_id              bigint,
firm_id                   bigint,
constraint uq_users_email unique (email),
constraint pk_users primary key (id));

create table blocks (
id                        bigint not null,
layout_id                 bigint,
weight                    integer,
delimeter                 integer,
name                      varchar(255),
checking                  varchar(255),
constraint pk_blocks primary key (id));

create table contents (
id                        bigint not null,
dostyk_id                 bigint,
dostyk_group_id           bigint,
title                     varchar(255),
body                      TEXT,
author_id                 bigint,
created_date              timestamp,
changed_date              timestamp,
constraint pk_contents primary key (id));

create table course_types (
id                        bigint not null,
code                      varchar(255),
name                      varchar(255),
class_number              integer,
constraint uq_course_types_code unique (code),
constraint uq_course_types_name unique (name),
constraint pk_course_types primary key (id));

create table detailed_results (
id                        bigint not null,
exam_result_id            bigint,
lesson_id                 bigint,
correct                   integer,
incorrect                 integer,
blank                     integer,
mark                      integer,
student_answers           varchar(255),
rank_group                integer,
rank_dostyk               integer,
rank_kz                   integer,
constraint pk_detailed_results primary key (id));

create table discounts_maxes (
id                        bigint not null,
name                      varchar(255),
value                     integer,
constraint uq_discounts_maxes_name unique (name),
constraint pk_discounts_maxes primary key (id));

create table discounts_values (
id                        bigint not null,
dostyk_id                 bigint,
value                     integer,
dmax_id                   bigint,
constraint pk_discounts_values primary key (id));

create table districts (
id                        bigint not null,
code                      varchar(255),
region_id                 bigint,
name_kz                   varchar(255),
name_rus                  varchar(255),
constraint pk_districts primary key (id));

create table dostyks (
id                        bigint not null,
name                      varchar(255),
code                      integer,
organization              varchar(255),
sms_left                  int NOT NULL default 0,
status                    integer,
constraint uq_dostyks_code unique (code),
constraint pk_dostyks primary key (id));

create table dostyk_groups (
id                        bigint not null,
name                      varchar(255),
grade                     varchar(255),
type                      varchar(255),
language                  varchar(255),
study_period              varchar(255),
curator_id                bigint,
time_table_id             bigint,
dostyk_id                 bigint,
created                   timestamp,
changed                   timestamp,
start_date                timestamp,
end_date                  timestamp,
payment_per_student       float,
status                    varchar(255),
lessonid                  bigint,
constraint pk_dostyk_groups primary key (id));

create table ent (
id                        bigint not null,
points                    integer,
user_id                   bigint,
kaz                       integer,
rus                       integer,
math                      integer,
history                   integer,
selected                  integer,
lesson_id                 bigint,
constraint pk_ent primary key (id));

create table exams (
id                        bigint not null,
name                      varchar(255),
exam_type_id              bigint,
start_date                timestamp,
end_date                  timestamp,
status                    varchar(255),
grade                     integer,
constraint pk_exams primary key (id));

create table exam_results (
id                        bigint not null,
student_id                bigint,
exam_id                   bigint,
variant                   varchar(255),
rank_group                integer,
rank_dostyk               integer,
rank_kz                   integer,
total                     integer,
constraint pk_exam_results primary key (id));

create table exam_types (
id                        bigint not null,
name                      varchar(255),
layout_id                 bigint,
constraint pk_exam_types primary key (id));

create table layouts (
id                        bigint not null,
name                      varchar(255),
constraint pk_layouts primary key (id));

create table lessons (
id                        bigint not null,
name                      varchar(255),
language                  varchar(255),
code                      varchar(255),
constraint pk_lessons primary key (id));

create table lesson_answer (
id                        bigint not null,
lesson_id                 bigint,
answer_key_id             bigint,
code                      varchar(255),
path_to_video             varchar(255),
constraint pk_lesson_answer primary key (id));

create table lesson_hours (
id                        bigint not null,
lesson_id                 bigint,
dostyk_id                 bigint,
hours                     integer,
constraint pk_lesson_hours primary key (id));

create table parents (
id                        bigint not null,
child_id                  bigint,
first_name                varchar(255),
last_name                 varchar(255),
parent_type               varchar(255),
phone_number1             varchar(255),
phone_number2             varchar(255),
work                      varchar(255),
responsible               boolean,
inn                       varchar(255),
constraint pk_parents primary key (id));

create table payments (
id                        bigint not null,
user_id                   bigint,
status                    varchar(255),
start_date                timestamp,
end_date                  timestamp,
material_cost             float,
planned_payment           float,
payed_payment             float,
debt_payment              float,
discount                  integer,
discount_bonus            float,
hours_per_week            float,
price_per_week            float,
weeks                     integer,
comment                   varchar(255),
contract                  varchar(255),
contract_date             TIMESTAMP,
constraint pk_payments primary key (id));

create table payment_periods (
id                        bigint not null,
payment_id                bigint,
period                    integer,
planned_payment           float,
planned_payment_date      timestamp,
payed_payment             float,
payed_payment_date        timestamp,
debt_payment              float,
status                    varchar(255),
comment                   varchar(255),
receipt_id                bigint,
constraint pk_payment_periods primary key (id));

create table payment_receipts (
id                        bigint not null,
number                    integer,
user_id                   bigint,
payment_id                bigint,
dostyk_id                 bigint,
created_date              timestamp,
period_id                 bigint,
constraint pk_payment_receipts primary key (id));

create table price_list (
id                        bigint not null,
dostyk_id                 bigint,
course_type_id            bigint,
hours                     integer,
price                     float,
constraint pk_price_list primary key (id));

create table profiles (
id                        bigint not null,
id_number                 varchar(255),
first_name                varchar(255),
last_name                 varchar(255),
birth_date                timestamp,
gender                    varchar(255),
phone_number              varchar(255),
parent_number             varchar(255),
ulgerimi                  varchar(255),
dostyk_group_id           bigint,
elective_group_id         bigint,
course_id                 bigint,
region_id                 bigint,
district_id               bigint,
school_id                 bigint,
second_name               varchar(255),
inn                       varchar(255),
exam_session_id           bigint,
constraint uq_profiles_id_number unique (id_number),
constraint pk_profiles primary key (id));

create table regions (
id                        bigint not null,
code                      varchar(255),
name_kz                   varchar(255),
name_rus                  varchar(255),
constraint pk_regions primary key (id));

create table registers (
id                        bigint not null,
timetable_id              bigint,
student_id                bigint,
date                      timestamp,
attendance                varchar(255),
lessonid                  bigint,
teacher_id                bigint,
constraint pk_registers primary key (id));

create table schools (
id                        bigint not null,
code                      varchar(255),
region_id                 bigint,
district_id               bigint,
name_kz                   varchar(255),
name_rus                  varchar(255),
constraint pk_schools primary key (id));

create table roles (
id                        bigint not null,
name                      varchar(255),
constraint pk_roles primary key (id));

create table timetables (
id                        bigint not null,
dostyk_group_id           bigint,
elective_group_id         bigint,
day                       integer,
start_time                timestamp,
end_time                  timestamp,
lesson_id                 bigint,
teacher_id                bigint,
cabinet                   varchar(255),
row                       integer,
week                      int NOT NULL default -1,
constraint pk_timetables primary key (id));

create table permissions (
id                        bigint not null,
permission_value          varchar(255),
constraint pk_permissions primary key (id));


create table users_roles (
users_id                       bigint not null,
roles_id                       bigint not null,
constraint pk_users_roles primary key (users_id, roles_id));

create table users_permissions (
users_id                       bigint not null,
permissions_id                 bigint not null,
constraint pk_users_permissions primary key (users_id, permissions_id));

create table dostyks_users (
dostyks_id                     bigint not null,
users_id                       bigint not null,
constraint pk_dostyks_users primary key (dostyks_id, users_id));

create table dostyk_groups_contents (
dostyk_groups_id               bigint not null,
contents_id                    bigint not null,
constraint pk_dostyk_groups_contents primary key (dostyk_groups_id, contents_id));

create table profiles_lessons (
profiles_id                    bigint not null,
lessons_id                     bigint not null,
constraint pk_profiles_lessons primary key (profiles_id, lessons_id));

create table requisites (
  id                        bigint not null,
  dostyk_official           varchar(255),
  dostyk_city               varchar(255),
  dostyk_address            varchar(255),
  dostyk_tel                varchar(255),
  dostyk_director           varchar(255),
  dostyk_bik                varchar(255),
  dostyk_iik                varchar(255),
  dostyk_bin                varchar(255),
  dostyk_bank               varchar(255),
  dostyk_kbe                varchar(255),
  constraint pk_requisites primary key (id));

create table topics (
  id                        bigint not null,
  name                      varchar(255),
  lesson_id                 bigint,
  path_to_file              varchar(255),
  file_name                 varchar(255),
  constraint pk_topics primary key (id));

create table attutt (
  id                        bigint not null,
  user_id					          bigint,
  type	                    varchar(255),
  lesson_id          		    bigint,
  number         			      integer,
  mark         			        integer,
  constraint pk_attutt	  primary key (id));

create table topic_registers (
  id                        bigint not null,
  topic_id			            bigint,
  dostyk_group_id           bigint,
  date         			        timestamp,
  constraint pk_topic_register	  primary key (id));

create table extra_journals (
  id                        bigint not null,
  user_id			      	      bigint,
  date         			        timestamp,
  kaz         			        integer,
  rus         			        integer,
  math         			        integer,
  history       			      integer,
  selected       			      integer,
  constraint pk_extra_journals	  primary key (id));

create table extra_lessons (
  id                        bigint not null,
  teacher_id			      	  bigint,
  date         			        timestamp,
  dostyk_group_id           bigint,
  lesson_id         		    bigint,
  topic_id         		      bigint,
  constraint pk_extra_lessons	  primary key (id));

create table rest_users (
  id                        bigint not null,
  token                     varchar(255),
  user_id                   bigint,
  device_id                 varchar(255),
  os_type                   character varying(255),
  constraint pk_rest_users primary key (id));

create table notifications (
  id                        bigint not null,
  message                   varchar(255),
  user_id                   bigint,
  teacher_id                bigint,
  date                      TIMESTAMP,
  constraint pk_notifications primary key (id));

create table firms (
  id                        bigint not null,
  name                      varchar(255),
  city                      varchar(255),
  address                   varchar(255),
  phone                     varchar(255),
  director                  varchar(255),
  bik                       varchar(255),
  iik                       varchar(255),
  bin                       varchar(255),
  bank                      varchar(255),
  kbe                       varchar(255),
  dostyk_id                 bigint,
  constraint pk_firms primary key (id));

create table exam_sessions (
  id                        bigint not null,
  dostyk_id                 bigint,
  session                   integer,
  session_max               integer,
  status                    varchar(255),
  constraint pk_exam_sessions primary key (id));

create table contracts (
  id                        bigint not null,
  student_id                bigint,
  payment_id                bigint,
  number                    varchar(255),
  prefix                    varchar(255),
  begin_date             timestamp,
  end_date               timestamp,
  refund                    integer,
  comment                   varchar(255),
  constraint pk_contracts primary key (id));

create table payment_suspends (
  id                        bigint not null,
  payment_id                bigint,
  payment_period_id         bigint,
  reason                    varchar(255),
  start_date                timestamp,
  end_date                  timestamp,
  suspend_payment           float,
  period_payment            float,
  diff_payment              float,
  study_price float,
  constraint pk_payment_suspends primary key (id));

create table courses (
  id                        bigint not null,
  title                     bigint,
  constraint pk_courses primary key (id));

create table lessons_courses (
  lessons_id                     bigint not null,
  courses_id                       bigint not null,
  constraint pk_lessons_courses primary key (lessons_id, courses_id));

create sequence jauaptar_seq;
create sequence jauapkilti_seq;
create sequence users_seq;
create sequence blocks_seq;
create sequence contents_seq;
create sequence course_types_seq;
create sequence detailed_results_seq;
create sequence discounts_maxes_seq;
create sequence discounts_values_seq;
create sequence districts_seq;
create sequence dostyks_seq;
create sequence dostyk_groups_seq;
create sequence task_seq;
create sequence exams_seq;
create sequence exam_results_seq;
create sequence exam_types_seq;
create sequence layouts_seq;
create sequence lessons_seq;
create sequence lesson_answer_seq;
create sequence lesson_hours_seq;
create sequence parents_seq;
create sequence payments_seq;
create sequence payment_periods_seq;
create sequence payment_receipts_seq;
create sequence price_list_seq;
create sequence profiles_seq;
create sequence regions_seq;
create sequence registers_seq;
create sequence schools_seq;
create sequence roles_seq;
create sequence timetables_seq;
create sequence permissions_seq;
create sequence requisites_seq;
create sequence topics_seq;
create sequence attutt_seq;
create sequence topic_registers_seq;
create sequence extra_journals_seq;
create sequence extra_lessons_seq;
create sequence rest_users_seq;
create sequence notifications_seq;
create sequence firms_seq;
create sequence exam_sessions_seq;
create sequence contracts_seq;
create sequence payment_suspends_seq;
create sequence courses_seq;

alter table jauaptar add constraint fk_jauaptar_lessonAnswer_1 foreign key (lesson_answer_id) references lesson_answer (id);
create index ix_jauaptar_lessonAnswer_1 on jauaptar (lesson_answer_id);
alter table users add constraint fk_users_profile_2 foreign key (profile_id) references profiles (id);
create index ix_users_profile_2 on users (profile_id);
alter table users add constraint fk_users_requisite_6 foreign key (requisite_id) references requisites (id);
create index ix_users_requisite_6 on users (requisite_id);
alter table users add constraint fk_users_firm_7 foreign key (firm_id) references firms (id);
create index ix_users_firm_7 on users (firm_id);
alter table blocks add constraint fk_blocks_layout_3 foreign key (layout_id) references layouts (id);
create index ix_blocks_layout_3 on blocks (layout_id);
alter table contents add constraint fk_contents_dostyk_4 foreign key (dostyk_id) references dostyks (id);
create index ix_contents_dostyk_4 on contents (dostyk_id);
alter table contents add constraint fk_contents_dostykGroup_5 foreign key (dostyk_group_id) references dostyk_groups (id);
create index ix_contents_dostykGroup_5 on contents (dostyk_group_id);
alter table contents add constraint fk_contents_author_6 foreign key (author_id) references users (id);
create index ix_contents_author_6 on contents (author_id);
alter table detailed_results add constraint fk_detailed_results_examResult_7 foreign key (exam_result_id) references exam_results (id);
create index ix_detailed_results_examResult_7 on detailed_results (exam_result_id);
alter table detailed_results add constraint fk_detailed_results_lesson_8 foreign key (lesson_id) references lessons (id);
create index ix_detailed_results_lesson_8 on detailed_results (lesson_id);
alter table discounts_values add constraint fk_discounts_values_dostyk_9 foreign key (dostyk_id) references dostyks (id);
create index ix_discounts_values_dostyk_9 on discounts_values (dostyk_id);
alter table discounts_values add constraint fk_discounts_values_dmax_10 foreign key (dmax_id) references discounts_maxes (id);
create index ix_discounts_values_dmax_10 on discounts_values (dmax_id);
alter table districts add constraint fk_districts_region_11 foreign key (region_id) references regions (id);
create index ix_districts_region_11 on districts (region_id);
alter table dostyk_groups add constraint fk_dostyk_groups_curator_12 foreign key (curator_id) references users (id);
create index ix_dostyk_groups_curator_12 on dostyk_groups (curator_id);
alter table dostyk_groups add constraint fk_dostyk_groups_timeTable_13 foreign key (time_table_id) references timetables (id);
create index ix_dostyk_groups_timeTable_13 on dostyk_groups (time_table_id);
alter table dostyk_groups add constraint fk_dostyk_groups_dostyk_14 foreign key (dostyk_id) references dostyks (id);
create index ix_dostyk_groups_dostyk_14 on dostyk_groups (dostyk_id);
alter table ent add constraint fk_ent_user_15 foreign key (user_id) references users (id);
create index ix_ent_user_15 on ent (user_id);
alter table ent add constraint fk_ent_lessons_1 foreign key (lesson_id) references lessons (id);
create index ix_ent_lessons_1 on ent (lesson_id);
alter table exams add constraint fk_exams_examType_16 foreign key (exam_type_id) references exam_types (id);
create index ix_exams_examType_16 on exams (exam_type_id);
alter table exam_results add constraint fk_exam_results_student_17 foreign key (student_id) references users (id);
create index ix_exam_results_student_17 on exam_results (student_id);
alter table exam_results add constraint fk_exam_results_exam_18 foreign key (exam_id) references exams (id);
create index ix_exam_results_exam_18 on exam_results (exam_id);
alter table exam_types add constraint fk_exam_types_layout_19 foreign key (layout_id) references layouts (id);
create index ix_exam_types_layout_19 on exam_types (layout_id);
alter table lesson_answer add constraint fk_lesson_answer_lesson_20 foreign key (lesson_id) references lessons (id);
create index ix_lesson_answer_lesson_20 on lesson_answer (lesson_id);
alter table lesson_answer add constraint fk_lesson_answer_answerKey_21 foreign key (answer_key_id) references jauapkilti (id);
create index ix_lesson_answer_answerKey_21 on lesson_answer (answer_key_id);
alter table lesson_hours add constraint fk_lesson_hours_lesson_22 foreign key (lesson_id) references lessons (id);
create index ix_lesson_hours_lesson_22 on lesson_hours (lesson_id);
alter table lesson_hours add constraint fk_lesson_hours_dostyk_23 foreign key (dostyk_id) references dostyks (id);
create index ix_lesson_hours_dostyk_23 on lesson_hours (dostyk_id);
alter table parents add constraint fk_parents_child_24 foreign key (child_id) references users (id);
create index ix_parents_child_24 on parents (child_id);
alter table payments add constraint fk_payments_user_25 foreign key (user_id) references users (id);
create index ix_payments_user_25 on payments (user_id);
alter table payment_periods add constraint fk_payment_periods_payment_26 foreign key (payment_id) references payments (id);
create index ix_payment_periods_payment_26 on payment_periods (payment_id);
alter table payment_periods add constraint fk_payment_periods_receipt_27 foreign key (receipt_id) references payment_receipts (id);
create index ix_payment_periods_receipt_27 on payment_periods (receipt_id);
alter table payment_receipts add constraint fk_payment_receipts_user_28 foreign key (user_id) references users (id);
create index ix_payment_receipts_user_28 on payment_receipts (user_id);
alter table payment_receipts add constraint fk_payment_receipts_payment_29 foreign key (payment_id) references payments (id);
create index ix_payment_receipts_payment_29 on payment_receipts (payment_id);
alter table payment_receipts add constraint fk_payment_receipts_dostyk_30 foreign key (dostyk_id) references dostyks (id);
create index ix_payment_receipts_dostyk_30 on payment_receipts (dostyk_id);
alter table payment_receipts add constraint fk_payment_receipts_period_31 foreign key (period_id) references payment_periods (id);
create index ix_payment_receipts_period_31 on payment_receipts (period_id);
alter table price_list add constraint fk_price_list_dostyk_32 foreign key (dostyk_id) references dostyks (id);
create index ix_price_list_dostyk_32 on price_list (dostyk_id);
alter table price_list add constraint fk_price_list_courseType_33 foreign key (course_type_id) references course_types (id);
create index ix_price_list_courseType_33 on price_list (course_type_id);
alter table profiles add constraint fk_profiles_dostykGroup_34 foreign key (dostyk_group_id) references dostyk_groups (id);
create index ix_profiles_dostykGroup_34 on profiles (dostyk_group_id);
alter table profiles add constraint fk_profiles_electiveGroup_35 foreign key (elective_group_id) references dostyk_groups (id);
create index ix_profiles_electiveGroup_35 on profiles (elective_group_id);
alter table profiles add constraint fk_profiles_course_36 foreign key (course_id) references course_types (id);
create index ix_profiles_course_36 on profiles (course_id);
alter table profiles add constraint fk_profiles_region_37 foreign key (region_id) references regions (id);
create index ix_profiles_region_37 on profiles (region_id);
alter table profiles add constraint fk_profiles_district_38 foreign key (district_id) references districts (id);
create index ix_profiles_district_38 on profiles (district_id);
alter table profiles add constraint fk_profiles_school_39 foreign key (school_id) references schools (id);
create index ix_profiles_school_39 on profiles (school_id);
alter table profiles add constraint fk_profiles_examSession_48 foreign key (exam_session_id) references exam_sessions (id);
create index ix_profiles_examSession_48 on profiles (exam_session_id);
alter table registers add constraint fk_registers_timetable_40 foreign key (timetable_id) references timetables (id);
create index ix_registers_timetable_40 on registers (timetable_id);
alter table registers add constraint fk_registers_student_41 foreign key (student_id) references users (id);
create index ix_registers_student_41 on registers (student_id);
alter table schools add constraint fk_schools_region_42 foreign key (region_id) references regions (id);
create index ix_schools_region_42 on schools (region_id);
alter table schools add constraint fk_schools_district_43 foreign key (district_id) references districts (id);
create index ix_schools_district_43 on schools (district_id);
alter table timetables add constraint fk_timetables_dostykGroup_44 foreign key (dostyk_group_id) references dostyk_groups (id);
create index ix_timetables_dostykGroup_44 on timetables (dostyk_group_id);
alter table timetables add constraint fk_timetables_electiveGroup_45 foreign key (elective_group_id) references dostyk_groups (id);
create index ix_timetables_electiveGroup_45 on timetables (elective_group_id);
alter table timetables add constraint fk_timetables_lesson_46 foreign key (lesson_id) references lessons (id);
create index ix_timetables_lesson_46 on timetables (lesson_id);
alter table timetables add constraint fk_timetables_teacher_47 foreign key (teacher_id) references users (id);
create index ix_timetables_teacher_47 on timetables (teacher_id);
alter table registers add constraint fk_registers_users_2 foreign key (teacher_id) references users (id);
create index ix_registers_users_2 on registers (teacher_id);
alter table jauapkilti add constraint fk_jauapkilti_exams_1 foreign key (exam_id) references exams (id);
create index ix_jauapkilti_exams_1 on jauapkilti (exam_id);
alter table topics add constraint fk_topics_lesson_48 foreign key (lesson_id) references lessons (id);
create index ix_topics_lesson_48 on topics (lesson_id);
alter table attutt add constraint fk_attutt_lesson_1 foreign key (lesson_id) references lessons (id);
create index ix_attutt_lesson_1 on attutt (lesson_id);
alter table attutt add constraint fk_attutt_users_1 foreign key (user_id) references users (id);
create index ix_attutt_users_1 on attutt (user_id);
alter table topic_registers add constraint fk_topic_register_dostyk_groups_1 foreign key (dostyk_group_id) references dostyk_groups (id);
create index ix_topic_registers_dostyk_groups_1 on topic_registers (dostyk_group_id);
alter table topic_registers add constraint fk_topic_register_topic_1 foreign key (topic_id) references topics (id);
create index ix_topic_registers_topic_1 on topic_registers (topic_id);
alter table extra_journals add constraint fk_extra_journals_users_1 foreign key (user_id) references users (id);
create index ix_extra_journals_users_1 on extra_journals (user_id);
alter table extra_lessons add constraint fk_extra_lessons_users_1 foreign key (teacher_id) references users (id);
create index ix_extra_lessons_users_1 on extra_lessons (teacher_id);
alter table extra_lessons add constraint fk_extra_lessons_dostyk_groups_1 foreign key (dostyk_group_id) references dostyk_groups (id);
create index ix_extra_lessons_dostyk_groups_1 on extra_lessons (dostyk_group_id);
alter table extra_lessons add constraint fk_extra_lessons_topic_1 foreign key (topic_id) references topics (id);
create index ix_extra_lessons_topic_1 on extra_lessons (topic_id);
alter table extra_lessons add constraint fk_extra_lessons_lesson_1 foreign key (lesson_id) references lessons (id);
create index ix_extra_lessons_lesson_1 on extra_lessons (lesson_id);
alter table rest_users add constraint fk_rest_users_user_60 foreign key (user_id) references users (id);
create index ix_rest_users_user_60 on rest_users (user_id);
alter table notifications add constraint fk_notifications_user_id_60 foreign key (user_id) references users (id);
create index ix_notifications_user_id_60 on notifications (user_id);
alter table notifications add constraint fk_notifications_teacher_id_60 foreign key (teacher_id) references users (id);
create index ix_notifications_teacher_id_60 on notifications (teacher_id);
alter table firms add constraint fk_firms_dostyk_30 foreign key (dostyk_id) references dostyks (id);
create index ix_firms_dostyk_30 on firms (dostyk_id);
alter table exam_sessions add constraint fk_exam_sessions_dostyk_24 foreign key (dostyk_id) references dostyks (id);
create index ix_exam_sessions_dostyk_24 on exam_sessions (dostyk_id);
alter table contracts add constraint fk_contracts_student_12 foreign key (student_id) references users (id);
create index ix_contracts_student_12 on contracts (student_id);
alter table contracts add constraint fk_contracts_payment_13 foreign key (payment_id) references payments (id);
create index ix_contracts_payment_13 on contracts (payment_id);
alter table payment_suspends add constraint fk_payment_suspends_payment_49 foreign key (payment_id) references payments (id);
create index ix_payment_suspends_payment_49 on payment_suspends (payment_id);
alter table payment_suspends add constraint fk_payment_suspends_paymentPe_50 foreign key (payment_period_id) references payment_periods (id);
create index ix_payment_suspends_paymentPe_50 on payment_suspends (payment_period_id);


alter table users_roles add constraint fk_users_roles_users_01 foreign key (users_id) references users (id);
alter table users_roles add constraint fk_users_roles_roles_02 foreign key (roles_id) references roles (id);
alter table users_permissions add constraint fk_users_permissions_users_01 foreign key (users_id) references users (id);
alter table users_permissions add constraint fk_users_permissions_permissi_02 foreign key (permissions_id) references permissions (id);
alter table dostyks_users add constraint fk_dostyks_users_dostyks_01 foreign key (dostyks_id) references dostyks (id);
alter table dostyks_users add constraint fk_dostyks_users_users_02 foreign key (users_id) references users (id);
alter table dostyk_groups_contents add constraint fk_dostyk_groups_contents_dos_01 foreign key (dostyk_groups_id) references dostyk_groups (id);
alter table dostyk_groups_contents add constraint fk_dostyk_groups_contents_con_02 foreign key (contents_id) references contents (id);
alter table profiles_lessons add constraint fk_profiles_lessons_profiles_01 foreign key (profiles_id) references profiles (id);
alter table profiles_lessons add constraint fk_profiles_lessons_lessons_02 foreign key (lessons_id) references lessons (id);

# --- !Downs

drop table if exists jauaptar cascade;
drop table if exists jauapkilti cascade;
drop table if exists users cascade;
drop table if exists users_roles cascade;
drop table if exists users_permissions cascade;
drop table if exists dostyks_users cascade;
drop table if exists blocks cascade;
drop table if exists contents cascade;
drop table if exists course_types cascade;
drop table if exists detailed_results cascade;
drop table if exists discounts_maxes cascade;
drop table if exists discounts_values cascade;
drop table if exists districts cascade;
drop table if exists dostyks cascade;
drop table if exists dostyk_groups cascade;
drop table if exists dostyk_groups_contents cascade;
drop table if exists ent cascade;
drop table if exists exams cascade;
drop table if exists exam_results cascade;
drop table if exists exam_types cascade;
drop table if exists layouts cascade;
drop table if exists lessons cascade;
drop table if exists lesson_answer cascade;
drop table if exists lesson_hours cascade;
drop table if exists parents cascade;
drop table if exists payments cascade;
drop table if exists payment_periods cascade;
drop table if exists payment_receipts cascade;
drop table if exists price_list cascade;
drop table if exists profiles cascade;
drop table if exists profiles_lessons cascade;
drop table if exists regions cascade;
drop table if exists registers cascade;
drop table if exists schools cascade;
drop table if exists roles cascade;
drop table if exists timetables cascade;
drop table if exists permissions cascade;
drop table if exists requisites cascade;
drop table if exists topics cascade;
drop table if exists attutt cascade;
drop table if exists topic_registers cascade;
drop table if exists extra_journals cascade;
drop table if exists extra_lessons cascade;
drop table if exists rest_users cascade;
drop table if exists notifications cascade;
drop table if exists firms cascade;
drop table if exists exam_sessions cascade;
drop table if exists contracts cascade;
drop table if exists payment_suspends cascade;
drop table if exists courses cascade;
drop table if exists lessons_courses cascade;


drop sequence if exists jauaptar_seq;
drop sequence if exists jauapkilti_seq;
drop sequence if exists users_seq;
drop sequence if exists blocks_seq;
drop sequence if exists contents_seq;
drop sequence if exists course_types_seq;
drop sequence if exists detailed_results_seq;
drop sequence if exists discounts_maxes_seq;
drop sequence if exists discounts_values_seq;
drop sequence if exists districts_seq;
drop sequence if exists dostyks_seq;
drop sequence if exists dostyk_groups_seq;
drop sequence if exists task_seq;
drop sequence if exists exams_seq;
drop sequence if exists exam_results_seq;
drop sequence if exists exam_types_seq;
drop sequence if exists layouts_seq;
drop sequence if exists lessons_seq;
drop sequence if exists lesson_answer_seq;
drop sequence if exists lesson_hours_seq;
drop sequence if exists parents_seq;
drop sequence if exists payments_seq;
drop sequence if exists payment_periods_seq;
drop sequence if exists payment_receipts_seq;
drop sequence if exists price_list_seq;
drop sequence if exists profiles_seq;
drop sequence if exists regions_seq;
drop sequence if exists registers_seq;
drop sequence if exists schools_seq;
drop sequence if exists roles_seq;
drop sequence if exists timetables_seq;
drop sequence if exists permissions_seq;
drop sequence if exists requisites_seq;
drop sequence if exists topics_seq;
drop sequence if exists attutt_seq;
drop sequence if exists topic_registers_seq;
drop sequence if exists extra_journals_seq;
drop sequence if exists extra_lessons_seq;
drop sequence if exists rest_users_seq;
drop sequence if exists notifications_seq;
drop sequence if exists firms_seq;
drop sequence if exists exam_sessions_seq;
drop sequence if exists contracts_seq;
drop sequence if exists payment_suspends_seq;
drop sequence if exists courses_seq;
