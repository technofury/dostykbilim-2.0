# --- !Ups
drop table if exists timetables cascade;
drop sequence if exists timetables_seq;
create table timetable_settings (
  id                        	bigint not null,
  name							varchar(255),
  lesson_days               	integer,
  max_lessons					integer,
  is_active						boolean,
  organization_id				bigint,	
  constraint pk_timetable_settings primary key (id))
;

create table lesson_periods (
  id                        	bigint not null,
  number						integer,
  start_time               		timestamp,
  end_time               		timestamp,
  settings_id					bigint,
  organization_id				bigint,
  constraint pk_lesson_periods primary key (id))
;

create table timetables (
  id                        	bigint not null,
  is_active						boolean,
  grade_id               		bigint,
  settings_id					bigint,
  organization_id				bigint,
  constraint pk_timetables primary key (id))
;

create table timetable_lessons (
  id                        	bigint not null,
  day			               	integer,
  period_id						bigint,
  subject_teacher_id			bigint,
  subject_id					bigint,
  timetable_id					bigint,
  organization_id				bigint,	
  constraint pk_timetable_lessons primary key (id))
;

create sequence timetable_settings_seq;

create sequence lesson_periods_seq;

create sequence timetables_seq;

create sequence timetable_lessons_seq;

alter table timetable_settings add constraint fk_timetable_settings_organization_1 foreign key (organization_id) references dostyks (id);
create index ix_timetable_settings_organization_1 on timetable_settings (organization_id);

alter table lesson_periods add constraint fk_lesson_periods_timetable_settings_1 foreign key (settings_id) references timetable_settings (id);
create index ix_lesson_periods_timetable_settings_1 on lesson_periods (settings_id);

alter table lesson_periods add constraint fk_lesson_periods_organization_1 foreign key (organization_id) references dostyks (id);
create index ix_lesson_periods_organization_1 on lesson_periods (organization_id);

alter table timetables add constraint fk_timetables_grade_1 foreign key (grade_id) references dostyk_groups (id);
create index ix_timetables_grade_1 on timetables (grade_id);

alter table timetables add constraint fk_timetables_organization_1 foreign key (organization_id) references dostyks (id);
create index ix_timetables_organization_1 on timetables (organization_id);

alter table timetables add constraint fk_timetables_timetable_settings_1 foreign key (settings_id) references timetable_settings (id);
create index ix_timetables_timetable_settings_1 on timetables (settings_id);

alter table timetable_lessons add constraint fk_timetable_lessons_period_1 foreign key (period_id) references lesson_periods (id);
create index ix_timetable_lessons_period_1 on timetable_lessons (period_id);

alter table timetable_lessons add constraint fk_timetable_lessons_user_1 foreign key (subject_teacher_id) references users (id);
create index ix_timetable_lessons_user_1 on timetable_lessons (subject_teacher_id);

alter table timetable_lessons add constraint fk_timetable_lessons_subject_1 foreign key (subject_id) references lessons (id);
create index ix_timetable_lessons_subject_1 on timetable_lessons (subject_id);

alter table timetable_lessons add constraint fk_timetable_lessons_timetable_1 foreign key (timetable_id) references timetables (id);
create index ix_timetable_lessons_timetable_1 on timetable_lessons (timetable_id);

alter table timetable_lessons add constraint fk_timetable_lessons_organization_1 foreign key (organization_id) references dostyks (id);
create index ix_timetable_lessons_organization_1 on timetable_lessons (organization_id);

# --- !Downs

drop sequence if exists timetable_settings_seq;

drop sequence if exists lesson_periods_seq;

drop sequence if exists timetables_seq;

drop sequence if exists timetable_lessons_seq;


drop table if exists timetable_settings cascade;

drop table if exists lesson_periods cascade;

drop table if exists timetables cascade;

drop table if exists timetable_lessons cascade;


