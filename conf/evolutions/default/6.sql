# --- !Ups
drop table if exists dostyk_groups cascade;
drop sequence if exists dostyk_groups_seq;

create table dostyk_groups(
  id                        	bigint not null,
  name							varchar(255),
  language                      character varying(255),
  dostyk_id				        bigint,
  curator_id                    bigint,
  courses_id                    bigint,
  constraint pk_dostyk_groups primary key (id))
;

create sequence dostyk_groups_seq;

alter table dostyk_groups add constraint fk_dostyk_groups_dostyk_1 foreign key (dostyk_id) references dostyks (id);
create index ix_dostyk_groups_dostyk_1 on dostyk_groups (dostyk_id);

alter table dostyk_groups add constraint fk_dostyk_groups_courses_1 foreign key (courses_id) references courses (id);
create index ix_dostyk_groups_courses_1 on dostyk_groups (courses_id);

alter table dostyk_groups add constraint fk_dostyk_groups_curator_1 foreign key (curator_id) references users (id);
create index ix_dostyk_groups_curator_1 on dostyk_groups (curator_id);

# --- !Downs

drop sequence if exists dostyk_groups_seq;


drop table if exists dostyk_groups cascade;


