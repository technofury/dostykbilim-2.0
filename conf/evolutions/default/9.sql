
# --- !Ups

alter table discounts_maxes add name_ru varchar(255);
alter table discounts_maxes add max_user_size integer;
alter table discounts_maxes rename name to name_kz;
alter table discounts_maxes add active integer;
alter table payments add discounts_maxes_id bigint;
alter table users drop constraint uq_users_email;

# --- !Downs

alter table discounts_maxes drop name_ru;
alter table discounts_maxes drop max_user_size;
alter table discounts_maxes rename name_kz to name;
alter table discounts_maxes drop active;
alter table payments drop discounts_maxes_id;
alter table users add constraint uq_users_email unique (email);


