unit = шт
nosmsleft = SMS не осталось, свяжитесь с EDUCON
sms.success = SMS отправлен
beforePayment = Предыд. оплата
afterPayment = После
diff= Разница
currentPayment = Последняя оплата
reason = Причина
courseSuspend = Приостановить курс
report.ten = Отчет АҚС
ten.exam.lang = Вопросник на
ten.kz = Казахском языке
ten.ru = Русском языке
ten.name = 10 класс АҚТ
Welcome = Здравствуйте
ten.signUp = Зарегистрироватся на 10 класс АҚТ
selectone = Выберите один
thisemailisalreadytaken = Емайл занят
Youmustacceptthetermsandconditions = Нужно отметить
select.session = Выберите сеанс
ten.full = Сеанс закрыт
ten.session = Время сеанса
ten.userManagement = 10 класс АҚТ список
revenue.report = Отчет о Начислении доходов
requisite.dostykOfficial = Официальное название Достыка
requisite.dostykCity     = Город Достыка
requisite.dostykAddress  = Адрес Достыка	
requisite.dostykTel      = Тел. Достыка
requisite.dostykDirector = Директор Достыка
requisite.dostykBIK      = БИК Достыка
requisite.dostykIIK      = ИИК Достыка
requisite.dostykBIN      = БИН Достыка
requisite.dostykBANK     = Банк Достыка
requisite.dostykKBE      = КБЕ Достыка
movement.report = Отчет о движении
left.report = Оқудан шыққандар баяндамасы
profile.secondName = Отчество
pko = ПРИХОДНЫЙ КАССОВЫЙ ОРДЕР
rko = РАСХОДНЫЙ КАССОВЫЙ ОРДЕР
agreement.no = Номер договора
agreement.date = Дата договора
groupWithNumberExists=Такая группа уже существует
profile.inn = ИИН
registration.report = Отчет о регистрации
all=Все
nameOftheFileMustBeNumber=Имя файла должно быть числом и в формате .mp4
uploadVideos=Загрузить видео
question=Вопрос
exportToExcel=Скачать Excel
back=Назад
extraLessonsNumber=Кол. доп. уроков
select.please=Не все выбраны!
selectTeacher=Выберите препода
Attendance=Присутствие
timetableOfGroup=Расписание группы
extraLesson=Доп. урок
report.byTeacher=Отчет по учителям
report.attutt=Отчет по ATT/YTT
Loading...=Загружается...
Saving...=Сохраняется...
Saved=Сохранено
downloadTest=Скачать тест
noTest=Тест ещё не закачен
5.pan=Предмет выбора
task=Задание
uploadTasksByTopic=Загрузить тесты по темам
lessonTopic=Тема урока
Number=Нoмeр
number=Нoмeр
Date=Дата
selectLesson=Выберите предмет
selectGroup=Выберите группу
extra.timetable=Расписание доп. уроков
Timetable=Расписание
journal=Журнал
extra.journal=Журнал доп. уроков
noVideo=Видео не загружен
incorrectInput=В этом варианте нету такого предмета
videoAnswers=Видео решения
DAB=ДАБ
DAS=ДАС
PKS=ПҚС
ENT=ЕНТ
deletedUsers=Вышедшие(закончившие)
average=Средний
Corrects=Правильные
All=Все
thereIsNoResult=Нету никаких результатов
topics.page=Список тем
topic.report=Отчет по темам
support.moderEmail.tooltip = Если не хотите каждый раз писать емайл пройдите по
support.moderEmail = Введите контактный емайл
support.send = Ваше обращение получено. Мы исследуем Ваш запрос и ответим на него в ближайшее время.
support.dostyk = Выберите ваш Достық
support.dinner = Обеденный перерыв
support.weekend = Выходной день
support = Техно помощь
support.notify = Время работы
percentage = Соотношение
paymentSize = Количество оплат
bill.adding2=Приложение 2
group.exam.report=Отчет группу
teacher=Учитель
student=Ученик
mother=Мать
father=Отец
guardian=Заказчик
profile.work=Профессия
responsible=Oтветственный
missed=Кол-во пропущенных уроков
kazLang=Казахский язык
rusLang=Русский язык
history_kz=История Казахстана
math=Математика
allResults=Результаты всех экзаменов
results=Результаты
resultsByTopic=Результаты по темам
resultsByLesson=Чтобы увидеть результаты по урокам
tabel=Табель
notAttended=Не был
detailed=Полный результат
January=Январь
February=Февраль
March=Март
April=Апрель
May=Май
June=Июнь
July=Июль
August=Август
September=Сентябрь
October=Октябрь
November=Ноябрь
December=Декабрь
Mo=Пн
Tu=Вт
We=Ср
Th=Чт
Fr=Пт
Sa=Сб
Su=Вс
CustomRange=Свое время
From=От
To=До
LastMonth=Прошлый месяц
LastYear=Прошлый год
Last30Days=Последние 30 дней
Last7Days=Последние 7 дней
ThisMonth=Этот месяц
Yesterday=Вчера
about=О нас
accounting = Бухгалтерия
Activate=Активировать
Active=Экзамен идет
ActiveExams=Ответы
add.director=Добавить директора
add.moderator=Добавить модератора
Add.New=Добавить
Add.new=Добавить
add.newuser=Добавить нового пользователя
add.questions=Добавить вопрос
add.student=Добавить ученика
add.teacher=Добавить учителя
add=Добавить
administer=Администрирование
altynBelgi=Алтын белгі
AnswerKeyList=Ответы
Answers=Ответы
AnswersList=Ответы
areYouSure=Вы уверены?
AttendanceList=Список посещаемости
attendanceList=Список посещаемости
AttendancePage=Посещаемость
Attention=Внимание
authorization.error=Проблема входа
bigEndian=Сохраняйте TXT файл со следующей кодировкой:
billForm=Форма оплаты
Biology=Биология
birthDate=Дата рождения
Blocks=Блоки
body=Текст
bonusPeriods=Скидка периода
Cabinet=Кабинет
calculate=Высчитать

cancel= Отмена
canNotPayMoreThanDue=Вы не можете оплатить больше нужного. Лишнее можете оплатить за следующий период!
Cancel=Отмена
Change=Изменить
change=изменить
changeEmail= Изменить емайл
changePassword= Изменить пароль
check=Выбрать
Check=Проверялка
checkEmailForNewPassword=Новый пароль отправлен на вашу почту
Chemistry=Химия
chooseGroup=Выберите группу
ChooseOne=Выберите
close=Закрыть
code.incorrect=ошибка в коде
comment=Комментарий
comments=Комментарии
completeCourse=Закончить нормальный курс
completed=Окончившие
confirmPassword=Повторите пароль
Congrats=Поздравления
considerHolidays=Считать праздники
content.create=Добавить контент
contentManagement=Контенты
contentPage=Контенты
Correct=Правильно
correctAndReupload=Исправить и закачать
courseType.edit=Изменить тип курса
CourseType.emptyName=Название пустое
CourseType.emptyNumber=Номер пуст
courseType.report=Отчет по Курсам
CourseType.usedName=Имя пустое
courseTypeList=Список Курсов
create=Сохранить
ctype.class=Класс
ctype.classNumber=Класс
ctype.code=Код типа курса
ctype.name=Наименование
ctype.setCourseTypes=Изменить Курс
curator=Куратор
curatorNameIsEmpty=К. без имени
Deactivate=Деактивировать
debt=Долг
Delete=Удалить
delete=Удалить
Deleted=Удален
deleteDescription=Удалить описание
DeleteFromDostyk=Удалить из Достыка
Delimeter=Делиметр
director=Директор
directorManagement=Управление директорами
discount.maxValue=мaкс. количество
discount.name=Наименование
discount.report=Отчет скидок
discount.value=Количество
discount=Скидка
discountedTotal=Общая стоимость
discountMax.discountValue=Ошибка колличества
discountMax.name=Наименование
discountMax.name.kz=Наименование на казахском
discountMax.name.ru=Наименование на русском
discountMax.user.size=Макс. пользователь
discountMax.edit=Изменить скидку
discountMax.nameMustBeUnique=Имя должно быть уникальным
discountMax.typeName=Дайте имя
discountMax.value=Количество
discountMax.zeroDiscount=Не может быть нулем
discountMaxes.set=Задать Maкс.
discounts.set=Изменить скидку
discounts=Скидки
discountValuesDidntBeenInitialised=Скидки не введены в систему
displaying=Показывает
district=Район
dmax.name=Наименование
dmax.value=Количество
DMR.code=Место на листе ответа
dontIncludeLastMonth=Не добавляй последний месяц
dostyk.attendedStudentsSize=К-во проверенных учеников
dostyk.code=Код
dostyk.edit=Изменить Достык
dostyk.errorCode=Ошибка кода
dostyk.errorName=Ошибка имени
dostyk.errorOrg=Ошибка названия организации
dostyk.errorRnn=Ошибка РНН-а
dostyk.examAverage=Средний балл
dostyk.name=Наименование
dostyk.organization=Организация
dostyk.rnn=РНН
dostyk.rnnIsTwelveDigits=РНН состоит из 12 цифр
dostyk.setDostyk=Управление Достыком
dostyk.studentsSize=Колличество учеников
dostyk.successfullyDeleted=Успешно удален
dostyk.twelveRnn=РНН состоит из 12 цифр
dostyk.usedCode=Код уже использован
dostyk.usedRnn=РНН уже использован
Dostyk=Достық
dostyks=Достыки
dostykGroup=Група
dostykGroupList=Список групп
dostykList=Список Достыков
dostykManagement=Управление Достыком
dublicate.id=ID уже использован
edit=Изменить
editCourse=Изменить курс
editEmail=Изменить email
editingLessonsIsUnavailableWhileThisUserDidntFinishedDuePayment=Нельзя изменить предметы пока не уплочены все платежы
editPassword=Изменить пароль
editPersonalInfo=Изменить профиль
electiveGroup=Группа выбранного предмета
Email=Емайл
email=Емайл
emptyDostykProbably=Наверное он/она без достыка
endCourse=Закончить курс
endCourseError=Ошибка окончания курса
endDate=Дата окончания
endDateMustBeAfterToday=Выберите дату окончания не ранее сегодняшней даты
EndTime=Дата окончания
endType=Вид
English=Английский язык
Enter.your.mail=Введите email
Error=Ошибка
error=Ошибка на сайте
ErrorLoggedWithId=Ошибка зарегистрирована с таким Id
ErrorConcactToSupport = Скомпируте текст ниже и напишите в ТехноПомощь 
exam.endDate = Дата окончания
exam.name = Наименование
exam.report=Отчет по Экзаменам
exam.results=Результаты экзамена
exam.startDate = Дата начала
examination=Экзамен
ExamList=Экзамены
examDidntUploaded=Результаты не закачены
examResult.major.correct=Выбранный предмет правилен
examResult.major=Предмет выбора
examResult.total=Всего
examResult.totalGrade=Балл
examResult.variant=Вариант
examResults=Результаты экзамена
exams=Экзамены
ExamType=Тип экзамена
ExamTypeList=Типы экзамена
excelExport=Экспорт в Excel
pdfExport=Экспорт в PDF
female=Жен.
fileNotFound=Файл не найден
FileParseError=Ошибка файла
fileUploadedSuccessfully=Файл успешно закачан
fileUploadError=Ошибка при закачке файла
fillBillFormFirst=Заполните форму оплаты сначала
Finish=Окончание
Finished=Экзамен закончился
forfeit=Штраф
forgotPassword=Забыл пароль
French=Франсузский язык
Friday=Пятница
fullname=ФИО
gender.female=Жен.
gender.male=Муж.
gender.report=Отчет полов
gender=Пол
Geography=География
German=Немецкий язык
Go=Далее
GototheDate=Указать дату
Grade=Класс
grade=Класс
group.am=До обеда
group.changeStatus=Изменить Статус
group.company=Компания
group.create=Создать группу
group.curator=Куратор
group.default=Стандартный
group.elective=Группа 5-ого предмета
group.help.lesson=Предмет
group.kazakh=Казахский
group.name=Имя
group.pm=После обеда
group.region=Районный
group.russian=Русский
group.size=Колличество учеников
group.status=Статус
group.students=Ученики
group.studyPeriod=Период Обучения
group.timetable=Расписание
group.type=Тип
Group=Группа
groupManagement=Управление Группой
help.agreement=Данные приведенные сверху являются 100% правильными!!!
help.courseType=Выберите Курс
help.language=Выберите Язык
help.dostyk=Выберите Достык
help.signIn=Войти
home=Главная
toHome= На главную
hoursPerWeek.error=Не введены часы предметов
hoursPerWeek=Часов в неделю
id=ID
IdNumber=ID номер
idNumber=ID номер
Inactive=Экзамен не начался
incorrect.5pan=Предмет выбора неправилен
incorrect.password.or.less.than.6.chars=Пароль должен состоять из более 6 символов
incorrect.variant=Неправильный вариант
incorrectDate=Неправильная дата
itsNotTimeYet=Еще не время
jaksy=Хорошо
kizilAttestat=Красный аттестат
language=Язык
late=Опоздал
Late=Опоздал/а
Layout=Экзаменационный лист
LayoutAnswerList=Ответы
LayoutList=Экзаменационная бумага
lesson.hours=Часы
lesson.language=Язык
lesson.name=Наименование
Lesson=Предмет
lesson=Предмет
lessonHoursForm=Часы занятии
lessonHoursManagement=Время занятий
Lessons=Предметы
LessonsList=Список Предметов
Lesson.code=Код предмета
Lesson.setDostyk=Управление Предметом
lesson.edit=Изменить Предмет
Literature=Литература
Logic=Логика
male=Муж.
manage.answerKeys=Управление Ответами
manage.exams=Управление Экзаменами
manage.examTypes=Управление типами экзамена
manage.layouts=Управление экзаменационной бумагой
materialCost=Стоимость материала
Mekeme=Компания
menu.signOut=Выйти
menu=Меню
moderManagement=Управление модераторами
Monday=Понедельник
moreOptions=Дополнительно
myProfile=Профиль
Name=Имя
name=Имя
New.exam=Новый
news=Новости
next=Следующий
no.such.id=Нет такого ID
no.such.idNumber=Нет такого ID номера
noComments=Нет комента
Not.Present=Отсутствие
Not.present=Отсутствие
notPresent=Отсутствует
noUserWithSuchEmail=Не найден такой пользователь
exp_list=Показать как лист
of= из
OK=ОК
onlineRegistration=Онлайн регистрация
PageNotFound=Запрошенная страница не найдена
password.confirm=Подтвердить пароль
password.new=Новый пароль
password.old=Пароль
Password=Пароль
password=Пароль
payed=Оплачено
payedAt=Оплачено в
payment.coeficient=Коэф.
payment.debt=Долг
payment.endDate=Дата окончания
payment.percentage=Оплатили
payment.payed=Оплачено
payment.period=Период
payment.planed=Запланировано
payment.report=Отчет Бухгалтерий
payment.startDate=Дата начала
payment.status.COMPLETED=Закончил курс
payment.status.CURRENT=Оплачивает
payment.status.PAYED=Покинул
payment.status=Статус
payment.students=Колич. Учеников
paymentDate=Время оплаты
paymentList=Оплата
paymentListError=Ошибка списка оплат
paymentPerStudent=Оплата за студента
paymentPrice=Оплата
period=Период
perPeriod=За период
perPeriodDiff=Разница за период
Phone.Number=Номер телефона
phoneNumber=Тел.
Physics=Физика
pleaseGoAndAddDiscountValues=Введите скидки!!!
Presence=Присутствие
Present=Присутствует
present=Присутствует
previous=Предыдущий
priceList.error=Ошибка прайс листа
priceList.Form = Прайслист
priceList.hours= Часы
priceList.price= Стоимость
pricePerWeek.error=Прайс-лист не добавлен
pricePerWeek=Недельная оплата
priceRounding=Округление платы
printReceiptError=Ошибка печати чека
profile.birthdate=Дата рождения
profile.courseType=Курс
profile.language=Язык
profile.dostyk=Достык
profile.firstName=Имя
profile.gender=Пол
profile.idNumber=ID номер
profile.lastName=Фамилия
profile.parentNumber=Тел. родителей
profile.phone=Тел.
profile.phoneNumber=Номер телефона
projectName=Достықбілім
rankDostyk=По Достыку
rankGroup=По группе
rankKZ=По Казахстану
receipt=чек
region=Область
report.courseType=Отчет по курсам
report.gender=Муж./Жен.
report.ulgerimi=Отчет успеваемости
Reset=Сброс
resetDescription=Сброс описания
role=Роль
Saturday=Суббота
Save=Сохранить
save=Сохранить
Saved.Successfully=Успешно сохранен
saveDostykError=Ошибка при сохранений изменений в Достыке
schedule=Расписание
school=Школа
Search=Искать
search=Искать
Searchbyusernameorsurname=Имя или фамилия пользователя
select.5pan=Выбрать предмет по выбору
selected.lesson=Предмет по выбору
SelectElectiveGroup=Выбрать группу выбранного предмета
SelectGroup=Выбрать группу
selectRegion=Выберите область
send=Отправить
showDiscountMaxes=Макс. Скидка
showDiscountValues= Скидки
showPriceList=Прайс-лист
signIn=Войти
signUp=Зарегистрироватся
signUpForm=Зарегистрировать
site=сайт
Start=Начало
startDate=Дата начала
StartTime=Дата начала
Status=Статус
student.firstnameLastname=ФИО
student.group=Группа
student.idCode=ID код
StudentCount=Кол. Учеников
studentlist=Список учеников
students.list=Список учеников
students=Ученики
studyPrice=Цена учебы
Submit=Ввод
Successfully.saved=Успешно сохранен
Successfully.uploaded=Успешно закачен
Sunday=Воскресенье
surname.name=ФИО
surname=Фамилия
takeAttendance=Посещаемость
Teacher=Учитель
thisPageIsRestrictedForYou=Вам сюда нельзя!
thisUserAlreadyHasHisPayment=Этот ученик уже имеет договор
Thursday=Четверг
Time=Время
timetable.am=До обеда
timetable.edit=Изменить расписание
timetable.pm=После обеда
timetable.time=Время
timetable=Расписание
title=Тема
to= до
today=Сегодня
topic.name=Тема
Topic=Тема
total=Общий
totalPayed=Оплочено в общем
totalPrice=Стоимость обучения
Tuesday=Вторник
ulgerimi.report=Отчет успеваемости
Ulgerimi=Успеваемость
Undo=Назад
undoDescription=Вернуть описание
updateDate=Изменить дату
updateDateLessThanToday=Дата изменения раньше чем сегодня
updateEndDate=Закончить
updateLessons=Обновить
updateLessonsError=Ошибка изменения предметов
upload.exam=Загрузить файл экзамена
uploadExcel=Закачать Excel
uploadFromFile=Закачать с файла
user.editProfile=Профиль
user.restorePassword=Восстановить пароль
user.signIn=Войти
user.signUp=Зерегистрироваться
user.status=Статус
userManagement=Управление пользователями
userNotActiveYet=Hе активирован
users.list.title=Список пользователей
users.list=Список пользователей
using.this=Используя это
Usual=Стандарт
Variant=Вариант
viewRegister=Посящаемость
warning=Внимание
Wednesday=Среда
weeks=Недели
Weight=Место
whenWillStart=Дата начала
World.history=Всемирная история
You.have.multiple.users.with.this.idNumber=Несколько пользователей с таким ID номером
YouCanLoginToOur=Вы можете войти в систему использую эти данные
Youhavelessonsatthistimeremovethemfirst=Вы уже выбрали предметы, удалите сначала их
Your.status.inactive=Ваш статус неактивный
youSetDatesIncorrectly=Вы задали даты неверно
Code=Код
DateRange=Интервал времени
StartPrice=Цена от
EndPrice=Цена до
NewOutlay=Новый расход
OutlayReportControls=Контроль отчета по расходам
PayedPayment=Оплачено
Price=Цена
Refresh=Обновить
SelectCode=Выберите код
ThisYear=Этот год
TimeRange=Интервал времени
nothingFound=Отчет пуст
payment.outlays=Расходы
Outlay=Расход
UseMp4Only=Используйте только mp4 формат
Print=Печать
StudentAnswerShort=Выб.
WrightAnswerShort=Прав.
Firms=Фирмы
Firm=Фирма
NewFirm=Новая фирма
City=Город
Address=Адрес
Phone=Телефон
BIK=БИК
IIK=ИИК
BIN=БИН
Bank=Банк
KBE=КБЕ
Director=Директор
CallTechnovision=Свяжитесь с Техновижном
FillFieldsFirst=Заполните обязательные поля
Deleted.Successfully=Успешно удален
ChooseFirm=Выберите фирму
newRegistrationReport=Отчет по ученикам
csvReportIns=Сейчас будет скачан .csv файл. Откройте его на Excel-е используя кодировку UTF-8 и Казахским языком
RedirectingToFirms=В этом Достыке не заполнены фирмы. Сначала заполните их.
RequiredField=Обязательно к заполнению
NameAlreadyUsed=Это имя уже используется
NewDealNumber=Номер дополнения к договору
CashBook=Кассовая книга
diactivateallstudents = Деактивировать всех учеников
holidays = Каникулы  
add.holiday=Добавить каникул
start.date = Время начала
end.date = Время окончания
date.error = Ошибка
holiday.edit = Изменить каникул
orders=Заказы
settings=Настройки
report=Отчет
category=Категория
add.category= Добавить категорию
supplier=Поставщик
NewSupplier=Новый поставщик
add.order= Добавить заказ
new.order=Новый заказ
order.name= Названия
order.price=Цена
order.last.date=Окончания срока
order.status= Статус
order.description=Описание
order.picture=Рисунок
upload.file = Загрузить файл
edit.order=Изменить заказ
updated=Успешно изменен
amount=Количество
debtors=Должники
payments=Платежи
new.peyment=Новый платеж
order.report=Отчет по заказам
category.used = Эта категория используется
category.exists=Такая категория уже существует
order.defaultValue=Констант значение
order.view=Список заказов
order.dostykAmaount=Оплаты
SavedSuccessfully=Сохранено
total.debt=Общая долг
numberError=Менще нуля не записывается
EditSettings=Изменить настройки
AllSettings=Все настройки
NewSettings=Новые настройки
Active=Активный
Inactive=Неактивный
SettingsName=Название настроек
LessonMinute=Время урока
LessonDays=Дни занятии
MaxLessons=Макс. кол. уроков
LessonPeriods=Периоды уроков
StartTime=Время начала
EndTime=Время окончания
Break=Перерыв
NotDeleted=Не удалено, настройки может быть активным
NotSaved=Не сохранено, заполните название настроек, периоды уроков
ActivatedSuccessfully=Активировано успешно
DeactivatedSuccessfully=Дезактивировано успешно
FillBreaks=Заполните время начала первого урока и все перерывы
Timetable=Расписание
GeneratedSuccessfully=Сгенерировано успешно
Term=Четверть
CantDeleteTimetable=Невозможно удалить активное расписание
CantActivateTimetable=Невозможно активировать расписание, выберите активные настройки
Deleted=Удален
NewTimetable=Новое расписание
day1=Понедельник
day2=Вторник
day3=Среда
day4=Четверг
day5=Пятница
day6=Суббота
day7=Воскресенье
day=День
status=Статус
user_view=Посмотреть
noTimetable=Топтың сабақ кестесі жоқ!
noSettings=Сабақ кестесінің баптаулары жоқ!
nogroup=Топ жоқ. Жаңа топ еңгізіңіз!
noTeacherTimetable=Сабақ кестесі жоқ!
present=Присутствует
late=Опоздал
absent=Отсутствует
attendance=Присутствие
noteacher=Мұғалімді таңдау міндетті!
sort=Сортировка
payment.not.exists=Платеж еще не открыть
student.schools=Школа школьников   