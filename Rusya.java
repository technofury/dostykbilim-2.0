import java.io.BufferedReader;
import java.io.PrintWriter;
import java.util.*;
import java.io.*;
public class Rusya{
    public static void main(String [] args) throws Exception{
        //settings
        String[] langs= new String[]{"kk"};

        //operations
        BufferedReader bf = new BufferedReader(new FileReader("./conf/messages.xx"));
        List<String> allMessages = new ArrayList<String>();
        String temp;
        while((temp = bf.readLine())!=null){
            String[] parts = temp.split("=");
            allMessages.add(parts[0]);
        }
        Collections.sort(allMessages);
        bf.close();
        for(String lang: langs) {
            BufferedReader bf2 = new BufferedReader(new FileReader("./conf/messages." + lang));
            List<String> oldMessages = new ArrayList<String>();
            List<String> oldTranslations = new ArrayList<String>();
            temp = "";
            while ((temp = bf2.readLine()) != null) {
                if (temp.contains("=")) {
                    String[] parts = temp.split("=");
                    oldMessages.add(parts[0]);
                    oldTranslations.add(parts[1]);
                }
            }
            bf2.close();
            List<String> newMessages = new ArrayList<String>();
            boolean tabyldy = false;
            String am;
            String om;
            for (int i = 0; i < allMessages.size(); i++) {
                tabyldy = false;
                am = allMessages.get(i);
                for (int j = 0; j < oldMessages.size(); j++) {
                    om = oldMessages.get(j);
                    if (am.equals(om)) {
                        tabyldy = true;
                        break;
                    }
                }
                if (tabyldy == false)
                    newMessages.add(am);
            }
            PrintWriter pw = new PrintWriter(new FileWriter("./conf/new_messages_" + lang + ".txt"));
            for (String nm : newMessages) {
                pw.println(nm + "=");
            }
            pw.close();
        }
    }
}