import java.io.*;

import java.nio.file.*;
import java.nio.file.attribute.*;
import java.nio.file.*;
import java.util.regex.*;
import java.util.Scanner;
import java.util.*;

public class Asya {
    public static void main(String[] args) {
        long starttime = System.currentTimeMillis();
        try {
            Path startPath = Paths.get("./app");
            Files.walkFileTree(startPath, new SimpleFileVisitor < Path > () {@Override
            public FileVisitResult preVisitDirectory(Path dir,
                                                     BasicFileAttributes attrs) {

                return FileVisitResult.CONTINUE;
            }

                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
                    Map<String,String> mymap = new HashMap<String,String>();
                    if (file.toString().endsWith(".scala.html")) {
                        Path path = file;

                        try {
                            Scanner scanner = new Scanner(path);
                            while (scanner.hasNextLine()) {
                                String line = scanner.nextLine();
                                Matcher m = Pattern.compile("Messages\\(.+?\\)").matcher(line);
                                while (m.find()) {
                                    Matcher mm = Pattern.compile(".*\\\"(.*)\\\".*").matcher(m.group(0));
                                    while (mm.find()) {
                                        String sss = ""+mm.group(1).trim();
                                        mymap.put(m.group(0),mm.group(1));
                                        // duplicateList.add(;
                                    }
                                }
                            }
                        } catch (Exception e) {}

                    }
                    if (file.toString().endsWith(".java")) {
                        Path path = file;
                        try {
                            Scanner scanner = new Scanner(path);
                            while (scanner.hasNextLine()) {
                                String line = scanner.nextLine();
                                Matcher m = Pattern.compile("Messages.get\\(.+?\\)").matcher(line);
                                while (m.find()) {
                                    Matcher mm = Pattern.compile(".*\\\"(.*)\\\".*").matcher(m.group(0));
                                    while (mm.find()) {
                                        String sss = ""+mm.group(1).trim();
                                        mymap.put(m.group(0),mm.group(1));
                                        // duplicateList.add(;
                                    }
                                }
                            }
                        } catch (Exception e) {}
                    }
                    try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("./conf/messages.xx", true)))) {
                        Iterator it = mymap.entrySet().iterator();
                        while (it.hasNext()) {
                            Map.Entry pairs = (Map.Entry)it.next();
                            out.println(pairs.getValue() + "=");

                            it.remove(); // avoids a ConcurrentModificationException
                        }

                    } catch (IOException e) {
                        //exception handling left as an exercise for the reader
                    }
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult visitFileFailed(Path file, IOException e) {
                    return FileVisitResult.CONTINUE;
                }
            });
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        try{
            BufferedReader reader = new BufferedReader(new FileReader("./conf/messages.xx"));
            Set<String> lines = new HashSet<String>(10000); // maybe should be bigger
            String line;
            while ((line = reader.readLine()) != null) {
                lines.add(line);
            }
            reader.close();
            Set<String> treeSet = new TreeSet<String>();
            treeSet.addAll(lines);
            BufferedWriter writer = new BufferedWriter(new FileWriter("./conf/messages.xx"));
            for (String unique : treeSet) {
                writer.write(unique);
                writer.newLine();
            }
            writer.close();

        }catch(Exception e){}
    }


}