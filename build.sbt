name := """dostykbilim2"""

version := "1.0"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.1"

libraryDependencies ++= Seq(
  javaJdbc,
  javaEbean,
  cache,
  javaWs,
  "be.objectify" %% "deadbolt-java" % "2.3.0-RC1",
  "net.sf.flexjson" % "flexjson" % "2.1",
  //"org.webjars" %% "webjars-play" % "2.3.0",
  //"org.webjars" % "bootstrap" % "3.2.0",
  //"org.webjars" % "bootstrap-switch" % "3.0.2",
  //"org.webjars" % "bootstrap-datepicker" % "1.3.0-3",
  "com.typesafe.play.plugins" %% "play-plugins-mailer" % "2.3.0",
  "com.loicdescotte.coffeebean" %% "html5tags" % "1.2.1",
  "postgresql" % "postgresql" % "9.1-901-1.jdbc4",
  "com.google.code.gson" % "gson" % "2.3.1"
)

resolvers += Resolver.url("github repo for html5tags", url("http://loicdescotte.github.io/Play2-HTML5Tags/releases/"))(Resolver.ivyStylePatterns)

resolvers += Resolver.url("Objectify Play Repository", url("http://schaloner.github.com/releases/"))(Resolver.ivyStylePatterns)


fork in run := true